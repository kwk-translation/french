﻿# TODO: Translation updated at 2017-10-04 12:12

# /home/max/Bureau/script.rpyc:218
translate french splashscreen_73841dfc:

    # centered "THIS IS AN ALPHA RELEASE\n\nIt's not meant for public.\nPlease do not distribute!{fast}"
    centered "CECI EST UNE RELEASE ALPHA\n\nElle n'est pas faite pour le public\nNe pas la distribuer!"

# /home/max/Bureau/script.rpyc:249
translate french konami_code_e5b259c9:

    # "Max le Fou" "What the heck am I doing here?..."
    "Max le Fou" "Qu'est-ce que je fous ici, moi ?..."

# /home/max/Bureau/script.rpyc:284
translate french gjconnect_4ae60ea0:

    # "Disconnected."
    "Déconnecté"

# /home/max/Bureau/script.rpyc:313
translate french gjconnect_e254a406:

    # "Connection to Game Jolt succesful.\nWARNING: The connection doesn't count for previously saved games."
    "Connection à Game Jolt réussie.\nATTENTION : La connection ne compte pas pour les parties précédemment sauvegardées."

translate french strings:

    # script.rpy:269
    old "Disconnect from Game Jolt?"
    new "Se déconnecter de Game Jolt ?"

    # script.rpy:269
    old "Yes, disconnect."
    new "Oui, se déconnecter."

    # script.rpy:269
    old "No, return to menu."
    new "Non, retourner au menu."

    # script.rpy:331
    old "A problem occured. Maybe your connection has a problem. Or maybe it's Game Jolt derping...\n\nTry again?"
    new "Un problème est survenu. Vous avez probablemetn un problème de connexion, ou bien les serveur de Game Jolt ont des problèmes...\n\nRéessayer ?"

    # script.rpy:331
    old "Yes, try again."
    new "Oui, réessayer."

    # script.rpy:349
    old "It seems the authentication failed. Maybe you didn't write correctly the username and token...\n\nTry again?"
    new "Un problème est survenu. Vous avez peut-être mal écrit votre pseudo et/ou votre token...\n\nRéessayer ?"

    # script.rpy:228
    old "Please enter your name and press Enter:"
    new "Entrez un nom et appuyez sur Entrée :"

    # script.rpy:295
    old "Type here your username and press Enter."
    new "Tapez ici votre pseudo Game Jolt et appuyez sur Entrée."

    # script.rpy:296
    old "Now, type here your game token and press Enter."
    new "Maintenant, tapez ici votre token Game Jolt et appuyez sur Entrée."

    # script.rpy:207
    old "Game Jolt trophy obtained!"
    new "Trophée Game Jolt remporté !"

    # script.rpy:177
    old "Sakura"
    new "Sakura"

    # script.rpy:178
    old "Rika"
    new "Rika"

    # script.rpy:179
    old "Nanami"
    new "Nanami"

    # script.rpy:180
    old "Sakura's mom"
    new "Mère de S."

    # script.rpy:181
    old "Sakura's dad"
    new "Père de S."

    # script.rpy:182
    old "Toshio"
    new "Toshio"

    # script.rpy:188
    old "Taichi"
    new "Taichi"

    # dialogs.rpy:199
    old "Master"
    new "Maître"

    # dialogs.rpy:200
    old "Big brother"
    new "Grand frère"

    # script.rpy:13
    old "{size=80}Thanks for playing!"
    new "{size=80}Merci d'avoir joué !"
# TODO: Translation updated at 2017-11-14 20:59

# game/script.rpy:222
translate french splashscreen_df2eb4ee:

    # centered "THIS IS A PREVIEW RELEASE\n\nIt's incomplete, might have minor bugs and missing sprites!{fast}"
    centered "CECI EST UNE PREVIEW\n\nElle est incomplète et peut comporter des bugs mineurs et des sprites manquants!{fast}"

# TODO: Translation updated at 2019-04-23 09:59

# game/script.rpy:286
translate french update_menu_dc3e4e02:

    # "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"
    "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"

# game/script.rpy:300
translate french gjconnect_dc3e4e02:

    # "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"
    "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"

# TODO: Translation updated at 2019-04-27 10:43

translate french strings:

    # script.rpy:226
    old "Achievement obtained!"
    new "Succès obtenu !"
