﻿# TODO: Translation updated at 2017-10-22 11:08

translate french strings:

    # omake.rpy:82
    old "{b}D.O.B.:{/b} 1978/09/29\n"
    new "{b}D.D.N.:{/b} 29 septembre 1978\n"

    # omake.rpy:83
    old "{b}P.O.B.:{/b} Shinjuku, Tokyo\n"
    new "{b}L.D.N.:{/b} Shinjuku, Tokyo\n"

    # omake.rpy:84
    old "{b}Height:{/b} 5.4ft\n"
    new "{b}Taille:{/b} 165cm\n"

    # omake.rpy:85
    old "{b}Weight:{/b} 136 pounds\n"
    new "{b}Poids:{/b} 62Kg\n"

    # omake.rpy:86
    old "{b}Measurements:{/b} 74-64-83\n"
    new "{b}Mensurations:{/b} 74-64-83\n"

    # omake.rpy:87
    old "{b}Blood type:{/b} A\n"
    new "{b}Groupe sanguin:{/b} A\n"

    # omake.rpy:88
    old "{b}Favourite manga:{/b} High School Samurai\n"
    new "{b}Manga préféré:{/b} High School Samurai\n"

    # omake.rpy:89
    old "{b}Favourite videogame:{/b} Lead of Fighters ‘96\n"
    new "{b}Jeu vidéo préféré:{/b} Lead of Fighters ‘96\n"

    # omake.rpy:90
    old "{b}Favourite food:{/b} American hamburgers\n"
    new "{b}Plat favori:{/b} Hamburgers\n"

    old "A young boy from Tokyo who has just moved to the village. At first, he thinks he's going to miss the urban life he knew before. But meeting Sakura will quickly change his mind...\nHe is a nice guy, determined, and sometimes a little bit crazy. He likes computers, mangas and loves to have fun with his friends. He is not afraid to face problems, especially when the sake of his friends is involved. He is quite uncertain on big decisions so he usually lets his instinct (or the player!) leading his decisions most of the time...\nHe got an older sister that is married and still lives in Tokyo."
    new "Un jeune garçon de Tokyo qui vient d'emménager au Village. Au début, il pensait que la vie de campagne ne lui plaîrait pas. Mais sa rencontre avec Sakura lui changera sa vie...\nC'est un jeune homme sympa et déterminé, parfois un peu fou. Il aime les ordinateurs, les mangas et adore s'amuser avec ses amis. Il n'a pas peur de faire face à des problèmes, surtout si c'est pour aider ses amis. Il est souvent indécis quand il faut prendre de grandes décisions et il laisse souvent son instinct (ou le joueur!) prendre ses décisions...\nIl a une grande sœur qui est mariée et est restée à Tokyo."

    # omake.rpy:114
    old "{b}D.O.B.:{/b} 1979/02/28\n"
    new "{b}D.D.N.:{/b} 28 février 1979\n"

    # omake.rpy:115
    old "{b}P.O.B.:{/b} Kameoka, Kyoto\n"
    new "{b}L.D.N.:{/b} Kameoka, Kyoto\n"

    # omake.rpy:116
    old "{b}Height:{/b} 5.1ft\n"
    new "{b}Taille:{/b} 157cm\n"

    # omake.rpy:117
    old "{b}Weight:{/b} 121 pounds\n"
    new "{b}Poids:{/b} 55Kg\n"

    # omake.rpy:118
    old "{b}Measurements:{/b} Unknown\n"
    new "{b}Mensurations:{/b} Inconnues\n"

    # omake.rpy:119
    old "{b}Blood type:{/b} AB\n"
    new "{b}Groupe sanguin:{/b} AB\n"

    # omake.rpy:120
    old "{b}Favourite manga:{/b} Uchuu Tenshi Moechan\n"
    new "{b}Manga préféré:{/b} Uchuu Tenshi Moechan\n"

    # omake.rpy:121
    old "{b}Favourite videogame:{/b} Taiko no Masuta EX 4’\n"
    new "{b}Jeu vidéo préféré:{/b} Taiko no Masuta EX 4’\n"

    # omake.rpy:122
    old "{b}Favourite food:{/b} Beef yakitori\n"
    new "{b}Plat favori:{/b} Yakitori de bœuf\n"

    # omake.rpy:123
    old "Sakura is a member of the school's manga club and she has a very deep secret that makes of her a mysterious girl...\nShe is very shy but incredibly pretty. She was the idol of the school until a strange rumor about her started to spread. She likes classical music and plays violin sometimes in the night at her window..."
    new "Sakura est membre du club manga de l'école et elle a un profond secret qui la rend assez mystérieuse...\nElle est très timide mais incroyablement belle. Elle a été l'idôle de l'école jusqu'à ce qu'une rumeur sur elle commence à se répandre. Elle aime la musique classique et joue du violon certains soirs à sa fenêtre..."

    # omake.rpy:146
    old "{b}D.O.B.:{/b} 1979/08/05\n"
    new "{b}D.D.N.:{/b} 5 août 1979\n"

    # omake.rpy:147
    old "{b}P.O.B.:{/b} The Village, Osaka\n"
    new "{b}L.D.N.:{/b} Le Village, Osaka\n"

    # omake.rpy:148
    old "{b}Height:{/b} 5ft\n"
    new "{b}Taille:{/b} 152cm\n"

    # omake.rpy:149
    old "{b}Weight:{/b} 110 pounds\n"
    new "{b}Poids:{/b} 50Kg\n"

    # omake.rpy:150
    old "{b}Measurements:{/b} 92-64-87\n"
    new "{b}Mensurations:{/b} 92-64-87\n"

    # omake.rpy:151
    old "{b}Blood type:{/b} O\n"
    new "{b}Groupe sanguin:{/b} O\n"

    # omake.rpy:152
    old "{b}Favourite manga:{/b} Rosario Maiden\n"
    new "{b}Manga préféré:{/b} Rosario Maiden\n"

    # omake.rpy:153
    old "{b}Favourite videogame:{/b} Super Musashi Galaxy Fight\n"
    new "{b}Jeu vidéo préféré:{/b} Super Musashi Galaxy Fight\n"

    # omake.rpy:154
    old "{b}Favourite food:{/b} Takoyaki\n"
    new "{b}Plat favori:{/b} Takoyaki\n"

    # omake.rpy:155
    old "Rika is the founder of the manga club.\nShe got very bad experiences with boys and she sees them as perverts since then. Rika cosplays as a hobby and her best and favourite cosplay is the heroine of the Domoco-chan anime. She have strange eyes minnows that makes every boys dreamy. She speaks in the Kansai dialect like most of the people originating from the Village.\nShe secretly have a little crush on Sakura..."
    new "Rika est la fondatrice du club manga.\nElle a eu de mauvaises expériences avec les garçons et elle les voit tous comme des pervers depuis. Rika fait du cosplay et son meilleur costume est celui de l'héroïne de Domoco-chan. Elle a des yeux vairons qui font rêver les garçons de l'école. Elle parle avec l'accent du Kansai comme les habitants originaires du Village.\nElle a secrètement un petit faible pour Sakura..."

    # omake.rpy:178
    old "{b}D.O.B.:{/b} 1980/10/11\n"
    new "{b}D.D.N.:{/b} 11 octobre 1980\n"

    # omake.rpy:179
    old "{b}P.O.B.:{/b} Ginoza, Okinawa\n"
    new "{b}L.D.N.:{/b} Ginoza, Okinawa\n"

    # omake.rpy:180
    old "{b}Height:{/b} 4.5ft\n"
    new "{b}Taille:{/b} 140cm\n"

    # omake.rpy:181
    old "{b}Weight:{/b} 99 pounds\n"
    new "{b}Poids:{/b} 45Kg\n"

    # omake.rpy:182
    old "{b}Measurements:{/b} 71-51-74\n"
    new "{b}Mensurations:{/b} 71-51-74\n"

    # omake.rpy:183
    old "{b}Blood type:{/b} B\n"
    new "{b}Groupe sanguin:{/b} B\n"

    # omake.rpy:184
    old "{b}Favourite manga:{/b} Nanda no Ryu\n"
    new "{b}Manga préféré:{/b} Nanda no Ryu\n"

    # omake.rpy:185
    old "{b}Favourite videogame:{/b} Pika Pika Rocket\n"
    new "{b}Jeu vidéo préféré:{/b} Pika Pika Rocket\n"

    # omake.rpy:186
    old "{b}Favourite food:{/b} Chanpuruu\n"
    new "{b}Plat favori:{/b} Chanpuruu\n"

    # omake.rpy:187
    old "Nanami lives alone with her big brother Toshio after their parents disappeared.\nShe's a quiet introvert girl at first glance, but once she's with her friends, she's a cute energy bomb. She has a natural talent for videogames, which made of her the champion of the Osaka prefecture in numerous videogames."
    new "Nanami vit seule avec son grand frère Toshio après la disparition de leurs parents.\nC'est une fille silencieuse et introvertie à première vue, mais quand elle est avec ses amis, elle est une petite bombe d'énergie. Elle a un talent naturel pour les jeux vidéo, ce qui fait d'elle la championne de la préfécture d'Osaka dans beaucoup de jeux vidéo."

    # omake.rpy:85
    old "Opening song \"TAKE MY HEART\""
    new "Générique \"TAKE MY HEART\""

    # omake.rpy:86
    old "Performed by Mew Nekohime\nLyrics by Max le Fou and Masaki Deguchi\nComposed and sequenced by Max le Fou\n© {a=http://www.maxlefou.com/}JMF{/a} 2018"
    new "Interprêté par Mew Nekohime\nParoles de Max le Fou et Masaki Deguchi\nComposé et séquencé par Max le Fou\n© {a=http://www.maxlefou.com/}JMF{/a} 2018"

    # omake.rpy:92
    old "{b}Japanese lyrics:{/b}\n"
    new "{b}Paroles japonaises:{/b}\n"

    # omake.rpy:124
    old "{b}Translation:{/b}\n"
    new "{b}Traduction:{/b}\n"

    # omake.rpy:125
    old "When you take my hand, I feel I could fly"
    new "Quand tu me tiens la main, je pourrais m'envoler"

    # omake.rpy:126
    old "When I dive into your eyes, I feel I could drown in happiness\n"
    new "Quand je plonge dans tes yeux, je pourrais me noyer dans le bonheur\n"

    # omake.rpy:127
    old "We sure look different"
    new "Nous sommes vraiment différents"

    # omake.rpy:128
    old "But despite that, my heart beats loud\n"
    new "Mais malgré tout, mon cœur bat fort\n"

    # omake.rpy:129
    old "A boy and a girl"
    new "Un garçon et une fille"

    # omake.rpy:130
    old "I'm just a human"
    new "Je ne suis qu'une humaine"

    # omake.rpy:131
    old "I hope you don't mind"
    new "J'espère que ça ne te dérangera pas"

    # omake.rpy:132
    old "I can't control my feelings"
    new "Je ne puis contrôler mes sentiments"

    # omake.rpy:133
    old "Come to me, take my heart"
    new "Viens à moi, prends mon cœur"

    # omake.rpy:134
    old "I will devote myself to you"
    new "Je te serai entièrement dévouée"

    # omake.rpy:135
    old "No matter what, I love you"
    new "Peu importe le reste, je t'aime"

    # omake.rpy:120
    old "{b}Romaji:{/b}\n"
    new "{b}Romaji:{/b}\n"
# TODO: Translation updated at 2019-04-27 10:43

translate french strings:

    # omake.rpy:278
    old "Chapter 1"
    new "Chapitre 1"

    # omake.rpy:278
    old "Complete the first chapter"
    new "Completer le premier chapitre"

    # omake.rpy:278
    old "Chapter 2"
    new "Chapitre 2"

    # omake.rpy:278
    old "Complete the second chapter"
    new "Completer le second chapitre"

    # omake.rpy:278
    old "Chapter 3"
    new "Chapitre 3"

    # omake.rpy:278
    old "Complete the third chapter"
    new "Completer le troisième chapitre"

    # omake.rpy:278
    old "Chapter 4"
    new "Chapitre 4"

    # omake.rpy:278
    old "Complete the fourth chapter"
    new "Completer le quatrième chapitre"

    # omake.rpy:278
    old "Chapter 5"
    new "Chapitre 5"

    # omake.rpy:278
    old "Complete the fifth chapter"
    new "Completer le cinquième chapitre"

    # omake.rpy:278
    old "Finally together"
    new "Enfin ensemble"

    # omake.rpy:278
    old "Finish the game for the first time"
    new "Finir le jeu pour la première fois"

    # omake.rpy:278
    old "The perfume of rice fields"
    new "Le parfum des champs de riz"

    # omake.rpy:278
    old "Get a kiss from Sakura"
    new "Embrasser Sakura"

    # omake.rpy:278
    old "It's not that I like you, baka!"
    new "C'est pas que je t'aime, baka !"

    # omake.rpy:278
    old "Get a kiss from Rika"
    new "Embrasser Rika"

    # omake.rpy:278
    old "A new kind of game"
    new "Un nouveau jeu"

    # omake.rpy:278
    old "Get a kiss from Nanami"
    new "Embrasser Nanami"

    # omake.rpy:278
    old "It's a trap!"
    new "It's a trap!"

    # omake.rpy:278
    old "Find the truth about Sakura"
    new "Apprenez la vérité sur Sakura"

    # omake.rpy:278
    old "Good guy"
    new "Mec sympa"

    # omake.rpy:278
    old "Tell Sakura the truth about the yukata"
    new "Dire la vérité à Sakura pour le yukata"

    # omake.rpy:278
    old "It's all about the Pentiums, baby"
    new "Le gaming sur un Pentium"

    # omake.rpy:278
    old "Choose to game at the beginning of the game."
    new "Choisir de jouer aux jeux vidéos au début de l'histoire"

    # omake.rpy:278
    old "She got me!"
    new "Elle m'a eu !"

    # omake.rpy:278
    old "Help Rika with the festival"
    new "Aider Rika pour le festival"

    # omake.rpy:278
    old "Grope!"
    new "Pouet !"

    # omake.rpy:278
    old "Grope Rika (accidentally)"
    new "Tombez sur Rika (accidentellement)"

    # omake.rpy:278
    old "A good prank"
    new "Une bonne blague"

    # omake.rpy:278
    old "Prank your friends with Nanami"
    new "Piéger vos amies avec Nanami"

    # omake.rpy:278
    old "I'm not little!"
    new "Je suis pas petite !"

    # omake.rpy:278
    old "Help Sakura tell the truth to Nanami"
    new "Aider Sakura à dire la vérité à Nanami"

    # omake.rpy:278
    old "Big change of life"
    new "Changement de vie"

    # omake.rpy:278
    old "Complete Sakura's route"
    new "Finir la route de Sakura"

    # omake.rpy:278
    old "City Rat"
    new "Citadin"

    # omake.rpy:278
    old "Complete Rika's route"
    new "Finir la route de Rika"

    # omake.rpy:278
    old "That new girl"
    new "La nouvelle fille"

    # omake.rpy:278
    old "Complete Nanami's route"
    new "Finir la route de Nanami"

    # omake.rpy:278
    old "Knee-Deep into the 34D"
    new "Knee-Deep into the 34D"

    # omake.rpy:278
    old "Find the secret code in the game"
    new "Trouver le code secret du jeu"
