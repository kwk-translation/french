﻿# TODO: Translation updated at 2017-10-04 12:12

translate french strings:

    # screens.rpy:253
    old "Back"
    new "Retour"

    # screens.rpy:254
    old "History"
    new "Historique"

    # screens.rpy:255
    old "Skip"
    new "Saut"

    # screens.rpy:256
    old "Auto"
    new "Auto"

    # screens.rpy:257
    old "Save"
    new "Sauver"

    # screens.rpy:258
    old "Q.save"
    new "S.Rapide"

    # screens.rpy:259
    old "Q.load"
    new "C.Rapide"

    # screens.rpy:260
    old "Settings"
    new "Options"

    # screens.rpy:302
    old "New game"
    new "Nouveau jeu"

    # screens.rpy:310
    old "Load"
    new "Charger"

    # screens.rpy:313
    old "Bonus"
    new "Bonus"

    # screens.rpy:319
    old "End Replay"
    new "Finir replay"

    # screens.rpy:323
    old "Main menu"
    new "Menu principal"

    # screens.rpy:325
    old "About"
    new "A propos"

    # screens.rpy:327
    old "Please donate!"
    new "Faire un don !"

    # screens.rpy:330
    old "Help"
    new "Aide"

    # screens.rpy:334
    old "Quit"
    new "Quitter"

    # screens.rpy:560
    old "Version [config.version!t]\n"
    new "Version [config.version!t]\n"

    # screens.rpy:566
    old "Ren'Py version: [renpy.version_only].\n\n[renpy.license!t]"
    new "Version de Ren'Py : [renpy.version_only].\n\n[renpy.license!t]"

    # screens.rpy:606
    old "Page {}"
    new "Page {}"

    # screens.rpy:606
    old "Automatic saves"
    new "Sauvegardes auto"

    # screens.rpy:606
    old "Quick saves"
    new "Sauvegardes rapides"

    # screens.rpy:648
    old "{#file_time}%A, %B %d %Y, %H:%M"
    new "{#file_time}%A, %B %d %Y, %H:%M"

    # screens.rpy:648
    old "empty slot"
    new "Emplacement vide"

    # screens.rpy:665
    old "<"
    new "<"

    # screens.rpy:668
    old "{#auto_page}A"
    new "{#auto_page}A"

    # screens.rpy:671
    old "{#quick_page}Q"
    new "{#quick_page}Q"

    # screens.rpy:677
    old ">"
    new ">"

    # screens.rpy:739
    old "Display"
    new "Affichage"

    # screens.rpy:740
    old "Windowed"
    new "Fenêtré"

    # screens.rpy:741
    old "Fullscreen"
    new "Plein écran"

    # screens.rpy:745
    old "Rollback Side"
    new "Retour en arrière"

    # screens.rpy:746
    old "Disable"
    new "Désactiver"

    # screens.rpy:747
    old "Left"
    new "Gauche"

    # screens.rpy:748
    old "Right"
    new "Droite"

    # screens.rpy:753
    old "Unseen Text"
    new "Texte non vu"

    # screens.rpy:754
    old "After choices"
    new "Après les choix"

    # screens.rpy:755
    old "Transitions"
    new "Transitions"

    # screens.rpy:770
    old "Text speed"
    new "Vitesse du texte"

    # screens.rpy:774
    old "Auto forward"
    new "Avancement auto"

    # screens.rpy:781
    old "Music volume"
    new "Volume musique"

    # screens.rpy:788
    old "Sound volume"
    new "Volume son"

    # screens.rpy:794
    old "Test"
    new "Test"

    # screens.rpy:798
    old "Voice volume"
    new "Volume voix"

    # screens.rpy:809
    old "Mute All"
    new "Muet"

    # screens.rpy:820
    old "Language"
    new "Langage"

    # screens.rpy:824
    old "English"
    new "English"

    # screens.rpy:825
    old "Français"
    new "Français"

    # screens.rpy:946
    old "The dialogue history is empty."
    new "L'historique des dialogues est vide."

    # screens.rpy:1011
    old "Keyboard"
    new "Clavier"

    # screens.rpy:1012
    old "Mouse"
    new "Souris"

    # screens.rpy:1015
    old "Gamepad"
    new "Manette"

    # screens.rpy:1028
    old "Enter"
    new ""

    # screens.rpy:1029
    old "Advances dialogue and activates the interface."
    new "Avance dans les dialogues et active l'interface."

    # screens.rpy:1032
    old "Space"
    new "Espace"

    # screens.rpy:1033
    old "Advances dialogue without selecting choices."
    new "Avance dans les dialogue sans faire de choix."

    # screens.rpy:1036
    old "Arrow Keys"
    new "Touches fléchées"

    # screens.rpy:1037
    old "Navigate the interface."
    new "Naviguer dans l'interface."

    # screens.rpy:1040
    old "Escape"
    new "Echap"

    # screens.rpy:1041
    old "Accesses the game menu."
    new "Accéder au menu."

    # screens.rpy:1044
    old "Ctrl"
    new "Ctrl"

    # screens.rpy:1045
    old "Skips dialogue while held down."
    new "Saute les dialogues quand maintenu."

    # screens.rpy:1048
    old "Tab"
    new "Tab"

    # screens.rpy:1049
    old "Toggles dialogue skipping."
    new "Active ou désactive le saut de dialogues."

    # screens.rpy:1052
    old "Page Up"
    new "Page Up"

    # screens.rpy:1053
    old "Rolls back to earlier dialogue."
    new "Revient au dialogue précédent."

    # screens.rpy:1056
    old "Page Down"
    new "Page Down"

    # screens.rpy:1057
    old "Rolls forward to later dialogue."
    new "Avance au dialogue suivant."

    # screens.rpy:1061
    old "Hides the user interface."
    new "Cache l'interface utilisateur."

    # screens.rpy:1065
    old "Takes a screenshot."
    new "Prend une capture d'écran"

    # screens.rpy:1069
    old "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."
    new "Active la {a=https://www.renpy.org/l/voicing}voix de synthèse{/a}."

    # screens.rpy:1075
    old "Left Click"
    new "Clic gauche"

    # screens.rpy:1079
    old "Middle Click"
    new "Clic milieu"

    # screens.rpy:1083
    old "Right Click"
    new "Clic droit"

    # screens.rpy:1087
    old "Mouse Wheel Up\nClick Rollback Side"
    new "Roulette haut"

    # screens.rpy:1091
    old "Mouse Wheel Down"
    new "Roulette bas"

    # screens.rpy:1098
    old "Right Trigger\nA/Bottom Button"
    new "Gachette droite\nA/Bouton du bas"

    # screens.rpy:1102
    old "Left Trigger\nLeft Shoulder"
    new "Gachette gauche\nBouton épaule gauche"

    # screens.rpy:1106
    old "Right Shoulder"
    new "Bouton épaule droite"

    # screens.rpy:1110
    old "D-Pad, Sticks"
    new "Croix directionelle, sticks analogiques"

    # screens.rpy:1114
    old "Start, Guide"
    new "Start, Guide"

    # screens.rpy:1118
    old "Y/Top Button"
    new "Y/Bouton du haut"

    # screens.rpy:1121
    old "Calibrate"
    new "Calibrer"

    # screens.rpy:1186
    old "Yes"
    new "Oui"

    # screens.rpy:1187
    old "No"
    new "Non"

    # screens.rpy:1233
    old "Skipping"
    new "Saut en cours"

    # screens.rpy:1454
    old "Menu"
    new "Menu"

    # screens.rpy:1545
    old "Picture gallery"
    new "Galerie d'images"

    # screens.rpy:1546
    old "Music player"
    new "Lecteur de musique"

    # screens.rpy:1543
    old "Musics and pictures"
    new "Images et musiques"

    # screens.rpy:1508
    old "Bonus chapters"
    new "Chapitres bonus"

    # screens.rpy:1511
    old "1 - A casual day at the club"
    new "1 - Un jour normal au club"

    # screens.rpy:1516
    old "2 - Questioning sexuality (Sakura's route)"
    new "2 - Questions de sexualité (Route de Sakura)"

    # screens.rpy:1521
    old "3 - Headline news"
    new "3 - A la une"

    # screens.rpy:1526
    old "4a - A picnic at the summer (Sakura's route)"
    new "4a - Un pique-nique d'été (Route de Sakura)"

    # screens.rpy:1531
    old "4b - A picnic at the summer (Rika's route)"
    new "4b - Un pique-nique d'été (Route de Rika)"

    # screens.rpy:1536
    old "4c - A picnic at the summer (Nanami's route)"
    new "4c - Un pique-nique d'été (Route de Nanami)"

    # char profiles
    old "Characters profiles"
    new "Profils des personnages"

    # screens.rpy:387
    old "Update Available!"
    new "Mise à jour\ndisponible !"

    # screens.rpy:1536
    old "Opening song lyrics"
    new "Paroles du générique"

    # screens.rpy:1633
    old "J.S. Bach - Air Orchestral suite #3"
    new "J.S. Bach - Ouverture n° 3 en ré majeur"

    # screens.rpy:1635
    old "Mew Nekohime - TAKE MY HEART (TV Size)"
    new "Mew Nekohime - TAKE MY HEART (TV Size)"

    # screens.rpy:1611
    old "Max le Fou - Taichi's Theme"
    new "Max le Fou - Thème de Taichi"

    # screens.rpy:1613
    old "Max le Fou - Sakura's Waltz"
    new "Max le Fou - Valse de Sakura"

    # screens.rpy:1615
    old "Max le Fou - Rika's theme"
    new "Max le Fou - Thème de Rika"

    # screens.rpy:1617
    old "Max le Fou - Of Bytes and Sanshin"
    new "Max le Fou - De Bits et un Sanshin"

    # screens.rpy:1619
    old "Max le Fou - Time for School"
    new "Max le Fou - L'Heure de l'Ecole"

    # screens.rpy:1621
    old "Max le Fou - Sakura's secret"
    new "Max le Fou - Le secret de Sakura"

    # screens.rpy:1623
    old "Max le Fou - I think I'm in love"
    new "Max le Fou - Je crois être amoureux"

    # screens.rpy:1625
    old "Max le Fou - Darkness of the Village"
    new "Max le Fou - Ténèbres sur le Village"

    # screens.rpy:1627
    old "Max le Fou - Love always win"
    new "Max le Fou - L'amour gagne toujours"

    # screens.rpy:1629
    old "Max le Fou - Ondo"
    new "Max le Fou - Ondo"

    # screens.rpy:1631
    old "Max le Fou - Happily Ever After"
    new "Max le Fou - Fin Heureuse"
# TODO: Translation updated at 2019-04-27 10:43

translate french strings:

    # screens.rpy:1552
    old "Achievements"
    new "Succès"
