﻿# TODO: Translation updated at 2018-03-13 13:10

# game/dialogs.rpy:3
translate french scene1_acf2102c:

    # centered "{size=+12}Summer of the 9th year of Heisei era\n(1997){/size}"
    centered "{size=+12}Été de la 9ème année de l'ère Heisei\n(1997){/size}"

# game/dialogs.rpy:6
translate french scene1_dd8c4dd5:

    # "Life has a way of changing. Often drastically without any warnings whatsoever."
    "La vie peut changer de cap d'un seul coup. Souvent, sans prévenir."

# game/dialogs.rpy:7
translate french scene1_0497b746:

    # "At first, a day may seem like any other."
    "Au début, c'est un jour comme un autre."

# game/dialogs.rpy:8
translate french scene1_1ab3b689:

    # "But by tomorrow, it can all change...{p}from where you live, to who you are as a person."
    "Puis le suivant, plus rien n'est pareil...{p}Tout change, du simple détail jusqu'à son moi propre..."

# game/dialogs.rpy:9
translate french scene1_522a76b6:

    # "Recently, I've experienced such a large change."
    "Cela m'est arrivé, récemment. Un grand changement."

# game/dialogs.rpy:10
translate french scene1_011ea937:

    # "My dad wanted to start his own grocery shop. So my parents and I moved to a small village near Osaka."
    "Mon père voulait ouvrir son épicerie dans un village tranquille. Alors nous avions déménagé dans un petit village de campagne près de Osaka."

# game/dialogs.rpy:11
translate french scene1_2b371705:

    # "I thought I would have a hard time readjusting."
    "Je l'ai mal vécu au départ."

# game/dialogs.rpy:12
translate french scene1_a26a5f2b:

    # "The move from Tokyo to a small lost village, far from a big city with shops and fast-food restaurants..."
    "Nous vivions à Tokyo auparavant, et nous voilà dans un village de campagne perdu à des kilomètres du prochain fast-food..."

# game/dialogs.rpy:13
translate french scene1_fa8a1f18:

    # "Being a 18-year-old student, it would be difficult. I was used to the excitement of the big city."
    "Une nouvelle vie plutôt dure pour un lycéen. J'étais habitué à la vie remuante de la grande ville."

# game/dialogs.rpy:14
translate french scene1_8d6f1c40:

    # "However, even before my first day of school, my life would change."
    "Mais la vie se révéla moins dure rapidement... avant mon premier jour dans ma nouvelle école."

# game/dialogs.rpy:19
translate french scene1_d7a7c4a2:

    # centered "{size=+35}CHAPTER 1\nWhere everything started{fast}{/size}"
    centered "{size=+35}CHAPITRE 1\nLà où tout a commencé{fast}{/size}"

# game/dialogs.rpy:26
translate french scene1_5bb0286e:

    # "It's sunday evening, after dinner."
    "Nous étions dimanche soir, après dîner."

# game/dialogs.rpy:27
translate french scene1_a651e12e:

    # "My stuff is all unpacked and placed in my new room."
    "Toutes mes affaires étaient en place dans ma nouvelle chambre."

# game/dialogs.rpy:28
translate french scene1_54a8b155:

    # "I love this new look!"
    "J'aime bien ce nouveau look !"

# game/dialogs.rpy:29
translate french scene1_8bb96761:

    # "It sure is smaller than my previous bedroom, but it's not like I needed that much space."
    "C'est certes plus petit que mon ancienne chambre, mais je n'avais pas besoin de tant d'espace."

# game/dialogs.rpy:30
translate french scene1_acf53d85:

    # "As long as I have room to sleep, play games, watch anime, and geek out, it's all good."
    "Tant que j'ai assez de place pour dormir, jouer, regarder des animés et faire le geek, tout va bien."

# game/dialogs.rpy:31
translate french scene1_7d722416:

    # "I'm not sure what I should do before going to bed."
    "Je ne sais pas quoi faire avant d'aller au lit."

# game/dialogs.rpy:32
translate french scene1_b2b74adf:

    # "I could study a bit, so I don't look too dumb at school tomorrow..."
    "Je devrais peut-être réviser un peu pour ne pas avoir l'air en retard sur les cours..."

# game/dialogs.rpy:33
translate french scene1_ad0d9f75:

    # "But I also want to rest and have a bit of fun..."
    "Mais j'ai aussi besoin de repos et de détente..."

# game/dialogs.rpy:48
translate french scene1_e05b5690:

    # "I should play a short game, or I'll stay up too long. So I decide to play some {i}Tetranet online{/i}."
    "Il ne faut pas que je joue à un jeu trop long. Alors j'ai décidé de faire quelques parties de {i}Tetranet online{/i}."

# game/dialogs.rpy:49
translate french scene1_7170a1ff:

    # "It's a puzzle game with blocks. The online version lets you attack your opponents and flood them with their own blocks."
    "C'est un puzzle avec des blocs. La version en ligne permet de lancer des attaques à ses adversaires pour qu'ils soient inondés de leurs blocs."

# game/dialogs.rpy:50
translate french scene1_8dd01706:

    # "After a few minutes, I found myself in a game room. One of the players named 'NAMINAMI' had two stars in front of their name."
    "Après quelques minutes de jeu, je suis resté face à un autre joueur nommé \"NAMINAMI\" avec deux étoiles devant le pseudo."

# game/dialogs.rpy:51
translate french scene1_962ec706:

    # "Two stars meant that they had won hundreds of games online. Since three stars are for the game developers, I guess that means I'm up against a pro of this game."
    "Ces deux étoiles signifiaient que le joueur a fait plus de cent victoires consécutives. Vu que les 3 étoiles sont pour les développeurs du jeu, j'imaginai que c'était un champion à ce jeu là."

# game/dialogs.rpy:52
translate french scene1_a71313b4:

    # "Judging by the low ping, I guess they live around the same prefecture I live in. Small world..."
    "Le ping bas me laissait deviner que le joueur vivait probablement dans la même préfécture que la mienne. Le monde est petit..."

# game/dialogs.rpy:53
translate french scene1_e0301d9f:

    # "In the dozens of games we played, NAMINAMI continued to beat everyone in the game room. But I was always the last one to fall."
    "Dans la douzaine de parties que j'ai faites, NAMINAMI a vaincu tout le monde dans la salle. Mais j'étais toujours le dernier à tomber."

# game/dialogs.rpy:55
translate french scene1_a40571de:

    # write "NAMINAMI> GG dude, u pretty much rock at this game!{fast}"
    write "NAMINAMI> GG mec, t tro bon a ce jeu!{fast}"

# game/dialogs.rpy:57
translate french scene1_7c6f0607:

    # write "%(stringhero)s>{fast} Nah i'm not that good"
    write "%(stringhero)s>{fast} Non t'exagères"

# game/dialogs.rpy:59
translate french scene1_0d6dddc9:

    # write "NAMINAMI> yes u r. i had trouble winning against u.{fast}"
    write "NAMINAMI> non sérieu. g eu du mal face à toi.{fast}"

# game/dialogs.rpy:61
translate french scene1_87ab3c60:

    # write "NAMINAMI> care 4 a rematch? just u and me?{fast}"
    write "NAMINAMI> On s'en refai 1? just toi & moi?{fast}"

# game/dialogs.rpy:63
translate french scene1_828e7297:

    # write "%(stringhero)s>{fast} Maybe next time. It's getting late, i have school tomorrow."
    write "%(stringhero)s>{fast} Une prochaine fois. Faut que j'aille dodo, j'ai école demain."

# game/dialogs.rpy:65
translate french scene1_0d8de120:

    # write "NAMINAMI> ur rite, me 2 anyway. hope 2 c u again soon!{fast}"
    write "NAMINAMI> Moi ossi tien. A 1 2 c 4!{fast}"

# game/dialogs.rpy:66
translate french scene1_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:73
translate french scene1_ee02f0bc:

    # "I decide to watch some old episodes of the anime based on my favorite manga, {i}High School Samurai{/i}."
    "J'ai regardé quelques épisodes de l'animé basé sur mon manga préféré, {i}High School Samurai{/i}."

# game/dialogs.rpy:74
translate french scene1_d91b4fcd:

    # "It's the story about a guy who lived in the mountains, learning the ways of Bushido. Then, all of a sudden, he must go to a new town to learn about life in the city."
    "C'est l'histoire d'un gars qui a vécu dans les montagnes pour apprendre l'art du Bushido, puis il doit aller vivre en ville pour apprendre la vie citadine."

# game/dialogs.rpy:75
translate french scene1_e72d0922:

    # "He resolves a lot of his problems with his samurai skills and his heroic determination."
    "Il résout un tas de problèmes avec ses talents de samourai et sa détermination héroique."

# game/dialogs.rpy:80
translate french scene1_a19cfb71:

    # hero "I decide to study. Keep up the great work, %(stringhero)s!"
    hero "Il faut que je révise. Tiens le coup, %(stringhero)s!"

# game/dialogs.rpy:82
translate french scene1_ad5fbe82:

    # "I studied until very late at night."
    "J'ai étudié jusque tard dans la nuit."

# game/dialogs.rpy:84
translate french scene1_2f455d53:

    # "I don't know how long I stayed up, but I was so tired, I ended up falling asleep..."
    "Je ne sais pas à quelle heure je suis allé au lit, mais je ne tenais plus debout..."

# game/dialogs.rpy:89
translate french scene1_cb77d3d7:

    # "*{i}Riiiiiiiiiiiing{/i}*"
    "*{i}Driiiiiiiiiiiing{/i}*"

# game/dialogs.rpy:91
translate french scene1_4037429a:

    # "I switched off the alarm clock and went back to sleep again."
    "J'éteignis mon réveil et me rendormis..."

# game/dialogs.rpy:92
translate french scene1_548721a3:

    # hero "I'm so tired..."
    hero "Je suis crevé..."

# game/dialogs.rpy:93
translate french scene1_2a13d6dc:

    # "Mom" "%(stringhero)s!! Wake up!!! You'll be late for school!!!"
    "Maman" "%(stringhero)s !! Debout !! Tu vas être en retard à l'école !!!"

# game/dialogs.rpy:94
translate french scene1_c83937b9:

    # hero "Huh... What time is it..."
    hero "Mmm... Quelle heure il est..."

# game/dialogs.rpy:95
translate french scene1_fc1e319f:

    # "Wait... 8:02AM?!{p} School starts at 8:30AM!!!"
    "Je lis 8:02.{p}Et je commence les cours à 8:30."

# game/dialogs.rpy:97
translate french scene1_4217b105:

    # hero "Dangit!"
    hero "Bon sang !"

# game/dialogs.rpy:99
translate french scene1_62c6f195:

    # hero "I stayed up and played too much! I'm late!"
    hero "J'ai trop joué ! Je suis en retard !"

# game/dialogs.rpy:101
translate french scene1_b8605e00:

    # hero "I watched too much anime! I'm late!"
    hero "J'ai trop veillé ! Je suis en retard !"

# game/dialogs.rpy:103
translate french scene1_a476aee9:

    # hero "I studied too much! I'm late!"
    hero "J'ai trop révisé ! Je suis en retard !"

# game/dialogs.rpy:106
translate french scene1_0ff90bcf:

    # "I jumped out of bed, put on my brand new school uniform, and ran down the stairs."
    "Je sautai du lit en vitesse, mis mon nouvel uniforme scolaire et descendis les escaliers quatre à quatre."

# game/dialogs.rpy:108
translate french scene1_5c1508de:

    # "I ate my breakfast faster than usual."
    "J'engloutis mon petit déj' en moins de temps qu'il ne faut pour le dire."

# game/dialogs.rpy:109
translate french scene1_6a44f1a3:

    # "I took a map with a route to school and left after saying goodbye to my parents."
    "Je pris le plan pour l'école et sortis après avoir salué mes parents."

# game/dialogs.rpy:112
translate french scene1_ad63def5:

    # "I followed the map and dashed through the streets."
    "Je suivis le chemin indiqué par la carte en courant."

# game/dialogs.rpy:113
translate french scene1_00e59ce7:

    # "This town looks pretty unique."
    "Ce village avait l'air unique."

# game/dialogs.rpy:114
translate french scene1_689e5094:

    # "It's a strange town, like a mix between a suburb and a rural village."
    "Une ville étrange, un mélange de banlieue et de village campagnard."

# game/dialogs.rpy:115
translate french scene1_a64e7e8b:

    # "Summer had just arrived. The cicadas were crying. And it sure was hot out here..."
    "L'été était arrivé, les cigales chantaient comme d'accoutumée au Japon, et il faisait chaud."

# game/dialogs.rpy:116
translate french scene1_71061539:

    # "I noticed only a few people on the streets."
    "Je ne croisais que peu de gens sur mon chemin."

# game/dialogs.rpy:117
translate french scene1_1d275b47:

    # "Some of them were kind enough to help me find my way to the school..."
    "Certains m'ont aidé à trouver mon chemin dans ce dédale de rues et de champs..."

# game/dialogs.rpy:118
translate french scene1_e123d412:

    # "Then, as I was nearing an intersection..."
    "Puis, en arrivant à un carrefour..."

# game/dialogs.rpy:119
translate french scene1_da3e8686:

    # "I see a girl in a school uniform."
    "Je vis une fille en uniforme scolaire."

# game/dialogs.rpy:121
translate french scene1_72d48eb1:

    # hero "Well, since village only has one school...that must be where she's heading!"
    hero "Comme le village n'a qu'une seule école, c'est sûrement là où elle va !"

# game/dialogs.rpy:122
translate french scene1_018ee7a3:

    # hero "And if that is where she's going, maybe she could help me out!"
    hero "Et si c'est le cas, elle pourra surement m'aider !"

# game/dialogs.rpy:123
translate french scene1_a95f40e0:

    # "I approached her carefully, as I didn't want to scare her."
    "Je m'approchai doucement, histoire de ne pas l'effrayer."

# game/dialogs.rpy:124
translate french scene1_b0e8ce5c:

    # "She looked beautiful; long black hair, thin body, and beautiful legs..."
    "Elle avait l'air très belle ; de longs cheveux noirs, un corps fin, de jolies jambes..."

# game/dialogs.rpy:125
translate french scene1_d501d47f:

    # "I hesitated for a moment before saying anything..."
    "Cela me fit hésiter un moment, mais je me lançai..."

# game/dialogs.rpy:126
translate french scene1_9195ee62:

    # hero "Ahem... Excuse me, miss?"
    hero "Hum... Excusez-moi ?"

# game/dialogs.rpy:131
translate french scene1_951b2409:

    # "She turned around to face me."
    "Elle se tourna pour me faire face."

# game/dialogs.rpy:132
translate french scene1_b7723325:

    # "Oh wow! She was insanely pretty!"
    "Waouh ! Elle était plus belle que ce que j'imaginais !"

# game/dialogs.rpy:133
translate french scene1_64e26830:

    # "Her eyes were blue.{p}A wonderful, deep, ocean blue. They were like the clear waters you'd find in Hawaii!"
    "Ses yeux étaient bleus.{p}Un bleu océanique profond et magnifique. Comme les mers qui entourent Hawaii !"

# game/dialogs.rpy:134
translate french scene1_a28c7063:

    # "She looked at me, confused, and a bit timid."
    "Elle me regarda, confuse et intimidée."

# game/dialogs.rpy:136
translate french scene1_40d86de1:

    # s "Yes?"
    s "Oui ?"

# game/dialogs.rpy:137
translate french scene1_013ed851:

    # "Wow! Her voice sounds so adorable!"
    "Sa voix égalait sa beauté."

# game/dialogs.rpy:138
translate french scene1_ed63afd9:

    # "She is so cute, that I was at a loss for words."
    "Elle était si mignonne que je perdis mes mots un instant."

# game/dialogs.rpy:139
translate french scene1_d015b36e:

    # hero "Uhhh... I'm new in the village, and I'm trying to find my school."
    hero "Euh... Je suis nouveau au village, et j'essaie de trouver l'école."

# game/dialogs.rpy:140
translate french scene1_759f29b6:

    # hero "And since you're wearing a uniform... I thought you could..."
    hero "Et comme tu portes un uniforme, j'ai pensé que..."

# game/dialogs.rpy:141
translate french scene1_9c182eb6:

    # "The girl stared at me with her big, majestic, blue eyes..."
    "La fille me regarda avec ses grands yeux bleus..."

# game/dialogs.rpy:142
translate french scene1_e85ec319:

    # "Finally, she smiled."
    "Elle me sourit."

# game/dialogs.rpy:144
translate french scene1_208138e6:

    # s "Of course I can!"
    s "Oui, bien sûr !"

# game/dialogs.rpy:145
translate french scene1_d94b34fa:

    # s "I'm going there right now. You can follow me!"
    s "J'y vais, là. Tu n'as qu'à me suivre !"

# game/dialogs.rpy:146
translate french scene1_453ef329:

    # hero "That would be great! Thank you!"
    hero "Ce serait bien ! Merci !"

# game/dialogs.rpy:150
translate french scene1_ef8271d0:

    # "We ended up walking to school together."
    "Nous sommes allés en direction de l'école, ensemble."

# game/dialogs.rpy:151
translate french scene1_e8a9e895:

    # "Darn, looks like it's my lucky day. {p}I'm walking to school with a beautiful girl on the first day!!!"
    "On dirait que c'est mon jour de chance...{p}Mon premier jour d'école et je suis déjà en charmante compagnie !"

# game/dialogs.rpy:152
translate french scene1_cb9f5aad:

    # s "You said you're new in town, right?"
    s "Tu viens d'arriver, alors ?"

# game/dialogs.rpy:153
translate french scene1_36317276:

    # hero "Hmm?"
    hero "Hmm?"

# game/dialogs.rpy:155
translate french scene1_ecbbbe32:

    # "I had been daydreaming, so I didn't notice that she was talking to me."
    "J'avais la tête dans les nuages et n'avais pas remarqué qu'elle me parlait."

# game/dialogs.rpy:156
translate french scene1_d86baf24:

    # "She started to blush."
    "Elle commença à rougir."

# game/dialogs.rpy:158
translate french scene1_0408e4ff:

    # s "Oh... I'm sorry! I didn't mean to surprise you!"
    s "Oh... Pardonne-moi si je suis indiscrète..."

# game/dialogs.rpy:159
translate french scene1_004a1338:

    # "She's just so adorable! I smiled to myself."
    "Si adorable! Je souris."

# game/dialogs.rpy:160
translate french scene1_02ac9e9f:

    # hero "You're not!{p}Actually, I just arrived last Friday. My family just moved here from Tokyo."
    hero "Oh non, pas du tout !{p}Je suis arrivé vendredi dernier. On vient de Tokyo."

# game/dialogs.rpy:162
translate french scene1_b0c354bc:

    # s "You're from Tokyo?"
    s "De Tokyo ?"

# game/dialogs.rpy:163
translate french scene1_7273bcfc:

    # s "It seems like a wonderful city! I wish I could visit it someday!"
    s "C'est une ville magnifique ! Je rêve de pouvoir la visiter un jour !"

# game/dialogs.rpy:164
translate french scene1_e87c5166:

    # hero "I bet... I noticed there are fewer distractions here than in Tokyo."
    hero "J'imagine... Depuis que je suis arrivé, je n'ai pas trouvé grand chose qui me rappelle cette ville."

# game/dialogs.rpy:166
translate french scene1_6ab8e3af:

    # s "Mmhmm, it's true. The village is kinda quiet; so there's usually nothing to do..."
    s "Mmm, c'est vrai. Le village est assez tranquille et il n'y a pas grand chose à y faire..."

# game/dialogs.rpy:167
translate french scene1_f5cb34b6:

    # s "When we want to enjoy ourselves, we usually go to the nearest city."
    s "Il faut aller à la grande ville la plus proche pour les distractions."

# game/dialogs.rpy:169
translate french scene1_f471c6c0:

    # s "It's only a short train ride away, so it's okay!"
    s "Mais le train prend peu de temps pour y aller, donc ça va !"

# game/dialogs.rpy:170
translate french scene1_6136ebba:

    # hero "There's a train that takes you to the city nearby!?"
    hero "Un train ? Pour la ville la plus proche ?!"

# game/dialogs.rpy:171
translate french scene1_bbdac77d:

    # hero "I hope there's a lot of stuff to do there."
    hero "J'espère qu'il y a de quoi s'amuser là-bas."

# game/dialogs.rpy:172
translate french scene1_aa2bb7ad:

    # s "There is, don't worry! There are fast food restaurants, shops, arcades..."
    s "Ne t'en fais pas ! Il y a des fast-foods, des magasins, des salles d'arcade..."

# game/dialogs.rpy:173
translate french scene1_cd44299c:

    # hero "You like arcade games?"
    hero "Tu aimes les jeux d'arcade ?"

# game/dialogs.rpy:175
translate french scene1_69578672:

    # s "Very much so!"
    s "J'adore !"

# game/dialogs.rpy:176
translate french scene1_13bf5fa0:

    # s "I'm especially a fan of music and rhythm games, but I enjoy shooting games too!"
    s "Surtout les jeux musicaux... Et les jeux de tirs, aussi !"

# game/dialogs.rpy:177
translate french scene1_0fef1f71:

    # hero "Shooting games?"
    hero "Des jeux de tirs ?"

# game/dialogs.rpy:178
translate french scene1_ad5c53dd:

    # "Wow... I couldn't imagine such a cute and timid girl shooting zombies or soldiers in an arcade game..."
    "Wow... J'ai du mal à imaginer une fille aussi mimi et timide tenir un flingue et tirer sur des zombies, même dans un jeu vidéo..."

# game/dialogs.rpy:179
translate french scene1_45a2a82d:

    # hero "By the way, what grade are you in?"
    hero "Dans quelle classe tu es ?"

# game/dialogs.rpy:181
translate french scene1_68018eef:

    # s "I'm in the second section."
    s "Je suis en seconde section."

# game/dialogs.rpy:182
translate french scene1_4c52535b:

    # hero "Uh...Second section?"
    hero "Uh...Seconde section ?"

# game/dialogs.rpy:183
translate french scene1_3b1fd997:

    # s "You see, our school is the only school in the village."
    s "C'est la seule école du village,"

# game/dialogs.rpy:184
translate french scene1_7e47a0e7:

    # s "There are students of many different ages and grades at our school, but there's an unbalanced amount of students for the usual grade system."
    s "et les étudiants sont d'âges tellement différents que ce n'est pas équilibré pour le système scolaire habituel."

# game/dialogs.rpy:185
translate french scene1_a82424ad:

    # s "So they made different classes, called sections, consisting of 25 students each, which are made up of students of different ages and grades."
    s "Alors il y a des classes différentes, appelées sections, de 25 étudiants de tous les âges."

# game/dialogs.rpy:186
translate french scene1_b83c9923:

    # hero "That sounds fun... I wonder where I'll be assigned to..."
    hero "Pas mal, ça... Je me demande où je vais aterrir..."

# game/dialogs.rpy:187
translate french scene1_696b7e4d:

    # s "Well, there are only five sections."
    s "Actuellement, il y a cinq sections."

# game/dialogs.rpy:188
translate french scene1_029cc297:

    # hero "So I'll have a 1-in-5 chance of being in the same section as you!"
    hero "J'ai une chance sur cinq de me retrouver dans ta classe, alors..."

# game/dialogs.rpy:189
translate french scene1_c959a464:

    # "I chuckled a bit and she giggled, a little embarrassed."
    "On rit un peu, en étant un peu embarassés."

# game/dialogs.rpy:191
translate french scene1_c152d24c:

    # s "My name is Sakura... And yours?"
    s "Je m'appelle Sakura.... Et toi ?"

# game/dialogs.rpy:192
translate french scene1_180ea3e3:

    # hero "Name's %(stringhero)s."
    hero "Moi c'est %(stringhero)s."

# game/dialogs.rpy:194
translate french scene1_abeffde2:

    # s "%(stringhero)s..."
    s "%(stringhero)s..."

# game/dialogs.rpy:196
translate french scene1_77296229:

    # s "Nice to meet you, %(stringhero)s-san!"
    s "Enchanté de te connaître, %(stringhero)s-san !"

# game/dialogs.rpy:197
translate french scene1_6ec72dd8:

    # hero "Pleased to meet you as well, Sakura-san!"
    hero "Pareillement, Sakura-san !"

# game/dialogs.rpy:198
translate french scene1_548f585d:

    # "She gave me a sweet smile."
    "Elle sourit doucement."

# game/dialogs.rpy:199
translate french scene1_4466f621:

    # "Gosh, if a smile could kill, I think I'd be dead right now..."
    "Si un sourire pouvait tuer, je ne donnerais pas cher de ma vie, là..."

# game/dialogs.rpy:201
translate french scene1_1a8c3634:

    # "Deep down, I really hoped to be in the same class as her."
    "En moi-même, je souhaitai de tout cœur d'arriver dans sa classe."

# game/dialogs.rpy:202
translate french scene1_2fcc0bdf:

    # "I wanted to see her again and learn more about her... {image=heart.png}"
    "J'avais envie de la revoir et de la connaître mieux... {image=heart.png}"

# game/dialogs.rpy:210
translate french scene1_df51f81c:

    # "I can't believe it!"
    "J'y crois pas !"

# game/dialogs.rpy:211
translate french scene1_06cbe8a5:

    # "I've been assigned to the second section!{p}I'm so happy!"
    "J'ai été affecté à la seconde section !{p}C'est trop cool !"

# game/dialogs.rpy:212
translate french scene1_a8b3f3cd:

    # "However, I wonder if it's a good section."
    "Cependant, j'espère que c'est une bonne classe."

# game/dialogs.rpy:213
translate french scene1_3c0ab1de:

    # "I wouldn't like to be in a classroom full of jerks or something..."
    "Je ne voudrais pas être dans une salle pleine de blaireaux..."

# game/dialogs.rpy:214
translate french scene1_9a96bdeb:

    # "If that's the case, I just hope I'll be near Sakura-san..."
    "Si c'est le cas, j'espère que je serai près de Sakura-san..."

# game/dialogs.rpy:216
translate french scene1_1111d955:

    # "*{i}Bump{/i}*"
    "*{i}Bump{/i}*"

# game/dialogs.rpy:217
translate french scene1_89c4aa2c:

    # "While I was daydreaming again, I bumped into someone."
    "Comme je rêvassais encore, je suis rentré dans quelqu'un."

# game/dialogs.rpy:218
translate french scene1_65939e34:

    # "It was a girl with pigtails."
    "C'était une jolie fille avec les cheveux bleu clair."

# game/dialogs.rpy:222
translate french scene1_7c921ed5:

    # r "Hey, you! Watch where you're going!"
    r "Hé, toi ! Tu pourrais regarder où tu marches !"

# game/dialogs.rpy:223
translate french scene1_5359da65:

    # hero "Sorry... It was my fault..."
    hero "Désolé... Je ne t'avais pas vu..."

# game/dialogs.rpy:225
translate french scene1_b17d24d0:

    # r "Wait... You're new here, aren't ya?"
    r "Mmm... Tu es nouveau ici, non ?"

# game/dialogs.rpy:227
translate french scene1_39364aa2:

    # r "Hmph, well whatever, I'll see you later."
    r "Hmph, on se retrouvera..."

# game/dialogs.rpy:229
translate french scene1_b67db50c:

    # "She ran down the hall, probably heading to her classroom."
    "Elle courut dans le couloir, probablement en direction de sa classe."

# game/dialogs.rpy:230
translate french scene1_e2ba9dde:

    # "That girl was kinda rude... But rather pretty-looking..."
    "Cette fille était rude... Mais plutôt mignonne..."

# game/dialogs.rpy:231
translate french scene1_cd4a6e19:

    # "She reminds me of the main female character of the anime Domoco-chan because of her hair... And from what I could tell, her personality matched as well..."
    "Elle me rappelle l'héroine du manga Domoco-chan, avec ses couettes... Et de ce que j'ai pu voir, sa personalité aussi..."

# game/dialogs.rpy:234
translate french scene1_41041ae3:

    # "Here it is... Second section's classroom."
    "Nous y voilà... La seconde section."

# game/dialogs.rpy:235
translate french scene1_201315b4:

    # "Some of students were already here."
    "Certains étudiants étaient déjà là."

# game/dialogs.rpy:236
translate french scene1_8928782e:

    # "Sakura-san was right, there are students of all ages here."
    "Sakura-san avait raison, ils étaient tous d'âge différents."

# game/dialogs.rpy:237
translate french scene1_563881d0:

    # "The youngest looked around 8 years old and the oldest, about 18."
    "Le plus jeune devait avoir 8 ans et le plus vieux dans les 18 ans."

# game/dialogs.rpy:239
translate french scene1_75dbcf3d:

    # "I saw Sakura-san sitting by the window."
    "Je vis Sakura-san à un bureau près de la fenêtre."

# game/dialogs.rpy:240
translate french scene1_3f9d91b2:

    # "She smiled as soon as saw me."
    "Elle me sourit en me voyant arriver."

# game/dialogs.rpy:242
translate french scene1_05ec4d50:

    # s "Ah, %(stringhero)s-san! Over here! There's a desk available here!"
    s "Ah, %(stringhero)s-san ! Par ici ! Il y a une place de libre ici !"

# game/dialogs.rpy:243
translate french scene1_a4ac0809:

    # "She pointed the empty desk behind her."
    "Elle pointa le bureau vide derrière elle."

# game/dialogs.rpy:244
translate french scene1_c00ae698:

    # "How lucky! A desk near her!"
    "Quelle chance ! Une place juste près d'elle !"

# game/dialogs.rpy:245
translate french scene1_95960ba8:

    # "This really is my lucky day!"
    "C'est vraiment mon jour de chance !"

# game/dialogs.rpy:247
translate french scene1_c25aa128:

    # hero "I'm so excited we're in the same section!"
    hero "Je suis content qu'on soit dans la même classe !"

# game/dialogs.rpy:248
translate french scene1_7882f4c9:

    # s "So am I!"
    s "Moi aussi !"

# game/dialogs.rpy:249
translate french scene1_ef153560:

    # hero "Our classmates really are made up of different ages!"
    hero "C'est drôle de voir autant d'âges différents dans la même classe."

# game/dialogs.rpy:250
translate french scene1_198a38e2:

    # s "Just like I told you!"
    s "Comme je te le disais !"

# game/dialogs.rpy:251
translate french scene1_44c33509:

    # hero "Actually,{w} this whole town seems pretty special..."
    hero "En fait,...{w}Toute la ville a l'air si spéciale..."

# game/dialogs.rpy:252
translate french scene1_7a669bce:

    # hero "I mean, the school system, the way the streets are, the kind-hearted townfolk..."
    hero "Ce système scolaire,...la façon dont sont faites les rues,...la gentillesse des habitants,..."

# game/dialogs.rpy:253
translate french scene1_3b8a00b5:

    # s "Do you like it?"
    s "Tu aimes ?"

# game/dialogs.rpy:254
translate french scene1_874f89c2:

    # hero "So far...Yeah, I think I do like it."
    hero "Pour l'instant...je pense que oui. J'aime ça."

# game/dialogs.rpy:255
translate french scene1_d703df5c:

    # s "I hope you'll enjoy your time here, %(stringhero)s-san!"
    s "J'espère que tu te plaîras ici, %(stringhero)s-san !"

# game/dialogs.rpy:256
translate french scene1_0afeb16b:

    # hero "Thank you, Sakura-senpai."
    hero "Merci, Sakura-senpai."

# game/dialogs.rpy:257
translate french scene1_034d31d6:

    # hero "So, what are we learning in class today?"
    hero "On a quoi comme cours, aujourd'hui ?"

# game/dialogs.rpy:258
translate french scene1_21c1af0a:

    # s "Math."
    s "Maths."

# game/dialogs.rpy:259
translate french scene1_a9d114f6:

    # hero "Oh geez..."
    hero "Ouille..."

# game/dialogs.rpy:260
translate french scene1_0940f6fc:

    # "Looks like my luck couldn't last all day..."
    "La chance m'a finalement tourné le dos."

# game/dialogs.rpy:261
translate french scene1_07506d68:

    # s "You don't like math?"
    s "Tu n'aimes pas les maths ?"

# game/dialogs.rpy:262
translate french scene1_280571a1:

    # hero "Well, I'm not very good at it."
    hero "Je ne suis pas très doué."

# game/dialogs.rpy:263
translate french scene1_21fba3a7:

    # s "I'm pretty good with math. I can help you, if you want!"
    s "Moi je le suis. Je pourrai te donner un coup de main, si tu veux !"

# game/dialogs.rpy:264
translate french scene1_0181a8b6:

    # hero "That sounds perfect!"
    hero "Ce serait super !"

# game/dialogs.rpy:265
translate french scene1_8b4aa3e9:

    # hero "Thanks a lot, Sakura-senpai!"
    hero "Merci beaucoup, Sakura-senpai !"

# game/dialogs.rpy:266
translate french scene1_1e4821cf:

    # s "Teehee! You're welcome!"
    s "Hihi ! De rien !"

# game/dialogs.rpy:268
translate french scene1_a8275802:

    # hero "Well, since I stayed up all night studying, it shouldn't be that hard."
    hero "Après cette nuit à réviser, ça ne devrait pas être trop dur."

# game/dialogs.rpy:269
translate french scene1_0df50adc:

    # s "I doubt we'll start with something too difficult anyway. But I'll be here if you need me!"
    s "Je doute que l'on commence avec quelque chose de difficile. Mais je serai là si tu as besoin !"

# game/dialogs.rpy:273
translate french scene1_6220dd69:

    # "Class started without anything noteworthy happening."
    "Les cours commencèrent sans rien de notable."

# game/dialogs.rpy:274
translate french scene1_5181e117:

    # "Math ended up being pretty easy. It was planned for every level, probably because of all the different ages in class."
    "Honnêtement, ce fut un cours facile. Il était prévu pour tous les niveaux, à cause de la différence d'âge."

# game/dialogs.rpy:276
translate french scene1_a1cb44f3:

    # "Or it could be that all that studying really paid off..."
    "Ou alors les études d'hier soir les ont rendus faciles..."

# game/dialogs.rpy:277
translate french scene1_3523ed26:

    # "The lessons felt like they were going on for awhile though..."
    "Les cours semblaient si longs..."

# game/dialogs.rpy:287
translate french scene3_2db6f6fb:

    # "The school bell rang to announce the end of classes."
    "La cloche sonna la fin des cours."

# game/dialogs.rpy:288
translate french scene3_9a7755fc:

    # "I was surprised by the noise. It looked more like a bell for firemen. This is really not a urban school!"
    "Je fus surpris par le bruit inhabituel de cette dernière. On aurait dit une cloche d'alarme pour les pompiers. Ça ne fait vraiment pas comme une école de la grande ville !"

# game/dialogs.rpy:289
translate french scene3_99288ad6:

    # "Sakura and I decided to take and eat our lunches together in the classroom, as well as some other classmates."
    "Sakura et moi-même prirent le déjeuner ensemble dans la classe, tout comme d'autres camarades."

# game/dialogs.rpy:290
translate french scene3_d58c404f:

    # "During lunch, I asked Sakura-san if there were clubs here like in normal schools."
    "Durant le déjeuner, je demandai à Sakura-san s'il y avait des clubs comme dans les autres écoles."

# game/dialogs.rpy:292
translate french scene3_0e1303f7:

    # s "Yes, for example, I'm in the manga club."
    s "Oui, je suis moi-même dans le club manga."

# game/dialogs.rpy:293
translate french scene3_5d47cb20:

    # s "I like it a lot, plus the leader of the club is a cosplayer."
    s "C'est un bon club, fondé par une amie cosplayer."

# game/dialogs.rpy:294
translate french scene3_aae30260:

    # hero "A manga club!?"
    hero "Un club manga ?!"

# game/dialogs.rpy:295
translate french scene3_04e97040:

    # hero "Awesome! I love manga!"
    hero "C'est super ! J'adore les mangasses !"

# game/dialogs.rpy:297
translate french scene3_6ec6ef6f:

    # s "You do?{p}That's great!"
    s "Vraiment ?{p}C'est génial !"

# game/dialogs.rpy:298
translate french scene3_f9c203d9:

    # s "You're more than welcome if you want to join us, %(stringhero)s-san!"
    s "Tu seras la bienvenue, si tu veux te joindre à nous, %(stringhero)s-san !"

# game/dialogs.rpy:299
translate french scene3_c7e8dac1:

    # hero "That would be awesome!"
    hero "Ce serait bien !"

# game/dialogs.rpy:300
translate french scene3_c0f4f625:

    # hero "I just hope the club leader will let me join..."
    hero "J'espère juste que la fondatrice me laissera vous rejoindre."

# game/dialogs.rpy:302
translate french scene3_fc29c8d9:

    # s "I'm sure she will! We're always looking for new members, especially right now!"
    s "Je suis sûre que oui ! On a toujours besoin de nouveaux membres, surtout en ce moment."

# game/dialogs.rpy:303
translate french scene3_3d9d0b13:

    # hero "'She'?"
    hero "Alors c'est bien une fille à la tête du club ?"

# game/dialogs.rpy:304
translate french scene3_ea9b5b89:

    # s "Yup!"
    s "Oui !"

# game/dialogs.rpy:305
translate french scene3_cca5acb7:

    # s "You know, a lot of girls enjoy anime and manga."
    s "Tu sais, il y a beaucoup de filles qui aiment les animes et les mangas."

# game/dialogs.rpy:306
translate french scene3_10b77e4e:

    # hero "Yeah, I know..."
    hero "Ouais, je sais..."

# game/dialogs.rpy:307
translate french scene3_65b45dcd:

    # "Actually, I have an older sister who loved shoujo anime and manga when we were younger."
    "En fait j'ai moi-même une grande sœur qui aimait beaucoup les mangas et animés shoujo."

# game/dialogs.rpy:308
translate french scene3_b56e0491:

    # "But that was a long time ago. She's married now and is living in Tokyo with her husband."
    "Mais c'est du passé. Aujourd'hui elle est mariée et est restée à Tokyo avec son mari."

# game/dialogs.rpy:309
translate french scene3_b784ebc5:

    # "I wonder how she is doing... I should e-mail her when I get back home."
    "Je me demandai comment elle va... Je pensai à lui écrire un e-mail en rentrant."

# game/dialogs.rpy:310
translate french scene3_2494a3a5:

    # hero "How many members are in the club?"
    hero "Combien il y a de membres dans ce club ?"

# game/dialogs.rpy:312
translate french scene3_cb2e3f44:

    # s "Actually, we only have three members right now..."
    s "En ce moment...on est trois..."

# game/dialogs.rpy:313
translate french scene3_76a2a0d6:

    # hero "Three? That's all?"
    hero "Trois ? C'est tout ?"

# game/dialogs.rpy:314
translate french scene3_88a7dbf3:

    # s "Yeah... Three girls."
    s "Oui... Trois filles."

# game/dialogs.rpy:315
translate french scene3_1f17e8f8:

    # s "It's rather hard to find new members, considering the only manga shop around is in the city..."
    s "En fait, c'est dur de recruter des nouveaux membres parce que la seule boutique de mangas est dans la grande ville..."

# game/dialogs.rpy:316
translate french scene3_9f89b658:

    # s "Usually, the people that join aren't too invested to get into new manga to read so they end up leaving the club."
    s "Les gens qui nous rejoignent lâchent l'affaire à cause du trajet et ils finissent par quitter le club."

# game/dialogs.rpy:317
translate french scene3_84b8bd32:

    # hero "I see."
    hero "Je vois."

# game/dialogs.rpy:318
translate french scene3_dd98eff8:

    # "Holy crap! Joining a manga club with only three girls!?"
    "Eh bien ! Intégrer un club de manga tenu que par des filles !"

# game/dialogs.rpy:319
translate french scene3_d931c57f:

    # "Seems like my luck is back! I feel like the hero of a harem anime or a visual novel!"
    "Je doute que d'autres mecs aient cette chance là !... J'ai l'impression d'être le héros d'un animé harem ou d'une visual novel !"

# game/dialogs.rpy:321
translate french scene3_454939b4:

    # s "So, shall we go?"
    s "On y va ?"

# game/dialogs.rpy:322
translate french scene3_0dab1b57:

    # hero "Sure! You lead the way, Sakura-senpai!"
    hero "D'accord, je te suis, senpai !"

# game/dialogs.rpy:324
translate french scene3_452bb69b:

    # s "Let's go!"
    s "OK !"

# game/dialogs.rpy:329
translate french scene3_afe022e1:

    # "I followed her through the halls. We climbed up the stairs and then entered a small classroom."
    "Je la suivis dans les couloirs. Nous montâmes les escaliers et entrâmes dans une salle de classe."

# game/dialogs.rpy:331
translate french scene3_31abf4ae:

    # "The room only had a few desks and they were all placed together to make a big table."
    "La salle avait quelques bureaux, mis côte à côte pour faire une grande table."

# game/dialogs.rpy:332
translate french scene3_1b03234c:

    # "There were some manga posters on the walls and a bookshelf full of manga and artbooks."
    "Il y avait quelques posters manga sur les murs et une étagère pleine à craquer de mangas et artbooks."

# game/dialogs.rpy:333
translate french scene3_bfcd2b6d:

    # "There was also a small TV with a NATO GEO gaming console and a VCR plugged in it."
    "Il y avait aussi une petite télé avec une console NATO GEO et un magnétoscope branchés dessus."

# game/dialogs.rpy:335
translate french scene3_f22a2c8a:

    # "Two girls were already sitting at the table. One of them was completely focused on her handheld console."
    "Deux filles étaient assises à la grande table improvisée. L'une d'elles était concentrée sur sa console portable."

# game/dialogs.rpy:336
translate french scene3_85e454f4:

    # "I instantly recognized the other one. She was the same girl that I had bumped into this morning!"
    "Je reconnus immédiatement l'autre. C'était la fille que j'avais bousculée accidentellement dans le couloir ce matin."

# game/dialogs.rpy:340
translate french scene3_074e44c0:

    # r "Sakura-chan! How are you?"
    r "Sakura-chan ! Comment va ?"

# game/dialogs.rpy:342
translate french scene3_bedfc3ef:

    # s "Rika-chan! Nanami-chan!"
    s "Rika-chan! Nanami-chan!"

# game/dialogs.rpy:343
translate french scene3_8ac194aa:

    # s "I found a new member for the club!"
    s "Je nous ai trouvé un nouveau membre !"

# game/dialogs.rpy:344
translate french scene3_a4e3e823:

    # hero "Huh... Hello there?"
    hero "Euh... Bonjour ?"

# game/dialogs.rpy:346
translate french scene3_7fdb9008:

    # r "You!"
    r "Toi !"

# game/dialogs.rpy:348
translate french scene3_0ca8552f:

    # s "You know him already?"
    s "Vous vous connaissez ?"

# game/dialogs.rpy:350
translate french scene3_771d8927:

    # r "Yeah, he bumped into me in the hallway this morning!"
    r "Oui, il m'est rentré dedans ce matin !"

# game/dialogs.rpy:351
translate french scene3_56462fb4:

    # hero "Hey, I already told you that I'm sorry! I was daydreaming and I didn't see you!"
    hero "Hé, j'ai dit que j'étais désolé ! Je ne t'ai pas vu parce que je rêvassais !"

# game/dialogs.rpy:352
translate french scene3_4b30e319:

    # r "Heh... I bet you were daydreaming about girls, weren't you!?"
    r "Heh... Tu pensais à des filles, j'imagine !"

# game/dialogs.rpy:353
translate french scene3_d003c469:

    # hero "Yikes!"
    hero "Argh !"

# game/dialogs.rpy:354
translate french scene3_67cc7e93:

    # "Well... she was kind of right..."
    "En fait, elle n'avait pas vraiment tort..."

# game/dialogs.rpy:355
translate french scene3_7ee0bf30:

    # "I felt a little embarrassed."
    "Je me sentis embarassé."

# game/dialogs.rpy:356
translate french scene3_b9df3e31:

    # "I noticed Nanami stopped playing and sneaked over silently to join us."
    "Je remarquai que Nanami avait arrêté de jouer pour venir nous rejoindre."

# game/dialogs.rpy:359
translate french scene3_2385f465:

    # s "Don't pay attention to her sourness. All boys are perverts to Rika-chan, but it's not in a mean way!"
    s "Ne t'en fais pas, %(stringhero)s-san. Rika-chan a toujours été comme ça avec les garçons, mais ce n'est pas méchant."

# game/dialogs.rpy:360
translate french scene3_bdadc851:

    # hero "I see. I just hope she doesn't kill me in my sleep."
    hero "Je vois. J'espère juste qu'elle n'ira pas me tuer dans mon sommeil."

# game/dialogs.rpy:361
translate french scene3_85dc831b:

    # r "Hmph!"
    r "Hmph !"

# game/dialogs.rpy:363
translate french scene3_0160ed38:

    # s "He likes anime and manga, plus he seems quite motivated. Don't you want to give him a chance?"
    s "Il aime les mangas et les animés, et il semble très motivé. Tu ne veux pas lui laisser une chance ?"

# game/dialogs.rpy:364
translate french scene3_bfbf7878:

    # s "We need more boys in this club too. Some people already think we're just a girls only club."
    s "On a besoin de plus d'hommes dans ce club, aussi. Certains commencent à croire qu'on est un club de filles."

# game/dialogs.rpy:368
translate french scene3_49d8fa00:

    # r "Alright, fine, I accept. Welcome to the club, Mr...?"
    r "Très bien, j'approuve. Bienvenue dans notre club, euh..."

# game/dialogs.rpy:369
translate french scene3_d2163b95:

    # hero "%(stringhero)s. Nice to meet you."
    hero "%(stringhero)s. Enchanté."

# game/dialogs.rpy:370
translate french scene3_da243e9b:

    # r "Yeah, likewise."
    r "Ouais, pareil."

# game/dialogs.rpy:371
translate french scene3_48579522:

    # "Rika spoke with a Kansai dialect. Just like most of the village's townfolk."
    "Rika avait un fort accent du Kansai, comme la plupart des habitants du village."

# game/dialogs.rpy:376
translate french scene3_d37e267a:

    # "I saw Nanami just looking at me curiously, so Sakura spoke."
    "Je remarquai Nanami qui me regardait curieusement. Sakura reprit."

# game/dialogs.rpy:377
translate french scene3_1901ff6f:

    # s "So you already met Rika-chan. Here we have our little Nanami-chan! She entered the club just two weeks ago!"
    s "Donc, c'est Rika-chan, et ici, c'est la petite Nanami-chan. Elle nous a rejoint il y a deux semaines."

# game/dialogs.rpy:379
translate french scene3_35deb10d:

    # n "Hey, I'm not little! I'm 16!"
    n "Hé, je suis pas petite ! J'ai 16 ans !"

# game/dialogs.rpy:380
translate french scene3_a2b33e03:

    # "It's true, she looks a lot younger than she is. But she was still pretty cute looking."
    "C'est vrai qu'elle faisait plus jeune. Elle avait l'air mimi."

# game/dialogs.rpy:381
translate french scene3_15aa8440:

    # r "She's small but strong! She doesn't brag much about it but she's good at video games. And I mean... {b}very{/b} good!"
    r "Petite mais douée ! Elle n'en a pas l'air mais elle est douée avec les jeux vidéos. {b}Vraiment{/b} douée !"

# game/dialogs.rpy:383
translate french scene3_28d5cd6d:

    # s "She's the prefecture's champion in a lot of games! {i}Lead of Fighter '96, Pika Rocket Online,{/i}... Even {i}Tetranet Online{/i}!"
    s "Elle est la championne préfécturale dans beaucoup de jeux ! {i}Lead of Fighter '96, Pika Rocket Online,{/i}... Même {i}Tetranet Online{/i} !"

# game/dialogs.rpy:384
translate french scene3_a50ea391:

    # hero "Cool!"
    hero "Cool !"

# game/dialogs.rpy:386
translate french scene3_a580dff0:

    # hero "Wait wait wait...{p}Isn't your username... NAMINAMI?"
    hero "Attends une minute...{p}Ton pseudo, ce serait pas... NAMINAMI ?"

# game/dialogs.rpy:388
translate french scene3_582ff77a:

    # n "Huh?! How did you know?"
    n "Comment tu le sais ?"

# game/dialogs.rpy:389
translate french scene3_8aa6fe25:

    # hero "I think we played {i}Tetranet Online{/i} last night!"
    hero "On a joué ensemble à {i}Tetranet Online{/i} hier soir, je crois !"

# game/dialogs.rpy:391
translate french scene3_88e0bb32:

    # n "Wait, you're that %(stringhero)s-dude I had a hard time beating?"
    n "C'est toi ce mec avec qui j'ai eu autant de mal à gagner ??"

# game/dialogs.rpy:392
translate french scene3_3052c7b9:

    # "{i}Dude{/i}?"
    "{i}Mec{/i} ?"

# game/dialogs.rpy:393
translate french scene3_0fd6ee48:

    # hero "Yeah that was me!"
    hero "Euhh... Oui, en effet."

# game/dialogs.rpy:394
translate french scene3_e6f7ec96:

    # hero "What a small world..."
    hero "Le monde est petit."

# game/dialogs.rpy:395
translate french scene3_a43e5a9b:

    # n "Let's meet up here after club activities. I challenge you to {i}Lead of Fighters '96{/i}!"
    n "Retrouvons nous ce soir ici, après les heures de club. Je te défie à {i}Lead of Fighters '96{/i} !"

# game/dialogs.rpy:396
translate french scene3_050d4a5f:

    # "Her eyes were sparkling, full of energy, like she was ready for a real challenge. It was pretty adorable."
    "Ses yeux brillaient de défi et d'énergie. Difficile de résister."

# game/dialogs.rpy:397
translate french scene3_6376ce6f:

    # "I'm sure I'll get my ass kicked again..."
    "Je vais sûrement me prendre une rouste encore une fois..."

# game/dialogs.rpy:398
translate french scene3_958d04dd:

    # "But whatever...She was really cute, so..."
    "Mais c'est pas grave. Elle est mignonne."

# game/dialogs.rpy:399
translate french scene3_65819df0:

    # hero "Challenge accepted!"
    hero "Défi accepté !"

# game/dialogs.rpy:403
translate french scene3_5de027f9:

    # n "So you're the dude Rika-nee was talking about... Nice to meet you!"
    n "Alors c'est toi le mec dont Rika-nee parlait... Enchantée !"

# game/dialogs.rpy:404
translate french scene3_3052c7b9_1:

    # "{i}Dude{/i}?"
    "{i}Mec{/i} ?"

# game/dialogs.rpy:405
translate french scene3_396612f8:

    # hero "Nice to meet you too."
    hero "Enchanté aussi."

# game/dialogs.rpy:406
translate french scene3_dd9f61de:

    # hero "Hehe, sorry to say this, but you really don't look like a video game champion."
    hero "Héhé, désolé de dire ça, mais tu fais vraiment pas championne de jeu vidéo."

# game/dialogs.rpy:407
translate french scene3_9458441a:

    # n "One of these days, I'll show you how skilled I am!"
    n "Je te montrerai un jour de quoi je suis capable !"

# game/dialogs.rpy:408
translate french scene3_54ab12a6:

    # "Her eyes were sparkling, full of energy. It was pretty adorable."
    "Ses yeux brillaient d'énergie. C'était mignon."

# game/dialogs.rpy:409
translate french scene3_40cb84ff:

    # hero "Sure, you're on anytime!"
    hero "Bien sûr, pourquoi pas !"

# game/dialogs.rpy:411
translate french scene3_bddf279b:

    # "Then I focused my attention on Rika."
    "Puis je m'attardai sur Rika."

# game/dialogs.rpy:412
translate french scene3_fff0f085:

    # "Now that I've seen her up close, I noticed that her eyes were unusual."
    "Maintenant que je la voyais en détail, je remarquai ses yeux peu communs."

# game/dialogs.rpy:413
translate french scene3_3429571b:

    # "One eye was blue and another was green."
    "Ils étaient vairons."

# game/dialogs.rpy:414
translate french scene3_14f143f3:

    # "The guy whose allowed to dive into those eyes will be one lucky guy."
    "Le gars qui aurait la chance de plonger dans son regard serait un homme chanceux."

# game/dialogs.rpy:415
translate french scene3_540f6b1e:

    # "But given her personality, is there any guy who will even get that chance?"
    "Mais est-ce que ça va arriver un jour, avec une personnalité pareille ?"

# game/dialogs.rpy:416
translate french scene3_1eaf8eb5:

    # hero "If you don't mind me saying, Rika-san,...you look like Domoco-chan."
    hero "Si je puis me permettre, Rika-san, tu ressembles beaucoup à Domoco-chan."

# game/dialogs.rpy:418
translate french scene3_24649718:

    # "Rika-san smiled. She looked really pretty with one."
    "Rika-san sourit, et ça lui allait bien."

# game/dialogs.rpy:419
translate french scene3_63ae1e58:

    # r "Yeah! I love cosplaying as her, she's one of my favorite anime characters!"
    r "Oui ! J'adore la cosplayer, c'est un de mes persos d'anime préférés !"

# game/dialogs.rpy:420
translate french scene3_e1b3e1b9:

    # r "You've seen the show?"
    r "T'as vu la série ?"

# game/dialogs.rpy:421
translate french scene3_dc1e9f8c:

    # hero "I love it. It's a really funny anime!"
    hero "J'adore. C'est vraiment un animé marrant !"

# game/dialogs.rpy:422
translate french scene3_7f21dac0:

    # s "Rika-chan can do a really good imitation of Domoco-chan!"
    s "Rika-chan peut faire une très bonne imitation de Domoco-chan !"

# game/dialogs.rpy:423
translate french scene3_7020cc49:

    # r "Watch this!{p}{i}Pipirupiru pépérato!{/i}{p}How is it?"
    r "Regarde ça !{p}{i}Pipirupiru pépérato !{/i}{p}Alors ?"

# game/dialogs.rpy:424
translate french scene3_30d87b21:

    # hero "That was pretty great! It looked just like the real thing!"
    hero "C'est super ! Comme la vraie !"

# game/dialogs.rpy:425
translate french scene3_87588078:

    # n "Very true!"
    n "C'est vrai !"

# game/dialogs.rpy:426
translate french scene3_e0982078:

    # r "I'm glad you liked it!"
    r "Contente que tu aimes !"

# game/dialogs.rpy:429
translate french scene3_ed04f436:

    # r "Now it's time for the question every member has to answer before they join the club!"
    r "Maintenant c'est l'heure de la question pour les nouveaux membres !"

# game/dialogs.rpy:430
translate french scene3_322a4cd3:

    # r "Go for it, Sakura!"
    r "Vas-y, Sakura !"

# game/dialogs.rpy:431
translate french scene3_5a20a3eb:

    # hero "What's with that face? It's creeping me out..."
    hero "Pourquoi cette tête ? Ça fait peur..."

# game/dialogs.rpy:432
translate french scene3_041b4444:

    # s "Don't worry, %(stringhero)s-san. It's not a hard question."
    s "Ne t'inquiète pas, %(stringhero)s-san. Ce n'est pas une question dure."

# game/dialogs.rpy:433
translate french scene3_9ae95b50:

    # s "Here it is:{p}What is your all-time favorite manga?"
    s "La voilà :{p}Quel est ton manga préféré ?"

# game/dialogs.rpy:434
translate french scene3_29f0a12c:

    # hero "My all-time favorite one? Hm..."
    hero "Mon préféré absolu ? Hmm..."

# game/dialogs.rpy:435
translate french scene3_2df7fab7:

    # "I started to think."
    "Je réfléchis."

# game/dialogs.rpy:436
translate french scene3_27a6f658:

    # "There are a few that I really like. I enjoy a lot of ecchi manga, but if I say that, the girls would look at me funny... Especially Rika-san."
    "Il y en a pas mal que j'aime bien. J'aime beaucoup les mangas ecchi, mais si je dis ça, elles vont me regarder bizarrement... Surtout Rika..."

# game/dialogs.rpy:437
translate french scene3_6275c604:

    # "But it's always good to be honest with girls as well..."
    "Mais c'est aussi mieux d'être honnête avec elles..."

# game/dialogs.rpy:441
translate french scene3_152e1c76:

    # "Let's tell the truth..."
    "Disons la vérité..."

# game/dialogs.rpy:442
translate french scene3_f2942a1c:

    # "After all, maybe they don't know about this one..."
    "Après tout, peut-être qu'elles ne connaissent pas celui-là..."

# game/dialogs.rpy:443
translate french scene3_923bf6bc:

    # hero "I love {i}High School Samurai{/i}! That's my favorite one!"
    hero "J'adore {i}High School Samurai{/i} ! Il est vraiment bien !"

# game/dialogs.rpy:448
translate french scene3_459d8d4d:

    # "They started to look funny, as I expected."
    "Elles me regardèrent d'un drôle d'air."

# game/dialogs.rpy:449
translate french scene3_ea8e9dca:

    # "Rika-san looked pissed off again and Sakura-chan was blushing red."
    "Rika prit un air énervé et Sakura se mit à rougir."

# game/dialogs.rpy:450
translate french scene3_10363d18:

    # "Nanami was about to burst into tears of laughter."
    "Nanami était au bord du fou rire."

# game/dialogs.rpy:451
translate french scene3_cebb2b3d:

    # "Aw crap, maybe I made the wrong choice..."
    "Oups, J'aurais peut-être pas dû répondre ça..."

# game/dialogs.rpy:452
translate french scene3_d83f0622:

    # r "You damn pervert! You're just like every other guy!"
    r "Ha ! T'es vraiment un homme. Un vrai obsédé !"

# game/dialogs.rpy:453
translate french scene3_244ff0c7:

    # n "Hahaha! You sure have guts, dude! Confessing something like that in front of Rika-nee!"
    n "Haha ! T'en as dans le ventre pour révéler un truc pareil, mec !!"

# game/dialogs.rpy:454
translate french scene3_e1554201:

    # s "..."
    s "..."

# game/dialogs.rpy:456
translate french scene3_e08d3442:

    # "I was hanging my head in shame, but suddenly Sakura-san spoke."
    "Je commençai à rougir de honte, mais Sakura prit soudainement la parole."

# game/dialogs.rpy:457
translate french scene3_76909f2d:

    # s "Actually, Rika-chan..."
    s "En fait, Rika-chan..."

# game/dialogs.rpy:458
translate french scene3_4570740d:

    # s "{i}High School Samurai{/i} isn't that bad at all."
    s "{i}High School Samurai{/i} n'est pas si terrible."

# game/dialogs.rpy:459
translate french scene3_6bb1ac2f:

    # s "You see, the hero isn't a typical boy like most harem manga. He's actually very heroic!"
    s "Le héros n'est pas le héros typique des mangas harem. Il ressemble plus à un vrai héros."

# game/dialogs.rpy:460
translate french scene3_b2661f33:

    # s "He has a great sense of honor and all the monologues he gives are very righteous and noble."
    s "Il a un très grand sens de l'honneur et ses monologues sont justes et épiques."

# game/dialogs.rpy:461
translate french scene3_8effa9e6:

    # s "I admire heroes like that."
    s "J'admire ce genre de héros."

# game/dialogs.rpy:462
translate french scene3_f902c5fb:

    # s "And by the way, the hero himself isn't really a pervert, since every ecchi scene where he's involved happens accidentally."
    s "De plus, il n'est pas réellement pervers, car toutes les scènes ecchi lui arrivent dessus par accident."

# game/dialogs.rpy:464
translate french scene3_0ddbdce1:

    # r "Really?"
    r "Vraiment ?"

# game/dialogs.rpy:465
translate french scene3_68f8b283:

    # n "I've seen some episodes. That guy reminds me of {i}Nanda no Ryu{/i}, who's always resolving conflicts with great punchlines and fighting when necessary."
    n "J'ai vu quelques épisodes. Le gars me rappelle {i}Nanda no Ryu{/i} où le héros résoud des conflits avec des bonnes punchlines et du combat."

# game/dialogs.rpy:466
translate french scene3_ced41922:

    # n "Sure, it's ecchi sometimes, but it's actually a nice show to watch."
    n "C'est parfois ecchi mais c'est fun à regarder."

# game/dialogs.rpy:467
translate french scene3_9816aa54:

    # r "Well... I guess it's okay, then..."
    r "Bon, ça va, j'imagine."

# game/dialogs.rpy:471
translate french scene3_eca282df:

    # "Let's not take any risks..."
    "Ne prenons pas de risques..."

# game/dialogs.rpy:472
translate french scene3_6ba925d2:

    # hero "I love {i}Rosario Maiden{/i}. It's a great manga!"
    hero "J'adore {i}Rosario Maiden{/i}. C'est un bon manga !"

# game/dialogs.rpy:477
translate french scene3_21a5901b:

    # "The three girls stared at me."
    "Les deux filles me fixèrent."

# game/dialogs.rpy:478
translate french scene3_7f0ae732:

    # "Finally, they all smiled. Rika-san spoke."
    "Finalement, Rika-san prit la parole."

# game/dialogs.rpy:483
translate french scene3_79487764:

    # r "I love that one too!"
    r "J'aime beaucoup celui-là aussi."

# game/dialogs.rpy:484
translate french scene3_ca38b9e6:

    # r "The art style is superb and the story is great. It's a pretty great series."
    r "Les dessins sont superbes, l'histoire est poignante... C'est un bon choix."

# game/dialogs.rpy:485
translate french scene3_c24a4394:

    # s "I like it too."
    s "J'aime bien aussi."

# game/dialogs.rpy:486
translate french scene3_3d29180a:

    # s "It's not my favorite but I like it."
    s "It's not my favorite but I like it."

# game/dialogs.rpy:487
translate french scene3_1324b4b2:

    # s "I especially love the dolls. I wish I could have one like that to take home!"
    s "J'aime surtout les poupées, j'aimerais bien en avoir une comme celles de ce manga !"

# game/dialogs.rpy:488
translate french scene3_29de2f7c:

    # hero "Yeah! They're really cute! Especially the green one!"
    hero "Ouais, elles sont mimi ! Surtout la verte !"

# game/dialogs.rpy:489
translate french scene3_42f3f6f5:

    # s "The green one is kinda mean but it's still my favorite one!"
    s "La verte est pas très gentille parfois, mais c'est ma préférée !"

# game/dialogs.rpy:490
translate french scene3_bf59c037:

    # n "I'm not too into the dolls. I prefer the protagonist. He's so handsome!"
    n "Les poupées ne me disent trop rien. Je préfère le protagoniste. Il est trop mignon !"

# game/dialogs.rpy:491
translate french scene3_f7e6d2b6:

    # r "I prefer the red doll. She's just so... dominant... hehe! {image=heart.png}"
    r "Je préfère la poupée rouge. Elle a un côté...dominant... héhé ! {image=heart.png}"

# game/dialogs.rpy:492
translate french scene3_40c59aa9:

    # "Rika-san gave me an evil look. What the heck could she be thinking? I'm confused and a bit scared."
    "Rika-san me fit un clin d'oeil. Je me demande ce qu'elle pense..."

# game/dialogs.rpy:493
translate french scene3_745f17e5:

    # "But it looks like she's starting to like me."
    "Mais on dirait qu'elle commence à m'apprécier."

# game/dialogs.rpy:494
translate french scene3_d79ab191:

    # "Well, at least I hope so..."
    "Enfin, je l'espère..."

# game/dialogs.rpy:502
translate french scene3_66851b4f:

    # "It was almost time to head home."
    "Le soir vint et ce fut l'heure de rentrer chez nous."

# game/dialogs.rpy:504
translate french scene3_a7a81522:

    # "Rika-chan and Sakura-chan left to head home. I ended up staying with Nanami-chan, ready to challenge her."
    "Rika et Sakura partirent de l'école. Moi et Nanami restions pour le défi promis."

# game/dialogs.rpy:509
translate french scene3_e5863c5e:

    # n "So, are you ready, dude?"
    n "Alors, t'es prêt, mec ?"

# game/dialogs.rpy:510
translate french scene3_30e27e13:

    # hero "Of course I am. I was born ready!"
    hero "Bien sûr ! Je suis né prêt !"

# game/dialogs.rpy:511
translate french scene3_fa127982:

    # n "I meant, ready to get your butt kicked?"
    n "J'veux dire, prêt à te prendre la pâtée du siècle ?"

# game/dialogs.rpy:512
translate french scene3_b87b3a25:

    # hero "Only if you're ready to get yours kicked first!"
    hero "Seulement si t'es prête à prendre la tienne !"

# game/dialogs.rpy:513
translate french scene3_7cf5de36:

    # "I pretty much know she's going to win the game. But I'm not gonna let win without a challenge. Although, she'll still call me a {i}dude{/i} afterwards."
    "Je suis pratiquement sûr qu'elle va gagner à chaque fois. Mais je ne vais pas me laisser battre si facilement en me faisant appeler {i}mec{/i}."

# game/dialogs.rpy:514
translate french scene3_c3b34a29:

    # "I gotta defend my pride, eh?"
    "Il faut bien défendre sa fierté, non ?"

# game/dialogs.rpy:518
translate french scene3_d0713ddb:

    # "She turned on the TV and the NATO GEO after inserting Lead of Fighters '96 cartridge."
    "Elle alluma la télé et la NATO GEO après y avoir mis la cartouche de {i}Lead of Fighters '96{/i}."

# game/dialogs.rpy:519
translate french scene3_3716d683:

    # hero "It's pretty amazing the club has this gaming console here! That's one of the most expensive systems available!"
    hero "C'est dingue que vous ayez cette console ici ! Ce bijou est une des plus chères du marché !"

# game/dialogs.rpy:520
translate french scene3_8ed428db:

    # n "Actually, it's mine. I lent to the club to use."
    n "Elle est à moi. J'en ai fait prêt au club."

# game/dialogs.rpy:521
translate french scene3_8a31c669:

    # n "Are you familiar with the {i}Lead of Fighters{/i} series?"
    n "Tu connais la série des {i}Lead of Fighters{/i} ?"

# game/dialogs.rpy:522
translate french scene3_165263c8:

    # hero "Yeah, actually this game is my favorite one so far."
    hero "En fait, cet opus est mon préféré jusqu'à maintenant."

# game/dialogs.rpy:523
translate french scene3_5ff36a65:

    # hero "It's my favorite game too, but I could only play it in the arcades in Tokyo."
    hero "C'est même mon jeu préféré tout court, mais je ne pouvais y jouer que sur arcade à Tokyo."

# game/dialogs.rpy:524
translate french scene3_facc5c5d:

    # n "I see."
    n "Je vois."

# game/dialogs.rpy:525
translate french scene3_9be30a14:

    # n "So, who's your favorite character?"
    n "C'est lequel ton perso préféré ?"

# game/dialogs.rpy:526
translate french scene3_913605a7:

    # n "I usually like to pick the cute girls."
    n "D'habitude, je prends les filles mignonnes."

# game/dialogs.rpy:527
translate french scene3_3cba88d7:

    # n "In most fighting games they rarely look threating, but when you know how to play with them, they can be deadly!"
    n "Dans les jeux de baston, elle sont rarement une menace, mais elles peuvent être mortelles quand on les maîtrise !"

# game/dialogs.rpy:528
translate french scene3_978b1237:

    # hero "I'm not sure what my favorite is... I usually pick randomly."
    hero "Je sais pas trop... Moi je prends au feeling."

# game/dialogs.rpy:529
translate french scene3_7e18c9db:

    # n "If you're a beginner, you should take this guy, here. {w}He's good for be-{nw}"
    n "Si tu débutes, prends ce gars, là. {w}Il est bien pour les déb-{nw}"

# game/dialogs.rpy:530
translate french scene3_0d60b236:

    # hero "Hey I'm not a beginner! I was one of the best at my previous school!"
    hero "Hé, je suis pas un débutant ! J'étais un des meilleurs de ma classe à ce jeu !"

# game/dialogs.rpy:531
translate french scene3_72095650:

    # n "Oh yeah? Let's see about that!"
    n "C'est ce qu'on va voir !"

# game/dialogs.rpy:532
translate french scene3_704fb29f:

    # "So we selected our fighters and started the virtual brawl."
    "On prit nos persos et on a commencé la lutte virtuelle."

# game/dialogs.rpy:533
translate french scene3_c8bd5427:

    # n "So you're from Tokyo?"
    n "Tu viens de Tokyo, alors ?"

# game/dialogs.rpy:534
translate french scene3_6e3125fc:

    # hero "Yeah. I moved here because my parents wanted to open a store in a quiet, peaceful village."
    hero "Ouais. On a déménagé parce que mes parents voulaient avoir leur commerce dans un petit village tranquille.."

# game/dialogs.rpy:535
translate french scene3_352bd956:

    # n "A store? You mean...the grocery store at the G. street?"
    n "Commerce ? Tu veux dire... Ce serait pas le magasin sur la rue G. ?"

# game/dialogs.rpy:536
translate french scene3_cf6c4656:

    # hero "How do you know?"
    hero "Comment t'as deviné ?"

# game/dialogs.rpy:537
translate french scene3_3b26e73f:

    # n "I... heard it was looking for new owners."
    n "J'ai...entendu dire qu'ils cherchaient des nouveaux proprios."

# game/dialogs.rpy:538
translate french scene3_9bf86260:

    # n "My brother worked there for a while, back then."
    n "Mon grand frère y travaillait autrefois."

# game/dialogs.rpy:540
translate french scene3_28e4c374:

    # "{i}Bing, poof, kick, bzzzz\nK O !!!{/i}"
    "{i}Bing, poof, kick, bzzzz\nK O !!!{/i}"

# game/dialogs.rpy:541
translate french scene3_cd3c8571:

    # "Of course, she beat the crap out of me."
    "Bien sûr elle m'a vaincu."

# game/dialogs.rpy:542
translate french scene3_7944ba15:

    # "I'm not sure if I lost because of her skills or because we're talking at the same time.{p}It might be both..."
    "Je ne savais pas trop si c'était parce qu'elle était bonne ou bien parce qu'on discutait en même temps.{p}Ou les deux..."

# game/dialogs.rpy:543
translate french scene3_66644a5e:

    # "We chose different fighters and started a new fight."
    "On refit un match avec différent persos."

# game/dialogs.rpy:544
translate french scene3_b7629213:

    # hero "So you have a brother?"
    hero "Tu as un frère alors ?"

# game/dialogs.rpy:545
translate french scene3_c1b9564a:

    # n "Yep, an older brother.{p}He's 27."
    n "Oui, un grand frère de 27 ans."

# game/dialogs.rpy:546
translate french scene3_6b9e255a:

    # hero "Is he any good at games?"
    hero "Il est doué en jeu vidéo, lui aussi ?"

# game/dialogs.rpy:547
translate french scene3_25066c43:

    # n "Not really."
    n "Pas vraiment."

# game/dialogs.rpy:548
translate french scene3_456c3557:

    # n "I don't think it's from his generation."
    n "Je crois pas que ce soit sa tasse de thé."

# game/dialogs.rpy:549
translate french scene3_5f66a4a4:

    # hero "That's possible. My sister is almost as old as him and it's not her thing either."
    hero "C'est possible. Ma grande sœur a le même âge et ce n'est pas son truc non plus."

# game/dialogs.rpy:550
translate french scene3_a2e58feb:

    # n "Oh yeah, your sister... What's she doing?"
    n "Ah oui, ta sœur... Elle travaille dans quoi ?"

# game/dialogs.rpy:551
translate french scene3_e1b6d146:

    # hero "She works as a banker."
    hero "Elle est banquière."

# game/dialogs.rpy:552
translate french scene3_30b55ef7:

    # n "That sounds boring."
    n "Ça a l'air d'un ennui, comme taf..."

# game/dialogs.rpy:553
translate french scene3_a5630de6:

    # hero "I know right! I don't understand how she can do something like that without getting bored."
    hero "C'est clair ! Je comprends pas comment elle fait pour pas s'ennuyer."

# game/dialogs.rpy:555
translate french scene3_28e4c374_1:

    # "{i}Bing, poof, kick, bzzzz\nK O !!!{/i}"
    "{i}Bing, poof, kick, bzzzz\nK O !!!{/i}"

# game/dialogs.rpy:556
translate french scene3_821cdbe8:

    # "And yet again, I lost."
    "Encore perdu."

# game/dialogs.rpy:557
translate french scene3_b0f56878:

    # hero "Alright. No more Mister Nice Guy, I'm gonna fight for real!"
    hero "OK, fini de jouer au gentil \"mec\" ! Là, j'y vais à fond !"

# game/dialogs.rpy:558
translate french scene3_7a9ca5b7:

    # n "It's about time..."
    n "Il était temps..."

# game/dialogs.rpy:559
translate french scene3_9f960747:

    # "Nanami gave me a mischievous grin.{p}That looked cute but kinda creepy too."
    "Nanami me lança un sourire malsain.{p}C'était mignon mais un peu flippant aussi."

# game/dialogs.rpy:560
translate french scene3_e6598e64:

    # "She probably knew I was already giving it my all..."
    "Elle savait probablement que j'étais déjà à fond..."

# game/dialogs.rpy:561
translate french scene3_ac835df6:

    # "We played through the next round without a single word."
    "On fit le combat suivant sans un mot."

# game/dialogs.rpy:562
translate french scene3_a3279e7e:

    # "I was finally was gaining an advantage! She stayed less and less calm as I was slowly winning."
    "Finalement, je réussis à prendre l'avantage. Elle perdit de plus en plus son calme à mesure que je gagnais."

# game/dialogs.rpy:563
translate french scene3_289633fa:

    # "And then finally, using a secret combo I knew well..."
    "Et finalement, avec un combo dont j'avais le secret..."

# game/dialogs.rpy:565
translate french scene3_28e4c374_2:

    # "{i}Bing, poof, kick, bzzzz\nK O !!!{/i}"
    "{i}Bing, poof, kick, bzzzz\nK O !!!{/i}"

# game/dialogs.rpy:566
translate french scene3_ed61300c:

    # "Nanami stared at me with a sad face and began to cry like a small child."
    "Nanami me regarda avec un visage triste et elle commença à pleurnicher."

# game/dialogs.rpy:567
translate french scene3_15a85014:

    # "It didn't look real at all though. There were no tears or anything, she was just moaning like a baby."
    "Ce n'était pas de la vraie tristesse. Je voyais bien qu'il n'y avait pas de larmes et qu'elle chouinait juste comme un bébé."

# game/dialogs.rpy:568
translate french scene3_decd73c2:

    # hero "Hey hey, calm down! I'm sorry... I'll let you win next time!"
    hero "Hé, hé pleure pas, pardonne-moi !... Je te laisserai gagner le prochain match !"

# game/dialogs.rpy:569
translate french scene3_ab688811:

    # "She instantly stopped crying and made her cute/creepy grin."
    "Elle s'arrêta instantanément et me refit son sourire mignon/flippant."

# game/dialogs.rpy:570
translate french scene3_c6891b22:

    # n "Works everytime! {image=heart.png}"
    n "Ça marche à chaque fois ! {image=heart.png}"

# game/dialogs.rpy:571
translate french scene3_56b514db:

    # hero "Oh man..."
    hero "Oh toi !"

# game/dialogs.rpy:572
translate french scene3_2d2950d5:

    # "I started laughing."
    "Mais au lieu de me fâcher, je me suis mis à rire."

# game/dialogs.rpy:573
translate french scene3_f2d7328e:

    # "Nanami made a goofy pouting face which made me laugh even more."
    "Nanami me fit une tête boudeuse qui ne fit qu'augmenter mon rire."

# game/dialogs.rpy:574
translate french scene3_05ab44b2:

    # n "Let's fight again!"
    n "On s'en refait une !"

# game/dialogs.rpy:575
translate french scene3_241aaaf7:

    # hero "Alright, alright, little one!"
    hero "Ok, ok, ma p'tite !"

# game/dialogs.rpy:576
translate french scene3_52c6a554:

    # n "I'm not little!"
    n "Je suis pas petite !"

# game/dialogs.rpy:581
translate french scene3_176ddd63:

    # "We played for a while, but eventually we both needed to get home."
    "On a continué à jouer un peu, puis nous sommes sortis de l'école."

# game/dialogs.rpy:582
translate french scene3_ce50b0ab:

    # n "That was fun! I hope we can do this again soon!"
    n "C'était marrant ! Faudra se refaire ça vite !"

# game/dialogs.rpy:583
translate french scene3_24edf3d9:

    # hero "Anytime, Nanami-chan."
    hero "Quand tu voudras, Nanami-chan."

# game/dialogs.rpy:585
translate french scene3_7074277a:

    # n "Oh by the way, here's my e-mail."
    n "Au fait, prends mon e-mail."

# game/dialogs.rpy:586
translate french scene3_cb953bdb:

    # "She gave me a tiny piece of paper with an e-mail written on it. There was a small heart where the @ sign was supposed to be."
    "Elle me donna un petit papier où était griffonné une adresse e-mail avec un cœur là ou il devait y avoir le @."

# game/dialogs.rpy:587
translate french scene3_a99fbe92:

    # "I blushed a bit."
    "Je rougis un peu."

# game/dialogs.rpy:588
translate french scene3_9f566895:

    # n "It's my personal e-mail... Just in case..."
    n "C'est mon e-mail personnel... Au cas où..."

# game/dialogs.rpy:589
translate french scene3_712996f5:

    # hero "Thanks, Nanami-chan."
    hero "Merci, Nanami-chan."

# game/dialogs.rpy:592
translate french scene3_05df8eb4:

    # n "See you tomorrow, %(stringhero)s-senpai! {image=heart.png}"
    n "A demain, %(stringhero)s-senpai ! {image=heart.png}"

# game/dialogs.rpy:595
translate french scene3_759e6781:

    # "She left the school, hopping happily like a kid on the street."
    "Elle quitta l'école en sautillant comme une gamine dans la rue."

# game/dialogs.rpy:596
translate french scene3_37a9b710:

    # "Dang, for a gaming champion...she's really something."
    "Elle est vraiment mimi pour une championne..."

# game/dialogs.rpy:599
translate french scene3_68c44298:

    # "The sun was starting to set."
    "Le soir tombait."

# game/dialogs.rpy:600
translate french scene3_6f9822aa:

    # "Rika-chan and Nanami-chan take different routes to get home, so only Sakura-chan and I ended up walking home together."
    "Rika et Nanami prenaient un chemin différent de nous, alors Sakura et moi firent le chemin ensemble."

# game/dialogs.rpy:606
translate french scene3_563d938f:

    # s "So you like seinen manga?"
    s "Alors tu aimes les mangas seinen ?"

# game/dialogs.rpy:607
translate french scene3_04f08300:

    # hero "Well yes..."
    hero "Moui..."

# game/dialogs.rpy:608
translate french scene3_14e7e4b7:

    # hero "On the same subject, you said {i}Rosario Maiden{/i} wasn't your favorite. Which one is your favorite, then?"
    hero "En parlant de ça, tu disais que {i}Rosario Maiden{/i} n'était pas ton préféré. C'est lequel que tu aimes ?"

# game/dialogs.rpy:609
translate french scene3_41af6c01:

    # s "In the seinen type? It's {i}Desu Motto{/i}."
    s "Dans le genre seinen ? {i}Desu Motto{/i}."

# game/dialogs.rpy:610
translate french scene3_a0282b9a:

    # hero "You like {i}Desu Motto{/i}?"
    hero "Tu aimes {i}Desu Motto{/i} ?"

# game/dialogs.rpy:611
translate french scene3_157e9fd5:

    # "I was kinda surprised."
    "J'étais surpris."

# game/dialogs.rpy:612
translate french scene3_3593807d:

    # "{i}Desu Motto{/i} is a story about a living book that eats human guts. It's full of all kinds of blood and gore, more so than in {i}Nanda no Ryu{/i}."
    "{i}Desu Motto{/i} est l'histoire d'un livre hanté qui mange les humains dans de grandes explosions de sang, plus grandes que dans {i}Nanda no Ryu{/i}."

# game/dialogs.rpy:613
translate french scene3_976cd68f:

    # "And there's a magical girl who pursues this book with a chainsaw in order to destroy it."
    "Et il y a cette magical girl qui veut détruire le bouquin avec une tronçonneuse."

# game/dialogs.rpy:614
translate french scene3_e7ee8e3e:

    # "Anyway, it's not the kind of thing girls her age would watch and like."
    "Bref, pas le genre de truc qu'une fille comme elle pourrait aimer lire."

# game/dialogs.rpy:615
translate french scene3_2ae24650:

    # "Appearances can be surprising, sometimes..."
    "Les apparences sont bien trompeuses, parfois..."

# game/dialogs.rpy:616
translate french scene3_28bb09a7:

    # hero "I like it too, but there's too much gore for me."
    hero "J'aime bien aussi, mais c'est un peu trop gore pour moi."

# game/dialogs.rpy:617
translate french scene3_ed0c1175:

    # hero "I can't stand too much blood in an anime. I remember being traumatized by {i}Elfic Leaves{/i}..."
    hero "Je supporte pas quand le coté gore est trop éxagéré. Je me souviens avoir été traumatisé par {i}Elfic Leaves{/i}..."

# game/dialogs.rpy:618
translate french scene3_58b557f4:

    # "{i}Elfic Leaves{/i} is known to be the most violent and brutal anime in all of japanimation history. It's so gory, it was never broadcast on TV and it only exists on VHS tapes."
    "{i}Elfic Leaves{/i} était connu pour être l'anime le plus violent et gore de l'histoire de la japanimation. A tel point qu'il n'est sorti qu'en VHS, jamais à la télé."

# game/dialogs.rpy:619
translate french scene3_64b818b7:

    # "I remember watching it by accident, about four years ago. It was on a VHS tape in my sister's room..."
    "J'avais vu cet animé par accident en empruntant une des cassettes de ma sœur, il y a quatre ans..."

# game/dialogs.rpy:621
translate french scene3_74bc980e:

    # s "I can understand that. That one is really hardcore."
    s "Je comprends ça. Celui-là est vraiment hardcore."

# game/dialogs.rpy:622
translate french scene3_fe827394:

    # s "Though I think it's okay, since they're only drawings. I don't think I could stand it either if it was real."
    s "Quoique ce ne sont que des dessins. Je crois que je ne supporterais pas si c'était en vrai."

# game/dialogs.rpy:623
translate french scene3_189e996b:

    # hero "Same for me."
    hero "Pareil."

# game/dialogs.rpy:625
translate french scene3_1f00d77e:

    # "Sakura stared at me for a moment."
    "Sakura me regarda intensément un moment."

# game/dialogs.rpy:626
translate french scene3_24848926:

    # "It's like she was wondering about something... Or that she came a conclusion about something..."
    "Comme si elle se demandait quelque chose... Ou qu'elle devinait quelque chose..."

# game/dialogs.rpy:627
translate french scene3_47159e79:

    # s "...{w}{i}Rosario Maiden{/i} isn't actually favorite manga ever, right?"
    s "...{w}{i}Rosario Maiden{/i} n'est pas ton préféré non plus, n'est-ce pas ?"

# game/dialogs.rpy:628
translate french scene3_ef3bce8f:

    # "Gah!!"
    "Ack !!"

# game/dialogs.rpy:629
translate french scene3_bb685cb0:

    # "How'd she make an incredible guess like that?!"
    "Cette fille est vraiment doué aux devinettes !"

# game/dialogs.rpy:630
translate french scene3_e633313f:

    # "I guess I couldn't hide it from her anymore..."
    "Je supposai que je ne pouvais plus le cacher, maintenant..."

# game/dialogs.rpy:631
translate french scene3_ae8949a7:

    # hero "You got me..."
    hero "D'accord, tu m'as eu..."

# game/dialogs.rpy:632
translate french scene3_8137fe91:

    # hero "My favorite is {i}High School Samurai{/i}..."
    hero "Mon préféré, c'est {i}High School Samurai{/i}..."

# game/dialogs.rpy:634
translate french scene3_5c82e516:

    # s "Really?"
    s "Vraiment ?"

# game/dialogs.rpy:637
translate french scene3_d2de1953:

    # s "So you like {i}High School Samurai{/i}?"
    s "Alors tu aimes {i}High School Samurai{/i} ?"

# game/dialogs.rpy:638
translate french scene3_3d802b0c:

    # "I turned my head away from her a bit."
    "Je rougis et me détournai un peu."

# game/dialogs.rpy:639
translate french scene3_6449a5e4:

    # hero "Thanks again for saving me back there... Looks like Rika-san doesn't like that kind of manga and anime."
    hero "Merci pour tout à l'heure... On dirait que Rika-san n'aime pas trop ce genre de manga."

# game/dialogs.rpy:641
translate french scene3_bd58aae1:

    # s "It's okay. Rika-chan probably already forgot about it."
    s "C'est rien. Rika-chan a probablement déjà oublié."

# game/dialogs.rpy:642
translate french scene3_5798ec89:

    # s "And to be honest...{p}I really enjoy {i}High School Samurai{/i}!"
    s "Et pour être honnête...{p}j'aime beaucoup {i}High School Samurai{/i} aussi !"

# game/dialogs.rpy:643
translate french scene3_b3ba8ed1:

    # hero "You do?!"
    hero "Vraiment ?"

# game/dialogs.rpy:644
translate french scene3_d207e06f:

    # s "Yes!"
    s "Oui !"

# game/dialogs.rpy:647
translate french scene3_2098e30f:

    # s "It's a great manga... The hero is really handsome."
    s "C'est un grand manga... Le héros est vraiment bien."

# game/dialogs.rpy:648
translate french scene3_2f77db13:

    # s "And... the girls are very cute, too..."
    s "Et...les filles sont jolies, aussi..."

# game/dialogs.rpy:649
translate french scene3_1ae45319:

    # hero "Ah?"
    hero "Ah ?"

# game/dialogs.rpy:650
translate french scene3_22eeaf4a:

    # s "Yes, especially the main one. I wish I could look like her someday..."
    s "Oui, surtout la principale. J'aimerais bien être aussi belle..."

# game/dialogs.rpy:651
translate french scene3_1171cfe9:

    # hero "You're already pretty cute though, Sakura-san."
    hero "Tu es aussi belle qu'elle, je trouve, Sakura-san."

# game/dialogs.rpy:652
translate french scene3_aa49989e:

    # "I said that without thinking."
    "J'ai lancé ça sans vraiment m'en rendre compte."

# game/dialogs.rpy:653
translate french scene3_f463b1f2:

    # "I was on the verge of taking my words back until she replied."
    "J'allais reprendre mes mots mais elle répondit."

# game/dialogs.rpy:655
translate french scene3_1c22c637:

    # s "...Do you really think so?"
    s "...Tu... Tu le penses vraiment ?"

# game/dialogs.rpy:656
translate french scene3_b836a32c:

    # "Her face turned red out of embarrassment."
    "Elle rougissait d'embarassement."

# game/dialogs.rpy:657
translate french scene3_87546ecc:

    # "So cute!"
    "Adorable !"

# game/dialogs.rpy:658
translate french scene3_4baf7e22:

    # "It was so cute that I felt a little embarrassed too."
    "A tel point que je me mis à rougir aussi."

# game/dialogs.rpy:659
translate french scene3_1c0b1c3e:

    # hero "Well y-yeah... You're a pretty girl, Sakura-san..."
    hero "Ben, oui... Tu es vraiment une jolie fille, Sakura-san..."

# game/dialogs.rpy:660
translate french scene3_557b9c7c:

    # s ". . ."
    s ". . ."

# game/dialogs.rpy:661
translate french scene3_5d8f36c3:

    # s "Thank you... %(stringhero)s-kun..."
    s "M-Merci,... %(stringhero)s-kun..."

# game/dialogs.rpy:662
translate french scene3_2758fdb5:

    # "She hesitated for awhile before thanking me."
    "Elle a eu une longue hésitation avant de me remercier."

# game/dialogs.rpy:663
translate french scene3_d3083c53:

    # "Was that her being timid? Probably."
    "Sa timidité ? Probablement."

# game/dialogs.rpy:668
translate french scene3_88d97835:

    # "We finally reached the crossroads of our respective houses."
    "On arriva finalement au carrefour qui séparait nos maisons respectives."

# game/dialogs.rpy:669
translate french scene3_95f36d4e:

    # hero "Sakura-chan, since I don't know the path to school very well, do you mind if we go to school together every day?"
    hero "Sakura-chan, vu que je ne connais pas bien le chemin de l'école pour l'instant, ça te dérange si on fait le chemin de l'école ensemble tous les jours ?"

# game/dialogs.rpy:670
translate french scene3_c1d7d0f9:

    # s "Sure! I'll wait for you here tomorrow."
    s "Pas du tout ! Je t'attendrai ici demain."

# game/dialogs.rpy:671
translate french scene3_139aa776:

    # hero "Alright, thank you, Sakura-chan!"
    hero "Okay, merci, Sakura-chan !"

# game/dialogs.rpy:672
translate french scene3_3257668c:

    # s "You're welcome..."
    s "De rien..."

# game/dialogs.rpy:674
translate french scene3_00bab8bf:

    # s "See you tomorrow, %(stringhero)s-kun!"
    s "A demain, %(stringhero)s-kun !"

# game/dialogs.rpy:675
translate french scene3_6215ba95:

    # hero "See you!"
    hero "A demain !"

# game/dialogs.rpy:678
translate french scene3_a46fbd6c:

    # "I watched her walk down the street."
    "Je la regardai partir dans la rue."

# game/dialogs.rpy:679
translate french scene3_5e5c6211:

    # "Her hips were swaying a little with each step, her hair moving with the summer wind."
    "Son derrière remuait un petit peu à chaque pas, ses cheveux flottant à la brise d'été."

# game/dialogs.rpy:681
translate french scene3_862f5cfa:

    # "The cicadas were crying, giving an atmosphere of something dreamy and peaceful with the hot summer weather."
    "Les cigales chantaient, ce qui donnait une atmosphère paisible avec cette chaleur estivale."

# game/dialogs.rpy:684
translate french scene3_82417432:

    # "Before going to bed, I decided to get a breath of fresh air in front of the house."
    "Avant d'aller au lit, je décidai de prendre un peu l'air de la nuit devant la maison."

# game/dialogs.rpy:685
translate french scene3_be4997dc:

    # "I must admit, this place is beautiful."
    "Cet endroit est magnifique, je le reconnais."

# game/dialogs.rpy:686
translate french scene3_ba75a6ad:

    # "For sure, finding everything you want isn't as convenient as in Tokyo..."
    "C'est sûr, certaines choses seront moins pratiques qu'à Tokyo..."

# game/dialogs.rpy:687
translate french scene3_d9ac4ff7:

    # "But gee,... It's so good to breath real air instead of smog..."
    "Mais c'est tellement bon de sentir de l'air et pas du smog..."

# game/dialogs.rpy:688
translate french scene3_8534ca42:

    # ". . ."
    ". . ."

# game/dialogs.rpy:691
translate french scene3_f6793b5b:

    # "What's that?..."
    "Qu'est-ce que c'est ?..."

# game/dialogs.rpy:692
translate french scene3_41539cb7:

    # "Sounds like someone is playing the violin."
    "On dirait que quelqu'un joue du violon, pas loin d'ici."

# game/dialogs.rpy:693
translate french scene3_f3b91659:

    # "It's kinda appropriate for a night like this..."
    "Avec une nuit pareille, c'est le moment idéal pour en jouer..."

# game/dialogs.rpy:694
translate french scene3_8534ca42_1:

    # ". . ."
    ". . ."

# game/dialogs.rpy:695
translate french scene3_f07946fa:

    # "I think I'm starting enjoying my new life here."
    "Je crois que je commençai enfin à apprécier ma nouvelle vie."

# game/dialogs.rpy:719
translate french scene4_840498c7:

    # centered "{size=+35}CHAPTER 2\nA fine date{fast}{/size}"
    centered "{size=+35}CHAPITRE 2\nUn petit rencart{fast}{/size}"

# game/dialogs.rpy:730
translate french scene4_46a27380:

    # write "Dear sister."
    write "Chère grande sœur."

# game/dialogs.rpy:732
translate french scene4_d033b41a:

    # write "How are things?{w} Mom, dad, and I have arrived at our new home.{w} Things here are way different than in Tokyo. It's peaceful, but there's nothing to do in the village.{w} You need to take the train to find something to do in the nearest town."
    write "Comment vas-tu ?{w} Moi et mes parents sommes bien arrivés dans notre nouvelle maison.{w} Tout est si différent de Tokyo. C'est paisible et il ne s'y passe jamais rien.{w} Il faut prendre le train pour arriver aux magasins spéciaux et le reste, dans la grande ville."

# game/dialogs.rpy:734
translate french scene4_dcc0ac1c:

    # write "I just came back from my first day at my new school. I've already made three friends!"
    write "Je viens de revenir de mon premier jour d'école. Je m'y suis fait trois amies."

# game/dialogs.rpy:736
translate french scene4_9b62289a:

    # write "Things have changed a lot, but I think I'll get used to it soon enough."
    write "Les choses ont bien changé, mais je pense m'y habituer bientôt."

# game/dialogs.rpy:737
translate french scene4_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:739
translate french scene4_66476017:

    # write "We're all part of a manga club at school.{w} Since they're all girls, can you recommend any shoujo manga that I can read and talk with them about?"
    write "Mes trois amies et moi-même sommes dans un club manga à l'école.{w} Vu que ce sont des filles, est-ce que tu as des idées de manga shoujo qui pourrait alimenter les conversations ?"

# game/dialogs.rpy:741
translate french scene4_c54efb3e:

    # write "I hope you'll visit us someday soon."
    write "J'espère que tu viendras nous voir chez nous bientôt."

# game/dialogs.rpy:743
translate french scene4_f60fe613:

    # write "Your dear brother{p}%(stringhero)s"
    write "Ton frère{p}%(stringhero)s"

# game/dialogs.rpy:744
translate french scene4_76b2fe88_1:

    # nvl clear
    nvl clear

# game/dialogs.rpy:751
translate french scene4_0b847f9b:

    # "The week passed by quickly and it's already Friday."
    "La semaine passa rapidement. Et voila déjà Vendredi."

# game/dialogs.rpy:752
translate french scene4_a3a675c2:

    # "Spending time with the manga club is great."
    "Cette semaine avec les filles était super."

# game/dialogs.rpy:753
translate french scene4_58d076ca:

    # "Sakura is a little shy, but very sweet.{p}Rika still thinks I'm a pervert..."
    "Sakura ést toujours mignonne et timide.{p}Rika me traite toujours comme un pervers..."

# game/dialogs.rpy:755
translate french scene4_8367dc19:

    # "Nanami is playful and competitive. Playing video games with her is always fun."
    "Nanami et moi jouons toujours à quelques jeux vidéos ensemble, après les cours."

# game/dialogs.rpy:757
translate french scene4_0cbaa7bb:

    # "Nanami and I even started to play games online a little bit!"
    "J'ai découvert que Nanami est à fond dans les jeux vidéo et nous y jouons de temps à autres en ligne."

# game/dialogs.rpy:758
translate french scene4_4995ec74:

    # "I know it sounds weird that the only friends I've made are three girls. But I already feel like I know I can count on them."
    "Je sais que ça parait étrange que mes seuls amis soient toutes des filles. Mais je sais que je peux compter sur elles."

# game/dialogs.rpy:759
translate french scene4_99d1ceb5:

    # "I think it's better to have a few close friends you can count on than a large group of friends that don't know each other very well."
    "Il vaut mieux juger vos amis sur leurs qualités que sur leur quantité, non ?"

# game/dialogs.rpy:760
translate french scene4_6dc4476d:

    # "I've learned a lot about manga, anime and the village itself thanks to them."
    "J'ai beaucoup appris sur les mangas, les animés et le village aussi grâce à elles..."

# game/dialogs.rpy:764
translate french scene4_bd1aa541:

    # "On my way to school, I was walking towards the crossroads where I usually join Sakura."
    "Ce vendredi, j'étais en route pour le carrefour où je rejoins Sakura d'habitude."

# game/dialogs.rpy:766
translate french scene4_e8c9ca4f:

    # "As I got closer to her, I noticed that she wasn't alone."
    "J'étais assez loin d'elle quand je remarquai qu'elle n'était pas seule."

# game/dialogs.rpy:767
translate french scene4_5108d11f:

    # "When I got closer, I could see three rough looking guys around her."
    "En m'approchant, je vis trois mecs autour d'elle."

# game/dialogs.rpy:768
translate french scene4_6898b2d9:

    # "It was like they were the typical bad boys you see in some manga or anime, with the same exact goofy hairstyles and everything."
    "Des grands mauvais bonhommes comme dans les animés, avec la coupe banane et le reste."

# game/dialogs.rpy:769
translate french scene4_f2f59080:

    # "I vaguely knew who they were. They were from our school, but I remember Rika-chan telling me these guys never show up to school."
    "Je les connaissais vaguement. Rika m'a dit que ceux là ne viennent pas souvent à l'école..."

# game/dialogs.rpy:771
translate french scene4_b5bd2c54:

    # "Bad guy 1" "Go on! Say it!"
    "Mec 1" "Allez ! Dis-le !"

# game/dialogs.rpy:772
translate french scene4_2c3b1b19:

    # "One of the guys shouted at Sakura. I instinctively hid myself behind a wall."
    "Le gars a crié si fort sur Sakura, je me suis instinctivement caché derrière un mur."

# game/dialogs.rpy:773
translate french scene4_b57e6ca3:

    # "Another guy who looked like their leader forcefully grabbed Sakura by her collar."
    "Leur chef attrapa Sakura par le col."

# game/dialogs.rpy:774
translate french scene4_bf711db5:

    # "Bad guy 1" "You're guy, we all know it! So say it! Tell us what you really are!"
    "Mec 1" "On sait tous que t'es un mec ! Alors avoue ! Ose le dire devant moi !"

# game/dialogs.rpy:775
translate french scene4_b765fd13:

    # "Bad guy 2" "Yeah, stop playing the cute little girl!"
    "Mec 2" "Ouais, arrête de jouer les midinettes !"

# game/dialogs.rpy:776
translate french scene4_dc755d36:

    # "Bad guy 3" "I'm sure you're just a sick pervert that disguises himself into a girl to peek at other girls in the toilets, right!?"
    "Mec 3" "J'suis sûr que t'es qu'un pervers qui se déguise pour mater les meufs dans les toilettes, hein ?!"

# game/dialogs.rpy:777
translate french scene4_7849a2e2:

    # "Bad guy 2" "Yeah, you're disgusting!"
    "Mec 2" "Ouais, t'es dégueu !"

# game/dialogs.rpy:778
translate french scene4_8006ab79:

    # "What the hell are they talking about?!"
    "Mais qu'est-ce qu'ils racontent ?!"

# game/dialogs.rpy:779
translate french scene4_e45090ba:

    # "Anyway, Sakura is trouble! I have to do something!"
    "Quoiqu'il en soit, Sakura est en danger ! Il faut faire quelque chose !"

# game/dialogs.rpy:780
translate french scene4_af06121a:

    # "I'm not very good at fighting... I don't have the strength of a super hero..."
    "Je ne suis pas très bon au combat... Je n'ai pas la force d'un héros..."

# game/dialogs.rpy:781
translate french scene4_77b9ebe0:

    # "But I don't care! Sakura-chan must be saved!"
    "Mais je m'en foutais ! Sakura doit être sauvée !"

# game/dialogs.rpy:782
translate french scene4_d3847689:

    # "Be strong like the {i}High School Samurai{/i}, %(stringhero)s!"
    "Sois fort comme le {i}High School Samurai{/i}, %(stringhero)s !"

# game/dialogs.rpy:783
translate french scene4_38f6654f:

    # "{size=+10}Chaaaaarge!!!!!{/size}"
    "{size=+10}Chaaaaargez !!!!!{/size}"

# game/dialogs.rpy:784
translate french scene4_e9b169e1:

    # "I ran between the group and started to shout at the guys."
    "Je courrai vers les mecs en criant."

# game/dialogs.rpy:785
translate french scene4_03cc4ea3:

    # hero "Hey! Leave her alone!"
    hero "Hé ! Laissez-la tranquille !"

# game/dialogs.rpy:786
translate french scene4_21ec0b43:

    # "Bad guy 2" "Huh? Who's that little brat?"
    "Mec 2" "Hein ? C'est qui ce morpion ?"

# game/dialogs.rpy:787
translate french scene4_5a5a90ca:

    # "Bad guy 3" "Who are you? His boyfriend? You a faggot!?"
    "Mec 3" "T'es qui ? T'es son mec ? T'es pédé ?!"

# game/dialogs.rpy:789
translate french scene4_62845cb0:

    # "Bad guy 1" "Pfft! Whatever, let's go, guys. I had enough fun anyway."
    "Mec 1" "Pfft ! Foutons le camp, on s'est bien marrés, t'façons."

# game/dialogs.rpy:790
translate french scene4_02769f02:

    # "B.g. 2 & 3" "What?"
    "Mecs 2 & 3" "Hein ?"

# game/dialogs.rpy:791
translate french scene4_9d93e425:

    # "Bad guy 1" "I said let's go!!!"
    "Mec 1" "J'ai dit cassos !!!"

# game/dialogs.rpy:792
translate french scene4_e19b48ff:

    # "Bad guy 1" "There's way more fun shit to do than kicking the asses of these faggots!"
    "Mec 1" "On a mieux à faire que de tabasser des tarlouzes !"

# game/dialogs.rpy:793
translate french scene4_aadc99c4:

    # "I was ready for a fight but fortunately, they ended up walking away, arguing amongst themselves."
    "J'étais prêt à combattre, mais finalement, ils sont partis comme ça."

# game/dialogs.rpy:794
translate french scene4_65210d1a:

    # "I doubt I scared them. Maybe they really thought that with me in the way, harassing Sakura wasn't fun anymore."
    "Je ne crois pas leur avoir fait peur. Peut-être que mon arrivée a déclenché leur envie de faire autre chose."

# game/dialogs.rpy:795
translate french scene4_5b2dcd53:

    # "Sakura collapsed on her knees and sat on the ground. She stayed motionless, looking down."
    "Ils relachèrent Sakura et partirent. Elle tomba sur le derrière et resta sans bouger, regardant par terre."

# game/dialogs.rpy:797
translate french scene4_14ee51d1:

    # "I watched them walk away. The cicadas were crying."
    "Je les regardai partir alors que les cigales chantaient."

# game/dialogs.rpy:799
translate french scene4_5714df67:

    # "I got on my knees to reach out to Sakura. She looked dead inside."
    "J'aidai Sakura à se relever. Elle semblait en état de choc."

# game/dialogs.rpy:800
translate french scene4_dcd2ce5e:

    # hero "Sakura-chan, are you okay? Are you hurt?"
    hero "Sakura-chan, est-ce que ça va ?"

# game/dialogs.rpy:801
translate french scene4_82675361:

    # "She didn't say a word. I took my bottle of water and gave it to her."
    "Elle ne dit rien. Je lui donnai ma bouteille d'eau."

# game/dialogs.rpy:802
translate french scene4_44a2c36a:

    # hero "Here, drink. Try to calm down."
    hero "Tiens bois. T'as besoin d'eau fraîche."

# game/dialogs.rpy:803
translate french scene4_ecea0509:

    # s "T... thank you..."
    s "M... Merci..."

# game/dialogs.rpy:804
translate french scene4_0196f5ab:

    # "She drank from the bottle slowly. Tears were running down her face. She was definitely traumatized by what just happened."
    "Elle but en silence. Je vis quelques larmes sortir de ses grands yeux bleus. Probablement par la fraîcheur soudaine de l'eau."

# game/dialogs.rpy:805
translate french scene4_e638db20:

    # "Or maybe it was just the refreshing feeling of drinking water, bringing her back to reality. I couldn't tell..."
    "Ou bien à cause de ce qui s'est passé. Qui peut le dire ?..."

# game/dialogs.rpy:806
translate french scene4_76b6a439:

    # hero "It's okay, they're gone... They won't bother you anymore..."
    hero "C'est rien, ils sont partis... Ils ne t'ennuieront plus..."

# game/dialogs.rpy:807
translate french scene4_46f4b04d:

    # "I stayed with Sakura in the area for a while, waiting for her to recover."
    "On est restés là un moment, le temps qu'elle récupère."

# game/dialogs.rpy:808
translate french scene4_2bd9d9ea:

    # "Why did those guys bullied her like that?"
    "Pourquoi ces mecs sont allés la brutaliser ?"

# game/dialogs.rpy:809
translate french scene4_50b0bbf2:

    # "There's no way an innocent girl like her could be a guy in disguise! No way!"
    "Une fille comme elle ne peut pas être un mec déguisé ! Pas possible !"

# game/dialogs.rpy:810
translate french scene4_4c3197ed:

    # "Maybe they picked on her because she has some tomboyish tastes or something...?"
    "Peut-être qu'ils s'amusaient sur ses goûts un peu garçon-manqué..."

# game/dialogs.rpy:811
translate french scene4_a20e210b:

    # "Whatever, that was disgusting of them!"
    "Mais même, c'est nul de faire ça !"

# game/dialogs.rpy:812
translate french scene4_3664d290:

    # "In fact, nothing they said made any sense... I still don't get it..."
    "En fait, tout ceci n'avait pas de sens... Je ne comprends pas..."

# game/dialogs.rpy:813
translate french scene4_846b18ad:

    # "Finally, after Sakura calmed down a bit, we got up and went to school."
    "Finalement, on prit la route pour l'école."

# game/dialogs.rpy:817
translate french scene4_c8300016:

    # "We silently made our way to school together."
    "On fit le chemin vers l'école sans dire mot."

# game/dialogs.rpy:818
translate french scene4_291de2b0:

    # "I was worried. I think I'll need the help of Rika-chan."
    "J'étais inquiet. Je pensai qu'on allait avoir besoin de Rika."

# game/dialogs.rpy:825
translate french scene4_2eae0bd6:

    # hero "...and that's what happened this morning."
    hero "...Voilà, c'est ce qui s'est passé."

# game/dialogs.rpy:826
translate french scene4_4ddab44b:

    # "I didn't bother telling Rika what the bullies actually said to Sakura. Just all the rest."
    "Je n'ai cependant pas dit à Rika ce que ces brutes ont dit à Sakura. Juste toute le reste."

# game/dialogs.rpy:827
translate french scene4_6d41b106:

    # "Anyway, I'm pretty sure what they were saying doesn't even matter. They just made up a reason to bully her."
    "Je suis sûr que ce détail n'avait pas d'importance. Ils cherchaient juste un prétexte pour s'attaquer à elle."

# game/dialogs.rpy:828
translate french scene4_60f0cbde:

    # n "Those bastards!"
    n "Ces enfoirés !"

# game/dialogs.rpy:829
translate french scene4_cf610c40:

    # r "They were lucky that I wasn't there! I'm pretty good at trashing boys!"
    r "Ils ont de la chance que j'étais pas là ! Ils auraient vu !"

# game/dialogs.rpy:830
translate french scene4_ab36d9dc:

    # s "It's okay, Rika-chan. They ran away when %(stringhero)s-kun came to rescue me."
    s "C'est rien, Rika-chan. Ils sont partis quand %(stringhero)s-kun est venu pour me sauver."

# game/dialogs.rpy:832
translate french scene4_9f03aed3:

    # s "In fact, without his help, I don't know what would have happened..."
    s "En fait, sans lui, je ne sais pas ce qui se serait passé..."

# game/dialogs.rpy:833
translate french scene4_188ed7b1:

    # s "Thank you... Thank you, %(stringhero)s-kun!"
    s "Merci... Merci %(stringhero)s-kun !"

# game/dialogs.rpy:834
translate french scene4_c1b029c3:

    # "Sakura bowed to me. I wasn't sure what to say."
    "Sakura s'inclina devant moi. Je me sentis gêné."

# game/dialogs.rpy:838
translate french scene4_03382661:

    # r "Well, I guess I have to thank you for helping Sakura-chan %(stringhero)s."
    r "Bon, j'imagine que je dois te remercier aussi pour avoir sauvé mon amie, %(stringhero)s."

# game/dialogs.rpy:839
translate french scene4_77f35da3:

    # hero "It's okay, it's okay. After all, it's my duty to save damsels in distress!"
    hero "C'est rien, c'est rien. C'est mon devoir de sauver les demoiselles en détresse."

# game/dialogs.rpy:840
translate french scene4_aadcce64:

    # "Sakura laughed and finally smiled."
    "Sakura sourit avec un rire."

# game/dialogs.rpy:841
translate french scene4_f25fd7bb:

    # hero "Sakura-chan..."
    hero "Sakura-chan..."

# game/dialogs.rpy:842
translate french scene4_75a10854:

    # hero "I promise you right now."
    hero "Je t'en fais la promesse."

# game/dialogs.rpy:843
translate french scene4_e7c0608a:

    # hero "I'll dedicate myself to escorting you to and from school everyday!"
    hero "Désormais, je viendrai te chercher directement devant chez toi tous les jours, pour aller à l'école ! Et je te ramènerai jusqu'à chez toi après les cours !"

# game/dialogs.rpy:845
translate french scene4_6d4d7801:

    # s "Really? You promise?"
    s "Vraiment ? C'est promis ?"

# game/dialogs.rpy:847
translate french scene4_c89aabf6:

    # s "Oh, thank you, %(stringhero)s-kun! {image=heart.png}"
    s "Oh, merci, %(stringhero)s-kun ! {image=heart.png}"

# game/dialogs.rpy:849
translate french scene4_6bd67288:

    # r "Hmph! Fine, I'll accept this for now...only because I live the exact opposite direction from her house!"
    r "Hmph ! J'accepte uniquement parce que je vis à l'opposé et que je ne peux pas l'accompagner moi-même !"

# game/dialogs.rpy:850
translate french scene4_12240494:

    # r "If I could, I would already be escorting her everyday!"
    r "Sinon, je le ferais déjà depuis longtemps !"

# game/dialogs.rpy:852
translate french scene4_87cde803:

    # n "Same for me..."
    n "Pareil pour moi..."

# game/dialogs.rpy:853
translate french scene4_641491a3:

    # r "I just know I can count on you, %(stringhero)s."
    r "Mais je ne peux pas, alors je m'en remets à toi, %(stringhero)s."

# game/dialogs.rpy:854
translate french scene4_b799ab6b:

    # "I smiled."
    "Je souris."

# game/dialogs.rpy:855
translate french scene4_ea6ddde8:

    # "Rika really does seem like a tsundere but I found it pretty lovable."
    "Rika avait vraiment l'air d'une tsundere mais je trouvais ça mignon."

# game/dialogs.rpy:856
translate french scene4_4401cc13:

    # "I felt even better when I saw Sakura's bright smile."
    "Je me sentis mieux en voyant le sourire de Sakura-chan."

# game/dialogs.rpy:861
translate french scene4_77f4ec72:

    # r "Anyways..."
    r "A part ça,..."

# game/dialogs.rpy:862
translate french scene4_72233eeb:

    # r "I have an important announcement for the members of the club!"
    r "J'ai une annonce importante à faire pour les membres du club !"

# game/dialogs.rpy:863
translate french scene4_40d86de1:

    # s "Yes?"
    s "Oui ?"

# game/dialogs.rpy:864
translate french scene4_5ebd74f9:

    # hero "Yes, ma'am!"
    hero "Oui, chef !"

# game/dialogs.rpy:865
translate french scene4_2dd18f3e:

    # n "What is it?"
    n "Qu'est-ce qu'il y a ?"

# game/dialogs.rpy:866
translate french scene4_8afe26e4:

    # r "Two weeks from now, the summer festival will be happening in the village. And the club must meet together on this day and enjoy the festival!"
    r "Dans deux semaines, il va y avoir le festival du village. Et le club doit être préparé pour ce jour !"

# game/dialogs.rpy:867
translate french scene4_b181bb98:

    # hero "A summer festival? Really?"
    hero "Un festival d'été ? Un vrai ?"

# game/dialogs.rpy:868
translate french scene4_7ea87b78:

    # "I've heard about japanese festivals like the Matsuri."
    "J'avais entendu parler des festivals japonais comme les Matsuri."

# game/dialogs.rpy:869
translate french scene4_0a708135:

    # "They're very popular in older villages. People do things like put on yukatas and visit food stands. There's also a lot games, traditional dances and even a fireworks display at the very end."
    "Ils sont populaires dans les vieux villages. Les gens mettent des yukatas et ils visitent des stands de nourriture et de jeux. Il y a aussi des danses traditionelles et des feux d'artifices."

# game/dialogs.rpy:870
translate french scene4_2339b27a:

    # "I've only see them in anime. I've always wanted to go to a real one!"
    "Je n'en ai vu que dans des animés. J'ai toujours voulu aller dans une de ces fêtes."

# game/dialogs.rpy:871
translate french scene4_97a399b0:

    # r "You never went to any summer festival? You city rat!"
    r "Tu n'es jamais allé dans un festival d'été ? T'es bien un citadin, toi !"

# game/dialogs.rpy:872
translate french scene4_e1074e1b:

    # hero "Hey, shut up!"
    hero "Hé oh !"

# game/dialogs.rpy:873
translate french scene4_b091f4a6:

    # s "You'll come to realize, the summer festival of this village is absolutely amazing."
    s "Tu verras, le festival d'été du village est super."

# game/dialogs.rpy:874
translate french scene4_15e427d2:

    # s "Sounds like it'll be a lot of fun!"
    s "Ce sera vraiment marrant !"

# game/dialogs.rpy:875
translate french scene4_0fcfce33:

    # hero "And no need to take the train this time, right?"
    hero "Et pas besoin de prendre le train pour ça, hein ?"

# game/dialogs.rpy:876
translate french scene4_970c7b2d:

    # s "True! *{i}giggles{/i}*"
    s "Exactement ! *{i}rires{/i}*"

# game/dialogs.rpy:877
translate french scene4_6e7d7bc8:

    # r "I hope you have your yukata, city rat!"
    r "J'espère que tu as ton yukata, le citadin !"

# game/dialogs.rpy:878
translate french scene4_7c767f63:

    # hero "Hey! I do!"
    hero "Hé ! Oui, j'en ai un !"

# game/dialogs.rpy:879
translate french scene4_2322fa79:

    # "Actually, I didn't. I just wanted her to quit nagging me."
    "En fait je n'en avais pas, mais j'en avais marre qu'elle me traite de citadin."

# game/dialogs.rpy:886
translate french scene4_5805cafa:

    # "As I promised with Sakura-chan, I escorted her on the way home, after school."
    "Comme promis, j'accompagnai Sakura jusqu'à chez elle après l'école."

# game/dialogs.rpy:887
translate french scene4_6713403c:

    # "Ah geez...It'll be a problem if I don't have a yukata before the summer festival. Rika would never let it go."
    "Je réfléchissais à mon problème. J'ai dit à Rika que j'avais un yukata pour le festival mais ce n'est pas vrai."

# game/dialogs.rpy:888
translate french scene4_df7e7b2d:

    # "I have to find one as quickly as possible."
    "Il faut que j'en trouve un le plus vite possible."

# game/dialogs.rpy:889
translate french scene4_20088daa:

    # "I know wearing a yukata isn't mandatory for summer festivals, but I just want to show that tsundere that I'm not the city rat she thinks I am."
    "Je sais que les yukatas ne sont pas obligatoires à ces festivals, mais j'ai envie de montrer à cette tsundere que je ne suis pas le citadin qu'elle croit que je suis."

# game/dialogs.rpy:890
translate french scene4_2100c01f:

    # hero "Sakura-chan?"
    hero "Sakura-chan ?"

# game/dialogs.rpy:891
translate french scene4_40d86de1_1:

    # s "Yes?"
    s "Oui ?"

# game/dialogs.rpy:892
translate french scene4_bf7e25cb:

    # hero "Are you free tomorrow?"
    hero "Tu es libre, demain ?"

# game/dialogs.rpy:893
translate french scene4_9b65d1b6:

    # s "Yes, why?"
    s "Oui, pourquoi ?"

# game/dialogs.rpy:894
translate french scene4_b543397f:

    # hero "Well..."
    hero "Et bien..."

# game/dialogs.rpy:895
translate french scene4_d5ca9051:

    # hero "I'm not familiar with the big city yet..."
    hero "Je ne suis pas encore allé à la grande ville..."

# game/dialogs.rpy:896
translate french scene4_fb7dbaed:

    # hero "And I'd like to go there tomorrow..."
    hero "Et j'aimerais bien y aller demain..."

# game/dialogs.rpy:897
translate french scene4_55bf8a2d:

    # hero "So I was wondering if you would..."
    hero "Alors je me demandais si tu accepterais..."

# game/dialogs.rpy:898
translate french scene4_4cf796da:

    # hero "... come... with me...?"
    hero "...de venir... avec moi...?"

# game/dialogs.rpy:899
translate french scene4_0dc65489:

    # "Those last few words were hard to say. It's like I'm asking her out on a date!"
    "Les derniers mots furent durs à sortir. C'était comme si je lui proposais un rencart."

# game/dialogs.rpy:900
translate french scene4_02bf7f98:

    # "No... Wait... I'm totally asking her on a date right now!"
    "Non... En fait... Je suis complètement en train de lui proposer un rencart !"

# game/dialogs.rpy:902
translate french scene4_01b0fe5a:

    # s "Of course! I'd love to come with you to the city! {image=heart.png}"
    s "Bien sûr ! J'adorerais te servir de guide ! {image=heart.png}"

# game/dialogs.rpy:903
translate french scene4_c2837dd9:

    # hero "Really? Thanks!"
    hero "Vraiment ? Merci !"

# game/dialogs.rpy:904
translate french scene4_12d152e0:

    # hero "I couldn't think of a better guide than you!"
    hero "Je ne pense pas trouver meilleure guide que toi pour la grande ville !"

# game/dialogs.rpy:905
translate french scene4_4b122407:

    # "We giggled. She blushed."
    "On rit. Elle rougit."

# game/dialogs.rpy:906
translate french scene4_2e510646:

    # hero "I'll see you at the train station at 9am."
    hero "On se retrouve à la gare demain à 9 heures."

# game/dialogs.rpy:907
translate french scene4_4131ec23:

    # "Perfect! Just according to keikaku... I mean, the plan."
    "Parfait ! Tout se déroule selon le keikaku... Enfin, le plan."

# game/dialogs.rpy:908
translate french scene4_2f4016d4:

    # "Not only will this tour in the city inform me of where all the important shops are, but I also can find a yukata!"
    "Non seulement ce tour dans la ville me fera connaître les magasins importants, mais je pourrai aussi résoudre mon problème de yukata !"

# game/dialogs.rpy:910
translate french scene4_de73d541:

    # "I think she just saved me again."
    "Je crois qu'elle va encore me sauver la mise."

# game/dialogs.rpy:912
translate french scene4_586d150a:

    # "I think she just saved me."
    "Je crois qu'elle va me sauver la mise."

# game/dialogs.rpy:913
translate french scene4_8713b332:

    # "I can't wait to go to the city with Sakura tomorrow!"
    "J'ai hâte d'aller en ville avec elle, demain..."

# game/dialogs.rpy:922
translate french scene5_83f03a3f:

    # "The next morning."
    "Le matin suivant."

# game/dialogs.rpy:923
translate french scene5_432ea515:

    # "I was at the train station in the village. I'm not even sure that it was a station. It's so small and old."
    "J'étais à la gare du village, à l'heure. Je n'étais pas sur que ça soit bien là, car la gare était vraiment ancienne."

# game/dialogs.rpy:924
translate french scene5_55ba7152:

    # "And so small... I didn't know there were such small train stations in the country..."
    "Et si petite... Je n'imaginais pas qu'il y avait des gares aussi petites dans le pays..."

# game/dialogs.rpy:925
translate french scene5_ee63449f:

    # s "%(stringhero)s-kuuuuuuuuuuuuun!!!"
    s "%(stringhero)s-kuuuuuuuuuuuuun !!!"

# game/dialogs.rpy:926
translate french scene5_09ca9e90:

    # "Sakura arrived."
    "Sakura arriva."

# game/dialogs.rpy:927
translate french scene5_f274dee3:

    # "She was wearing a small blue summer dress, a striped shirt, and cute laced shoes that didn't hide her tiny feet."
    "Elle portait une petite robe d'été avec un t-shirt rayé. et des sandales qui ne cachaient pas beaucoup ses jolis pieds."

# game/dialogs.rpy:928
translate french scene5_535385a4:

    # "I usually find school uniforms attractive, but I have to admit that the casual outfit she was wearing suits her so much."
    "Je trouvais déjà l'uniforme sexy sur elle. Mais je dois admettre que les tenues de tous les jours lui allaient tout autant !"

# game/dialogs.rpy:931
translate french scene5_7eeab041:

    # s "Good morning, %(stringhero)s-kun!{p}Sorry for the wait!"
    s "Bonjour, %(stringhero)s !{p}Désolée du retard !"

# game/dialogs.rpy:932
translate french scene5_a5707cad:

    # hero "Good morning, Sakura-chan!"
    hero "Bonjour, Sakura-chan !"

# game/dialogs.rpy:934
translate french scene5_8736489d:

    # s "Ready to visit the city?"
    s "Prêt à visiter la ville ?"

# game/dialogs.rpy:935
translate french scene5_eef83bf6:

    # s "It's not as big as Tokyo for sure, but there's a lot more there than the village!"
    s "Ce n'est pas aussi grand que Tokyo ; mais c'est bien plus grand que le village !"

# game/dialogs.rpy:936
translate french scene5_fc3b562c:

    # hero "I guess you'll be my tour guide, senpai!"
    hero "Je suis prêt pour la visite, senpai !"

# game/dialogs.rpy:937
translate french scene5_c2497b21:

    # "I laughed a bit but I kinda realized something..."
    "Je ris un peu, mais j'étais embarassé."

# game/dialogs.rpy:938
translate french scene5_2326053d:

    # "If I go to the city to buy a yukata, she'll obviously notice it."
    "Si j'y vais pour acheter un yukata, elle s'en rendra compte."

# game/dialogs.rpy:939
translate french scene5_1b0c84a6:

    # "But if I don't, Rika will torment me at the festival."
    "Mais si je n'en achète pas un, Rika va encore se moquer de moi."

# game/dialogs.rpy:943
translate french scene5_2100c01f:

    # hero "Sakura-chan?"
    hero "Sakura-chan ?"

# game/dialogs.rpy:944
translate french scene5_40d86de1:

    # s "Yes?"
    s "Oui ?"

# game/dialogs.rpy:952
translate french scene5_20b30901:

    # hero "Yesterday...I lied. I don't have any yukata."
    hero "J'ai menti, hier... En fait, je n'ai pas de yukata."

# game/dialogs.rpy:953
translate french scene5_adb520a7:

    # hero "In fact, I was planning to buy one in the city while visiting it with you..."
    hero "J'avais prévu en acheter un durant notre visite dans la grande ville..."

# game/dialogs.rpy:955
translate french scene5_584030bd:

    # s "Is that so?"
    s "Vraiment ?"

# game/dialogs.rpy:957
translate french scene5_ef055bc2:

    # s "It's okay! I'll help you to find one!"
    s "Héhé, c'est pas grave ! Je t'aiderai à en trouver un !"

# game/dialogs.rpy:958
translate french scene5_404c741c:

    # hero "Really? That's a relief! Thanks!"
    hero "Vraiment ? Merci beaucoup !"

# game/dialogs.rpy:959
translate french scene5_12d1e584:

    # hero "I said I had one to avoid Rika-chan nagging me."
    hero "J'avais dit ça parce que j'en avais marre des taquineries de Rika-chan."

# game/dialogs.rpy:960
translate french scene5_a7f1d53d:

    # s "Rika-chan likes to give you a hard time, but I'm sure she means well!"
    s "Je comprends. J'aurais probablement fait la même chose à ta place ! *{i}rires{/i}*"

# game/dialogs.rpy:961
translate french scene5_fb14ae9d:

    # s "Our Rika-chan is very unique!"
    s "Notre Rika-chan est vraiment unique !"

# game/dialogs.rpy:962
translate french scene5_fe794b00:

    # "I sighed with relief and laughed with Sakura."
    "Je soupirai de soulagement avec un rire."

# game/dialogs.rpy:963
translate french scene5_a45c2161:

    # "I'm glad she's so understanding!"
    "Je crois que Sakura m'a encore tiré d'affaire !"

# game/dialogs.rpy:967
translate french scene5_e1b13f3b:

    # "Hmm... no... It's bad if I tell her..."
    "Mmm... Non... Mauvaise idée de lui dire..."

# game/dialogs.rpy:968
translate french scene5_7857c1c2:

    # "I'll just go out to have fun with her today. Once I see a yukata shop, I'll take note and I'll will come back tomorrow alone to buy it."
    "Je vais juste profiter de mon rendez-vous avec elle. Et dès que je vois un magasin de yukatas, je le note et j'y retourne demain."

# game/dialogs.rpy:969
translate french scene5_8ba6ee94:

    # "Yeah, that should be a good idea..."
    "Oui, bon plan, ça..."

# game/dialogs.rpy:971
translate french scene5_1e69c51f:

    # s "%(stringhero)s-kun, are you okay?"
    s "%(stringhero)s-kun, ça va ?"

# game/dialogs.rpy:972
translate french scene5_fcde3cfe:

    # hero "Oh, yes, nevermind... I was daydreaming again..."
    hero "Ah, oui oui... Ce n'est rien. Je rêvassais encore, je suppose..."

# game/dialogs.rpy:979
translate french scene5_c85106ec:

    # "The train arrived and we boarded it towards the city."
    "Le train arriva et nous fîmes route vers la grande ville."

# game/dialogs.rpy:980
translate french scene5_97c5ca7d:

    # "The city was three stations ahead. About 30 minutes of travel time. Sakura and I made small talk on the train."
    "C'était 3 stations plus loin. 30 minutes de voyage."

# game/dialogs.rpy:982
translate french scene5_66ca24f3:

    # "We finally arrived at the city."
    "On arriva enfin à la grande ville."

# game/dialogs.rpy:983
translate french scene5_0a3e36c2:

    # "It really is pretty big. Just as Sakura said."
    "C'était aussi grand que ce que me disait Sakura."

# game/dialogs.rpy:985
translate french scene5_0c293bd8:

    # s "Here we are!"
    s "On y est !"

# game/dialogs.rpy:986
translate french scene5_5e4b4e2f:

    # s "The shopping area isn't too far. Only two streets away."
    s "La zone commerciale n'est pas très loin. C'est à deux rues d'ici."

# game/dialogs.rpy:987
translate french scene5_75ad6856:

    # hero "Great! Let's get going, senpai!"
    hero "Ok. On y va, senpai ?"

# game/dialogs.rpy:988
translate french scene5_9e4c1aaf:

    # "Sakura smiled and we started walking through the busy streets."
    "Sakura sourit et on commença notre visite."

# game/dialogs.rpy:992
translate french scene5_1b70d467:

    # "After some time, we finally reached a street full of a variety of different shops."
    "Après un moment, on arriva dans une rue pleine de magasins."

# game/dialogs.rpy:993
translate french scene5_454785ef:

    # "Clothing, multimedia, restaurants, arcades,..."
    "Vêtements, multimédia, restaurants, salles d'arcade..."

# game/dialogs.rpy:994
translate french scene5_a2720e0f:

    # "They had everything here!"
    "Il y avait de tout !"

# game/dialogs.rpy:995
translate french scene5_4f036fa9:

    # "I hasn't been shopping since I moved from Tokyo. I was pretty excited."
    "J'étais euphorique. Je n'avais plus fait de shopping depuis mon départ de Tokyo !"

# game/dialogs.rpy:997
translate french scene5_0b0fbfd9:

    # "But first things first, I have to find a yukata shop."
    "Mais avant tout, je devais trouver un yukata."

# game/dialogs.rpy:998
translate french scene5_69e0143d:

    # "So we started with the clothes shops."
    "Alors nous commençâmes par les magasins de fringues."

# game/dialogs.rpy:1001
translate french scene5_8534ca42:

    # ". . ."
    ". . ."

# game/dialogs.rpy:1002
translate french scene5_aceff1a2:

    # "Argh!!"
    "Argh !!"

# game/dialogs.rpy:1003
translate french scene5_249d4d18:

    # "There's absolutely no yukata under the price of ¥200,000!"
    "Il n'y a aucun yukata en dessous de 200 000 ¥ !"

# game/dialogs.rpy:1004
translate french scene5_6a82deb5:

    # "It's definitely too expensive for the budget I had."
    "C'est définitivement trop cher pour mon budget."

# game/dialogs.rpy:1006
translate french scene5_3101ced6:

    # "As I was checking the prices, I noticed Sakura seemed to be thinking about something."
    "Sakura semblait réfléchir à quelque chose pendant que je regardais les prix."

# game/dialogs.rpy:1007
translate french scene5_01c5b3f7:

    # "She looked kinda sorry that I couldn't afford a yukata for myself."
    "Elle semblait navrée que je ne trouve rien."

# game/dialogs.rpy:1008
translate french scene5_0ddddc7d:

    # hero "Nah, forget it.{p}After all, it's not like my life depends on getting one."
    hero "Oh et puis c'est pas grave.{p}Après tout, c'est pas comme si j'en avais besoin."

# game/dialogs.rpy:1009
translate french scene5_a727d569:

    # hero "Although...I just hope Rika forgot about it already. I should brace myself for anything..."
    hero "Peut-être que Rika-chan a déjà oublié..."

# game/dialogs.rpy:1010
translate french scene5_41a74897:

    # hero "Let's go somewhere else for now..."
    hero "On devrait juste profiter de notre ballade !"

# game/dialogs.rpy:1011
translate french scene5_5c76b703:

    # s "Hmm... Okay..."
    s "Hmm... Ok alors..."

# game/dialogs.rpy:1016
translate french scene5_041a4807:

    # s "So, where do you want to go? Is there anything you want to see?"
    s "Par où on commence ? Y a un truc que tu voudrais voir en premier ?"

# game/dialogs.rpy:1017
translate french scene5_72bc6d39:

    # hero "How about checking out manga in the bookstores?"
    hero "Si on allait voir les librairies de mangas ?"

# game/dialogs.rpy:1018
translate french scene5_b6d74bf5:

    # hero "That could be useful for the club!"
    hero "Ça pourrait être bien pour le club !"

# game/dialogs.rpy:1020
translate french scene5_fa9a9f16:

    # s "Nice idea! Let's go!"
    s "Bonne idée ! Allons-y !"

# game/dialogs.rpy:1024
translate french scene5_2cd8d6de:

    # "The manga bookstore was gigantic!"
    "La librairie était énorme !"

# game/dialogs.rpy:1025
translate french scene5_3f7f2473:

    # "It was three floors full of manga as far as the eye could see!"
    "Les étages étaient remplis de mangas !"

# game/dialogs.rpy:1026
translate french scene5_17b06ea6:

    # "I start by browsing through the shonen harem manga, since it's my favorite genre."
    "J'ai commencé par le rayon des shonen, vu que c'est ce que je préfère."

# game/dialogs.rpy:1027
translate french scene5_54cf672e:

    # "I found all the volumes of {i}High School Samurai{/i}, but I had them already."
    "J'y ai trouvé tous les volumes de {i}High School Samurai{/i}, mais je les avais déjà."

# game/dialogs.rpy:1028
translate french scene5_e98d6302:

    # "Then I saw a series I didn't anything know, called {i}Uchuu Tenshi Moechan{/i}. I found the manga cover to be quite attractive. As I was reaching out to grab a volume..."
    "Puis j'ai vu une série que je ne connaissais pas, appelée {i}Uchuu Tenshi Moechan{/i}. Je trouvais les dessins mignons et j'avançai ma main pour le feuilleter."

# game/dialogs.rpy:1030
translate french scene5_2c810467:

    # "My hand touched another which went towards the same book. We both retracted our hands back quickly."
    "Ma main toucha une autre main qui allait dans la même direction. On retira vivement nos mains."

# game/dialogs.rpy:1031
translate french scene5_931b518d:

    # hero "Oh, I'm so sorry..."
    hero "Oh, pardon..."

# game/dialogs.rpy:1032
translate french scene5_56a93df9:

    # s "Sorry, my bad..."
    s "Désolé..."

# game/dialogs.rpy:1033
translate french scene5_8534ca42_1:

    # ". . ."
    ". . ."

# game/dialogs.rpy:1034
translate french scene5_cc9a5909:

    # "!!!"
    "!!!"

# game/dialogs.rpy:1036
translate french scene5_85b2e7cc:

    # "Sakura!?"
    "Sakura !?"

# game/dialogs.rpy:1037
translate french scene5_a3335b11:

    # "I was a little caught off guard."
    "J'étais un peu surpris"

# game/dialogs.rpy:1039
translate french scene5_960abe9a:

    # "Well only a little, since I knew that she liked {i}High School Samurai{/i} .{w} But I never imagined that she would like shonen harem manga that were aimed towards guys!"
    "Enfin seulement un peu, car je savais qu'elle aimait bien {i}High School Samurai{/i} ;{w} Mais je n'imaginais pas qu'elle aimerait d'autres mangas shônen et harem."

# game/dialogs.rpy:1040
translate french scene5_adf46165:

    # hero "Heh so...you must really like that genre huh, Sakura-chan?"
    hero "Tu aimes ce genre-là, Sakura-chan ?"

# game/dialogs.rpy:1041
translate french scene5_cd229797:

    # s "Well... y-yes..."
    s "Ben... O-oui..."

# game/dialogs.rpy:1042
translate french scene5_0735783a:

    # "She was blushing... Looks like it was embarrassing for her to talk about it."
    "Elle rougit... On dirait que ça l'embarassait d'en parler."

# game/dialogs.rpy:1043
translate french scene5_40d8c136:

    # s "I... I like these kinds of manga... The girls in them are very attractive..."
    s "J'ai... j'aime bien ce genre de manga... Les filles sont jolies, dedans..."

# game/dialogs.rpy:1044
translate french scene5_cde483c5:

    # s "I wish I was as beautiful as the women in these manga..."
    s "J'aimerais bien être aussi jolie..."

# game/dialogs.rpy:1045
translate french scene5_6ce4f6f3:

    # "That's kinda adorable."
    "Elle est adorable."

# game/dialogs.rpy:1046
translate french scene5_b7cf4dac:

    # "Honestly though, she's far prettier than any girl I've seen in any shonen manga!"
    "Mais elle EST bien plus belle que toutes ces nanas en 2D !"

# game/dialogs.rpy:1047
translate french scene5_7658f518:

    # "If I wasn't such a nervous wreck half the time, I'd say it again to her face..."
    "Si je n'étais pas aussi timide, je le lui dirais encore..."

# game/dialogs.rpy:1048
translate french scene5_f50ba84f:

    # "But I just couldn't... I lacked the courage to say it out loud."
    "Mais je n'y arrivais pas... J'étais trop timide, cette fois."

# game/dialogs.rpy:1049
translate french scene5_e3f223bb:

    # "My mind started to wander off about touching her hand by accident."
    "Mes doigts se rappelaient de l'éffleurement ressenti par les doigts de Sakura, plus tôt..."

# game/dialogs.rpy:1050
translate french scene5_25f48c38:

    # "It was so soft... It seemed almost surreal..."
    "Si doux... Surnaturellement doux..."

# game/dialogs.rpy:1051
translate french scene5_875908a3:

    # "I realized that I was daydreaming, and quickly regained my composure."
    "J'essayai de me reprendre."

# game/dialogs.rpy:1052
translate french scene5_aa64fa57:

    # hero "Oh...Uhh...So do you know what the story is about?"
    hero "Oh... Et ça parle de quoi, l'histoire ?"

# game/dialogs.rpy:1054
translate french scene5_dc5fd421:

    # s "Oh, it's a nice one."
    s "Oh, c'est assez original."

# game/dialogs.rpy:1055
translate french scene5_9b8c5139:

    # s "It's the story about a boy from Earth who is kidnapped by female aliens because they think he is a god that will save their planet."
    s "C'est l'histoire d'un garçon de la terre qui est kidnappé par des filles alien parce qu'elles pensent qu'il est le dieu qui sauvera leur planète."

# game/dialogs.rpy:1056
translate french scene5_85fb97ae:

    # s "They all want to go out with him but he's very shy; plus he has to fight against an horrible and powerful entity..."
    s "Elles veulent toutes sortir avec lui mais il est timide ; de plus il doit combattre une entitée puissante et horrible..."

# game/dialogs.rpy:1057
translate french scene5_f86e719b:

    # s "But fortunately, the girls have super powers and he fights alongside the girls..."
    s "Heureusement, les filles ont toutes un pouvoir spécial et il n'a qu'à se joindre au combat..."

# game/dialogs.rpy:1058
translate french scene5_ea859548:

    # s "It's kinda funny and heroic at same time!"
    s "C'est assez drôle et épique en même temps !"

# game/dialogs.rpy:1059
translate french scene5_b293cdd7:

    # hero "That sounds pretty entertaining!"
    hero "Ça a l'air pas mal !"

# game/dialogs.rpy:1060
translate french scene5_fa4d34a0:

    # hero "Maybe I should start reading it..."
    hero "Je devrais peut-être le commencer..."

# game/dialogs.rpy:1062
translate french scene5_40a4c6ba:

    # s "I can lend you the first few volumes at school if you want!"
    s "Je te prêterai les premiers volumes si tu veux, à l'école !"

# game/dialogs.rpy:1063
translate french scene5_7fe42341:

    # hero "That would be great! Thanks, Sakura-chan!"
    hero "Ce serait génial ! Merci, Sakura-chan !"

# game/dialogs.rpy:1067
translate french scene5_86f22ee4:

    # "After looking all around the store, we finally ended up leaving the shop."
    "On est finalement sortis du magasin."

# game/dialogs.rpy:1068
translate french scene5_b7801019:

    # "She bought the new volume of {i}Moechan{/i} while I got the first volume of {i}OGT{/i}, a seinen that I heard of a long time ago."
    "Elle a pris le nouveau volume de {i}Moechan{/i} alors que j'avais acheté le premier volume de {i}OGT{/i}, un seinen dont j'avais entendu parler il y a longtemps."

# game/dialogs.rpy:1071
translate french scene5_5ebb4f12:

    # s "So, what else do you want to do?"
    s "Alors, qu'est-ce qu'on fait, maintenant ?"

# game/dialogs.rpy:1072
translate french scene5_f7ff6212:

    # hero "How about the arcade?"
    hero "Si on allait voir les salles de jeux ?"

# game/dialogs.rpy:1073
translate french scene5_d8b99f39:

    # hero "I'm curious to see how good you are at shooting games."
    hero "Je suis curieux de voir comment tu te débrouilles au tir."

# game/dialogs.rpy:1075
translate french scene5_5b8b0c8c:

    # s "Teehee! Alright, let's go! But I won't hold back!"
    s "Hihi ! Ok, allons-y !"

# game/dialogs.rpy:1079
translate french scene5_fb2b36bb:

    # "We played a game of {i}Silent Crisis{/i} together."
    "On a joué tous les deux à {i}Silent Crisis{/i}."

# game/dialogs.rpy:1080
translate french scene5_e30e02a0:

    # "She was definitely a pro at it!"
    "Elle était vraiment douée !"

# game/dialogs.rpy:1081
translate french scene5_6f318a21:

    # "She knew exactly where the zombies spawned and shot them before they could even attack!"
    "Elle avait l'air de savoir exactement où étaient les ennemis et les shootaient dès qu'ils apparaissaient !"

# game/dialogs.rpy:1082
translate french scene5_1c41f171:

    # "I ended up using all my credits since I died over and over, while she didn't lose a single life at all!"
    "J'avais déjà dépensé tous mes crédits alors qu'elle n'avait perdu qu'une vie ou deux !"

# game/dialogs.rpy:1083
translate french scene5_ac2581f9:

    # hero "Wow... Sakura-chan, you're definitely better than I am..."
    hero "Wow... Sakura-chan, tu es vraiment meilleure que moi à ce jeu..."

# game/dialogs.rpy:1084
translate french scene5_509bc659:

    # hero "You know the game by heart or something?"
    hero "Tu connais le jeu par cœur ou bien ?"

# game/dialogs.rpy:1085
translate french scene5_7ab00054:

    # "She was concentrated on the game."
    "Elle était concentrée sur le jeu."

# game/dialogs.rpy:1086
translate french scene5_924b98a0:

    # s "Not really... In fact, it's only the third time I played this one."
    s "Pas vraiment en fait... C'est la troisième fois que j'y joue."

# game/dialogs.rpy:1087
translate french scene5_8c2adcf7:

    # s "I think it's my reflexes that make me good at these types of games."
    s "Je crois que c'est mes réflexes qui me guident."

# game/dialogs.rpy:1089
translate french scene5_7846d5b4:

    # "???" "Hey guys! You're here too?"
    "???" "Hé les gars ! Vous êtes là aussi ?"

# game/dialogs.rpy:1095
translate french scene5_43c67a2f:

    # hero "Oh! Nanami-chan!"
    hero "Ooh. Nanami-chan !"

# game/dialogs.rpy:1096
translate french scene5_640b15c3:

    # s "Hey Nana-chan!"
    s "Salut Nana-chan!"

# game/dialogs.rpy:1097
translate french scene5_bccc3e1d:

    # hero "What are you doing here?"
    hero "Qu'est-ce que tu fais là ?"

# game/dialogs.rpy:1099
translate french scene5_dbdeb2ab:

    # n "Duh, playing arcade games, you dork!"
    n "Duh, je suis venue pour jouer, idiot !"

# game/dialogs.rpy:1100
translate french scene5_86073654:

    # hero "Eh? I'm a dork now?"
    hero "Je suis idiot, maintenant ?"

# game/dialogs.rpy:1101
translate french scene5_a555e2b6:

    # n "Yes, you're a dork dude! {image=heart.png}"
    n "Oui, t'es un mec idiot ! {image=heart.png}"

# game/dialogs.rpy:1102
translate french scene5_040d24b0:

    # "Sakura laughed so much at the lame joke, she almost took a hit in the game."
    "Sakura rit tellement à la blague qu'elle faillit perdre un point de vie au jeu."

# game/dialogs.rpy:1103
translate french scene5_013529c0:

    # "Still, it's incredible how she can do multiple things at the same time. I heard it's an ability that girls naturally have.{p}I kinda envy them."
    "C'est quand même dingue comment elle arrive à faire plusieurs choses à la fois. J'ai entendu dire que c'est un talent que les filles ont.{p}Je les envie."

# game/dialogs.rpy:1105
translate french scene5_38444d54:

    # "Nanami put a token in the arcade machine and pressed the button labeled 'Player 2'."
    "Nanami glissa un jeton dans la machine et se joignit à Sakura en appuyant sur le bouton start du joueur 2."

# game/dialogs.rpy:1107
translate french scene5_a015009c:

    # n "Looks like you could use some back up!"
    n "Attends, j'arrive. T'as l'air en mauvaise posture."

# game/dialogs.rpy:1108
translate french scene5_4bf0d20d:

    # "Then Nanami started to shoot at the virtual mobs in the game with Sakura."
    "Puis Nanami commença à tirer sur les mobs virtuels avec Sakura."

# game/dialogs.rpy:1110
translate french scene5_2bae2874:

    # n "Hey, %(stringhero)s-senpai, do you know that some video game experts are saying that the golden age of arcade games is coming to an end?"
    n "Hé, %(stringhero)s-senpai, tu savais qu'on vit dans l'âge d'or des jeux d'arcade d'après des experts du gaming ?"

# game/dialogs.rpy:1111
translate french scene5_e7066814:

    # hero "Really?"
    hero "Vraiment ?"

# game/dialogs.rpy:1112
translate french scene5_b3e58373:

    # hero "But arcade games seem pretty recent, though..."
    hero "Mais les jeux d'arcade ne sont pourtant pas si vieux..."

# game/dialogs.rpy:1113
translate french scene5_2f385a6e:

    # n "It's because of the Internet and online gaming. It's taking more and more room in the gaming domain."
    n "C'est à cause d'Internet. Ça prend de plus en plus d'ampleur dans le jeu vidéo. Surtout à cause du jeu en ligne."

# game/dialogs.rpy:1114
translate french scene5_c0e7820a:

    # n "Soon there won't be as many arcades as there are now. Some are even afraid that arcade games will just end up being boring and flat compared to what you can find online."
    n "Bientôt il n'y aura plus autant de jeux d'arcade qu'actuellement. Ou bien les nouveaux jeux seront ennuyeux et sans saveur."

# game/dialogs.rpy:1115
translate french scene5_34b00fac:

    # n "At least rhythm games that need specific peripherals like {i}Para Para Revolution{/i} will be alright!"
    n "Ou alors il ne restera plus que des jeux de rythme qui demandent des périphériques spéciaux comme {i}Para Para Revolution{/i}."

# game/dialogs.rpy:1116
translate french scene5_5b2c12b3:

    # s "Oooh I love that game!"
    s "Ohhh j'adore celui-là !"

# game/dialogs.rpy:1117
translate french scene5_b5fe983b:

    # hero "Ah..."
    hero "Ah..."

# game/dialogs.rpy:1118
translate french scene5_7c35731c:

    # hero "Well, what about making arcade games that can be played online?"
    hero "Mmmm, et s'ils faisaient des machines d'arcade qui se connectent en ligne, un jour ?"

# game/dialogs.rpy:1119
translate french scene5_b075ad7f:

    # n "Hmm..."
    n "Hmm..."

# game/dialogs.rpy:1120
translate french scene5_0817fef1:

    # n "Hey, maybe you're getting at something, senpai!"
    n "Hé, tu tiens peut-être quelque chose, senpai !"

# game/dialogs.rpy:1121
translate french scene5_88898146:

    # s "I'm not sure that would work for rhythm games..."
    s "Je ne sais pas si ça marcherait pour les jeux musicaux..."

# game/dialogs.rpy:1122
translate french scene5_25648fa4:

    # "While Nanami was talking about the history of arcade games, she was still doing an amazing job shooting at monsters on the screen."
    "Alors que Nanami parlait toujours de l'histoire des jeux d'arcades, elle tirait sur l'écran."

# game/dialogs.rpy:1127
translate french scene5_70915e9e:

    # "After a while, Sakura finally lost her last life. Even Nanami was struggling to stay alive."
    "Après un moment, Sakura perdit son dernier crédit et Nanami se retrouva seule à jouer."

# game/dialogs.rpy:1128
translate french scene5_1c665d0e:

    # "Nanami was pretty good, but not as good as when she's playing fighting or puzzle games..."
    "Elle était douée, mais pas autant que dans les jeux de baston ou les puzzles..."

# game/dialogs.rpy:1129
translate french scene5_c09efcd8:

    # hero "Looks like we found a game she's not a master of!"
    hero "On dirait qu'on a trouvé un jeu où elle est moyennement bonne."

# game/dialogs.rpy:1130
translate french scene5_b4b28eb9:

    # s "Well, nobody's perfect, right?"
    s "Personne n'est parfait, hein ?"

# game/dialogs.rpy:1133
translate french scene5_ca00aade:

    # s "You know,..."
    s "Tu sais..."

# game/dialogs.rpy:1134
translate french scene5_29a8910b:

    # s "Nana-chan...{w}she... She is more fragile than she looks..."
    s "Nana-chan...{w}elle... Elle est plus fragile qu'elle n'y paraît..."

# game/dialogs.rpy:1135
translate french scene5_6d59e0e0:

    # hero "What do you mean?"
    hero "Comment ça ?"

# game/dialogs.rpy:1136
translate french scene5_af6cfb46:

    # s "Well, something happened to her that makes her pretty introverted and nervous around people she doesn't know..."
    s "Eh bien, il lui est arrivé quelque chose qui l'a changée. Elle est devenue un peu plus méfiante et introvertie..."

# game/dialogs.rpy:1137
translate french scene5_eb73848e:

    # hero "What happened?"
    hero "Que s'est-il passé ?"

# game/dialogs.rpy:1139
translate french scene5_557b9c7c:

    # s ". . ."
    s ". . ."

# game/dialogs.rpy:1140
translate french scene5_b475ba93:

    # s "I shouldn't tell you more, I'm sorry for bringing it up. I prefer she tells you herself..."
    s "Pardonne-moi, je ne peux pas t'en dire plus. Je préfère qu'elle te le dise elle-même..."

# game/dialogs.rpy:1143
translate french scene5_8811f347:

    # s "But on a more positive note,. {p}it's the first time I've seen her speak so much with someone she's only known for a few days."
    s "Mais une chose est certaine :{p}C'est la première fois que je la vois aussi bavarde avec quelqu'un qu'elle ne connait que depuis une semaine."

# game/dialogs.rpy:1144
translate french scene5_f97de379:

    # s "That's a pretty good sign for you!"
    s "C'est un très bon signe pour toi !"

# game/dialogs.rpy:1145
translate french scene5_291c83e9:

    # s "Maybe you'll be one of her best friends!"
    s "Tu pourrais devenir un de ses meilleurs amis !"

# game/dialogs.rpy:1146
translate french scene5_723e1b93:

    # hero "Does she has alot of best friends?"
    hero "Elle en a beaucoup, de meilleurs amis ?"

# game/dialogs.rpy:1148
translate french scene5_94da3bfe:

    # s "So far there's only Rika and me."
    s "Je sais qu'il y a moi et Rika..."

# game/dialogs.rpy:1150
translate french scene5_9a43a0d7:

    # s "I'm sure she'll tell you herself someday, if she trusts you enough."
    s "Un jour, elle te le dira, peut-être, si elle te fait assez confiance."

# game/dialogs.rpy:1151
translate french scene5_6a223ad6:

    # s "Anyways, don't mind her if she doesn't speak too much. Befriending her takes time."
    s "Quoi qu'il en soit, ne lui en veut pas si elle ne parle pas beaucoup. Avoir sa confiance prend du temps."

# game/dialogs.rpy:1152
translate french scene5_b6cbfe55:

    # hero "I understand...{p}Does she trust a lot of people?"
    hero "Je comprends...{p}Elle fait confiance à beaucoup de monde ?"

# game/dialogs.rpy:1153
translate french scene5_84d423c2:

    # s "So far, she only trusts her big brother, Rika and me."
    s "Je sais qu'elle fait confiance à moi, Rika-chan et son grand-frère."

# game/dialogs.rpy:1154
translate french scene5_ed700bc4:

    # s "I'm sure you'll be next on the list sooner or later!"
    s "Je suis sûre que ton tour viendra à un moment donné."

# game/dialogs.rpy:1156
translate french scene5_e668a3b6:

    # hero "I see..."
    hero "Je vois..."

# game/dialogs.rpy:1157
translate french scene5_0f2a7110:

    # hero "I'll do my best to be a good friend to her."
    hero "Je ferai de mon mieux !"

# game/dialogs.rpy:1159
translate french scene5_e866394b:

    # s "That's great to hear, %(stringhero)s-kun!"
    s "Merci, %(stringhero)s-kun !"

# game/dialogs.rpy:1160
translate french scene5_d01d2141:

    # s "I'm sure someday you and her will be best friends!"
    s "Je suis sûre qu'elle t'appréciera beaucoup un jour."

# game/dialogs.rpy:1161
translate french scene5_e741d135:

    # "For sure, but I wonder what Nanami thinks of me right now..."
    "Je me demande ce que Nanami pense de moi en ce moment..."

# game/dialogs.rpy:1162
translate french scene5_4464bb7e:

    # "And I'm wondering what Sakura thinks of me as well..."
    "Et je me demande ce que pense Sakura aussi..."

# game/dialogs.rpy:1167
translate french scene5_2f43b611:

    # "After Nanami finished playing, we ate some yakitori at a fast food restaurant together."
    "Après cela, nous sommes tous allés manger des yakitori à un fast-food."

# game/dialogs.rpy:1168
translate french scene5_b84a1878:

    # "Then we boarded the train to return to the village."
    "Puis nous sommes rentrés au village."

# game/dialogs.rpy:1169
translate french scene5_b6fe7fc0:

    # "The three of us had a pretty fun day together."
    "Nous nous sommes bien amusés, ce matin, tous les trois."

# game/dialogs.rpy:1171
translate french scene5_841a64f2:

    # "Nanami waved goodbye to us as she left to get back home."
    "Nanami repartit chez elle."

# game/dialogs.rpy:1173
translate french scene5_be1b33c3:

    # "Sakura and I were alone once again."
    "Sakura et moi restions seuls, comme au début de notre rendez-vous."

# game/dialogs.rpy:1175
translate french scene5_4fcb2681:

    # "Sakura seemed to be deep in thought about something again."
    "Sakura semblait réfléchir intensément à quelque chose."

# game/dialogs.rpy:1177
translate french scene5_5dcdb866:

    # "As we arrived at Sakura's house, Sakura spoke."
    "Elle était toujours en grande réflexion alors que nous arrivions devant chez elle."

# game/dialogs.rpy:1178
translate french scene5_007ceac2:

    # s "%(stringhero)s-kun..."
    s "%(stringhero)s-kun..."

# game/dialogs.rpy:1179
translate french scene5_a4db12cc:

    # hero "What's up?"
    hero "Ouais ?"

# game/dialogs.rpy:1181
translate french scene5_0f62d32d:

    # s "I think I have an idea! Just wait here a minute!"
    s "J'ai une idée ! Attends-moi là !"

# game/dialogs.rpy:1183
translate french scene5_91bb60f0:

    # "She ran into her house and came back with something. It looked like a set neatly wrapped of clothes."
    "Elle courut chez elle et revint avec quelque chose de bleu. On aurait dit un paquet de fringues."

# game/dialogs.rpy:1185
translate french scene5_8941c000:

    # s "Here... Please, take it..."
    s "Tiens... Prends-le..."

# game/dialogs.rpy:1186
translate french scene5_e8a32052:

    # s "It's a bit small but you can make it bigger with some touch-ups."
    s "C'est un peu petit, mais tu pourras faire faire des retouches facilement."

# game/dialogs.rpy:1187
translate french scene5_a7e82bdc:

    # "I unwrapped the set of clothes. {p}I couldn't believe it!"
    "Je déballai la chose bleue.{p}Je n'en croyais pas mes yeux !"

# game/dialogs.rpy:1188
translate french scene5_b12f3dbd:

    # "It was a blue yukata!"
    "C'était un yukata !"

# game/dialogs.rpy:1189
translate french scene5_10b50536:

    # "I chuckled for a second. For a second, I thought it was one of her own yukata. I was laughing imagining how I would look like in a yukata made for girls."
    "Je fis un petit rire. Je pensais que c'était un de ses propres yukatas. J'imaginais déjà de quoi j'aurais l'air avec un yukata pour femme."

# game/dialogs.rpy:1190
translate french scene5_d616579f:

    # "But after a closer look, I notice that it was really a yukata for men, with the right cuttings and colors!"
    "Mais après une plus grande inspection, je remarquai que c'était bel et bien un yukata pour homme, avec les bonnes couleurs et coupes !"

# game/dialogs.rpy:1191
translate french scene5_ac50db4a:

    # hero "Whoa... For real? I can borrow this?"
    hero "Wow... Sérieusement ? Je peux te l'emprunter ?"

# game/dialogs.rpy:1193
translate french scene5_3e45378b:

    # s "You can keep it if you want, %(stringhero)s-kun, it's okay!"
    s "Tu peux même le garder, %(stringhero)s-kun, ça ne me dérange pas !"

# game/dialogs.rpy:1194
translate french scene5_68f22fc9:

    # hero "Gee...thank you... Thank you so much!"
    hero "Waouh... Merci... Merci infiniment !"

# game/dialogs.rpy:1195
translate french scene5_882e22b6:

    # hero "Well... I gotta go... See you at school, Sakura-chan!"
    hero "Il faut que j'y aille... On se revoit à l'école, Sakura-chan !"

# game/dialogs.rpy:1196
translate french scene5_1a4bf588:

    # s "Have a nice weekend, %(stringhero)s-kun!"
    s "Passe un bon weekend, %(stringhero)s-kun !"

# game/dialogs.rpy:1199
translate french scene5_bb555e9c:

    # "Sakura waved goodbye and went back into her house."
    "Elle me fit au revoir de la main en rentrant chez elle."

# game/dialogs.rpy:1200
translate french scene5_020b0a9c:

    # "She really has a habit of helping me out in a jam!"
    "Elle a vraiment le don pour me sauver la mise !"

# game/dialogs.rpy:1201
translate french scene5_66a17c47:

    # "But it felt kinda strange."
    "Mais c'est quand même bizarre."

# game/dialogs.rpy:1202
translate french scene5_51ab6c7b:

    # "Whose yukata is this?"
    "Comment a-t-elle eu ce yukata ?"

# game/dialogs.rpy:1203
translate french scene5_4735f789:

    # "It looked like it's sized for her..."
    "Il avait l'air à sa taille..."

# game/dialogs.rpy:1204
translate french scene5_960d182a:

    # "Maybe it was her father's from when he was young..."
    "Oh... C'est peut-être un yukata qui appartenait à son père durant sa jeunesse..."

# game/dialogs.rpy:1205
translate french scene5_cb071560:

    # "But...It looks brand new..."
    "Mais il a l'air flambant neuf..."

# game/dialogs.rpy:1206
translate french scene5_0074c533:

    # "They couldn't have bought one by mistake..."
    "Ils n'ont pas pu acheter ça par erreur, non ?"

# game/dialogs.rpy:1212
translate french scene5_8c3a38cf:

    # hero "So... I'll see you later at school!"
    hero "On se revoit à l'école !"

# game/dialogs.rpy:1213
translate french scene5_c2d0f8e4:

    # s "See you around %(stringhero)s-kun!"
    s "Bon weekend, %(stringhero)s-kun !"

# game/dialogs.rpy:1214
translate french scene5_c0d35692:

    # s "I had a lot of fun with you today!"
    s "On s'est bien amusés !"

# game/dialogs.rpy:1215
translate french scene5_da65aa5d:

    # hero "Likewise!"
    hero "Je trouve aussi !"

# game/dialogs.rpy:1218
translate french scene5_bb555e9c_1:

    # "Sakura waved goodbye and went back into her house."
    "Elle me fit au revoir de la main en rentrant chez elle."

# game/dialogs.rpy:1219
translate french scene5_c8d8f45f:

    # "Thanks to her, I knew how to get to the city and where the shops were."
    "Grâce à elle, je savais comment aller en ville et où se trouvait les magasins."

# game/dialogs.rpy:1220
translate french scene5_67c032e7:

    # "During my visit, I spotted some yukata shops."
    "Dans ma visite, j'ai trouvé des magasins avec des yukatas."

# game/dialogs.rpy:1221
translate french scene5_5854a13b:

    # "Tomorrow, I'll go there alone and I'll buy myself a yukata."
    "Demain, j'y retournerai pour m'en acheter un."

# game/dialogs.rpy:1222
translate french scene5_bb429b41:

    # "I'll catch that loudmouth Rika by surprise at the festival!"
    "Cette grande gueule de Rika verra au festival !"

# game/dialogs.rpy:1224
translate french scene5_a20cefa7:

    # "..."
    "..."

# game/dialogs.rpy:1225
translate french scene5_667ec4fd:

    # "Oh well... Let's go home, now..."
    "Bon, rentrons chez nous, à présent..."

# game/dialogs.rpy:1246
translate french scene7_237e24d1:

    # centered "{size=+35}CHAPTER 3\nSakura's secret{fast}{/size}"
    centered "{size=+35}CHAPITRE 3\nLe secret de Sakura{fast}{/size}"

# game/dialogs.rpy:1251
translate french scene7_58905c5b:

    # "Another week has passed."
    "Une autre semaine passa."

# game/dialogs.rpy:1252
translate french scene7_19ce328a:

    # "School was great, with Rika, Nanami and Sakura around."
    "L'école se passait bien, avec Rika, Sakura et Nanami."

# game/dialogs.rpy:1253
translate french scene7_2fc12cae:

    # "During these last two weeks, I really started to feel that I was a part of the village now."
    "Durant ces deux semaines, j'ai vraiment eu le sentiment que je faisais désormais partie du village."

# game/dialogs.rpy:1254
translate french scene7_2c9e68a3:

    # "I'm happy, but I'm unsure if I have feelings for Sakura..."
    "J'en suis heureux, mais je ne suis pas sûr de mes sentiments envers Sakura..."

# game/dialogs.rpy:1255
translate french scene7_909fa517:

    # "Or maybe even for Nanami..."
    "Ou même envers Nanami..."

# game/dialogs.rpy:1257
translate french scene7_4dd65759:

    # "One thing's for sure, Nanami and I talk and play games more and more.{p}I think she really likes me."
    "Une chose est sûre, elle et moi parlions ensemble de plus en plus.{p}Je crois qu'elle m'apprécie vraiment."

# game/dialogs.rpy:1258
translate french scene7_ef8f393e:

    # "But I still don't know what that thing Sakura told me about her was about..."
    "Mais je ne sais toujours pas de quoi m'avait parlé Sakura à propos d'elle..."

# game/dialogs.rpy:1259
translate french scene7_28d00c59:

    # "Well... At least I don't have any feelings for Rika...No way!"
    "Enfin bon... C'est sûr, je ne ressens rien pour Rika ! Duh ! Pas moyen !"

# game/dialogs.rpy:1261
translate french scene7_10129566:

    # r "Hey, %(stringhero)s!"
    r "Hé, %(stringhero)s!"

# game/dialogs.rpy:1266
translate french scene7_57e4be97:

    # "Yikes!!! Speak of the devil herself..."
    "Argh ! Quand on parle du loup..."

# game/dialogs.rpy:1267
translate french scene7_391047cd:

    # hero "Oh, hey, Rika-chan..."
    hero "Oh, salut, Rika-chan..."

# game/dialogs.rpy:1268
translate french scene7_9c2ccd47:

    # r "Do you have some free time next weekend?"
    r "Tu as du temps libre le prochain weekend ?"

# game/dialogs.rpy:1269
translate french scene7_158b0faf:

    # hero "This weekend?"
    hero "Ce weekend, là ?"

# game/dialogs.rpy:1270
translate french scene7_3a013f55:

    # r "No, you idiot. The weekend after this one!"
    r "Non, idiot, le weekend d'après !"

# game/dialogs.rpy:1271
translate french scene7_9d4e7e0f:

    # hero "Oh... Sure I guess. What do you need?"
    hero "Oh... Oui, pourquoi ?"

# game/dialogs.rpy:1272
translate french scene7_acfd623e:

    # r "Well, since you're a manly man that has muscles..."
    r "Eh bien, vu que tu es un homme avec des muscles..."

# game/dialogs.rpy:1273
translate french scene7_67864244:

    # r "I thought, maybe you could make yourself useful and help me prepare for festival!"
    r "Je pensais que tu pourrais m'aider à préparer le matériel pour le festival !"

# game/dialogs.rpy:1274
translate french scene7_e98e7579:

    # "Argh! Why did I agree before knowing why?!"
    "Arf ! Pourquoi j'ai dit oui sans avoir demandé pourquoi avant ?!"

# game/dialogs.rpy:1275
translate french scene7_df0ced9f:

    # "Looks like I don't have a choice now..."
    "On dirait que je n'ai plus le choix, à présent..."

# game/dialogs.rpy:1283
translate french scene7_8b156e71:

    # hero "Alright, fine, I'll help."
    hero "D'accord, je viendrai !"

# game/dialogs.rpy:1285
translate french scene7_10eea5f6:

    # r "Great!"
    r "Super !"

# game/dialogs.rpy:1287
translate french scene7_6f5d2035:

    # r "I'll tell you where to meet later!"
    r "Je t'en dirai plus plus tard !"

# game/dialogs.rpy:1288
translate french scene7_9766f460:

    # hero "Rika-chan, I didn't know you were from a temple."
    hero "Rika-chan, je ne savais pas que tu travaillais au temple."

# game/dialogs.rpy:1289
translate french scene7_82bf0a2d:

    # r "My parents are shinto priests. Sometimes I help them for festivals and other temple related stuff."
    r "Mes parents sont les prêtres shinto du village. Des fois je les aide pour la préparation des cérémonies."

# game/dialogs.rpy:1290
translate french scene7_c90a7240:

    # hero "Do you live in the temple with your parents?"
    hero "Et tu ne vis pas directement au temple avec eux ?"

# game/dialogs.rpy:1292
translate french scene7_a6c17ff8:

    # r "My..."
    r "Mes..."

# game/dialogs.rpy:1293
translate french scene7_95b777d9:

    # "Her face fell dark."
    "Son expression s'assombrit."

# game/dialogs.rpy:1294
translate french scene7_51eeeb24:

    # r "My parents divorced."
    r "Mes parents sont divorcés."

# game/dialogs.rpy:1295
translate french scene7_95db867f:

    # r "I live with my mother and my father guards the temple."
    r "Je vis avec ma mère pendant que mon père vit au temple."

# game/dialogs.rpy:1297
translate french scene7_5fbbef96:

    # r "But they're still on good terms and they still have their faith in the shinto order, so it's okay."
    r "Mais ils sont en bons termes et ils ont toujours leur foi shintoïste, donc ça va."

# game/dialogs.rpy:1298
translate french scene7_868b412a:

    # hero "Ah, I'm sorry, I didn't know. It's good they're on positive terms though."
    hero "Ah, je vois.... Désolé de l'apprendre... Mais s'ils sont en bons termes, ça va..."

# game/dialogs.rpy:1299
translate french scene7_0d2419ec:

    # r "Yeah...I guess so. Anyway, I'll meet you at the club, see ya!"
    r "Je suppose, oui...{p} Ok, on se revoit au club. A toute !"

# game/dialogs.rpy:1301
translate french scene7_36df1f8b:

    # "I guess I learned something new about Rika. Still...I hope she won't be a total jerk towards me this weekend..."
    "Eh bien... J'espère qu'elle ne m'ennuiera pas trop à ce weekend..."

# game/dialogs.rpy:1308
translate french scene7_219621f1:

    # s "%(stringhero)s-kun!!"
    s "%(stringhero)s-kun !!"

# game/dialogs.rpy:1309
translate french scene7_51a60250:

    # hero "Sakura-chan! How goes?"
    hero "Sakura-chan ! Ça va ?"

# game/dialogs.rpy:1310
translate french scene7_f4d7051e:

    # "Sakura bowed at me. As she rose back up, I noticed her face was flushed."
    "Sakura s'inclina devant moi. Elle rougissait un peu."

# game/dialogs.rpy:1312
translate french scene7_85835f50:

    # s "%(stringhero)s-kun,...are you doing anything this weekend?"
    s "%(stringhero)s-kun,...tu fais quelque chose, ce weekend ?"

# game/dialogs.rpy:1314
translate french scene7_31d438a9:

    # hero "The next weekend?"
    hero "Le weekend prochain ?"

# game/dialogs.rpy:1315
translate french scene7_9d85daa8:

    # hero "I'm sorry, I'm helping Rika-chan set up stuff for the festival."
    hero "Je ne pourrai pas. J'irai aider Rika-chan à préparer le festival."

# game/dialogs.rpy:1316
translate french scene7_77ee7366:

    # s "Huh?"
    s "Eh ?"

# game/dialogs.rpy:1317
translate french scene7_8480da2c:

    # s "Oh... No, I mean, the weekend of this week"
    s "Oh... Non, je veux dire, ce weekend qui vient, là."

# game/dialogs.rpy:1318
translate french scene7_cb5a59ac:

    # hero "Oh whoops. Guess my mind was wandering to far into the future... Hm..."
    hero "Ah, pardon... Hum..."

# game/dialogs.rpy:1319
translate french scene7_d0aa7f64:

    # hero "I'm not doing anything at all."
    hero "Non, je ne fais rien ce weekend là..."

# game/dialogs.rpy:1320
translate french scene7_2217ef70:

    # hero "Do you want to go out to the city again?"
    hero "Tu voudrais aller en ville ?"

# game/dialogs.rpy:1322
translate french scene7_1d124fd8:

    # "Sakura's face lit up and she started to giggled."
    "Sakura rit doucement."

# game/dialogs.rpy:1323
translate french scene7_0f5eeead:

    # s "I...I was about to ask you the same question! {image=heart.png}"
    s "J'étais sur le point de te le proposer ! {image=heart.png}"

# game/dialogs.rpy:1324
translate french scene7_9e2e5ae5:

    # hero "Of course, I'd love to go out with you to the city!"
    hero "Bien sûr, j'adorerais aller avec toi en ville !"

# game/dialogs.rpy:1325
translate french scene7_22e33626:

    # hero "Yeah, I'd like to have even more fun than last time!"
    hero "Bien sûr, j'adorerais un peu de fun comme la dernière fois !"

# game/dialogs.rpy:1327
translate french scene7_c31b4dec:

    # s "Same here! I'll wait for you at the train station at 9AM!"
    s "Chouette ! Je t'attendrai à la gare à 9 heures."

# game/dialogs.rpy:1328
translate french scene7_5409a3f5:

    # hero "Alright! I'll see you then!"
    hero "D'ac' !"

# game/dialogs.rpy:1330
translate french scene7_a7db0154:

    # "Ah, it's almost time for class..."
    "Ah, c'est l'heure des cours..."

# game/dialogs.rpy:1334
translate french scene7_13719c4f:

    # "The school day dragged on and on... Maybe I just couldn't wait to hang out with Sakura."
    "Le jour passa rapidement, comme d'habitude... Ou bien j'avais juste hâte d'aller au rencart..."

# game/dialogs.rpy:1335
translate french scene7_85ae4e7c:

    # "The next day came, and I arrived at the train station in the morning to go to the city with Sakura."
    "Puis le rendez-vous avec Sakura arriva le matin suivant..."

# game/dialogs.rpy:1341
translate french SH1_e0b76a41:

    # "We took the train to the city. We started our day out eating at an American fast food place because we were already hungry."
    "Cette fois, on avait commencé par un fast-food américain parce qu'on avait déjà faim."

# game/dialogs.rpy:1342
translate french SH1_d68b3fb1:

    # "After that, we decided to try karaoke."
    "Après ça, on est allé se faire un karaoké."

# game/dialogs.rpy:1344
translate french SH1_0c6feee4:

    # "Sakura's voice was absolutely mesmerizing."
    "La voix de Sakura était merveilleuse."

# game/dialogs.rpy:1345
translate french SH1_a925770d:

    # hero "You could be a famous pop idol, Sakura-chan."
    hero "Tu pourrais être une vraie chanteuse pop, Sakura-chan."

# game/dialogs.rpy:1346
translate french SH1_ee5a9ea8:

    # s "Thank you... But I don't want to be one, though..."
    s "Merci... Mais je ne pense pas que je voudrais cela..."

# game/dialogs.rpy:1347
translate french SH1_b809126d:

    # hero "Eh? Why is that?"
    hero "Ah ? Pourquoi ?"

# game/dialogs.rpy:1348
translate french SH1_93f8ec4b:

    # s "Well..."
    s "Eh bien..."

# game/dialogs.rpy:1349
translate french SH1_3098f4a6:

    # "She hesitated a bit before replying."
    "Elle hésita un long moment avant de répondre."

# game/dialogs.rpy:1350
translate french SH1_25b5d3d5:

    # s "I don't think I'd like the lifestyle of a pop idol..."
    s "Je ne crois pas que j'aimerais le style de vie d'une chanteuse pop..."

# game/dialogs.rpy:1351
translate french SH1_9c5f10fe:

    # s "It's so much work and you don't have a lot of time for yourself."
    s "C'est tellement de boulot et ça te laisse trop peu de temps pour toi-même."

# game/dialogs.rpy:1352
translate french SH1_67650f2f:

    # hero "Yeah, it must be rough..."
    hero "Ouais, je comprends..."

# game/dialogs.rpy:1353
translate french SH1_759de575:

    # hero "By the way, what would you would like to do in the future?"
    hero "Tu aimerais faire quoi, plus tard ?"

# game/dialogs.rpy:1354
translate french SH1_9028f52b:

    # s "I don't really know yet..."
    s "Je ne sais pas encore..."

# game/dialogs.rpy:1355
translate french SH1_36411a29:

    # s "Maybe I'll be a part of a symphony orchestra if I can."
    s "Peut-être entrer au conservatoire et faire partie d'un orchestre symphonique..."

# game/dialogs.rpy:1356
translate french SH1_ba98acc9:

    # hero "Do you play any instruments?"
    hero "Tu joues d'un instrument ?"

# game/dialogs.rpy:1357
translate french SH1_cc1fe723:

    # s "Yes, the violin."
    s "Oui, le violon."

# game/dialogs.rpy:1358
translate french SH1_5632a1a6:

    # "I suddenly remembered..."
    "Je me souvins alors..."

# game/dialogs.rpy:1363
translate french SH1_1075e8d3:

    # "So the violin I hear sometimes at night... That must be her..."
    "Le violon que j'entendais certains soirs... C'était donc elle..."

# game/dialogs.rpy:1368
translate french SH1_6ae339be:

    # hero "I think I hear you playing the violin during the evening from time to time."
    hero "C'est donc toi ce violon que j'entends certains soirs ?"

# game/dialogs.rpy:1369
translate french SH1_ccbcdf1d:

    # "She looked down, kinda embarrassed."
    "Elle regarda le sol, intimidée."

# game/dialogs.rpy:1370
translate french SH1_457a5f49:

    # s "Oh... You heard me playing?"
    s "Oh... Tu m'as entendue jouer ?"

# game/dialogs.rpy:1371
translate french SH1_0027dd08:

    # hero "Yeah... You play so perfectly... It's amazing!"
    hero "Oui... Et tu joues divinement bien... C'est impressionnant !"

# game/dialogs.rpy:1372
translate french SH1_55a77664:

    # s "W-why T-t-thank you..."
    s "M-merci..."

# game/dialogs.rpy:1373
translate french SH1_7b7e5c26:

    # hero "I hope I can watch you play someday."
    hero "J'espère pouvoir te voir en jouer un jour."

# game/dialogs.rpy:1374
translate french SH1_32eac59f:

    # s "I'd be happy to play for you, %(stringhero)s-kun!"
    s "J'adorerais jouer pour toi un jour, %(stringhero)s-kun !"

# game/dialogs.rpy:1378
translate french SH1_3e846a41:

    # "I tried to sing the next song, but I was horribly awful at singing. At least it made Sakura laugh."
    "Après quelques chansons où j'ai été horriblement mauvais au chant, (ce qui a, au moins, fait rire Sakura)"

# game/dialogs.rpy:1379
translate french SH1_7f6f8e16:

    # "When we were done, we headed back to to the city's train station."
    "nous étions sur le point de rentrer."

# game/dialogs.rpy:1383
translate french SH1_884ab08c:

    # "As we were about to cross the road..."
    "Alors qu'on allait traverser la rue,"

# game/dialogs.rpy:1384
translate french SH1_5b2451aa:

    # "A car, ignoring the red light, kept driving and almost hit Sakura!"
    "une voiture fonça en grillant le feu rouge, manquant de rentrer dans Sakura."

# game/dialogs.rpy:1385
translate french SH1_d2261bcd:

    # "Sakura quickly stepped back but lost her balance and was about to fall on her back."
    "Sakura fit un pas en arrière pour l'éviter et manqua de tomber en arrière."

# game/dialogs.rpy:1387
translate french SH1_eb7a286f:

    # "I managed to catch her just in time and she fell into arms."
    "Je réussis à la rattrapper à temps et elle atterit dans mes bras."

# game/dialogs.rpy:1388
translate french SH1_e5780a08:

    # "She looked into my eyes and I got lost in hers."
    "Elle me regarda avec ses magnifiques yeux bleus."

# game/dialogs.rpy:1389
translate french SH1_ac769ce2:

    # "Time stopped."
    "Le temps sembla s'arrêter."

# game/dialogs.rpy:1390
translate french SH1_e75587c1:

    # "As we stared at each other, we forgot all about the car incident."
    "Alors qu'on se regardait intensément, on avait presque oublié l'incident avec la voiture."

# game/dialogs.rpy:1391
translate french SH1_0657ef81:

    # s "%(stringhero)s....-kun...."
    s "%(stringhero)s....-kun...."

# game/dialogs.rpy:1392
translate french SH1_c6dc2ed4:

    # hero "....Sakura-chan..."
    hero "....Sakura-chan..."

# game/dialogs.rpy:1394
translate french SH1_ae0ed22a:

    # "Her eyes slowly shut, her face closed in on mine, her small mouth beckoned for something... It was her lips that seemed to be waiting for me."
    "Elle ferma les yeux, me faisant face, sa bouche un peu en cœur... Ses lèvres semblaient appeler les miennes."

# game/dialogs.rpy:1395
translate french SH1_07a9d021:

    # "Oh my gosh... I think she wants me to kiss her! I can't believe it! What do I do!?"
    "... Je crois qu'elle veut que je l'embrasse ! J'y crois pas !"

# game/dialogs.rpy:1397
translate french SH1_4b8ffc21:

    # "I could hear my heart beating loudly. It felt so loud, I was almost sure that the people around us could hear it too."
    "Mon cœur battait fortement... Si fort que j'avais l'impression que tout le monde pouvait l'entendre."

# game/dialogs.rpy:1398
translate french SH1_fe20f7a5:

    # "This was it. It's now or never. My face slowly approached hers..."
    "J'approchai mes lèvres des siennes, doucement..."

# game/dialogs.rpy:1399
translate french SH1_8538723d:

    # "Very slowly..."
    "Très doucement..."

# game/dialogs.rpy:1400
translate french SH1_f1ab825c:

    # "I could feel her breath on my face."
    "Je pouvais sentir sa respiration."

# game/dialogs.rpy:1405
translate french SH1_16f58986:

    # "And the second my nose touched hers...{p}She turned her face away, completely embarrassed, and stood up almost immediately."
    "Au moment où mon nez toucha le sien,...{p}elle détourna le visage, intimidée, et se leva."

# game/dialogs.rpy:1406
translate french SH1_56dcd521:

    # s "I'm... I'm okay..."
    s "Je... Ça va, je n'ai rien..."

# game/dialogs.rpy:1410
translate french SH1_557b9c7c:

    # s ". . ."
    s ". . ."

# game/dialogs.rpy:1411
translate french SH1_b64fd408:

    # s "Let's go home..."
    s "Rentrons..."

# game/dialogs.rpy:1416
translate french SH1_32abdb6f:

    # "On the train ride home, I was in a complete daze..."
    "Je me sentais un peu bizarre alors qu'on rentrait..."

# game/dialogs.rpy:1417
translate french SH1_06b56af7:

    # "I thought she wanted me to kiss her... But in the end, she refused..."
    "J'étais pratiquement sûr qu'elle voulait que je l'embrasse... Mais elle a finalement refusé..."

# game/dialogs.rpy:1418
translate french SH1_a20cefa7:

    # "..."
    "..."

# game/dialogs.rpy:1419
translate french SH1_23dda12a:

    # "Dammit, stop being paranoid, %(stringhero)s!...{p}Maybe it's just because it was her first time. She might not have felt ready..."
    "Oh, arrête ta paranoïa, %(stringhero)s!...{p}Elle est peut-être extrèmement timide parce que ce serait son premier baiser et qu'elle ne se sent pas prête pour ça..."

# game/dialogs.rpy:1420
translate french SH1_e1f82eca:

    # "I felt kinda happy inside though. At least it means that she's attracted to me... I think..."
    "Je me sentis heureux, du coup. Cela voulait dire qu'elle était attirée par moi... Je crois..."

# game/dialogs.rpy:1421
translate french SH1_a3dbd16d:

    # "But I also felt just as confused..."
    "Mais j'étais quand même confus..."

# game/dialogs.rpy:1422
translate french SH1_246f930a:

    # "As we sat together in silence, I occasionally looked over to Sakura."
    "Je regardai Sakura, de temps en temps."

# game/dialogs.rpy:1423
translate french SH1_acd10f88:

    # "She didn't look angry. But she seemed really embarrassed still."
    "Elle n'avait pas l'air fâchée. Juste embarassée."

# game/dialogs.rpy:1424
translate french SH1_045668b1:

    # "But, most of all, she looked upset about something. She just sat there lost in her thoughts like she was profoundly thinking about something."
    "Mais surtout, elle était perdue dans ses pensées et avait l'air de réfléchir intensément à quelque chose."

# game/dialogs.rpy:1427
translate french SH1_3c386e89:

    # "Sakura was completely quiet along the way."
    "Sakura ne dit aucun mot durant le voyage."

# game/dialogs.rpy:1428
translate french SH1_849b7889:

    # "I didn't know what to say...or what to do."
    "Je ne savais pas quoi dire ou faire..."

# game/dialogs.rpy:1429
translate french SH1_c312d8ea:

    # "The atmosphere was uncomfortable."
    "L'atmosphère était tendue depuis l'incident."

# game/dialogs.rpy:1430
translate french SH1_59826635:

    # "Suddenly, she broke the silence."
    "Soudainement, elle brisa le silence."

# game/dialogs.rpy:1431
translate french SH1_1eeb4b66:

    # s "...%(stringhero)s-kun?"
    s "...%(stringhero)s-kun ?"

# game/dialogs.rpy:1432
translate french SH1_4edce7c8:

    # hero "Yes?"
    hero "Oui ?"

# game/dialogs.rpy:1434
translate french SH1_782acf27:

    # s "I...{p}........................{p}I have something I want to tell you..."
    s "J'ai...{p}........................{p}J'ai quelque chose à te dire."

# game/dialogs.rpy:1435
translate french SH1_d2dcc110:

    # hero "What is it?"
    hero "Qu'y a-t-il ?"

# game/dialogs.rpy:1436
translate french SH1_719ce3f8:

    # "I followed her as she entered a rice field."
    "Alors qu'on parlait, elle entra dans un champ de riz. Je la suivis..."

# game/dialogs.rpy:1437
translate french SH1_d7b94be6:

    # "The path inside was small, surrounded by big rice plants."
    "La route était petite et entourée de grandes plantes de riz."

# game/dialogs.rpy:1438
translate french SH1_481e4154:

    # "It was completely desolate. There was nobody around us. Only the chants of the cicadas could be heard."
    "C'était une route vide. Pas âme qui vive autour... On n'entendait que le chant des cigales."

# game/dialogs.rpy:1440
translate french SH1_8329e10f:

    # s "This is something very difficult to tell you...{p}Painfully difficult..."
    s "C'est quelque chose de difficile à dire...{p}Très difficile..."

# game/dialogs.rpy:1441
translate french SH1_c02f0d8f:

    # s "This isn't something you might not even comprehend, but..."
    s "C'est autant dur pour moi de te le dire...que pour toi de le comprendre."

# game/dialogs.rpy:1442
translate french SH1_7cf20b6f:

    # hero "This sounds pretty serious."
    hero "Ça a l'air sérieux..."

# game/dialogs.rpy:1443
translate french SH1_a4c7d906:

    # "I could feel a sort of anxiety building up from inside me."
    "Je sentis mon cœur battre d'anxiété."

# game/dialogs.rpy:1444
translate french SH1_ec713d94:

    # "I've never seen Sakura behave this seriously before."
    "Je n'avais jamais vu Sakura aussi déterminée."

# game/dialogs.rpy:1445
translate french SH1_49f22c99:

    # s "I'm about to tell you this because I trust you."
    s "Je vais te le dire parce que je te fais confiance."

# game/dialogs.rpy:1446
translate french SH1_afd8f97b:

    # "I smiled, but I was nervous."
    "Je souris derrière ma nervosité."

# game/dialogs.rpy:1447
translate french SH1_5066f9b8:

    # hero "Y-you can trust me, Sakura-chan. You're my friend. You, Rika and Nanami are my best friends and I'll never betray any of you. Never."
    hero "E-Et tu le peux, Sakura-chan. Tu es mon amie. Toi, Nanami et Rika êtes mes meilleures amies. Et jamais je ne vous trahirai."

# game/dialogs.rpy:1448
translate french SH1_9b73dceb:

    # "I mean that, from the bottom of my heart."
    "Je le pensais sincèrement, au fond de mon cœur."

# game/dialogs.rpy:1449
translate french SH1_64b40e00:

    # s "Do you promise? Never?"
    s "Tu le promets ? Jamais ?"

# game/dialogs.rpy:1450
translate french SH1_b7fb0fa9:

    # hero "Forever and ever."
    hero "Pour toujours et à jamais !"

# game/dialogs.rpy:1451
translate french SH1_ac397e42:

    # "This time I smiled with a little more confidence."
    "Je souris une nouvelle fois, un peu plus confiant."

# game/dialogs.rpy:1452
translate french SH1_43dc30fb:

    # "She gave a faint smile but reverted back to her serious face after."
    "Elle me rendit faiblement mon sourire mais reprit son sérieux tout de suite après."

# game/dialogs.rpy:1453
translate french SH1_ee01b059:

    # s "It's... So hard for me to tell you...{p}Besides my parents, only Rika-chan knows about it.{p}Not even Nanami-chan knows..."
    s "C'est... Tellement difficile à dire...{p}En fait, à part mes parents, il n'y a que Rika-chan qui soit au courant."
    s "Même Nanami-chan ne le sait pas encore..."

# game/dialogs.rpy:1454
translate french SH1_a56d7a94:

    # s "Everyone else seems to have just heard rumors about it... or aren't sure at all..."
    s "Tous ceux qui croient le savoir n'ont entendu que des rumeurs...ou ne sont pas sûrs..."

# game/dialogs.rpy:1455
translate french SH1_d0d64bc6:

    # hero "Oh..."
    hero "Oh..."

# game/dialogs.rpy:1456
translate french SH1_2fcf5cc9:

    # "I was confused... What was she trying to say?"
    "J'étais confus... De quoi parlait-elle ?"

# game/dialogs.rpy:1457
translate french SH1_0ebf712b:

    # "Obviously, she and Rika-chan knew a secret.{p}And Sakura-chan was about to tell me this secret."
    "Apparemment, elle et Rika connaissaient un secret.{p}Et j'allais désormais faire partie de la confidence."

# game/dialogs.rpy:1458
translate french SH1_4df61edb:

    # "I feel happy. That means she must feel I'm a close enough friend and that she really trusts me."
    "Je me sentis heureux. Cela veut dire qu'elle m'a vraiment accepté comme meilleur ami et qu'elle me fait confiance."

# game/dialogs.rpy:1460
translate french SH1_32e60ae0:

    # "She stopped after walking a small distance from me, facing away from me."
    "Elle s'arrêta de marcher. Elle était à trois mètres de moi, face au soleil, me tournant le dos."

# game/dialogs.rpy:1461
translate french SH1_a0dd6cdb:

    # "Her body covered the sun and I saw only her shadow in the distance."
    "Je ne pouvais voir que son ombre dans cette position."

# game/dialogs.rpy:1463
translate french SH1_e1554201:

    # s "..."
    s "..."

# game/dialogs.rpy:1464
translate french SH1_007ceac2:

    # s "%(stringhero)s-kun..."
    s "%(stringhero)s-kun..."

# game/dialogs.rpy:1465
translate french SH1_d5eced1a:

    # s "Do you remember the day when those thugs bullied me?"
    s "Tu te souviens de ces brutes, l'autre fois ?"

# game/dialogs.rpy:1466
translate french SH1_d19c5e51:

    # hero "Yes, I remember."
    hero "Oui, je m'en rappelle."

# game/dialogs.rpy:1467
translate french SH1_3b33aa23:

    # s "Do you remember what they said to me?"
    s "Tu te souviens de ce qu'ils disaient ?"

# game/dialogs.rpy:1468
translate french SH1_ec6b7626:

    # hero "Yeah, I remember."
    hero "Oui, clairement."

# game/dialogs.rpy:1469
translate french SH1_b28fcf52:

    # hero "They were trying to make you admit that you were a boy, or something like that."
    hero "Ils essayaient de te faire dire que tu étais un mec, un truc comme ça..."

# game/dialogs.rpy:1470
translate french SH1_bf9bdd8b:

    # s "Yes."
    s "Oui."

# game/dialogs.rpy:1471
translate french SH1_b5a2fb72:

    # s "It's because they..."
    s "En fait, ils..."

# game/dialogs.rpy:1472
translate french SH1_9fce5758:

    # s "They guessed it... For some reason they knew it..."
    s "Ils ont deviné... Je ne sais pas comment, mais, ils savent..."

# game/dialogs.rpy:1473
translate french SH1_bbc36fab:

    # hero "I... What are you trying to say?..."
    hero "Je... Que veux-tu dire ?..."

# game/dialogs.rpy:1474
translate french SH1_007ceac2_1:

    # s "%(stringhero)s-kun..."
    s "%(stringhero)s-kun..."

# game/dialogs.rpy:1483
translate french SH1_e4a4445c:

    # s "I {b}really{/b} am a boy."
    s "Je suis {b}vraiment{/b} un garçon."

# game/dialogs.rpy:1484
translate french SH1_16bb48d0:

    # hero "...W-what?"
    hero "...Quoi ?"

# game/dialogs.rpy:1505
translate french SH1_47f1e757:

    # centered "{size=+35}CHAPTER 4\nBig decisions{fast}{/size}"
    centered "{size=+35}CHAPITRE 4\nGrandes décisions{fast}{/size}"

# game/dialogs.rpy:1515
translate french SH1_bb09a9d7:

    # hero "You... You're... a boy!?"
    hero "Tu... Tu es...un garçon ?!"

# game/dialogs.rpy:1516
translate french SH1_a82ece96:

    # "My mind went completely numb for a second."
    "Je ne savais pas quoi penser."

# game/dialogs.rpy:1517
translate french SH1_7bdf1789:

    # "I couldn't believe it."
    "Je n'arrivais pas à y croire."

# game/dialogs.rpy:1518
translate french SH1_cd989119:

    # "No, she couldn't be a boy!{p} That's impossible!!!"
    "Non, Elle ne peut pas être un garçon !{p}C'est impossible !!!"

# game/dialogs.rpy:1519
translate french SH1_9f02c3f2:

    # "I started to nervously laugh to myself. There's no way. It was just a joke..."
    "Je commençai à rire nerveusement. Je n'y croyais pas. C'était sûrement une blague."

# game/dialogs.rpy:1520
translate french SH1_1faf5341:

    # hero "Haha it's a joke...No way...No, you can't be a boy, Sakura-chan!"
    hero "Haha non, c'est une blague. Tu ne peux pas être un garçon, Sakura-chan !"

# game/dialogs.rpy:1521
translate french SH1_b293358c:

    # "Sakura-chan didn't say a thing."
    "Sakura ne répondit rien."

# game/dialogs.rpy:1522
translate french SH1_b2a3712a:

    # "She suddenly turned around, and approached me."
    "Puis soudainement elle se tourna et s'approcha."

# game/dialogs.rpy:1523
translate french SH1_fcff274b:

    # s "...If you don't believe me, take a look for yourself!"
    s "...Si tu ne me crois pas, regarde !"

# game/dialogs.rpy:1528
translate french SH1_93439998:

    # "She suddenly lifted her skirt up, showing me her panties!"
    "Elle souleva légèrement sa robe, sa culotte devenant visible !"

# game/dialogs.rpy:1529
translate french SH1_6a7c30f8:

    # "I gasped out of embarrassment and shock. I took a look around, but fortunately we were still alone in the rice fields."
    "Je criai d'embarassement et de surprise. Je regardai autour de nous mais nous étions, heureusement, toujours seuls."

# game/dialogs.rpy:1530
translate french SH1_6db43b58:

    # "I tried my hardest to not look at Sakura and covered my eyes."
    "Je couvris mes yeux, essayant de ne pas regarder."

# game/dialogs.rpy:1531
translate french SH1_7987ae0e:

    # hero "No, Sakura-chan, I can't look, this is just inappropriate!"
    hero "Non, Sakura-chan, je ne peux pas, c'est trop gênant !"

# game/dialogs.rpy:1532
translate french SH1_ea014391:

    # s "Please, just look at me!"
    s "S'il te plaît, fais-le !"

# game/dialogs.rpy:1533
translate french SH1_14a15247:

    # "I peeked a little and could see her eyes intensely glaring at me. She was dead serious."
    "Ses jolis yeux bleus me regardaient. Elle était vraiment sérieuse."

# game/dialogs.rpy:1534
translate french SH1_453421f4:

    # "My eyes slowly scrolled down to look below her waistline."
    "Alors j'ai lentement regardé vers son bassin."

# game/dialogs.rpy:1535
translate french SH1_952d1725:

    # "She was wearing blue-colored panties..."
    "Elle portait une petite culotte féminine bleue, très mignonne."

# game/dialogs.rpy:1536
translate french SH1_ede13101:

    # "But then, I noticed that...in her panties...there was...something.{p}Something that shouldn't have been there."
    "Mais dedans, il y avait...quelque chose.{p}Quelque chose qui n'était pas censé y être."

# game/dialogs.rpy:1537
translate french SH1_dbbe62c0:

    # "...But the evidence was right there... "
    "...Impossible de nier l'évidence, à présent... "

# game/dialogs.rpy:1538
translate french SH1_a97eb7ed:

    # "I couldn't believe my eyes but the truth was there..."
    "Je n'en croyais pas mes yeux mais les faits étaient là..."

# game/dialogs.rpy:1540
translate french SH1_6ae69507:

    # "She was really a HE!!!"
    "Elle était bien un garçon !!!"

# game/dialogs.rpy:1541
translate french SH1_20403ffb:

    # "I started to shake."
    "Je commençai à trembler."

# game/dialogs.rpy:1542
translate french SH1_99484c94:

    # "So the cute girl I was following for school.{p}The wonderful girl who was talking about manga and anime with Rika and Nanami..."
    "La jolie fille avec qui j'allais à l'école.{p}La jolie fille avec qui je causais manga avec Rika et Nanami..."

# game/dialogs.rpy:1543
translate french SH1_0e81ba72:

    # "The girl I was infatuated with for almost two weeks...{p}The girl I was about to kiss today..."
    "La fille avec qui j'avais des rendez-vous...{p}La fille que j'ai failli embrasser aujourd'hui..."

# game/dialogs.rpy:1544
translate french SH1_a2b0302d:

    # "That girl... Was a boy! A guy! A dude! A male!!!!!"
    "Cette fille... Etait un garçon ! Un mec !"

# game/dialogs.rpy:1545
translate french SH1_51a16ccf:

    # "I stood there, motionless."
    "J'étais pétrifié de stupeur."

# game/dialogs.rpy:1546
translate french SH1_0cbdd6b2:

    # hero "You... yo-yo-yo-yo-you're a..."
    hero "Tu...T-t-t-t-u... Tu es..."

# game/dialogs.rpy:1547
translate french SH1_29fb07f0:

    # hero "{size=+10}You're a boy?!{/size}"
    hero "{size=+10}Tu es un mec ?!{/size}"

# game/dialogs.rpy:1548
translate french SH1_37a5e581:

    # "I ended up shouted at her. I couldn't believe it."
    "Je criai cette réplique. Je n'arrivais pas à y croire."

# game/dialogs.rpy:1549
translate french SH1_024d5fe2:

    # "My outburst made her...I mean him a bit scared. He stepped back a little."
    "Le volume de ma voix lui fit peur et elle fit un pas en arrière. Je la rassurai."

# game/dialogs.rpy:1550
translate french SH1_12900b8c:

    # hero "Sorry... Sorry, Sakura-chan, I didn't mean to scare you..."
    hero "Pardonne-moi... Désolé, Sakura-chan, je ne voulais pas te faire peur..."

# game/dialogs.rpy:1551
translate french SH1_a613c480:

    # hero "Huh...no, I... I mean, Sakura-kun...? Huh... Aaaaargh!!"
    hero "Enfin, je veux dire, Sakura-kun...? Euh... Aaaargh !!"

# game/dialogs.rpy:1552
translate french SH1_32d25ac1:

    # "My mind felt like it split into a million pieces."
    "J'étais en totale confusion et ma tête tournait."

# game/dialogs.rpy:1553
translate french SH1_e0277eeb:

    # hero "It's... incredible... You..."
    hero "C'est... incroyable... Tu..."

# game/dialogs.rpy:1554
translate french SH1_00287bf2:

    # hero "But... You know, I..."
    hero "Mais... Tu sais, je..."

# game/dialogs.rpy:1559
translate french SH1_a783e872:

    # "As if reading my mind, Sakura turned around, facing the sun again, and he... or she... started to calmly speak."
    "Devinant mes questions, Sakura me tourna le dos une fois de plus. J'entendis son ombre me parler."

# game/dialogs.rpy:1561
translate french SH1_b0e013d4:

    # s "I was born with a very rare... {w}Well, I don't know if it's a genital disorder or something..."
    s "Je suis né avec...{w} Je ne sais pas si c'est un dérèglement génétique ou quelque chose..."

# game/dialogs.rpy:1562
translate french SH1_41913518:

    # s "Technically, I inherited all the chromosomes of my mother, but I got a Y chromosome anyway."
    s "Techniquement j'ai tous les chromosomes de ma mère, sauf un. Un chromosome Y."

# game/dialogs.rpy:1563
translate french SH1_f7622dac:

    # s "It made me a very girlish boy. So girlish that even my brain is one of a girl."
    s "It made me a very girlish boy. So girlish that even my brain is one of a girl."

# game/dialogs.rpy:1564
translate french SH1_20638c61:

    # s "I was a normal boy when I was young, but my mind and body transformed me as I grew older."
    s "Ça a fait de moi un garçon très féminin. Si féminin que même mon cerveau est celui d'une femme."

# game/dialogs.rpy:1565
translate french SH1_1cb3b079:

    # s "I was more attracted by dolls than little cars, and I preferred talking with girls than playing soccer with the boys..."
    s "J'étais plus attiré par les poupées que les petites voitures, et je préférais parler avec les autres filles que de jouer au foot avec les garçons..."

# game/dialogs.rpy:1566
translate french SH1_425c4b72:

    # s "When I was 11-years-old, my dad started to get exhausted of this... \"joke\", that change inside of me... So he started getting me back into \"male stuff\"..."
    s "Quand j'ai eu 11 ans, mon père a commencé à en avoir marre de cette \"comédie\"... Alors il a commencé à essayer de me reconditionner à aimer des \"choses de garçons\"..."

# game/dialogs.rpy:1567
translate french SH1_c69a2dcb:

    # s "He bought me outfits and books usually made for boys..."
    s "Il m'achetait des fringues ou des livres pour garçons..."

# game/dialogs.rpy:1568
translate french SH1_31185f69:

    # s "But as time passed by...he started to drink more... and his alcohol addiction grew. Ge started to become more violent."
    s "Mais avec le temps, il est devenu de plus en plus alcoolique. Il a commencé à devenir violent."

# game/dialogs.rpy:1569
translate french SH1_c8d0b471:

    # s "He even tried to cut my hair once."
    s "Une fois, il a même essayé de me couper les cheveux de force."

# game/dialogs.rpy:1570
translate french SH1_f480d83d:

    # s "But my mother protected me. She has always accepted me for who I am, no matter what."
    s "Mais ma mère m'a protégée. Elle m'a toujours acceptée telle que je suis, quoi qu'il arrive."

# game/dialogs.rpy:1571
translate french SH1_64529bc1:

    # s "I can't help it. {p}I feel like a girl inside, that's all."
    s "Je n'y peux rien. {p}Je me sens fille en moi, c'est tout."

# game/dialogs.rpy:1572
translate french SH1_f674ff4c:

    # s "Actually, my father is really pissed off at me. My mother tries to protect me but my father becomes crazy when I'm around..."
    s "Mon père m'en veut énormément. Ma mère essaie de me protéger, mais mon père a tendance à devenir fou quand je suis dans le coin."

# game/dialogs.rpy:1573
translate french SH1_b935cc96:

    # "I nodded, taking in her...his monologue slowly, digesting his story..."
    "J'acquiesçai, écoutant son histoire..."

# game/dialogs.rpy:1575
translate french SH1_0317f7d7:

    # "I didn't know what to do..."
    "Je ne savais pas quoi faire ou dire..."

# game/dialogs.rpy:1576
translate french SH1_03c57d51:

    # "His...her life must have been pretty tough growing up...but..."
    "Il... Ou elle... n'était pas très heureuse dans sa vie..."

# game/dialogs.rpy:1577
translate french SH1_20a5c853:

    # hero "So you want to be a girl. No matter what everybody thinks, and whatever your own body thinks, you want to be a girl and that's all."
    hero "Alors tu veux être une fille. Peu importe comment est ton corps, ou ce que pensent les gens, tu veux être une fille et c'est tout."

# game/dialogs.rpy:1578
translate french SH1_10c7cae4:

    # s "Yes...more than anything."
    s "Oui...plus que tout."

# game/dialogs.rpy:1579
translate french SH1_71387f7a:

    # hero "Hmm..."
    hero "Hmm..."

# game/dialogs.rpy:1580
translate french SH1_60e224a2:

    # "I thought for a moment...and I finally smiled and accepted it."
    "Je réfléchis un moment, puis finalement, je l'acceptai et fit un sourire."

# game/dialogs.rpy:1581
translate french SH1_c35613b6:

    # "After all... Maybe she's a boy, but Sakura is still Sakura."
    "Après tout... C'est peut-être un garçon, mais Sakura reste Sakura, non ?"

# game/dialogs.rpy:1582
translate french SH1_de287c57:

    # hero "Hmm okay... It's okay for me..."
    hero "D'accord... Ça me va, je comprends."

# game/dialogs.rpy:1583
translate french SH1_5c82e516:

    # s "Really?"
    s "Vraiment ?"

# game/dialogs.rpy:1584
translate french SH1_456228e3:

    # hero "Yeah! If you're happy like this, then I'm happy for you as well!"
    hero "Oui ! peu importe... Tant que tu es heureux...ou heureuse, je suis heureux pour toi !"

# game/dialogs.rpy:1585
translate french SH1_923bc3f8:

    # "Sakura was still facing away from me, she still seemed a little unsatisfied."
    "Sakura faisait toujours face au soleil."

# game/dialogs.rpy:1586
translate french SH1_8a085455:

    # s "You must have some questions at least, don't you?"
    s "Tu as des questions, n'est-ce pas."

# game/dialogs.rpy:1587
translate french SH1_bc81f33c:

    # "Sheesh...she's right, I do have a lot of questions. It's scary how he...uh...she can read my mind."
    "Ce n'était pas une question. C'est flippant comment elle...ou il devine mes pensées !"

# game/dialogs.rpy:1588
translate french SH1_9614bee5:

    # "She spoke softly."
    "Je sentis un sourire dans sa voix."

# game/dialogs.rpy:1589
translate french SH1_3caf349f:

    # s "Go ahead, please. You're my friend, it's normal to have questions about this. And you have the right to know more."
    s "Vas-y, je t'en prie. Tu es mon ami. C'est normal d'avoir ces questions. Et tu es en droit de savoir."

# game/dialogs.rpy:1590
translate french SH1_64036f15:

    # "I calmed myself down a bit and gave Sakura a smile."
    "Je souris en essayant de me ressaisir."

# game/dialogs.rpy:1591
translate french SH1_9877ea0f:

    # hero "Okay...{p}So you prefer that everyone say 'she' to you instead of 'he'?"
    hero "Ok...{p}Donc, tu préfères qu'on dise \"Elle\" pour toi, et pas \"lui\" ?"

# game/dialogs.rpy:1592
translate french SH1_bf9bdd8b_1:

    # s "Yes."
    s "Oui."

# game/dialogs.rpy:1593
translate french SH1_caa3249e:

    # hero "I see... Alright..."
    hero "Je vois. D'accord..."

# game/dialogs.rpy:1594
translate french SH1_20b19b0b:

    # "The problem of she and he is all solved now. It feels as confusion is already going away."
    "Le problème du lui et elle est désormais réglé. Je souris en sentant ma confusion s'en aller petit à petit."

# game/dialogs.rpy:1595
translate french SH1_9ef84186:

    # hero "And, well... What about your... uhh... b... breasts?"
    hero "Et... En ce qui concerne tes... ta... poitrine ?"

# game/dialogs.rpy:1596
translate french SH1_28e89323:

    # hero "I see that y-your chest is... Well..."
    hero "Parce que... Je vois que... Il y a..."

# game/dialogs.rpy:1597
translate french SH1_7622ed66:

    # s "Mmm? Oh... {p}Just a bra with some cotton inside..."
    s "Mmm ? Oh...{p}Un soutien-gorge avec du coton..."

# game/dialogs.rpy:1598
translate french SH1_38942254:

    # hero "Oh...why do that?"
    hero "Oh... Pourquoi ?"

# game/dialogs.rpy:1599
translate french SH1_18c07f8f:

    # s "I've always wanted real breasts and would like to wear a bra..."
    s "J'ai toujours voulu avoir une poitrine et porter un soutien-gorge."

# game/dialogs.rpy:1600
translate french SH1_f56274ef:

    # s "They don't look big as Rika-chan's but it's enough to make me feel like I have breasts."
    s "Ils ne sont pas aussi gros que ceux de Rika-chan, mais assez pour me sentir femme."

# game/dialogs.rpy:1601
translate french SH1_972441ce:

    # hero "Oh, on the same subject...Have you ever thought about doing... surgery?"
    hero "En parlant de ça... Tu as déjà songé à la...chirurgie ?"

# game/dialogs.rpy:1602
translate french SH1_a711c810:

    # s "Yes, I've thought about it..."
    s "J'y ai pensé..."

# game/dialogs.rpy:1603
translate french SH1_a18ea8c8:

    # s "But I don't want to... I'm too afraid of surgery!"
    s "Mais je ne veux pas. J'ai trop peur des scalpels !"

# game/dialogs.rpy:1604
translate french SH1_27bb92e4:

    # "The way she said that sounded pretty cute but she seemed pretty frustrated."
    "Elle semblait frustrée du fait. Mais sa manière de le dire était mignonne."

# game/dialogs.rpy:1605
translate french SH1_b5f35068:

    # "...huh? What the heck am I saying? It's a guy!"
    "...Mais de quoi je parle ? C'est un mec !"

# game/dialogs.rpy:1606
translate french SH1_d25154cc:

    # "I can't fall in love with a dude! Even if he looks like a girl!"
    "Je ne peux pas tomber amoureux d'un mec, même s'il a l'air d'une fille !"

# game/dialogs.rpy:1607
translate french SH1_86c14a59:

    # "Even if he looks like...{w}such a wonderful girl...{w}who seems so fragile in the shadow..."
    "Même s'il a l'air...{w}d'une magnifique jeune fille...{w}qui semble si douce et fragile dans l'ombre..."

# game/dialogs.rpy:1608
translate french SH1_31273a30:

    # "Ugh dammit! What should I do?! I'm still confused!!!"
    "Bon sang, que dois-je faire ?! Je suis perdu !!"

# game/dialogs.rpy:1612
translate french SH1_7072f3b4:

    # "No..."
    "Non..."

# game/dialogs.rpy:1613
translate french SH1_7eb4948e:

    # "No way... I can't! I am a pure and straight guy..."
    "Pas moyen... Je ne peux pas ! Je suis pûrement hétéro..."

# game/dialogs.rpy:1614
translate french SH1_5b52ab19:

    # "But whatever...{p}What's the big deal?"
    "Mais qu'importe... Où est le problème ?"

# game/dialogs.rpy:1615
translate french SH1_81380db8:

    # "It's a boy? Okay. So what? I'm just fixed on her gender."
    "C'est un garçon ? D'accord. Et puis ? Je suis juste fixé sur son genre..."

# game/dialogs.rpy:1616
translate french SH1_27c4c879:

    # "But that doesn't change much. I can't love her, but we're still friends!"
    "Ça ne change rien d'autre. Je ne peux pas l'aimer, mais je peux rester son ami !"

# game/dialogs.rpy:1617
translate french SH1_d4af9d2c:

    # "With such a big secret between us, I feel like we're even closer friends now!"
    "Et avec un tel secret partagé, je sens être devenu un ami très proche !"

# game/dialogs.rpy:1618
translate french SH1_2b941147:

    # hero "Oh...Now I understand why you refused the kiss earlier..."
    hero "Oh...Je comprends maintenant pourquoi tu n'as pas voulu qu'on s'embrasse..."

# game/dialogs.rpy:1619
translate french SH1_508719ae:

    # hero "It's because you didn't want to break my heart, huh?"
    hero "Tu craignais de briser mon cœur en découvrant la vérité, n'est-ce pas ?"

# game/dialogs.rpy:1620
translate french SH1_e17592e6:

    # s "Yes, exactly..."
    s "Oui, exactement..."

# game/dialogs.rpy:1621
translate french SH1_1f18b742:

    # hero "You know, I feel happy, now."
    hero "Tu sais, je me sens heureux, maintenant."

# game/dialogs.rpy:1623
translate french SH1_5ab99daa:

    # "Sakura turns around to face me."
    "Sakura me fit face."

# game/dialogs.rpy:1624
translate french SH1_51437fd0:

    # s "Eh? Why is that?"
    s "Ah ? Comment cela ?"

# game/dialogs.rpy:1625
translate french SH1_d89b0eb5:

    # hero "Because you shared your secret with me. And that you didn't want to break my heart."
    hero "Parce que tu m'as partagé ton grand secret. Et que tu n'as pas voulu briser mon cœur."

# game/dialogs.rpy:1626
translate french SH1_1a3b0477:

    # hero "I feel like you must trust me a lot. And that means that you consider me a close friend..."
    hero "Je sens vraiment que tu me fais confiance. Et que je compte comme un ami très spécial pour toi... Tu es d'une gentillesse incroyable..."

# game/dialogs.rpy:1627
translate french SH1_d9d841a7:

    # hero "I'm happy to be your friend, Sakura-chan."
    hero "Je suis heureux d'être ton ami, Sakura-chan."

# game/dialogs.rpy:1629
translate french SH1_6caf3c5b:

    # "Sakura didn't say anything for a moment..."
    "Sakura ne dit rien un moment..."

# game/dialogs.rpy:1630
translate french SH1_bdc6ac8a:

    # "Finally she gave me a big smile. She looked like she was going to cry from happiness."
    "Finalement, elle montra un sourire. Je sentis qu'elle était au bord des larmes de joie."

# game/dialogs.rpy:1633
translate french SH1_3f0e1dcd:

    # s "Thank you, %(stringhero)s-kun!"
    s "Merci, %(stringhero)s-kun !"

# game/dialogs.rpy:1634
translate french SH1_9db5fc3e:

    # s "I'm so glad that you took the time to understand me! {p}And I'm happy to be your friend, too!"
    s "Je suis si heureuse que tu me comprennes !{p}Et si heureuse d'être ton amie aussi !"

# game/dialogs.rpy:1635
translate french SH1_cc14d6a4:

    # hero "I promise, I'll keep your secret safe."
    hero "Et je te le promets. Je garderai ton secret pour toujours."

# game/dialogs.rpy:1636
translate french SH1_639e5f3c:

    # "We held our pinky fingers together to seal the promise. We both gave each other a big smile."
    "On serra nos petits doigts pour sceller la promesse. On sourit l'un envers l'autre."

# game/dialogs.rpy:1637
translate french SH1_3c805b45:

    # "Maybe she's a he... but she's a friend first and foremost."
    "C'est peut-être un garçon... Mais c'est avant tout un ami."

# game/dialogs.rpy:1643
translate french SH1_c8994519:

    # "We reached her house talking happily along the way about a variety of things."
    "On rejoignit sa maison en parlant de choses et d'autres."

# game/dialogs.rpy:1644
translate french SH1_fa80129a:

    # "We were enjoying our time, as the best of friends."
    "On profitait de notre temps, en meilleurs amis."

# game/dialogs.rpy:1645
translate french SH1_d62f35ea:

    # hero "I wonder how Rika-chan will react when she realizes I know your secret too."
    hero "Je me demande ce que va penser Rika quand elle saura que tu sais aussi."

# game/dialogs.rpy:1647
translate french SH1_589baf2e:

    # s "Teehee! She will probably try to hit on you, then! {image=heart.png}"
    s "Hihi ! Elle va peut-être essayer de te draguer ! {image=heart.png}"

# game/dialogs.rpy:1648
translate french SH1_d5bffef3:

    # hero "Gah! That's a bit cruel, Sakura-chan!"
    hero "Arf ! Parle pas de malheur, Sakura-chan !"

# game/dialogs.rpy:1650
translate french SH1_b112bb86:

    # hero "...Although you're probably right..."
    hero "...Mais t'as peut-être raison..."

# game/dialogs.rpy:1651
translate french SH1_2a493962:

    # "We both laughed."
    "On rit ensemble un peu."

# game/dialogs.rpy:1653
translate french SH1_7a9a62fd:

    # "Something crossed my mind though..."
    "Puis quelque chose me traversa l'esprit..."

# game/dialogs.rpy:1655
translate french SH1_2100c01f:

    # hero "Sakura-chan?"
    hero "Sakura-chan..."

# game/dialogs.rpy:1656
translate french SH1_282f01ee:

    # hero "You know..."
    hero "Tu sais..."

# game/dialogs.rpy:1657
translate french SH1_e9e23b04:

    # hero "Nanami-chan...{p}She really seems to care about you. More than you think."
    hero "Nanami-chan...{p}Elle tient vraiment beaucoup à toi. Plus que tu ne l'imagines."

# game/dialogs.rpy:1658
translate french SH1_82f05805:

    # hero "So... Maybe it's time for her to know the truth as well."
    hero "Alors... Peut-être qu'il faudrait aussi lui dire la vérité."

# game/dialogs.rpy:1660
translate french SH1_37bc0b4d:

    # "Sakura started to think."
    "Sakura réfléchit."

# game/dialogs.rpy:1661
translate french SH1_eef310db:

    # s "I know...{p}I really do want to tell her..."
    s "Je sais...{p}Et j'ai vraiment envie de le lui dire..."

# game/dialogs.rpy:1662
translate french SH1_08cb90bc:

    # s "The fact is... I'm so scared of her reaction..."
    s "Mais... J'ai tellement peur de sa réaction..."

# game/dialogs.rpy:1663
translate french SH1_337166d2:

    # hero "Hmmm...I can understand."
    hero "Hmmm...Je comprends."

# game/dialogs.rpy:1664
translate french SH1_93f8ec4b_1:

    # s "Well..."
    s "Eh bien..."

# game/dialogs.rpy:1665
translate french SH1_d9c81ff9:

    # s "I don't know..."
    s "Je sais pas..."

# game/dialogs.rpy:1666
translate french SH1_aac62100:

    # hero "There's no reason to be scared, Sakura-chan."
    hero "Il n'y a pas raison d'avoir peur, Sakura-chan."

# game/dialogs.rpy:1667
translate french SH1_45627ed5:

    # hero "Listen..."
    hero "Ecoute..."

# game/dialogs.rpy:1669
translate french SH1_ad02dfe3:

    # hero "I'll talk to Rika-chan. Next Monday, let's tell Nanami-chan when we're all together at the club."
    hero "Je parlerai à Rika-chan. Lundi prochain, on le dira tous les trois à Nanami, au club."

# game/dialogs.rpy:1670
translate french SH1_82ea3c85:

    # hero "Rika and I will be there to support you."
    hero "Comme ça, Rika et moi serons là pour te soutenir."

# game/dialogs.rpy:1671
translate french SH1_f63d0a16:

    # hero "Nanami-chan is your close friend, right? She has the right to know too. I'm sure she'll understand."
    hero "Nanami-chan est une amie proche, n'est-ce pas ? Elle a le droit de savoir. Je suis sûr qu'elle comprendra."

# game/dialogs.rpy:1672
translate french SH1_c91ce444:

    # hero "You must take your life in your own hands, Sakura-chan!!!"
    hero "Tu dois prendre ton courage à deux mains, Sakura-chan !"

# game/dialogs.rpy:1673
translate french SH1_938668c5:

    # "Sakura laughed at the lame line I said and finally smiled."
    "Sakura réfléchit un moment encore, puis sourit finalement."

# game/dialogs.rpy:1675
translate french SH1_757d342d:

    # s "Alright. Let's do this!"
    s "Oui... Faisons-le !"

# game/dialogs.rpy:1676
translate french SH1_6a86d9b7:

    # hero "On Monday then."
    hero "Prépare-toi bien pour lundi."

# game/dialogs.rpy:1677
translate french SH1_47e672b9:

    # hero "Don't forget: Rika and I will be there for you. You won't be alone!"
    hero "Et n'oublie pas : Moi et Rika serons là pour toi. Tu ne seras pas seule !"

# game/dialogs.rpy:1678
translate french SH1_69496358:

    # s "Thank you, %(stringhero)s-kun!{p}I'll be as brave as I can!"
    s "Merci, %(stringhero)s-kun!{p}Je serai prête !"

# game/dialogs.rpy:1681
translate french SH1_e029963b:

    # "She giggled and I waved goodbye to her as she went back to her house."
    "Elle rit et me fit au revoir de la main en rentrant chez elle."

# game/dialogs.rpy:1682
translate french SH1_5feddf4e:

    # "She is really unique. I'm glad to have a friend like her."
    "Elle est vraiment unique. J'ai de la chance d'avoir une amie aussi unique."

# game/dialogs.rpy:1683
translate french SH1_8534ca42:

    # ". . ."
    ". . ."

# game/dialogs.rpy:1684
translate french SH1_aed2ad78:

    # "Although, I was a kinda depressed... I was so in love with her..."
    "J'étais quand même un peu triste... J'étais si amoureux d'elle..."

# game/dialogs.rpy:1685
translate french SH1_051a7c18:

    # "My heart was a bit broken..."
    "J'avais le cœur un peu brisé."

# game/dialogs.rpy:1686
translate french SH1_7af2582c:

    # "But strangely, I wasn't completely devastated."
    "Mais étrangement, je n'étais pas éffondré de chagrin."

# game/dialogs.rpy:1687
translate french SH1_12bd407a:

    # "Despite the fact we can't be lovers, I feel a lot closer to her."
    "Et malgré le fait qu'on ne puisse pas être amoureux, je me sens vraiment tout autant proche d'elle."

# game/dialogs.rpy:1688
translate french SH1_46bb790b:

    # "Plus, it looked like she felt the same... she didn't look that sad either."
    "Et puis, en supposant qu'elle ressentait la même chose, elle n'avait pas l'air d'être triste non plus..."

# game/dialogs.rpy:1689
translate french SH1_a20cefa7_1:

    # "..."
    "..."

# game/dialogs.rpy:1690
translate french SH1_8d8f367d:

    # "Nah... Everything is just fine.{p}Fine and even better."
    "Non... Tout va bien.{p}Tout va vraiment bien."

# game/dialogs.rpy:1696
translate french SH1_e3ea38e8:

    # "Yeah, okay... It's a guy..."
    "Ok alors... C'est un garçon..."

# game/dialogs.rpy:1697
translate french SH1_40db10a1:

    # "So what???"
    "Et alors ???"

# game/dialogs.rpy:1698
translate french SH1_d6afd40a:

    # "Who said that love can only work with the opposite gender!?"
    "Qui a dit que l'amour ne marchait qu'avec le sexe opposé ?!"

# game/dialogs.rpy:1699
translate french SH1_4158b541:

    # "I loved Sakura since the start. I desired her since the beginning."
    "J'aimais Sakura depuis le début. Je la désirais depuis le début."

# game/dialogs.rpy:1700
translate french SH1_12080c5e:

    # "She told me that she's a boy, but I still love and want her! So be it!"
    "Elle m'a dit qu'elle était un garçon mais mon désir et mon amour ne s'est pas arrêté pour autant ! Qu'il en soit ainsi !"

# game/dialogs.rpy:1701
translate french SH1_a337f545:

    # "I love Sakura-chan, gender doesn't matter!"
    "J'aime Sakura, peu importe ce qu'elle est !"

# game/dialogs.rpy:1702
translate french SH1_b5134f34:

    # hero "I understand, now, why you rejected me when I tried to kiss you..."
    hero "Je comprends maintenant pourquoi tu n'as pas voulu qu'on s'embrasse..."

# game/dialogs.rpy:1703
translate french SH1_c7f3c846:

    # s "Yes..."
    s "Oui..."

# game/dialogs.rpy:1704
translate french SH1_690e9f0a:

    # "I smiled and started to come closer to her."
    "Je souris et m'approchai d'elle."

# game/dialogs.rpy:1705
translate french SH1_a66f34d1:

    # s "As a girl in soul, I prefer boys to girls,"
    s "En tant que fille dans mon âme, je préfère les garçons aux filles,"

# game/dialogs.rpy:1706
translate french SH1_a5e73d0f:

    # s "But I was afraid to break our hearts if I do this..."
    s "mais je craignais de briser ton cœur si je me laissais faire..."

# game/dialogs.rpy:1707
translate french SH1_d4993a91:

    # "She felt that I was behind her so she turned around."
    "Elle sentit que j'étais derrière elle et se tourna."

# game/dialogs.rpy:1708
translate french SH1_ef87d060:

    # s "I didn't want to lose my friend, and-"
    s "Je ne voulais pas perdre mon ami et-"

# game/dialogs.rpy:1711
translate french SH1_5cc40c7f:

    # "I don't even let her finish her sentence."
    "Je ne la laissai pas finir sa phrase."

# game/dialogs.rpy:1712
translate french SH1_747a95a0:

    # "I took her in my arms and kissed her."
    "Alors qu'elle parlait, je l'avais prise dans mes bras et l'avait embrassée par surprise."

# game/dialogs.rpy:1713
translate french SH1_de5e4ab3:

    # "She tried to step back for a second, {p}but finally she gave in."
    "Elle tenta de se dégager une demi seconde, {w}puis se laissa faire en fermant les yeux."

# game/dialogs.rpy:1714
translate french SH1_ddcc906a:

    # "Our lips were lovingly united."
    "Nos lèvres s'étaient unies amoureusement."

# game/dialogs.rpy:1715
translate french SH1_96042711:

    # "Yes... I love her."
    "Oui... Je l'aime."

# game/dialogs.rpy:1716
translate french SH1_5b84a0a9:

    # "I don't care about what people say.{p}She's a girl and I love her. It's as simple as that."
    "Je me foutais de ce que les gens diraient.{p}C'est une fille et je l'aime. C'est tout..."

# game/dialogs.rpy:1718
translate french SH1_21f57c0f:

    # "We stopped kissing and Sakura-chan looked me in the eyes."
    "On arrêta de s'embrasser et Sakura me regarda dans les yeux."

# game/dialogs.rpy:1719
translate french SH1_7673b5e7:

    # "I found myself swimming into the endless ocean of beauty her gaze offered me..."
    "Je nageai dans ces deux merveilleux océans..."

# game/dialogs.rpy:1720
translate french SH1_04263bd9:

    # s "... %(stringhero)s... -kun..."
    s "... %(stringhero)s... -kun..."

# game/dialogs.rpy:1721
translate french SH1_752a2eec:

    # hero "... Sa... Sakura-san..."
    hero "... Sa... Sakura-chan..."

# game/dialogs.rpy:1730
translate french SH1_aab57b73:

    # "We kissed again, but this time, even more intensely."
    "Nous nous embrassâmes encore, d'un baiser plus torride et passionné."

# game/dialogs.rpy:1731
translate french SH1_d01f1362:

    # "I found my hands were running down her body. Her body was so womanly, so perfect."
    "Mes mains caressaient son corps si féminin, si parfait..."

# game/dialogs.rpy:1732
translate french SH1_b11b14d6:

    # "Her fingers were ruffling my hair as she was held me tighter."
    "Ses doigts exploraient ma chevelure, elle me serra plus tendrement..."

# game/dialogs.rpy:1733
translate french SH1_fd13e895:

    # "We stayed in the rice fields for a long while. We just couldn't stop..."
    "On resta de longues minutes dans le champ de riz. Silencieusement, en s'embrassant et en se câlinant... Incapables de s'arrêter..."

# game/dialogs.rpy:1734
translate french SH1_4237d00f:

    # "Even though my head was spinning a bit from the situation, I still couldn't stop giving her my love..."
    "Ma tête tournait un peu par la situation. Mais je ne pouvais pas m'arrêter de lui donner mon amour."

# game/dialogs.rpy:1735
translate french SH1_a7d8f7db:

    # "The sun began to set and filled the sky with a beautiful shade of orange as the evening arrived."
    "Le soleil tourna à l'orange quand le soir vint."

# game/dialogs.rpy:1739
translate french SH1_5b186bfb:

    # "It was getting late. I had to let Sakura return home. We held hands as we walked to her house."
    "Nous nous tenions la main alors que je la rammenai chez elle."

# game/dialogs.rpy:1740
translate french SH1_0163bcac:

    # "We were silent. There was no need for words. We were just enjoying every moment together."
    "Nous étions silencieux. Pas besoin de parler. On appréciait juste la présence de l'autre, en amoureux."

# game/dialogs.rpy:1741
translate french SH1_94a25b9b:

    # "My heart was still racing. I was overwhelmed with joy."
    "Mon cœur battait si fort, j'ai cru qu'il exploserait..."

# game/dialogs.rpy:1742
translate french SH1_32219c59:

    # "Suddenly, Sakura held my arm tighter."
    "D'un coup, elle serra mon bras un peu plus fort."

# game/dialogs.rpy:1743
translate french SH1_15b1a829:

    # s "I wonder what Rika-chan will say when she finds out about us!"
    s "Je me demande ce que va penser Rika-chan quand elle va savoir qu'on sort ensemble !"

# game/dialogs.rpy:1745
translate french SH1_164ae856:

    # s "Knowing her, she might try to kill you! Haha!"
    s "Hihi ! Elle va peut-être essayer de te tuer, la connaissant !"

# game/dialogs.rpy:1746
translate french SH1_d0bb62e3:

    # hero "...That's kinda scary to think about..."
    hero "...Ça fait un peu peur, ce que tu dis..."

# game/dialogs.rpy:1747
translate french SH1_d7c1b42b:

    # s "In fact, I've always wondered if she was in love with me or something..."
    s "En fait, je me suis souvent demandé si elle n'était pas un peu amoureuse de moi..."

# game/dialogs.rpy:1748
translate french SH1_555aded3:

    # s "She is the Rika-chan you know since she knows my secret."
    s "Elle est comme tu la connais depuis qu'elle sait mon secret."

# game/dialogs.rpy:1749
translate french SH1_e7066814:

    # hero "Really?"
    hero "Vraiment ?"

# game/dialogs.rpy:1750
translate french SH1_2e63f4b7:

    # hero "Heh... To be honest... She kinda scares me more, now!"
    hero "Heh... Pour être honnête... Elle me fait encore plus peur, maintenant !"

# game/dialogs.rpy:1751
translate french SH1_edeec3ff:

    # "Sakura-chan giggles."
    "Sakura rit."

# game/dialogs.rpy:1752
translate french SH1_c9ae305b:

    # s "Don't worry, I'll protect you! {image=heart.png}"
    s "Ne t'en fais pas. Je te protégerai ! {image=heart.png}"

# game/dialogs.rpy:1753
translate french SH1_9554a6c1:

    # "As we approached the front of her house, she rubbed her head against my shoulder."
    "Elle caressa sa tête contre mon épaule amoureusement alors qu'on arrivait devant chez elle."

# game/dialogs.rpy:1755
translate french SH1_ee198ceb:

    # "I couldn't resist her touch. I took her in my arms and kissed her again."
    "Je n'ai pu résister à cette attention et l'embrassa encore en la prenant dans mes bras."

# game/dialogs.rpy:1756
translate french SH1_ab476564:

    # "She softly kissed me back."
    "Elle me rendit mon baiser tout aussi passionnément."

# game/dialogs.rpy:1758
translate french SH1_8944b322:

    # "When she stopped, she smiled, squeezed my butt and whispered in my ear."
    "Quand on s'arrêta, elle sourit, me pinçant les fesses discrètement et me chuchotant à l'oreille."

# game/dialogs.rpy:1759
translate french SH1_cc8b4089:

    # s "See you tomorrow... sweetheart! {image=heart.png}"
    s "A demain,...mon chéri ! {image=heart.png}"

# game/dialogs.rpy:1760
translate french SH1_30f6ec48:

    # hero "Heh, you're acting more like a guy when you do stuff like that!"
    hero "Héhé, tu sais que c'est plutôt les mecs qui font ce genre de trucs, hein ?"

# game/dialogs.rpy:1761
translate french SH1_f2fe27d5:

    # s "Meanie! {image=heart.png}"
    s "Méchant ! {image=heart.png}"

# game/dialogs.rpy:1763
translate french SH1_31fe93a4:

    # "She sticked her tongue out at me before disappearing into her house..."
    "Elle me tira la langue avant de disparaître dans sa maison..."

# game/dialogs.rpy:1765
translate french SH1_f5ae9218:

    # "I... don't know what happened within me..."
    "Je ne sais pas ce qui m'arriva..."

# game/dialogs.rpy:1766
translate french SH1_506f97de:

    # "I was flooding with energy and happiness!"
    "J'étais rempli d'énergie et de bonheur !"

# game/dialogs.rpy:1767
translate french SH1_9e8998ef:

    # "I ran along the streets."
    "Avant de rentrer chez moi, j'ai couru dans les rues."

# game/dialogs.rpy:1768
translate french SH1_3f79e021:

    # "I ran, I ran, I ran. As fast as I could. Overwhelmed with joy."
    "J'ai couru, couru, couru, aussi vite que possible."

# game/dialogs.rpy:1769
translate french SH1_91e3f3ea:

    # "I wanted to sing. I wanted to laugh. I wanted to dance."
    "Je voulais chanter. Je voulais rire. Je voulais danser."

# game/dialogs.rpy:1770
translate french SH1_eb6be300:

    # "I felt completely crazy!!!"
    "Je crus devenir fou !!"

# game/dialogs.rpy:1771
translate french SH1_375cf97b:

    # "I had never felt that kind of euphoria before..."
    "Je n'avais jamais senti telle euphorie !..."

# game/dialogs.rpy:1773
translate french SH1_e1e2560b:

    # "As I was running home, I passed by Nanami who was coming back from groceries."
    "Dans ma course, je croisai Nanami qui revenait des courses."

# game/dialogs.rpy:1774
translate french SH1_6bbff298:

    # n "Hey, %(stringhero)s-senpai! What's cookin'?"
    n "Hé, %(stringhero)s-senpai ! Ça roule ?"

# game/dialogs.rpy:1775
translate french SH1_bfe48993:

    # "I lifted her off the ground like a small kitten and spun with her around happily!"
    "Je la soulevai comme un chaton et tournoya avec elle !"

# game/dialogs.rpy:1776
translate french SH1_0da09179:

    # hero "I'm so happy, Nanami-chan! So so so so so so {b}SO{/b} happy!"
    hero "Je suis si heureux, Nanami-chan !! Tellement mais {b}TELLEMENT{/b} heureux !!"

# game/dialogs.rpy:1778
translate french SH1_7e7bd642:

    # n "Eeeeek! W-what the heck!? W-w-why you're so happy? You're acting crazy!"
    n "Aaaaaah !! P-p-p-pourquoi ? T'es devenu dingue ou quoi !!"

# game/dialogs.rpy:1779
translate french SH1_c2832101:

    # hero "It's because I..."
    hero "C'est parce que je..."

# game/dialogs.rpy:1780
translate french SH1_192f06a3:

    # "I put back Nanami on the ground."
    "Je reposai Nanami sur ses pieds."

# game/dialogs.rpy:1781
translate french SH1_00ab9ecc:

    # hero "No, wait- I don't want to spoil the surprise."
    hero "Non, attends... Je ne veux pas te gâcher la surprise."

# game/dialogs.rpy:1782
translate french SH1_99c8156c:

    # hero "I'll tell you at school on Monday alongside Rika-chan!"
    hero "Je te le dirai lundi, avec Rika !"

# game/dialogs.rpy:1784
translate french SH1_a79e612d:

    # n "Monday?! But that's too looooong!"
    n "Lundi ?! Mais c'est trop looong !"

# game/dialogs.rpy:1785
translate french SH1_380da8df:

    # n "Now I really want to know!"
    n "Tu m'as donné envie de savoir maintenant !!!"

# game/dialogs.rpy:1786
translate french SH1_5f2414ee:

    # hero "Patience, little one, patience!"
    hero "Patience, ma p'tite, patience !"

# game/dialogs.rpy:1788
translate french SH1_f5bde8dc:

    # "I continued running back home, screaming \"WOOOOOOOOOOOOO!\" out loud. I heard a scream from Nanami-"
    "Et alors que je reprenais ma course avec un \"Banzaaaaaai!\", j'entendis le cri de Nanami, au loin..."

# game/dialogs.rpy:1790
translate french SH1_84fa99ca:

    # n "{size=+15}I'M NOT LITTLE!{/size}"
    n "{size=+15}JE SUIS PAS PETITE !!!{/size}"

# game/dialogs.rpy:1792
translate french SH1_d69bfabe:

    # "My gosh, what a day...!"
    "Wow, quelle journée..."

# game/dialogs.rpy:1793
translate french SH1_0d4f6adf:

    # "Sakura called earlier. I'm going on another date with her tomorrow!"
    "Je me demande comment ce sera demain... Vu que Sakura m'a appelé et proposé qu'on se revoit demain."

# game/dialogs.rpy:1794
translate french SH1_dece2bab:

    # "I was about to ask her out on a date myself...looks like she beat me to it."
    "Ça m'allait très bien, j'avais la même idée, de toutes façons."

# game/dialogs.rpy:1795
translate french SH1_e91f1c64:

    # "Maybe we can read each other's minds now that we're lovers! Anyway...I better get some rest."
    "C'est dingue comment les amoureux peuvent souvent prédire les pensées de leurs partenaires !"

# game/dialogs.rpy:1805
translate french sceneS1_b5e51d3c:

    # "I was waiting for Sakura at the train station of the village."
    "J'attendais Sakura à la gare du village."

# game/dialogs.rpy:1806
translate french sceneS1_6f70efee:

    # "I was getting used to these trips, now. But I'm a bit tired today."
    "Je m'étais habitué à ces voyages. Mais j'étais un peu fatigué."

# game/dialogs.rpy:1807
translate french sceneS1_6a4ff20d:

    # "I couldn't sleep at all last night...I was full of excitement thinking about our lovely date."
    "J'ai eu du mal à dormir cette nuit... J'étais tellement excité par l'idée de la retrouver le lendemain..."

# game/dialogs.rpy:1808
translate french sceneS1_32c4a28b:

    # "Then a joyful Sakura appeared! She was listening to music on a CD player."
    "Sakura arriva, joyeusement. Elle écoutait de la musique dans un balladeur CD."

# game/dialogs.rpy:1809
translate french sceneS1_cfc9f30e:

    # "When she saw me, a big smile filled her face. She turned off the CD player and walked toward me."
    "Quand elle me vit, elle sourit, éteignit son balladeur et s'approcha de moi."

# game/dialogs.rpy:1811
translate french sceneS1_641c11ba:

    # s "%(stringhero)s-kun!! Sorry for the wait! {image=heart.png}"
    s "%(stringhero)s-kun !! Désolé pour l'attente ! {image=heart.png}"

# game/dialogs.rpy:1812
translate french sceneS1_a5707cad:

    # hero "Good morning, Sakura-chan!"
    hero "Bonjour, ma Sakura-chan !"

# game/dialogs.rpy:1814
translate french sceneS1_bd4c0f66:

    # "We embraced each other with a kiss."
    "On s'embrassa tendrement."

# game/dialogs.rpy:1815
translate french sceneS1_a85140f3:

    # "Darn, I missed these sweet lips so much."
    "Comme ces lèvres douces ont pu me manquer."

# game/dialogs.rpy:1816
translate french sceneS1_0e294c8c:

    # "I had a feeling she had put on a little more makeup on her face for the occasion."
    "J'avais l'impression qu'elle s'était un peu maquillée pour l'occasion."

# game/dialogs.rpy:1818
translate french sceneS1_2c25f5b4:

    # hero "You look wonderful today Sakura-chan."
    hero "Tu as l'air très en beauté ce matin, Sakura-chan."

# game/dialogs.rpy:1819
translate french sceneS1_280a32f7:

    # "She was overflowing with happiness."
    "Elle rayonait de bonheur comme jamais."

# game/dialogs.rpy:1820
translate french sceneS1_f3a61c54:

    # "I have never seen her so happy before."
    "Je ne l'avais jamais vu comme ça avant."

# game/dialogs.rpy:1822
translate french sceneS1_1bc30b12:

    # s "Oh, does that mean I'm not pretty everyday...?"
    s "Hmm, ça veut dire que je ne suis pas jolie les autres jours ?"

# game/dialogs.rpy:1823
translate french sceneS1_103429d2:

    # hero "Ah! N-no, I mean, you're as pretty as usual... I mean you are always..."
    hero "Argh ! Non, je veux dire, T'es aussi jolie que... Enfin, tu vois,..."

# game/dialogs.rpy:1825
translate french sceneS1_ee5f4b9e:

    # "Sakura gave me a mischievous grin and gave me a quick kiss on the lips."
    "Sakura fit un sourire coquin et me fit un baiser sur les lèvres."

# game/dialogs.rpy:1826
translate french sceneS1_7641d5e7:

    # s "Silly... I'm just messing with you! {image=heart.png}"
    s "Idiot... C'était pour rire ! {image=heart.png}"

# game/dialogs.rpy:1827
translate french sceneS1_fe43f145:

    # hero "Gah! That's too cruel! {image=heart.png}"
    hero "Arf ! Pas cool, Sakura-chan ! {image=heart.png}"

# game/dialogs.rpy:1828
translate french sceneS1_73ac0c99:

    # "We laughed together as the train arrived in the station."
    "On rit ensemble alors que le train entra en gare."

# game/dialogs.rpy:1833
translate french sceneS1_c93ea523:

    # "We sat on the train, going towards the grand city."
    "Nous étions assis dans le train, en route pour la grande ville."

# game/dialogs.rpy:1834
translate french sceneS1_2af5fbd9:

    # "I was wondering about her CD player, so I decided to ask her."
    "Je me demandais ce qu'elle écoutait sur son balladeur alors je lui demandai."

# game/dialogs.rpy:1835
translate french sceneS1_95d5b90a:

    # hero "What were you listening to on your CD player?"
    hero "C'est quoi que tu écoutais sur ton CD ?"

# game/dialogs.rpy:1836
translate french sceneS1_d701c031:

    # s "Oh, some classical music."
    s "Oh, de la musique classique."

# game/dialogs.rpy:1837
translate french sceneS1_9071067a:

    # hero "You like classical music?"
    hero "Tu aimes la musique classique ?"

# game/dialogs.rpy:1838
translate french sceneS1_5ad3424b:

    # s "I love it!"
    s "J'adore !"

# game/dialogs.rpy:1839
translate french sceneS1_302806ba:

    # "I'm not especially a fan of that kind of music but I will admit that it's good to listen to sometimes. It can be very relaxing."
    "Je ne suis pas vraiment fan de ce style. Mais j'admets que c'est agréable à écouter pour se relaxer."

# game/dialogs.rpy:1840
translate french sceneS1_865dd143:

    # hero "What's your favorite song?"
    hero "C'est laquelle, ta musique préférée ?"

# game/dialogs.rpy:1842
translate french sceneS1_ae336353:

    # "Sakura gave me one side of her headphones and started the CD player."
    "Sakura sourit, me passa un des écouteurs et démarra le lecteur CD."

# game/dialogs.rpy:1845
translate french sceneS1_8534ca42:

    # ". . ."
    ". . ."

# game/dialogs.rpy:1846
translate french sceneS1_083198ae:

    # hero "Oh, I know this song... but I don't remember its name..."
    hero "Oh, je connais ce truc... J'ai pas le nom en tête, mais je connais..."

# game/dialogs.rpy:1847
translate french sceneS1_ef73b932:

    # s "It's the {i}Air Orchestral suite #3{/i} of Bach."
    s "C'est la {i}Suite pour Orchestre n°3{/i} de Bach."

# game/dialogs.rpy:1848
translate french sceneS1_00935374:

    # s "I don't know why... But I love to listen to this when I'm in a good mood..."
    s "Je ne sais pas pourquoi... Mais j'aime l'entendre quand je suis heureuse..."

# game/dialogs.rpy:1849
translate french sceneS1_6bbb4eeb:

    # hero "You must be in a pretty good mood then, huh?"
    hero "Et tu es heureuse en ce moment ?..."

# game/dialogs.rpy:1850
translate french sceneS1_1833fef5:

    # "She doesn't reply. She just leans her head on my shoulder and closes her eyes."
    "Elle ne répondit pas. Elle posa simplement sa tête sur mon épaule en fermant les yeux."

# game/dialogs.rpy:1851
translate french sceneS1_bb3922e5:

    # "I gently lean my head against her."
    "Ma tête reposa doucement sur la sienne en retour."

# game/dialogs.rpy:1852
translate french sceneS1_54d8e14e:

    # "Her presence, her warmth, her strangely feminine smell, along with the music..."
    "Sa présence, sa chaleur, son odeur si étrangement féminine, la musique..."

# game/dialogs.rpy:1853
translate french sceneS1_b2bf1fcc:

    # "I wish this train would never stop and that we could stay like this forever."
    "J'aimerais que le train ne s'arrête jamais et que l'on reste comme ça pour toujours."

# game/dialogs.rpy:1858
translate french sceneS1_4bdd421c:

    # hero "...Some things make more sense now that you've told me your secret."
    hero "...Certaines choses ont du sens maintenant que tu m'as dit ton secret."

# game/dialogs.rpy:1860
translate french sceneS1_49c95ed0:

    # s "What do you mean?"
    s "Quoi donc ?"

# game/dialogs.rpy:1861
translate french sceneS1_35d9c8b5:

    # hero "Well, I finally understand why you like shooting games and harem ecchi manga."
    hero "Je comprends maintenant pourquoi tu aimes les jeux de tir et les mangas ecchi."

# game/dialogs.rpy:1862
translate french sceneS1_1dcf5a89:

    # hero "You have some more boyish tastes in you, despite that the rest of you is pretty feminine."
    hero "T'as quelques goûts de garçon, même si tu es une fille."

# game/dialogs.rpy:1863
translate french sceneS1_1ae549d3:

    # s "Yes, it's true."
    s "Oui, c'est vrai."

# game/dialogs.rpy:1864
translate french sceneS1_64d14f00:

    # s "I think I got that from my father."
    s "J'ai eu ça de mon père, je crois."

# game/dialogs.rpy:1865
translate french sceneS1_75219ee1:

    # s "For my last few birthdays, my father bought me shooting games and ecchi manga."
    s "C'est lui qui m'a initié à tout ça, à mes derniers anniversaires."

# game/dialogs.rpy:1866
translate french sceneS1_0d2a822f:

    # s "I guess it just stuck with me..."
    s "Et c'est resté en moi..."

# game/dialogs.rpy:1867
translate french sceneS1_e668a3b6:

    # hero "I see..."
    hero "Je vois..."

# game/dialogs.rpy:1868
translate french sceneS1_0676a71e:

    # hero "How it was when you were younger? Was it difficult?"
    hero "Comment ça a été, dans ta jeunesse ? Difficile ?"

# game/dialogs.rpy:1870
translate french sceneS1_b6297f8f:

    # s "My childhood was kinda chaotic, yes."
    s "Mon enfance a été assez chaotique, oui."

# game/dialogs.rpy:1871
translate french sceneS1_125b816d:

    # s "I was a boy like the others during my first years at the kindergarten."
    s "J'étais un garçon comme les autres pendant la maternelle."

# game/dialogs.rpy:1872
translate french sceneS1_f7b3d45f:

    # s "But the time passed..."
    s "Puis le temps a passé..."

# game/dialogs.rpy:1873
translate french sceneS1_cc5f078f:

    # s "It was clear that I had more girlish tendencies. Very strong ones in fact..."
    s "J'avais clairement, de plus en plus, un comportement féminin."

# game/dialogs.rpy:1876
translate french sceneS1_e1554201:

    # s "..."
    s "..."

# game/dialogs.rpy:1877
translate french sceneS1_146f1b24:

    # s "When I was going through puberty, I clearly expressed my desire to be considered as a real girl."
    s "Quand ma puberté est arrivée, j'ai exprimé clairement mon désir d'être considéré comme une fille."

# game/dialogs.rpy:1878
translate french sceneS1_384c09e4:

    # s "I couldn't help it... My mind was locked in more feminine habits..."
    s "Je n'y pouvais rien... Mon esprit est bloqué là-dessus... Et ma voix n'a pas muée en grandissant."

# game/dialogs.rpy:1879
translate french sceneS1_eb52cb72:

    # s "It looked like my own body was confused and didn't know how to evolve. For example, my voice didn't deepen like a male."
    s "Comme si mon corps était confus et ne savait pas comment évoluer."

# game/dialogs.rpy:1880
translate french sceneS1_167b4ba2:

    # s "So I let my hair grow, pierced my ears, started to wear skirts, used bras and even put stuff inside my bra to simulate real breasts..."
    s "Alors j'ai laissé mes cheveux pousser, percé mes oreilles, commencé à porter des jupes, utilisé des soutien-gorges rembourrés,..."

# game/dialogs.rpy:1881
translate french sceneS1_2476fd15:

    # s "When I looked at myself in a mirror dressed like a girl, I knew definitely that I was one. I wanted to be one... And I will be one."
    s "Quand j'ai vu la fille que j'étais dans le miroir, pour la première fois, j'ai définitivement compris que je voulais en être une... Et que j'en deviendrai une."

# game/dialogs.rpy:1882
translate french sceneS1_e1554201_1:

    # s "..."
    s "..."

# game/dialogs.rpy:1883
translate french sceneS1_a678d317:

    # s "My mother accepted it well. She loves me more than anything and will accept anything as long as it makes me happy."
    s "Ma mère l'a accepté. Elle m'aime plus que tout et accepte tout tant que ça me rend heureuse."

# game/dialogs.rpy:1884
translate french sceneS1_6bc7ecb8:

    # s "But other people didn't want to understand..."
    s "Mais les autres gens ne le comprenaient pas..."

# game/dialogs.rpy:1885
translate french sceneS1_22ca470a:

    # s "My father, my classmates,..."
    s "Mon père, mes camarades de classe,..."

# game/dialogs.rpy:1886
translate french sceneS1_f7a73d77:

    # s "You get the picture...{p}Well to continue on, my first teenage years were horrible..."
    s "Tu vois d'ici le tableau...{p}En gros, mon début d'adolescence a été terrible..."

# game/dialogs.rpy:1887
translate french sceneS1_e0339899:

    # hero "You mean here? In the village?"
    hero "Ici, au village ?"

# game/dialogs.rpy:1888
translate french sceneS1_e40cc235:

    # s "No.{p}We were living in Kyoto before, but we moved out here..."
    s "Non.{p}On vivait à Kyoto avant. Et on a dû déménager ici..."

# game/dialogs.rpy:1889
translate french sceneS1_b830f777:

    # s "When we started our new life, I decided that I would fight to be considered as a girl in the new town."
    s "Quand on a commencé notre nouvelle vie, j'ai décidé de me battre pour être reconnu comme fille dans le nouveau village."

# game/dialogs.rpy:1890
translate french sceneS1_e1a08602:

    # s "Only my parents and Rika-chan know the secret...{p}And you, of course."
    s "Et jusqu'à présent, seuls mes parents et Rika-chan connaissent mon secret...{p}Ainsi que toi."

# game/dialogs.rpy:1891
translate french sceneS1_fdbe45ca:

    # s "And Nanami-chan, on Monday."
    s "Et Nanami-chan, lundi."

# game/dialogs.rpy:1892
translate french sceneS1_e1554201_2:

    # s "..."
    s "..."

# game/dialogs.rpy:1893
translate french sceneS1_92a39690:

    # s "You know, I..."
    s "Tu sais, je..."

# game/dialogs.rpy:1894
translate french sceneS1_8e16ee4d:

    # s "I wish I was a real girl..."
    s "J'aimerais tellement être une vraie fille..."

# game/dialogs.rpy:1895
translate french sceneS1_c10c0ef9:

    # s "I've always wanted to be a girl. {p}I've always wanted to have long hair.{p}I've always wanted to wear dresses..."
    s "J'ai toujours voulu être une fille.{p}D'avoir librement des longs cheveux.{p}De porter des robes..."

# game/dialogs.rpy:1896
translate french sceneS1_d8ba4cdd:

    # s "And I've always wanted... to have a boyfriend..."
    s "...D'avoir un petit ami..."

# game/dialogs.rpy:1897
translate french sceneS1_dbdb5e9d:

    # "I listened to her in complete silence."
    "Je l'écoutai silencieusement."

# game/dialogs.rpy:1898
translate french sceneS1_c2573e27:

    # "As she was remembering her childhood memories, I could see some tears were about to come out from her eyes."
    "Alors qu'elle se rappelait de souvenirs douloureux, quelques larmes perlaient à ses yeux."

# game/dialogs.rpy:1899
translate french sceneS1_64770d3e:

    # "I put my hand on her shoulder and kiss her head."
    "Je mis ma main sur son épaule et lui déposa un baiser sur le front."

# game/dialogs.rpy:1900
translate french sceneS1_f25fd7bb:

    # hero "Sakura-chan..."
    hero "Sakura-chan..."

# game/dialogs.rpy:1902
translate french sceneS1_1ad3847b:

    # hero "I know your secret. But I promise you... {p}To me, you'll always be a girl. A real girl..."
    hero "Je connais ton secret. Mais je te le promets...{p}Pour moi, tu seras toujours une fille. Une vraie fille..."

# game/dialogs.rpy:1904
translate french sceneS1_4ee0f788:

    # hero "I don't care about what other people think.{p}I don't care what your body thinks."
    hero "Je me fous de ce que les gens diront.{p}Je me fous de ce que ton corps dira."

# game/dialogs.rpy:1905
translate french sceneS1_01d48485:

    # hero "I don't even care if your father hates me someday because of us going out together."
    hero "Je me fous même de ton père s'il décide de me haïr parce qu'on sort ensemble toi et moi."

# game/dialogs.rpy:1906
translate french sceneS1_f0c87851:

    # hero "Sakura-chan...You are a girl and I...{p}And I......{p}I........."
    hero "Sakura-chan... Tu es une fille et je...{p}Et je... Je..."

# game/dialogs.rpy:1907
translate french sceneS1_2573e7bb:

    # "I never thought it would be so hard to say it, even if you want to express it so much at the same time."
    "Je n'aurais jamais cru qu'une chose qu'on a autant sur le cœur soit aussi difficile à exprimer ouvertement."

# game/dialogs.rpy:1908
translate french sceneS1_8dca2ab0:

    # "I forced myself to let it all out."
    "Je me forçai à la sortir."

# game/dialogs.rpy:1909
translate french sceneS1_5bec7fb2:

    # hero "I... Sakura-chan, I lo... I lo......{p}{size=+15}I love you, Sakura-chan!!!{/size}"
    hero "Je... Sakura-chan, Je... Je t'...{p}{size=+15}Je t'aime, Sakura-chan !!!{/size}"

# game/dialogs.rpy:1910
translate french sceneS1_c352284f:

    # "I ended up loudly shouting that out."
    "C'est sorti si fort que je l'ai crié en pleine rue !"

# game/dialogs.rpy:1911
translate french sceneS1_68b19ed1:

    # "Some people in the street were so surprised, they stopped to stare at us."
    "Certains passants surpris s'arrêterent en nous regardant."

# game/dialogs.rpy:1912
translate french sceneS1_1496db50:

    # "I felt terribly embarrassed."
    "Je me sentis terriblement gêné."

# game/dialogs.rpy:1913
translate french sceneS1_7bb8b5bf:

    # "Even Sakura turned red with embarrassment."
    "Sakura rougit d'embarassement, elle aussi."

# game/dialogs.rpy:1914
translate french sceneS1_c3bbc64c:

    # "Suddenly I realized the deep sense of what I just said."
    "Soudainement je réalisai le sens profond de ce que j'avais dit."

# game/dialogs.rpy:1915
translate french sceneS1_0c128ea7:

    # "Yes... Yes, I love her...{p}And I'm not afraid to say it!!!"
    "Oui... Oui, je l'aime...{p}Et je n'ai plus peur de le dire !!!"

# game/dialogs.rpy:1916
translate french sceneS1_5d447c9e:

    # hero "Yes everyone!! I love her!!! You hear me??? I love HER!!!"
    hero "Oui, messieurs-dames ! Je l'aime !!! Vous m'entendez ?! Je suis amoureux d'ELLE !!!!"

# game/dialogs.rpy:1917
translate french sceneS1_71bc4397:

    # "Strangers just stared at us awkwardly."
    "Les gens nous regardaient toujours bizarrement."

# game/dialogs.rpy:1918
translate french sceneS1_df63e596:

    # "Sakura was still blushing... But suddenly..."
    "Sakura rougissait toujours... Puis soudainement..."

# game/dialogs.rpy:1919
translate french sceneS1_0f3f7530:

    # "She started to reply, in the same tone as me... Stuttering and trembling a bit but with the same conviction, and as loud as me!"
    "Elle a commencé à répondre, dans le même ton... En bégayant un petit peu mais avec la même volonté et le même volume !"

# game/dialogs.rpy:1920
translate french sceneS1_c4c3f14c:

    # s "Y... Ye...Yes!!!"
    s "O...Ou... Oui !!!"

# game/dialogs.rpy:1921
translate french sceneS1_d1b481d2:

    # s "{size=+10}Yes, I am a girl!...{p}I am a girl and I love you too, %(stringhero)s-kun!!!!{/size}"
    s "{size=+10}Oui, je suis une fille !...{p}Je suis une fille et moi aussi je t'aime, %(stringhero)s-kun !!!!{/size}"

# game/dialogs.rpy:1922
translate french sceneS1_6252a7c4:

    # "People continued to stare at us. But soon, they carried on with their lives, ignoring us."
    "Les gens continuèrent à nous regarder puis s'en allèrent à leurs occupations en nous ignorant."

# game/dialogs.rpy:1923
translate french sceneS1_82c8f385:

    # "I rolled my eyes, appalled at the people."
    "Je roulai des yeux, exaspéré."

# game/dialogs.rpy:1924
translate french sceneS1_7e3455d5:

    # hero "Pfft... People..."
    hero "Hah... Les gens..."

# game/dialogs.rpy:1925
translate french sceneS1_e9431aa5:

    # "Sakura made a strange face... She was holding her mouth while blushing...{p}She was... laughing???"
    "Sakura fit une drôle de tête... Elle se couvrait la bouche en rougissant...{p}Elle...riait ??"

# game/dialogs.rpy:1927
translate french sceneS1_17b9939f:

    # "She starts to laugh loudly. It was a nice laugh. The sound filled me with comfort."
    "Elle commença à rire. Un joli rire musical et réconfortant."

# game/dialogs.rpy:1929
translate french sceneS1_cf3fcfe1:

    # "I was a little surprised, but her laugh made me want to laugh as well."
    "J'étais surpris, mais son rire déclencha le mien."

# game/dialogs.rpy:1930
translate french sceneS1_5da66cff:

    # hero "Hahaha, what's so funny, Sakura-chan?"
    hero "Hahaha, qu'est-ce qu'il y a de drôle, Sakura-chan ?"

# game/dialogs.rpy:1931
translate french sceneS1_8da8d865:

    # "She tried to reply, but couldn't stop laughing."
    "Elle tenta de répondre entre deux éclats de rire."

# game/dialogs.rpy:1932
translate french sceneS1_b3fc2700:

    # s "It's... It's just I'm... I'm relieved now that I said it!!"
    s "C'est... C'est juste que... Je suis tellement soulagée de l'avoir dit !!"

# game/dialogs.rpy:1933
translate french sceneS1_22593ea9:

    # s "And.... the way you rolled your eyes... It was too funny, I couldn't hold it!"
    s "Et... Quand tu as roulé des yeux... C'était trop drôle, j'ai pas pu me retenir !"

# game/dialogs.rpy:1934
translate french sceneS1_917bb3ec:

    # hero "Hey, that's just how I naturally react!"
    hero "Hé, c'est pas cool !"

# game/dialogs.rpy:1935
translate french sceneS1_1809902d:

    # s "Teeheehee! I'm so sorry, %(stringhero)s-kun..."
    s "Hihihihi ! Pardonne moi, %(stringhero)s-kun..."

# game/dialogs.rpy:1936
translate french sceneS1_50395e0e:

    # "I rolled my eyes again without realizing and she started to laugh even louder..."
    "Je roulai des yeux encore une fois sans m'en rendre compte, ce qui lui déclencha un autre fou rire..."

# game/dialogs.rpy:1937
translate french sceneS1_36e4217d:

    # "I began to laugh as well."
    "Je ne sais pas pourquoi, mais j'étais d'humeur à rire aussi."

# game/dialogs.rpy:1938
translate french sceneS1_3b7641fe:

    # "I was so happy."
    "J'étais si heureux."

# game/dialogs.rpy:1940
translate french sceneS1_8efda62d:

    # "She stopped after a while, wiping away her tears of laughter."
    "Elle s'arrêta un moment, essuyant ses larmes de rire."

# game/dialogs.rpy:1941
translate french sceneS1_e15c0017:

    # s "I've never felt happiness like this."
    s "Je n'avais jamais senti un tel bonheur."

# game/dialogs.rpy:1942
translate french sceneS1_1baf5e06:

    # s "Thanks to you, I feel that those sad memories have gone away now."
    s "Grâce à toi, tous ces vilains souvenirs sont repartis."

# game/dialogs.rpy:1943
translate french sceneS1_b27b381b:

    # s "Thank you so much...%(stringhero)s-kun! Thank you!"
    s "Merci, ...%(stringhero)s-kun ! Merci !"

# game/dialogs.rpy:1944
translate french sceneS1_0cb95e5a:

    # "She embraced me tightly like a plushie, like a little girl to her father, with an adorable smile on her lips. I could feel her heart beating against my chest..."
    "Elle m'enlaça tendrement, comme une peluche, comme une petite fille à son père, avec un sourire aux lèvres. Je sentais son cœur battre contre mon corps."

# game/dialogs.rpy:1950
translate french sceneS1_00f63e60:

    # "We had a lot of fun today in the city."
    "On s'est bien amusés en ville, ce jour là."

# game/dialogs.rpy:1951
translate french sceneS1_936e8fa2:

    # "Holding hands, we were on our way back to the station."
    "On était en route pour la gare de la grande ville, en se tenant la main."

# game/dialogs.rpy:1952
translate french sceneS1_eb6c4ac4:

    # "But suddenly, Sakura had stopped."
    "Puis soudain, je sentis Sakura s'arrêter."

# game/dialogs.rpy:1953
translate french sceneS1_e8cdcca5:

    # "I turned around to look at her, while still holding her hand."
    "Je me retournai pour la voir, lui tenant toujours la main."

# game/dialogs.rpy:1955
translate french sceneS1_e9b16d76:

    # "Her expression was blank, then turned to embarrassment. She was beet-root red."
    "Son expression était indescriptible... Mais intimidée... Elle était rouge comme une tomate."

# game/dialogs.rpy:1956
translate french sceneS1_27c88c8d:

    # hero "Something wrong?"
    hero "Quelque chose ne va pas, mon ange ?"

# game/dialogs.rpy:1957
translate french sceneS1_0d15642b:

    # "She pointed toward a building near us..."
    "Elle pointa l'immeuble près de nous."

# game/dialogs.rpy:1958
translate french sceneS1_1c82a33d:

    # "I looked to see, and it was...{w}{size=+5}love hotel!?!{/size}"
    "C'était...{w}{size=+5}un love hotel !!!{/size}"

# game/dialogs.rpy:1959
translate french sceneS1_df826d7c:

    # "Whoa wait what!?"
    "Ouuuh lala !?"

# game/dialogs.rpy:1960
translate french sceneS1_58865132:

    # "Does she want to..."
    "Est-ce qu'elle veut qu'on..."

# game/dialogs.rpy:1961
translate french sceneS1_57d99560:

    # hero "D-do you...you want to...go there?"
    hero "Tu... Tu veux qu'on aille... Là-dedans ?"

# game/dialogs.rpy:1962
translate french sceneS1_be50fa6b:

    # "She nodded and looked down at the ground to hide her shyness. Her hand gripped me tighter."
    "Elle hocha timidement en regardant le sol, sa main serrant un peu plus la mienne. Qu'elle était mignonne."

# game/dialogs.rpy:1964
translate french sceneS1_c7711bb7:

    # hero "S-sure, let's go."
    hero "D-D'accord... Allons-y."

# game/dialogs.rpy:1968
translate french sceneS1_8534ca42_1:

    # ". . ."
    ". . ."

# game/dialogs.rpy:1969
translate french sceneS1_2c0af0e2:

    # "I woke up in the bed of the hotel."
    "Je me réveillai dans la chambre d'hôtel."

# game/dialogs.rpy:1970
translate french sceneS1_1b6c4bc4:

    # "I think I doozed off for only 15 minutes or so. I was feeling pretty sleepy."
    "J'ai du m'assoupir un quart d'heure. Je me sentais un peu crevé."

# game/dialogs.rpy:1971
translate french sceneS1_d36164f4:

    # "In my arms, Sakura was sleeping against me."
    "Dans mes bras, Sakura était aussi assoupie."

# game/dialogs.rpy:1972
translate french sceneS1_9f8a91dc:

    # "She held me tight..."
    "Elle me serrait contre elle..."

# game/dialogs.rpy:1973
translate french sceneS1_f78bfd73:

    # "I guess she must have been exhausted, after so much fun in that room..."
    "Elle devait être aussi épuisée que moi, après ce moment fou dans cette chambre..."

# game/dialogs.rpy:1974
translate french sceneS1_2418a26e:

    # "She woke up still a little drowsy, and spoke with a weary voice,"
    "Elle se réveilla et parla avec une voix un peu fatiguée."

# game/dialogs.rpy:1975
translate french sceneS1_943f038e:

    # s "%(stringhero)s-kun?.."
    s "%(stringhero)s-kun ?.."

# game/dialogs.rpy:1976
translate french sceneS1_36317276:

    # hero "Hmm?"
    hero "Hmm ?"

# game/dialogs.rpy:1977
translate french sceneS1_e490aa43:

    # s "Was it... your first time too...?"
    s "C'était...ta première fois aussi...?"

# game/dialogs.rpy:1978
translate french sceneS1_36fd2a62:

    # hero "Y-yeah..."
    hero "O-ouais..."

# game/dialogs.rpy:1979
translate french sceneS1_f2210395:

    # s "You... Do you regret it?"
    s "Tu... Tu regrettes ?"

# game/dialogs.rpy:1980
translate french sceneS1_13b2e8fe:

    # hero "Me?... No... Why should I?"
    hero "Moi ?... Non.... Pourquoi ? Je devrais ?"

# game/dialogs.rpy:1981
translate french sceneS1_d723f1ce:

    # s "Nevermind..."
    s "Non, pour rien..."

# game/dialogs.rpy:1982
translate french sceneS1_3191b879:

    # s "I'm just... I still can't believe I finally found a boy who accepts me the way I am..."
    s "C'est juste... J'arrive toujours pas à croire que j'ai enfin trouvé un garçon qui m'accepte comme je suis..."

# game/dialogs.rpy:1983
translate french sceneS1_67fabef6:

    # s "You even gave me my first time..."
    s "Accepté au point de m'avoir donné ma première fois..."

# game/dialogs.rpy:1984
translate french sceneS1_f8751689:

    # hero "I'm happy to be your first...{p}And I'm happy that you were my first too."
    hero "Je suis heureux d'être ta première fois...{p}Autant que tu sois ma première."

# game/dialogs.rpy:1985
translate french sceneS1_61308ad3:

    # s "I'm so happy %(stringhero)s-kun... This is almost like a dream..."
    s "Je suis si heureuse, %(stringhero)s-kun... J'ai l'impression de rêver..."

# game/dialogs.rpy:1986
translate french sceneS1_66805949:

    # "We lay there in peaceful silence together..."
    "On resta silencieux un moment..."

# game/dialogs.rpy:1987
translate french sceneS1_0af1e620:

    # "I was completely crazy about her... "
    "J'étais complètement fou d'elle..."

# game/dialogs.rpy:1988
translate french sceneS1_a71c4c3b:

    # "We got lost in each other's eyes."
    "On se regarda dans les yeux intensément."

# game/dialogs.rpy:1989
translate french sceneS1_4a521333:

    # "Her eyes were filled with love and happiness. She smiled at me..."
    "Ses yeux étaient remplis de bonheur et d'amour. Elle me sourit."

# game/dialogs.rpy:1990
translate french sceneS1_dd054cb9:

    # "We ended up making love one more time... then some time later..."
    "On a fait l'amour une fois de plus, après ça..."

# game/dialogs.rpy:1991
translate french sceneS1_0dcaa626:

    # "We took a shower, left the hotel, and took the train home. I walked her home."
    "Puis après une douche, nous sommes partis prendre le train pour chez nous."

# game/dialogs.rpy:1995
translate french sceneS1_2724f4f7:

    # s "See you at school, honey!"
    s "On se revoit à l'école, mon chéri !"

# game/dialogs.rpy:1996
translate french sceneS1_90456e76:

    # hero "See you tomorrow, love..."
    hero "A demain, mon amour..."

# game/dialogs.rpy:1998
translate french sceneS1_adc5375c:

    # "She waved goodbye and entered her house."
    "Elle me fit au revoir en rentrant chez elle."

# game/dialogs.rpy:1999
translate french sceneS1_8c41d669:

    # "So much happened this weekend..."
    "Ce weekend fut vraiment riche en évènements..."

# game/dialogs.rpy:2000
translate french sceneS1_3eb2b85d:

    # "Sakura told me that she was a boy,{p}then we kissed and went on a date,{p}then we did \"it\"..."
    "Sakura m'a rêvélé qu'elle est née garçon,{p}puis on s'est embrassés et on s'est mis ensemble,{p}puis on...l'a fait..."

# game/dialogs.rpy:2001
translate french sceneS1_e36e1081:

    # "I'm so tired... So many things to wrap my head around..."
    "Je suis crevé... Trop de choses à penser..."

# game/dialogs.rpy:2002
translate french sceneS1_45a34284:

    # "I'd like to take the day off tomorrow, but I'll get to see Sakura tomorrow too!"
    "Je prendrais un jour de repos demain si Sakura ne venait pas !"

# game/dialogs.rpy:2010
translate french sceneS1_443e2e2a:

    # r "{size=+15}WHAAAAAAAAAT??????{/size}"
    r "{size=+15}KEEEUWAAAAAHH ??????{/size}"

# game/dialogs.rpy:2016
translate french sceneS1_d1f76e80:

    # r "You... Y-Y-Y-Y-You are lo-lo-lo-lovers?!!!"
    r "Vous...V-v-v-v-vous sortez ensemble ?!!!"

# game/dialogs.rpy:2017
translate french sceneS1_1182fed0:

    # s "Teehee!..."
    s "Hihi !..."

# game/dialogs.rpy:2018
translate french sceneS1_70b77763:

    # hero "Well, yeah..."
    hero "Ben, oui..."

# game/dialogs.rpy:2019
translate french sceneS1_bf2888c1:

    # n "Seriously, guys!!!!"
    n "Sérieux, les gars !!!"

# game/dialogs.rpy:2020
translate french sceneS1_87964ca3:

    # r "B... B-B-B-Bu-Bu-Bu...."
    r "M...M-m-m-m-mais..."

# game/dialogs.rpy:2021
translate french sceneS1_0e57396a:

    # r "I'm... I-I-I....I'm...."
    r "J-j-j-j-e... Je..."

# game/dialogs.rpy:2023
translate french sceneS1_322f4f6b:

    # r "I'm so happy for both of you!!!"
    r "Je suis trop contente pour vous deux !!!"

# game/dialogs.rpy:2025
translate french sceneS1_d35b9154:

    # r "No, wait. I'm not. Not at all."
    r "Non, attends. Je le suis pas."

# game/dialogs.rpy:2026
translate french sceneS1_b5c49fda:

    # r "%(stringhero)s is a damn pervert, he... He will..."
    r "%(stringhero)s est un pervers, il... Il va..."

# game/dialogs.rpy:2027
translate french sceneS1_5d72dbb5:

    # hero "Hey!!!"
    hero "Hé oh !!"

# game/dialogs.rpy:2028
translate french sceneS1_e5583358:

    # n "{size=+10}Seriously, guys!!!!{/size}"
    n "{size=+10}Sérieux, les gars !!!!{/size}"

# game/dialogs.rpy:2029
translate french sceneS1_35b8ccde:

    # "Sakura looked at me and smiles brightly as she turned red."
    "Sakura me regarda et sourit tendrement en rougissant."

# game/dialogs.rpy:2030
translate french sceneS1_124abe51:

    # "I looked right back at her and I smirked. She starts to laugh a little."
    "Je la regardai et rit. Elle me rendit un rire."

# game/dialogs.rpy:2032
translate french sceneS1_df3a5fe2:

    # "Rika sees this exchange and starts to stutter."
    "Rika remarqua l'échange et bégaya."

# game/dialogs.rpy:2033
translate french sceneS1_6151b676:

    # r "No... N-N-No, d-d-d-don't tell me you... Don't tell me you already...."
    r "Non !... N-non n-n-n-ne me dites pas que... Vous avez déjà..."

# game/dialogs.rpy:2034
translate french sceneS1_1d81a8c5:

    # hero "Hmm, alright, we won't tell you, then..."
    hero "Ok ok, on ne te le dira pas alors..."

# game/dialogs.rpy:2040
translate french sceneS1_cc4c3814:

    # r "{size=+15}EEEEEEEEHHHHHHHH!!!{/size}"
    r "{size=+15}EEEEEEEEHHHHHHHH !!!{/size}"

# game/dialogs.rpy:2042
translate french sceneS1_e4cfb08c:

    # n "{size=+15}SERIOUSLY, GUYS!!!!{/size}"
    n "{size=+15}SERIEUX, LES GARS !!!{/size}"

# game/dialogs.rpy:2043
translate french sceneS1_0e6125af:

    # "Rika and Nanami were absolutely shocked."
    "Vous l'avez deviné, Rika et Nanami sont choqués à mort..."

# game/dialogs.rpy:2044
translate french sceneS1_1067a846:

    # "Even after a few days passed, Rika still wasn't over it!"
    "Rika est resté dans cet état pendant trois jours !"

# game/dialogs.rpy:2045
translate french sceneS1_5c7feca9:

    # "And Nanami randomly shouted \"Seriously, guys!!\" at us for the same amount of time!"
    "Et Nanami n'a pas arrêté de dire \"Sérieux, les gars !!\" pendant ce temps !"

# game/dialogs.rpy:2046
translate french sceneS1_9ed40c66:

    # "It was kinda fun to watch."
    "C'était drôle à voir."

# game/dialogs.rpy:2068
translate french secretnanami_d274ba9e:

    # "The next day, Nanami invited me to play videogames at her house"
    "Le jour suivant, Nanami m'a invité à venir jouer chez elle."

# game/dialogs.rpy:2070
translate french secretnanami_d7735442:

    # "Nanami's room was a bit messy."
    "La chambre de Nanami était en bazar."

# game/dialogs.rpy:2071
translate french secretnanami_2bf0b11f:

    # "Her bed and computer setup took up most of the room. Her computer looked more powerful than the eMac I had in my bedroom."
    "A part le lit, la seule grande partie de sa chambre était son ordinateur. Un truc dernier cri, bien plus puissant que mon eMac."

# game/dialogs.rpy:2072
translate french secretnanami_992fbe8e:

    # "There was also an old TV with several gaming consoles plugged in it."
    "Il y avait aussi une vieille télé avec plusieurs consoles branchées dessus."

# game/dialogs.rpy:2073
translate french secretnanami_2bd3e307:

    # "Random bags of snacks and clothes were scattered about. It was a mess, but kind of an impressive mess."
    "I y avait des sacs de bouffe et des fringues partout. C'était vraiment un gros foutoir."

# game/dialogs.rpy:2074
translate french secretnanami_2f4f2a6e:

    # "Nanami seemed a bit embarrassed as I was looking around the room."
    "Nanami rougissait alors que je regardais sa chambre."

# game/dialogs.rpy:2076
translate french secretnanami_4a017f1e:

    # n "D-don't pay any attention to the mess, %(stringhero)s-senpai..."
    n "N-ne fais pas attention au rangement, %(stringhero)s-senpai..."

# game/dialogs.rpy:2077
translate french secretnanami_7a1bf678:

    # hero "It's okay, I don't mind."
    hero "C'est pas grave, j'en ai vu d'autres..."

# game/dialogs.rpy:2078
translate french secretnanami_9b653222:

    # hero "You have so much gaming stuff. It's impressive!"
    hero "T'as tellement de trucs de gaming, c'est impressionnant !"

# game/dialogs.rpy:2079
translate french secretnanami_2b844856:

    # hero "But there's barely any space. Doesn't it feel cramped for you at all?"
    hero "Mais c'est si petit... Tu ne te sens pas serrée, ici ?"

# game/dialogs.rpy:2081
translate french secretnanami_d4d4357b:

    # n "Not at all!"
    n "Pas du tout !"

# game/dialogs.rpy:2082
translate french secretnanami_8d45096b:

    # n "Everything I could ever need is here!"
    n "Il y a tout ce qu'il me faut ici !"

# game/dialogs.rpy:2083
translate french secretnanami_3e97bcd0:

    # hero "Heh, well after all, you're small. Seems perfect for you."
    hero "Heh, après tout, t'es pas bien grande, ça doit aller."

# game/dialogs.rpy:2085
translate french secretnanami_db57c973:

    # n "I'm not that small!"
    n "Je suis pas petite !"

# game/dialogs.rpy:2086
translate french secretnanami_2fc28d42:

    # "I laughed at her adorable reaction. I continued checking out the room."
    "Je ris par sa mignonneté, mes yeux continuant d'explorer cette impressionnante chambre."

# game/dialogs.rpy:2087
translate french secretnanami_51b37813:

    # hero "Your brother isn't here, today?"
    hero "Ton frère n'est pas là aujourd'hui ?"

# game/dialogs.rpy:2089
translate french secretnanami_d2016bd8:

    # n "Toshio-nii? No. He usually works on Sunday. He's only home on Mondays."
    n "Toshio-nii ? Non. Il travaille les dimanches. Il ne se repose que les lundis."

# game/dialogs.rpy:2090
translate french secretnanami_b67b5fcc:

    # n "We have the house to ourselves until 6PM!"
    n "On a la maison pour nous jusqu'à 18 heures !"

# game/dialogs.rpy:2091
translate french secretnanami_84b8bd32:

    # hero "I see."
    hero "Je vois..."

# game/dialogs.rpy:2093
translate french secretnanami_fec633fd:

    # "I noticed a framed picture near her computer desk."
    "Je remarquai alors un cadre photo sur l'écran de son ordi."

# game/dialogs.rpy:2094
translate french secretnanami_03d84c1c:

    # "It looked like it was an old picture of Nanami as a child."
    "C'était une vieille photo de Nanami. Elle avait l'air d'avoir 12 ans dessus."

# game/dialogs.rpy:2095
translate french secretnanami_865a0544:

    # "Beside her, there was an older child. This must be her brother.{p}And behind the both of them, there was an adult couple. Probably her parents..."
    "Près d'elle, il y avait un adolescent. Son frère, j'imagine.{p}Derrière eux, un couple adulte. Sûrement ses parents..."

# game/dialogs.rpy:2098
translate french secretnanami_d381eb8b:

    # "Nanami noticed I was looking at it and came closer. Her pleasant expression faded away."
    "Nanami remarqua que je regardai la photo. Son expression s'assombrit."

# game/dialogs.rpy:2099
translate french secretnanami_3d2aa8bc:

    # n "That's...{w}the last picture I have of my parents."
    n "C'est...{w}la dernière photo que j'ai de mes parents..."

# game/dialogs.rpy:2100
translate french secretnanami_2462192a:

    # hero "You mean... They're gone? Are they divorced or something?"
    hero "Tu veux dire... Ils sont partis ? Divorcés ou même chose ?"

# game/dialogs.rpy:2102
translate french secretnanami_3ac96ee3:

    # "She turned away from me. I faced her back, and she began to speak..."
    "Elle me tourna le dos. Je regardai son dos, l'écoutant..."

# game/dialogs.rpy:2103
translate french secretnanami_03e340ee:

    # n "No..."
    n "Non..."

# game/dialogs.rpy:2104
translate french secretnanami_ad2cdc06:

    # n "They are...{w}dead."
    n "Ils sont...{w}morts."

# game/dialogs.rpy:2106
translate french secretnanami_cc9f4dc4:

    # "I stayed motionless and was speechless. Shocked by the revelation."
    "Je restai pétrifié, choqué par sa révélation."

# game/dialogs.rpy:2107
translate french secretnanami_2c0862cf:

    # hero "I-... I'm so sorry to hear that, Nanami-chan... I didn't mean to bring up a sore subject..."
    hero "Je... Je suis désolé, Nanami-chan..."

# game/dialogs.rpy:2109
translate french secretnanami_80110a90:

    # "Nanami sat on the floor, holding her legs."
    "Nanami s'assit par terre."

# game/dialogs.rpy:2111
translate french secretnanami_eb85d77c:

    # n "You know...They were working at the grocery store your parents are managing today."
    n "Tu sais... Ils travaillaient avant dans l'épicerie que tes parents ont récupéré aujourd'hui."

# game/dialogs.rpy:2112
translate french secretnanami_de2b803f:

    # hero "What...!?"
    hero "Hein...!?"

# game/dialogs.rpy:2113
translate french secretnanami_59529ad1:

    # "Just then, I remembered something. My parents got the shop because the previous owners hadn't been around for a few years."
    "Je me souvins que mes parents avaient pu avoir ce magasin car il avait été abandonné depuis des années par leurs anciens propriétaires."

# game/dialogs.rpy:2114
translate french secretnanami_9fec9592:

    # "I didn't think they were actually gone..."
    "Je ne pensais pas qu'ils étaient métaphoriquement partis..."

# game/dialogs.rpy:2115
translate french secretnanami_ee152b17:

    # "I sat next to Nanami."
    "Je m'assis près de Nanami."

# game/dialogs.rpy:2116
translate french secretnanami_b4b7b899:

    # n "It happened four years ago."
    n "C'est arrivé il y a 4 ans."

# game/dialogs.rpy:2117
translate french secretnanami_f2bc7b70:

    # n "It... It was on my brother's birthday."
    n "C'était... à l'anniversaire de mon frère."

# game/dialogs.rpy:2118
translate french secretnanami_90a4c3c8:

    # n "They went to the city to get a surprise present for him..."
    n "Ils sont allés dans la grande ville pour lui acheter un cadeau..."

# game/dialogs.rpy:2119
translate french secretnanami_1a38e7d1:

    # n "But they never came back..."
    n "Ils ne sont jamais revenus..."

# game/dialogs.rpy:2120
translate french secretnanami_728dd651:

    # n "I heard they were hit by a truck when they were coming back..."
    n "J'ai entendu dire qu'un camion leur était rentré dedans à leur retour..."

# game/dialogs.rpy:2121
translate french secretnanami_28efb3c9:

    # n "Toshio-nii told me. He was the one to get the phone call about my parents."
    n "Toshio-nii me l'a dit. C'est lui qui avait décroché le téléphone à ce moment."

# game/dialogs.rpy:2123
translate french secretnanami_e75dc79b:

    # n "You know... My brother, he...{p}he changed a lot after that."
    n "Tu sais,... Mon frère, il...{p}Il a changé à partir de ce moment."

# game/dialogs.rpy:2124
translate french secretnanami_d070d077:

    # n "He blames himself for the death of our parents."
    n "Il croit qu'il est responsable de la mort de nos parents."

# game/dialogs.rpy:2125
translate french secretnanami_d8670523:

    # n "I told him that wasn't true. That it's not his fault."
    n "Je lui ai dit que ce n'était pas vrai. Que ce n'était pas sa faute."

# game/dialogs.rpy:2126
translate french secretnanami_65090fe7:

    # hero "That's right. It's not his fault... It was the fate..."
    hero "C'est vrai. Ce n'est pas sa faute... C'était le destin..."

# game/dialogs.rpy:2127
translate french secretnanami_b6f21cea:

    # n "But he doesn't listen."
    n "Mais il n'écoute rien."

# game/dialogs.rpy:2128
translate french secretnanami_76cf9acc:

    # n "He's become a little unstable because of that. Sometimes, he scares me a bit."
    n "Il est devenu un peu fou à cause de ça. Parfois il me fait peur."

# game/dialogs.rpy:2129
translate french secretnanami_d1c2fbdd:

    # "I instantly thought about Sakura's relationship with her father. But Nanami corrected me immediately like she knew what I was thinking."
    "Je pensai instantanément au père de Sakura. Mais Nanami me corrigea rapidement, comme si elle avait deviné mes pensées."

# game/dialogs.rpy:2130
translate french secretnanami_d01d8042:

    # n "He doesn't beat me or argue with me... It's more like the opposite..."
    n "Il n'est pas méchant avec moi. C'est plutôt le contraire..."

# game/dialogs.rpy:2131
translate french secretnanami_d40e7776:

    # n "He's become over-protective of me."
    n "Il est devenu surprotecteur..."

# game/dialogs.rpy:2132
translate french secretnanami_43d2f391:

    # n "He says we're each other's only family now and that we have to take good care of each other."
    n "Il dit qu'on ne peut plus compter que l'un sur l'autre et qu'on doit prendre soin l'un de l'autre."

# game/dialogs.rpy:2133
translate french secretnanami_f8c997a1:

    # hero "You don't have any grand-parents?"
    hero "Tu n'as pas de grands-parents ?"

# game/dialogs.rpy:2134
translate french secretnanami_8fcd3561:

    # n "My father's parents passed away before I was born."
    n "Les parents de mon père sont morts avant ma naissance."

# game/dialogs.rpy:2135
translate french secretnanami_eb73e5cd:

    # n "My mother's parents are still alive... But they're all the way in Okinawa."
    n "Ceux de ma mère vivent toujours... Mais ils résident à Okinawa."

# game/dialogs.rpy:2136
translate french secretnanami_4b6a67a0:

    # n "We write to them, from time to time..."
    n "On leur écrit de temps en temps..."

# game/dialogs.rpy:2137
translate french secretnanami_eb3d403f:

    # n "Afterwards, Toshio-nii took a job to afford the house rent and our other needs."
    n "Toshio-nii a pris un boulot pour subvenir aux besoins de la maison."

# game/dialogs.rpy:2138
translate french secretnanami_e668a3b6:

    # hero "I see..."
    hero "Je vois..."

# game/dialogs.rpy:2139
translate french secretnanami_a20cefa7:

    # "..."
    "..."

# game/dialogs.rpy:2140
translate french secretnanami_0b1702b3:

    # "Darn, I don't know what to say."
    "Mince, je ne savais pas quoi dire."

# game/dialogs.rpy:2141
translate french secretnanami_12251861:

    # "Losing your parents at this age must be traumatic. I can barely imagine it."
    "Perdre ses parents à cett âge là doit être une éxpérience horrible."

# game/dialogs.rpy:2142
translate french secretnanami_1d53eed8:

    # "I felt like Nanami was about to cry."
    "Je sentis que Nanami commençait à pleurer."

# game/dialogs.rpy:2143
translate french secretnanami_966d23a4:

    # "I wrapped an arm around her shoulder and she started to sob silently in my arms."
    "J'ai instinctivement mis un bras sur ses épaules et elle pleura silencieusement dans mes bras."

# game/dialogs.rpy:2144
translate french secretnanami_ee5f81b5:

    # "She must have greatly loved her parents..."
    "Elle devait vraiment aimer ses parents... La pauvre..."

# game/dialogs.rpy:2146
translate french secretnanami_8b459908:

    # "After a while, she stopped crying. She wiped her tears and smiled at me."
    "Après un moment, elle s'arrêta de pleurer, essuya ses larmes et me sourit."

# game/dialogs.rpy:2150
translate french secretnanami_42d6bc4c:

    # n "Only a few people know the truth about my parents. Sakura-nee, Rika-nee... And now, you too."
    n "Peu de gens savent ce qui s'est vraiment passé. Sakura-nee, Rika-nee... Et toi aussi, maintenant."

# game/dialogs.rpy:2151
translate french secretnanami_0fc2108d:

    # n "People think our parents have gone back to Okinawa, since Toshio-nii is now an adult."
    n "La plupart des gens pensent qu'ils sont repartis à Okinawa, vu que Toshio-nii est un adulte."

# game/dialogs.rpy:2156
translate french secretnanami_511241cb:

    # "So that's what Sakura was talking about at the arcade..."
    "Alors c'était donc ça, ce dont parlait Sakura dans la salle d'arcade..."

# game/dialogs.rpy:2161
translate french secretnanami_c3c4b0be:

    # hero "How are you feeling now? Are you okay?"
    hero "Comment tu te sens, mainteant ? Ça va ?"

# game/dialogs.rpy:2162
translate french secretnanami_8ed55284:

    # n "Yeah... I'm okay, %(stringhero)s-nii."
    n "Oui... Je vais mieux, %(stringhero)s-nii."

# game/dialogs.rpy:2164
translate french secretnanami_c259ee4e:

    # "I smiled and gently pat her head."
    "Je souris en lui caressant la tête gentiment."

# game/dialogs.rpy:2165
translate french secretnanami_8f2bc02b:

    # hero "So, are we playing some games? You did invite me for that."
    hero "Alors, à quoi on joue ? Je suppose que tu m'as invité pour du jeu vidéo, non ?"

# game/dialogs.rpy:2167
translate french secretnanami_7b9494dd:

    # n "Sure! Let me show you what I have."
    n "Oui ! Je vais te montrer ce que j'ai."

# game/dialogs.rpy:2169
translate french secretnanami_b5ed2554:

    # "Some time later, we were playing a fighting game called {i}Mortal Battle{/i}."
    "Un peu plus tard, on était en train de jouer à un jeu de baston nommé {i}Mortal Battle{/i}."

# game/dialogs.rpy:2170
translate french secretnanami_b69a371f:

    # "This was an American game known to have complicated and unconventional special attacks. It's hard to get used to it when you mostly play Japanese fighting games."
    "Ce jeu américain était connu pour avoir des coups spéciaux compliqués à faire et non-conventionnels. C'est dur de s'y habituer quand on joue à des jeux de baston japonais."

# game/dialogs.rpy:2171
translate french secretnanami_1145ba4f:

    # "But I ended up mastering one of the characters in a short amount of time."
    "Mais je maîtrise plutôt bien un des personnages de ce jeu-là."

# game/dialogs.rpy:2172
translate french secretnanami_c49f161e:

    # "Every time I selected this character, I ended up beating the crap out of Nanami's character."
    "A chaque fois que je le prenais, je gagnais contre le perso de Nanami."

# game/dialogs.rpy:2174
translate french secretnanami_073340cb:

    # n "Eeeeeh! How did you do that special attack?!"
    n "Eeeeeh ! Mais comment tu fais ce combo là ?!"

# game/dialogs.rpy:2175
translate french secretnanami_0883b3a9:

    # hero "It's not too hard. You just roll the stick like this, then like this and you press HK."
    hero "C'est pas difficile. Faut juste rouler le stick comme ça, puis comme ça, et t'appuies sur HK."

# game/dialogs.rpy:2177
translate french secretnanami_45d61fa6:

    # n "...I still don't get it..."
    n "...Comment tu fais ?..."

# game/dialogs.rpy:2178
translate french secretnanami_27514623:

    # hero "Hold on, I'll show you."
    hero "Attends, je vais te montrer."

# game/dialogs.rpy:2183
translate french secretnanami_9d9c7a4c:

    # "I placed myself behind Nanami."
    "Je me plaçai derrière Nanami."

# game/dialogs.rpy:2184
translate french secretnanami_d11638d3:

    # hero "Don't move, I'll show you exactly how."
    hero "Bouge pas, je vais te montrer exactement comment faire."

# game/dialogs.rpy:2185
translate french secretnanami_98b0a82d:

    # n "O-okay!"
    n "O-ok !"

# game/dialogs.rpy:2186
translate french secretnanami_9b84d6a3:

    # "I placed my hands on Nanami's, holding the controller with her."
    "Je plaçai mes mains sur celles de Nanami, tenant la manette avec elle."

# game/dialogs.rpy:2187
translate french secretnanami_e5d45e92:

    # "I realized how small her hands were...but they were so soft and warm as well. I started to get a little nervous and blush."
    "Je commençai à rougir en sentant à quel point ses petites mains de gameuse étaient douces et chaudes."

# game/dialogs.rpy:2188
translate french secretnanami_b142513a:

    # "I felt like she was starting get just as nervous as well."
    "Je sentis qu'elle rougissait aussi."

# game/dialogs.rpy:2189
translate french secretnanami_72fc66bd:

    # "I went into the training mode in the game."
    "Dans le jeu, je me mis en mode entraînement."

# game/dialogs.rpy:2190
translate french secretnanami_068e1468:

    # "Behind the game's sound effects, I could faintly hear Nanami's soft breathing."
    "Derrière les effets sonores du jeu, j'entendais faiblement la respiration de Nanami."

# game/dialogs.rpy:2191
translate french secretnanami_dad13c6d:

    # "I started to feel the warmth of her back on my chest. I could feel myself losing focus..."
    "Je sentis sa chaleur corporelle m'envahir le torse et un désir commençait à monter en moi."

# game/dialogs.rpy:2192
translate french secretnanami_a3e2db4f:

    # hero "O-okay... S-so you... You roll the left stick like that..."
    hero "O-ok... A-alors tu... Tu roules le stick comme ça..."

# game/dialogs.rpy:2193
translate french secretnanami_b73c31d9:

    # "I tried the special attack but it failed."
    "J'essayai le coup spécial mais échouai."

# game/dialogs.rpy:2194
translate french secretnanami_f8130d09:

    # hero "W-wait, let me try again..."
    hero "A-attends, je réessaie."

# game/dialogs.rpy:2195
translate french secretnanami_8fe679da:

    # "I couldn't concentrate with Nanami in my arms like this."
    "Je n'arrivais pas à me concentrer avec Nanami dans mes bras comme ça."

# game/dialogs.rpy:2197
translate french secretnanami_9f50bc6f:

    # "I could feel my heart beating against her back."
    "Mon cœur battait fort contre son dos."

# game/dialogs.rpy:2198
translate french secretnanami_516207af:

    # "I tried to redo the special attack but I just couldn't do it correctly. My hands were shaking."
    "Je réessayai le coup spécial mais je n'y arrivais pas. Mes mains tremblaient un peu."

# game/dialogs.rpy:2199
translate french secretnanami_9a928511:

    # "All my thoughts were floating towards Nanami."
    "Toutes mes pensées allaient vers Nanami."

# game/dialogs.rpy:2200
translate french secretnanami_11daa19b:

    # "Her soft smell, her warmth invading my hands and my chest against her back..."
    "Sa douce odeur, le bruit de sa respiration, sa chaleur qui envahissait mes mains et mon torse..."

# game/dialogs.rpy:2201
translate french secretnanami_702709c6:

    # "I... I think I'm falling in love with Nanami..."
    "Je... Je crois que je suis tombé amoureux d'elle..."

# game/dialogs.rpy:2202
translate french secretnanami_0290a533:

    # "Nanami turned to face to me, her face was glowing red, just like mine."
    "Nanami tourna son visage vers le mien, rouge comme une tomate, exactement comme moi."

# game/dialogs.rpy:2203
translate french secretnanami_1126b44f:

    # "Her eyes were locked to mine. I got lost in her big emerald green eyes."
    "Nos regards se connectèrent. Je plongeai dans les deux grandes émeraudes qu'étaient ses yeux."

# game/dialogs.rpy:2204
translate french secretnanami_1b126339:

    # "It was the coup de grace"
    "Ce fut le coup de grâce."

# game/dialogs.rpy:2205
translate french secretnanami_f5a66b6d:

    # n ".....%(stringhero)s-nii...."
    n ".....%(stringhero)s-nii...."

# game/dialogs.rpy:2206
translate french secretnanami_c7ff9e11:

    # hero "...Na....Nanami-chan..."
    hero "...Na....Nanami-chan..."

# game/dialogs.rpy:2210
translate french secretnanami_07a482ff:

    # "I felt myself moving toward her lips."
    "Attirées par elles, mes lèvres touchèrent les siennes."

# game/dialogs.rpy:2218
translate french secretnanami_6546765d:

    # "She came towards me as well, and our lips met."
    "Elle répondit instantanément, rendant le baiser."

# game/dialogs.rpy:2219
translate french secretnanami_ad565af5:

    # "We both dropped the controller and embraced each other tightly, still kissing. Neither of us could stop at all."
    "On laissa tous les deux tomber la manette, s'enlaçant passionément. Aucun de nous n'arrivait à s'arrêter."

# game/dialogs.rpy:2220
translate french secretnanami_52b5a252:

    # "We fell to the floor in our embrace, trying to avoid some of the mess around."
    "On était au sol, évitant le bazar de la chambre, à se câliner amoureusement."

# game/dialogs.rpy:2221
translate french secretnanami_ca782044:

    # "Her kiss, her softness, her moaning of desire and her sweet smell were driving me crazy."
    "Ses baisers, sa douceur, ses petits gémissement de désirs et sa douce odeur me rendaient fou."

# game/dialogs.rpy:2222
translate french secretnanami_474523d0:

    # "We kept going, sometimes stopping to pronounce each other's name..."
    "On a continué, parfois arrêtant le baiser pour prononcer le nom de l'autre..."

# game/dialogs.rpy:2223
translate french secretnanami_5ca23c3c:

    # "After a while, she took my hand and gently dragged me to her bed."
    "Après un moment, elle prit ma main et m'attira sur son lit pour continuer."

# game/dialogs.rpy:2224
translate french secretnanami_6d6ab9c6:

    # "We continued kissing there, more torridly..."
    "On a continué là, plus passionnément..."

# game/dialogs.rpy:2228
translate french secretnanami_8534ca42:

    # ". . ."
    ". . ."

# game/dialogs.rpy:2229
translate french secretnanami_0e5d8dad:

    # "The idle video game was still making some noise in the room."
    "Le jeu vidéo continuait à faire du bruit tout seul en arrière plan."

# game/dialogs.rpy:2230
translate french secretnanami_95a4b2f0:

    # "I was on the bed, holding Nanami by my side."
    "J'étais sur le lit, câlinant Nanami à mes côtés."

# game/dialogs.rpy:2231
translate french secretnanami_d89bb97f:

    # "She was hugging me like a plushie."
    "Elle me tenait comme une peluche."

# game/dialogs.rpy:2232
translate french secretnanami_45685a47:

    # hero "W-was it okay?"
    hero "Ça...ça a été ?"

# game/dialogs.rpy:2233
translate french secretnanami_d1a94b7f:

    # n "Y-yeah... It hurt a lil' bit at first,... but it felt good at the end..."
    n "O-oui... Ça a fait un peu mal au début... Mais vers la fin c'était bon..."

# game/dialogs.rpy:2234
translate french secretnanami_5abb8e5f:

    # n "I'm so happy that you were my first, %(stringhero)s-nii..."
    n "Je suis heureuse que tu sois ma première fois, %(stringhero)s-nii..."

# game/dialogs.rpy:2235
translate french secretnanami_fa1ef15c:

    # hero "Me too..."
    hero "Moi aussi..."

# game/dialogs.rpy:2236
translate french secretnanami_1d6ad191:

    # n "I feel like a woman now thanks to you..."
    n "J'ai l'impression d'être une vraie femme maintenant, grâce à toi !"

# game/dialogs.rpy:2237
translate french secretnanami_4ce41c79:

    # "I kissed her forehead with a smile."
    "Je lui posai un baiser sur le front, tendrement, avec un sourire."

# game/dialogs.rpy:2238
translate french secretnanami_e9590123:

    # "Nanami suddenly giggled."
    "Nanami eut soudain un rire."

# game/dialogs.rpy:2239
translate french secretnanami_08ea1428:

    # n "I can't wait to see Sakura-nee and Rika-nee's faces when they find out about us!"
    n "J'ai hâte de voir la tête de Rika-nee et Sakura-nee quand elles sauront pour nous deux !"

# game/dialogs.rpy:2240
translate french secretnanami_d3d236dd:

    # hero "Hehe I'm curious to see that too..."
    hero "Héhé, je suis curieux, moi aussi..."

# game/dialogs.rpy:2241
translate french secretnanami_523e30b8:

    # "Then I had a mischievous thought..."
    "Puis j'eus un sourire coquin..."

# game/dialogs.rpy:2242
translate french secretnanami_ceb62c81:

    # hero "I have a fun idea:{p}Why not just let them figure it out themselves?"
    hero "J'ai une idée !{p}Si on ne leur disait rien et qu'on les laissait deviner toutes seules ?"

# game/dialogs.rpy:2243
translate french secretnanami_46f913e1:

    # hero "They might end up even more surprised!"
    hero "Ça pourrait être encore plus drôle !"

# game/dialogs.rpy:2244
translate french secretnanami_ae388490:

    # "Nanami giggled louder."
    "Nanami rit plus fort."

# game/dialogs.rpy:2245
translate french secretnanami_6ab9da22:

    # n "Good idea! Let's do this!"
    n "Super idée ! On va faire ça !"

# game/dialogs.rpy:2246
translate french secretnanami_b3625191:

    # hero "Right!"
    hero "Ok !"

# game/dialogs.rpy:2247
translate french secretnanami_ec3a4b43:

    # "I giggled and kissed her lips again. She embraced me tighter, kissing back..."
    "Je ris en embrassant ses lèvres. Elle m'enlaça tendrement..."

# game/dialogs.rpy:2248
translate french secretnanami_82623620:

    # "Before I could even notice, we were on for a second round..."
    "Avant qu'on s'en rende compte, on était partis pour un deuxième round..."

# game/dialogs.rpy:2249
translate french secretnanami_8116efac:

    # "Hehe... \"Second round\"...{p}I bet she finds this 'new game' better than any other game she's played!"
    "Héhé... \"Deuxième round\"...{p}Je parie qu'elle aime ce nouveau jeu plus que n'importe quel autre jeu vidéo !"

# game/dialogs.rpy:2253
translate french secretnanami_3402d7bf:

    # "After that, it was late so it was time for me to go. "
    "Après ça, il était tard et je dus rentrer chez moi."

# game/dialogs.rpy:2254
translate french secretnanami_924e253f:

    # hero "Don't forget: Don't tell anyone in the club!"
    hero "N'oublie pas : On ne dit rien à personne au club pour l'instant !"

# game/dialogs.rpy:2256
translate french secretnanami_91f1d653:

    # n "Yes sir!"
    n "Oui chef !"

# game/dialogs.rpy:2258
translate french secretnanami_743da19c:

    # "We kissed again and then I left for home."
    "On s'embrassa encore et je quittai sa maison."

# game/dialogs.rpy:2262
translate french secretnanami_ffc14834:

    # "I was so happy. I was hopping around and dancing a bit."
    "J'étais si heureux, je sautillai de bonheur sur la route."

# game/dialogs.rpy:2263
translate french secretnanami_bb28eb57:

    # "People probably thought I was crazy but I didn't care."
    "Les gens ont du penser que je devenais fou mais je m'en foutais..."

# game/dialogs.rpy:2267
translate french secretnanami_50788d9c:

    # "Monday arrived..."
    "Lundi arriva..."

# game/dialogs.rpy:2268
translate french secretnanami_e94279e1:

    # "I was thinking about all the new things I learned this weekend..."
    "Je repensais à toutes les nouvelles choses de ce weekend..."

# game/dialogs.rpy:2269
translate french secretnanami_ff29a530:

    # "I still can't believe Sakura was born as a boy."
    "J'arrive pas à me faire à l'idée que Sakura est née garçon."

# game/dialogs.rpy:2270
translate french secretnanami_d6d86bf5:

    # "But I can't wait to see her again anyway. She's my best friend."
    "Mais j'ai quand même hâte de la voir. Elle est ma meilleure amie."

# game/dialogs.rpy:2272
translate french secretnanami_12a5358b:

    # "I also thought about Nanami... My dear love..."
    "Je pensais aussi à Nanami... Mon cher amour..."

# game/dialogs.rpy:2273
translate french secretnanami_55bde2ad:

    # "I can't wait to see her again too... And to make that joke to our friends!"
    "J'ai hâte de la revoir aussi... Et de mettre notre blague à éxécution !"

# game/dialogs.rpy:2274
translate french secretnanami_e6c4d8b3:

    # "Thinking of Sakura and Nanami made me remember my plan for Sakura."
    "Penser à Sakura et Nanami me fit repenser à mon plan pour Sakura."

# game/dialogs.rpy:2275
translate french secretnanami_da787f5b:

    # "I found Rika and told her I knew about Sakura, and that she needs help to tell the truth to Nanami as well."
    "Je trouvai Rika et lui révéla que je savais pour Sakura. Et qu'elle avait besoin d'aide pour le révéler à Nanami."

# game/dialogs.rpy:2276
translate french secretnanami_f5d57b70:

    # "She gladly accepted."
    "Elle a grandement accepté."

# game/dialogs.rpy:2277
translate french secretnanami_eb899bec:

    # "For the first time, I saw on her a sincere sweet smile."
    "Et pour la première fois, j'ai vu sur elle un sourire sincère."

# game/dialogs.rpy:2278
translate french secretnanami_258e716e:

    # "She told me that I'm sure to be a special member of the club now."
    "Elle m'a dit que, maintenant, j'étais vraiment un membre spécial du club."

# game/dialogs.rpy:2280
translate french secretnanami_4fe7e943:

    # "Of course, I didn't tell her about Nanami and I..."
    "Bien sûr, je ne lui ai rien dit pour moi et Nanami..."

# game/dialogs.rpy:2281
translate french secretnanami_e02d74e7:

    # "Yet..."
    "Pas encore..."

# game/dialogs.rpy:2287
translate french secretnanami_3e63050d:

    # "Lunch time came."
    "Puis le déjeuner arriva."

# game/dialogs.rpy:2289
translate french secretnanami_7d31f878:

    # "I was eating lunch at my desk with Sakura, as we always do."
    "Je déjeunai à mon bureau avec Sakura comme à notre habitude."

# game/dialogs.rpy:2290
translate french secretnanami_96641885:

    # "Usually, Rika and Nanami join us a few minutes after we start."
    "D'habitude, Rika et Nanami nous rejoignent quelques minutes après."

# game/dialogs.rpy:2291
translate french secretnanami_fcd3f037:

    # hero "Oh, guess what? I know about Nanami, now."
    hero "Oh, tu sais quoi ? Je sais maintenant, pour Nanami."

# game/dialogs.rpy:2292
translate french secretnanami_7f54d05d:

    # "Sakura nodded, knowing what I meant."
    "Sakura acquiesça, devinant ce que je voulais dire."

# game/dialogs.rpy:2293
translate french secretnanami_26f2656b:

    # s "Looks like she really likes and trusts you, now! That's great!"
    s "On dirait qu'elle te fait vraiment confiance, maintenant. C'est super !"

# game/dialogs.rpy:2294
translate french secretnanami_d5961b9f:

    # "I held a laugh. If only she knew how much she likes me now!"
    "Je retins un rire. Si seulement elle savait à quel point on était proches, elle  et moi, maintenant !"

# game/dialogs.rpy:2297
translate french secretnanami_ba640f4d:

    # "Nanami came first."
    "Nanami arriva en premier."

# game/dialogs.rpy:2298
translate french secretnanami_eb443bf4:

    # n "Hey, Sakura-nee!"
    n "Salut, Sakura-nee !"

# game/dialogs.rpy:2300
translate french secretnanami_0f4878e5:

    # n "Hey,...honey!"
    n "Salut,...mon chéri !"

# game/dialogs.rpy:2302
translate french secretnanami_349c95fd:

    # "Nanami left a kiss on my cheek, took a free chair and table and joined us like if nothing happened."
    "Nanami déposa un baiser sur ma joue, prit une chaise libre et rejoignit le repas comme si rien ne s'était passé."

# game/dialogs.rpy:2303
translate french secretnanami_91c5f81c:

    # "Wow, she's good. It's hard to not blush in this situation."
    "Waouh, elle est douée à ça. J'ai du mal à ne pas rougir de la situation."

# game/dialogs.rpy:2304
translate french secretnanami_0083c6b0:

    # "The face Sakura made was priceless."
    "La tête que fit Sakura n'avait pas de prix."

# game/dialogs.rpy:2305
translate french secretnanami_91e03536:

    # "Her face was flustered and expressed something along the lines of \"What the heck?\" and \"That's embarassing!\""
    "Elle rougissait et son visage disait \"Hein quoi ??\" et \"C'est gênant !!\""

# game/dialogs.rpy:2306
translate french secretnanami_d1b32a3c:

    # "I had a hard time trying to not laugh out loud.{p}I could tell, Nanami was holding it in too."
    "J'ai eu un mal fou à me retenir de rire.{p}Et de ce que je voyais, Nanami aussi."

# game/dialogs.rpy:2308
translate french secretnanami_5e2e8af1:

    # "Then Rika appeared and joined us."
    "Puis Rika arriva à son tour."

# game/dialogs.rpy:2309
translate french secretnanami_2f6f7af4:

    # r "Hey guys! Did you have a good Sunday?"
    r "Salut vous trois ! Passé un bon dimanche ?"

# game/dialogs.rpy:2310
translate french secretnanami_b408e187:

    # "Nanami and I almost choked on our meal at the question."
    "Nanami et moi manquions de recracher nos bentos à la question."

# game/dialogs.rpy:2312
translate french secretnanami_e5fcea13:

    # r "Sakura, you're okay?"
    r "Sakura, qu'est-ce qui t'arrive ?"

# game/dialogs.rpy:2313
translate french secretnanami_5ab8d803:

    # s "I'm... I'm not sure..."
    s "Je... Je sais pas..."

# game/dialogs.rpy:2314
translate french secretnanami_5ddf9bba:

    # "All during lunch, Nanami and I were exchanging glances, and every time we had to hold a laugh."
    "Durant le repas, Nanami et moi s'échangions des regards et on avait beaucoup de mal à ne pas rire."

# game/dialogs.rpy:2315
translate french secretnanami_3df5cd5f:

    # "All of this was making Sakura and Rika even more confused and uncomfortable."
    "Tout cela mettait Sakura et Rika en pleine confusion."

# game/dialogs.rpy:2316
translate french secretnanami_0469525e:

    # "Finally, Rika decided to speak up."
    "Finalement, Rika décida de régler ça."

# game/dialogs.rpy:2318
translate french secretnanami_848c0a9a:

    # r "Alright, guys. It looks like Sakura and I missed the memo. What's going on?"
    r "Ok, les gars. On dirait que Sakura et moi on a manqué un épisode !"

# game/dialogs.rpy:2319
translate french secretnanami_78a3d7bc:

    # r "What happened this weekend?!"
    r "Il s'est passé quoi, ce weekend ?!"

# game/dialogs.rpy:2321
translate french secretnanami_a2210385:

    # "Nanami finally cracked. She burst into laughter. Some classmates turned their heads wondering what was going on."
    "C'est finalement Nanami qui craqua en première. Elle relacha un énorme fou rire qui fit se retourner quelques camarades de classe."

# game/dialogs.rpy:2322
translate french secretnanami_e7188aa7:

    # "Her laugh instantly unlocked mine and I finished laughing as well!"
    "Son rire déclencha directement le mien et me joignit à elle."

# game/dialogs.rpy:2324
translate french secretnanami_1b6c8610:

    # r "What the!?"
    r "Mais ?!!"

# game/dialogs.rpy:2326
translate french secretnanami_5bc75703:

    # r "Sakura, tell me! You must know something!"
    r "Sakura, dis quelque chose !! Dis-moi que tu sais ce qui se passe !!"

# game/dialogs.rpy:2327
translate french secretnanami_a3d554dc:

    # s "I... They... I don't know!..."
    s "Je... Ils... Je sais pas !!..."

# game/dialogs.rpy:2328
translate french secretnanami_d6d264c2:

    # s "Nana-chan kissed %(stringhero)s-kun on the cheek and they've been acting like this since!"
    s "Nana-chan a fait un bisou à %(stringhero)s-kun et ils sont comme ça tous les deux depuis !"

# game/dialogs.rpy:2330
translate french secretnanami_dba98263:

    # r "No way! {w}You mean..."
    r "Sans dec' !{w} C'est quand même pas..."

# game/dialogs.rpy:2331
translate french secretnanami_8fd173cb:

    # "I calmed down and spoke with my natural voice:"
    "Et, essayant avec ma voix la plus naturelle :"

# game/dialogs.rpy:2332
translate french secretnanami_588f721e:

    # hero "Oh yeah, Nanami and I started going out together."
    hero "Ah, oui. Nanami et moi sortons ensemble depuis hier."

# game/dialogs.rpy:2333
translate french secretnanami_83c319f1:

    # hero "We didn't tell you?"
    hero "On vous l'avait pas dit ?"

# game/dialogs.rpy:2345
translate french secretnanami_e9fe3d46:

    # s "{size=+25}EEEEEEEEHHHHHHHHH???!!!!!{/size}"
    s "{size=+25}EEEEEEEEHHHHHHHHH ???!!!!!{/size}"

# game/dialogs.rpy:2347
translate french secretnanami_e1326833:

    # r "{size=+25}I KNEW IT!!!!!{/size}"
    r "{size=+25}JE L'SAVAIS !!!!!{/size}"

# game/dialogs.rpy:2348
translate french secretnanami_e9aabc9d:

    # "Another burst of laughter occurred between Nanami and I."
    "Et c'est reparti pour un autre fou rire avec Nanami."

# game/dialogs.rpy:2353
translate french secretnanami_597fe66a:

    # "After lunch, I found Sakura speaking with Rika alone."
    "Après le repas, je trouvai Sakura en train de parler seule avec Rika."

# game/dialogs.rpy:2354
translate french secretnanami_d72a5ad0:

    # "When Rika saw me, she beckoned me to come over."
    "Quand Rika m'aperçut, elle me fit signe de les rejoindre."

# game/dialogs.rpy:2358
translate french secretnanami_eec796bb:

    # hero "What's going on?"
    hero "Qu'y a-t-il ?"

# game/dialogs.rpy:2359
translate french secretnanami_7f82104e:

    # r "It's time. Nanami is waiting for us at the club room."
    r "C'est le moment. Nanami nous attend dans la salle du club."

# game/dialogs.rpy:2360
translate french secretnanami_b6e3bb19:

    # r "Are you both ready?"
    r "Vous êtes prêts ?"

# game/dialogs.rpy:2361
translate french secretnanami_326263c8:

    # hero "I am. As long as Sakura is."
    hero "Je le suis. Tant que Sakura l'est."

# game/dialogs.rpy:2363
translate french secretnanami_feb04fe2:

    # s "I am... Let's go."
    s "Je le suis... Allons-y."

# game/dialogs.rpy:2369
translate french secretnanami_9df2c3c5:

    # n "Hey again, guys!"
    n "Vous revoilà !"

# game/dialogs.rpy:2373
translate french secretnanami_4ceec5a6:

    # r "Hey, Nanami."
    r "Re-salut, Nanami."

# game/dialogs.rpy:2374
translate french secretnanami_fe8acb83:

    # "I whispered \"Go on, don't worry!\" into Sakura's ear and she nodded."
    "Je chuchotai \"Tiens le coup !\" à l'oreille de Sakura et elle acquiesça."

# game/dialogs.rpy:2376
translate french secretnanami_10c08f00:

    # s "Nanami-chan..."
    s "Nana-chan..."

# game/dialogs.rpy:2378
translate french secretnanami_ee407c47:

    # s "There's something I want to tell you."
    s "Il y a quelque chose que je dois te dire."

# game/dialogs.rpy:2380
translate french secretnanami_54f6b843:

    # n "What is it?...{p}It looks serious... {w}Are you guys okay?"
    n "C'est quoi ?...{p}Ça a l'air sérieux...{w}Vous allez tous bien ?"

# game/dialogs.rpy:2381
translate french secretnanami_1feba992:

    # hero "Yeah, no worries. It's just...something that you need to know..."
    hero "Oui, rien de grave... Juste... Quelque chose que tu dois savoir..."

# game/dialogs.rpy:2382
translate french secretnanami_609aaa6d:

    # s "Since Rika-chan and %(stringhero)s-kun are aware of it, I thought it would be unfair for you to not know..."
    s "Comme Rika-chan et %(stringhero)s-kun sont au courant, je pensais que c'était injuste pour toi de ne pas le savoir..."

# game/dialogs.rpy:2385
translate french secretnanami_827414d5:

    # n "Hey hey, that's okay! Hiding things can be fun sometimes! Remember the lunch?"
    n "Hé hé, c'est pas grave ! Cacher des choses ça peut être drôle ! T'as bien vu au déjeuner !"

# game/dialogs.rpy:2386
translate french secretnanami_1ac65a8e:

    # s "It's not really the same thing, Nana-chan..."
    s "Ce n'est pas vraiment la même chose, là, ma Nana-chan..."

# game/dialogs.rpy:2388
translate french secretnanami_73096c17:

    # n "Ah?..."
    n "Ah ?..."

# game/dialogs.rpy:2389
translate french secretnanami_790191ec:

    # s "It's always hard for me to find the right words so it doesn't shock you too much..."
    s "C'est toujours dur pour moi de trouver les bon mots pour ne pas que ça te choque trop..."

# game/dialogs.rpy:2390
translate french secretnanami_5667ce78:

    # n "It's something that big?"
    n "C'est si gros que ça ?"

# game/dialogs.rpy:2391
translate french secretnanami_22a96dc3:

    # "Sakura nodded."
    "Sakura hocha."

# game/dialogs.rpy:2392
translate french secretnanami_f8f4fa69:

    # s "And before you ask..."
    s "Et par avance, avant que tu ne me demandes pourquoi..."

# game/dialogs.rpy:2393
translate french secretnanami_6d099b03:

    # s "I'm so sorry I didn't tell you earlier. I didn't know how to tell you."
    s "Je suis vraiment désolée que tu l'apprennes aussi tard. Je ne savais pas quels mots choisir pour t'en parler."

# game/dialogs.rpy:2394
translate french secretnanami_2a2228a7:

    # s "Before I tell you, can you forgive me for this?"
    s "Avant que je ne te le révèle, pourrais-tu me pardonner ça ?"

# game/dialogs.rpy:2396
translate french secretnanami_564d9e25:

    # "Nanami smiled softly."
    "Nanami sourit doucement."

# game/dialogs.rpy:2397
translate french secretnanami_67350be4:

    # n "Of course I can. You're my Sakura-nee!"
    n "Bien sûr que oui. Tu es ma Sakura-nee !"

# game/dialogs.rpy:2398
translate french secretnanami_c66e8457:

    # "Sakura nodded again with a faint smile. Then, after a moment, she started to reveal her secret."
    "Sakura hocha à nouveau, avec un faible sourire. Puis après un moment, elle révéla son secret."

# game/dialogs.rpy:2399
translate french secretnanami_727b9f43:

    # s "Nana-chan..."
    s "Nana-chan..."

# game/dialogs.rpy:2400
translate french secretnanami_5b84e198:

    # s "I am...{w}not...{w}technically a girl..."
    s "Je...{w}ne suis...{w}pas{w}...techniquement une fille..."

# game/dialogs.rpy:2402
translate french secretnanami_33d36d5f:

    # n "Huh?"
    n "Hein ?"

# game/dialogs.rpy:2403
translate french secretnanami_e8a331c8:

    # s "I..."
    s "Je..."

# game/dialogs.rpy:2404
translate french secretnanami_ce4432b4:

    # "Rika came closer and held Sakura's hand to give her strength. Rika helped Sakura gather herself."
    "Rika s'approcha et tint la main de Sakura pour la soutenir. Elle l'aida à trouver les autres mots."

# game/dialogs.rpy:2405
translate french secretnanami_d8383a12:

    # r "Sakura...{w}wasn't born as a girl, like you and me."
    r "Sakura...{w}n'est pas née fille, comme toi et moi."

# game/dialogs.rpy:2406
translate french secretnanami_80777581:

    # "I placed my hand on Sakura's shoulder. I helped Rika speak on behalf of Sakura."
    "Ma main sur l'épaule de Sakura, j'achevai la phrase de Rika."

# game/dialogs.rpy:2407
translate french secretnanami_fd25aa6e:

    # hero "Yeah...She's a girl on the inside, but originally, she was born as a boy. Like me."
    hero "Oui... Elle est une fille dans son âme, mais à l'origine, elle est née garçon. Comme moi."

# game/dialogs.rpy:2409
translate french secretnanami_77609d5b:

    # "Nanami didn't say a word. She was trying to process the information. Her face displayed confusion."
    "Nanami resta silencieuse, essayant d'assimiler l'information. Son visage trahissait sa surprise."

# game/dialogs.rpy:2410
translate french secretnanami_8961280a:

    # "Sakura gathered herself. She started to speak about her childhood, almost repeating word by word what she told me."
    "Débloquée par notre soutien, Sakura commença à parler de son enfance à Nanami, répétant presque mot pour mot ce qu'elle m'avait dit avant-hier."

# game/dialogs.rpy:2412
translate french secretnanami_d7384a8d:

    # "Nanami listened silently, nodding from time to time.{p}After Sakura finished, Nanami had similar questions to mine at time."
    "Nanami écouta silencieusement, acquiesçant de temps en temps.{p}Après le monologue de Sakura, Nanami eut les quelques questions que j'avais."

# game/dialogs.rpy:2413
translate french secretnanami_e145542f:

    # n "I understand now..."
    n "Je comprends maintenant..."

# game/dialogs.rpy:2415
translate french secretnanami_8d8cc9b7:

    # "Then Nanami grew a cute smile."
    "Puis Nanami fit un sourire."

# game/dialogs.rpy:2416
translate french secretnanami_5399f645:

    # n "That's okay for me!"
    n "D'accord, alors !"

# game/dialogs.rpy:2420
translate french secretnanami_e160708b:

    # n "I'm happy that you told me your secret!"
    n "Je suis contente que tu m'aies dit ton secret !"

# game/dialogs.rpy:2421
translate french secretnanami_93b2b022:

    # n "I know I'm special to you, Sakura-nee, but now I feel it more than ever!"
    n "Je sais que j'étais spéciale pour toi, Sakura-nee ; Mais là, je me sens vraiment unique à tes yeux !"

# game/dialogs.rpy:2422
translate french secretnanami_bfeb43f4:

    # "Sakura smiled, some tears coming from her eyes."
    "Sakura sourit, quelques larmes venant à ses yeux bleus."

# game/dialogs.rpy:2425
translate french secretnanami_957c3f61:

    # "Nanami jumped on Sakura and hugged her tightly with a bright smile and some tears."
    "Nanami sauta dans les bras de Sakura, la serrant très fort contre elle, avec un sourire brillant et quelques larmes."

# game/dialogs.rpy:2426
translate french secretnanami_7c8449b8:

    # "That was touching."
    "C'était touchant."

# game/dialogs.rpy:2427
translate french secretnanami_106608f8:

    # n "I understand why it was so hard to tell me that.{p}I'm sure it's not easy to tell a secret like that!"
    n "Je comprends pourquoi tu as eu autant de mal à me le dire.{p}Ça doit être si difficile de dire un tel secret !"

# game/dialogs.rpy:2428
translate french secretnanami_df955372:

    # "Sakura chuckles."
    "Sakura rit doucement."

# game/dialogs.rpy:2429
translate french secretnanami_3c734a09:

    # s "It sure isn't!"
    s "C'est vrai !"

# game/dialogs.rpy:2430
translate french secretnanami_120f3ff5:

    # s "But now I'm the happiest girl ever..."
    s "Mais maintenant, je suis vraiment heureuse."

# game/dialogs.rpy:2431
translate french secretnanami_ba0e9538:

    # s "Because, all the people I love and care about know my secret now."
    s "Car maintenant, tous les gens qui comptent le plus pour moi connaîssent mon secret."

# game/dialogs.rpy:2432
translate french secretnanami_1abc70b9:

    # s "I don't need to hide anything to any of you now."
    s "Je n'ai plus besoin de le cacher parmi vous."

# game/dialogs.rpy:2433
translate french secretnanami_128578da:

    # n "Hiding what?"
    n "Cacher quoi ?"

# game/dialogs.rpy:2434
translate french secretnanami_77ee7366:

    # s "Huh?"
    s "Hein ?"

# game/dialogs.rpy:2435
translate french secretnanami_ce51de81:

    # n "You're a girl, Sakura-nee."
    n "Tu es une fille, Sakura-nee."

# game/dialogs.rpy:2436
translate french secretnanami_20f09b8b:

    # n "You're born as a boy? That's a small detail."
    n "Tu es née garçon ? C'est un détail."

# game/dialogs.rpy:2437
translate french secretnanami_1ca8316e:

    # n "To me, you'll always be my Sakura-nee!"
    n "Pour moi, tu seras toujours comme ma grande sœur !"

# game/dialogs.rpy:2438
translate french secretnanami_e130ea9c:

    # "Sakura kissed Nanami's head and rubbed her hair like for a child."
    "Sakura déposa un baiser sur le front de Nanami et lui caressa la tête, comme une mère avec son enfant."

# game/dialogs.rpy:2439
translate french secretnanami_b45c3328:

    # s "Thank you so much... My Nana-chan...{p}My little one..."
    s "Merci... Merci, ma Nana-chan...{p}Ma petite..."

# game/dialogs.rpy:2446
translate french secretnanami_9e1d86cb:

    # n "I'm not little!..."
    n "Je suis pas petite !"

# game/dialogs.rpy:2471
translate french scene_R_13535866:

    # centered "{size=+35}CHAPTER 5\nThe festival{fast}{/size}"
    centered "{size=+35}CHAPITRE 5\nLe festival{fast}{/size}"

# game/dialogs.rpy:2475
translate french scene_R_31591cda:

    # "The week went by quickly..."
    "La semaine passa..."

# game/dialogs.rpy:2478
translate french scene_R_57141cf2:

    # "Friday evening came. The day before the festival..."
    "Puis le vendredi arriva. Le vendredi d'avant le festival..."

# game/dialogs.rpy:2481
translate french scene_R_1b27c60b:

    # "Preparations for the festival were awfully hard. But I promised Rika I'd help her out."
    "Les préparations du festival étaient vraiment dur. Mais j'avais promis à Rika que je l'aiderais."

# game/dialogs.rpy:2482
translate french scene_R_2d950149:

    # "We had to prepare the stands and the decorations."
    "Il fallait préparer les stands et les décorations."

# game/dialogs.rpy:2483
translate french scene_R_9be7bc2a:

    # "Rika came to tell what to do and where to go."
    "Mais Rika était juste là pour me dire où poster les affaires et tout."

# game/dialogs.rpy:2484
translate french scene_R_b0c123df:

    # hero "Hey, maybe could you give us a hand!?"
    hero "Tu pourrais pas me donner un peu plus un coup de main ?"

# game/dialogs.rpy:2485
translate french scene_R_8a0b645d:

    # r "I can't, I'm guiding you, idiot!"
    r "Je peux pas, je te guide, déjà, idiot !"

# game/dialogs.rpy:2486
translate french scene_R_139a795e:

    # "We really don't have the same understanding of the word \"help.\" "
    "On a vraiment pas la même conception de ce qu'est de l'aide..."

# game/dialogs.rpy:2488
translate french scene_R_08547004:

    # "I had to move some boxes inside the temple. I was alone."
    "Il fallait que je trouve des affaires dans le temple. J'étais seul."

# game/dialogs.rpy:2489
translate french scene_R_166ebec1:

    # "The boxes were inside a big dark room with a lot of old junk."
    "Les affaires étaient dans une grande salle sombre avec beaucoup de bazar."

# game/dialogs.rpy:2490
translate french scene_R_9899d4dc:

    # "It's so dark. I can't see a thing..."
    "Il faisait si noir que je n'y voyais rien..."

# game/dialogs.rpy:2491
translate french scene_R_7f278c6a:

    # hero "Rika-chan? Are you in there?"
    hero "Rika-chan ? T'es là ?"

# game/dialogs.rpy:2492
translate french scene_R_a5dde9ae:

    # r "Yes, come over here!"
    r "Oui, par ici !"

# game/dialogs.rpy:2493
translate french scene_R_cd339c0e:

    # "I follow the direction of her voice. But I suddenly stumbled onto something and I fell on the floor."
    "Je suivis la direction de la voix. Mais soudainement, je trébuchai sur quelque chose et atterrit au sol."

# game/dialogs.rpy:2494
translate french scene_R_4aa7bb8a:

    # "Well, not exactly the floor. I fell on something warm and comfy."
    "Enfin, pas exactement le sol. J'avais atterri sur du mou."

# game/dialogs.rpy:2495
translate french scene_R_e0fa74ca:

    # hero "Ah, I fell on something..."
    hero "Y a un truc, là..."

# game/dialogs.rpy:2496
translate french scene_R_4642c748:

    # "My hand squeezed the mellow thing I was on."
    "Ma main palpa la chose moelleuse où elle était tombée."

# game/dialogs.rpy:2504
translate french scene_R_02b7fdcb:

    # hero "What is this... I don't even-"
    hero "C'est quoi ce truc... J'ai même pas-"

# game/dialogs.rpy:2505
translate french scene_R_d557eb58:

    # r "Y-You {size=+20}IDIOT!!!!!!{/size}"
    r "E-Espèce {size=+20}d'IDIOT !!!!!!{/size}"

# game/dialogs.rpy:2508
translate french scene_R_73674ca2:

    # "*{i}SMACK!{/i}*"
    "*{i}PLAF !{/i}*"

# game/dialogs.rpy:2509
translate french scene_R_c81c00b8:

    # "Something hit my cheek and I instantly got up by instinct..."
    "Quelque chose me heurta la joue et je me relevai instinctivement..."

# game/dialogs.rpy:2510
translate french scene_R_ece959c9:

    # "I realized what I fell on. Or who I fell on..."
    "Je réalisai sur quoi j'étais tombé. Ou plutôt sur qui..."

# game/dialogs.rpy:2511
translate french scene_R_b70add4f:

    # r "You PERVERT!!! Stop touching me!!!"
    r "Arrête de me toucher espèce de PERVERS !!!"

# game/dialogs.rpy:2512
translate french scene_R_5462e3bc:

    # hero "I stopped! I stopped!"
    hero "J'ai arrêté ! J'ai arrêté !!"

# game/dialogs.rpy:2513
translate french scene_R_5c272729:

    # hero "I'm sorry, it was an accident!"
    hero "Désolé, c'était un accident !"

# game/dialogs.rpy:2514
translate french scene_R_bd0bcebc:

    # r "Turn the freakin light on and help me lift this box up there!"
    r "Allume la lumière et aide-moi à déplacer ce truc !"

# game/dialogs.rpy:2516
translate french scene_R_268b921a:

    # "Damn! This girl is nuts!!!"
    "Bon sang ! Cette fille est insupportable !!!"

# game/dialogs.rpy:2517
translate french scene_R_a20cefa7:

    # "..."
    "..."

# game/dialogs.rpy:2518
translate french scene_R_4d9f847b:

    # "Although her breast felt pretty nice...{p}It was warm... and mellow... and big..."
    "Mais sa poitrine était vraiment douce...{p}Douce et moelleuse..."

# game/dialogs.rpy:2519
translate french scene_R_45a00c24:

    # "Eh!? What the hell am I thinking about!?"
    "Mais à quoi je pense ?!!"

# game/dialogs.rpy:2520
translate french scene_R_62d361bd:

    # "Rika is definitely not my type of girl. Not at all!"
    "Cette fille n'est pas mon genre !!!"

# game/dialogs.rpy:2522
translate french scene_R_9f1c3606:

    # "Plus I'm already with Sakura-chan!"
    "En plus, je suis déjà avec Sakura !"

# game/dialogs.rpy:2524
translate french scene_R_076bdc09:

    # "Plus I'm already with Nanami-chan!"
    "En plus, je suis déjà avec Nanami !"

# game/dialogs.rpy:2526
translate french scene_R_a20cefa7_1:

    # "..."
    "..."

# game/dialogs.rpy:2532
translate french decision_31826e00:

    # "The day of the matsuri arrived."
    "Puis le jour du matsuri arriva."

# game/dialogs.rpy:2533
translate french decision_400d0a26:

    # "As the club planned, I will go with Sakura-chan and Rika-chan. And maybe Nanami-chan if she's motivated."
    "Comme le club le prévoyait, j'irai avec Sakura et Rika. Et peut-être Nanami si elle est motivée."

# game/dialogs.rpy:2534
translate french decision_07ca0e28:

    # "I can't wait to see them!"
    "J'ai hâte de les voir !"

# game/dialogs.rpy:2545
translate french matsuri_S_2ef9ea22:

    # "I was waiting for Sakura in front of her house."
    "J'attendais Sakura devant chez elle."

# game/dialogs.rpy:2547
translate french matsuri_S_1dcaa12e:

    # "I was wearing my brand new yukata that she gave me."
    "Je portais le yukata tout neuf qu'elle m'avait donné."

# game/dialogs.rpy:2549
translate french matsuri_S_201b46ce:

    # "I was wearing the yukata I recently bought... One which was awfully expensive..."
    "Je portais le yukata que j'avais acheté avec mes économies... Qui sont parties en fumée avec..."

# game/dialogs.rpy:2550
translate french matsuri_S_9c64454a:

    # "She finally appeared, in a wonderful pink and red kimono."
    "Elle apparut finalement avec un joli kimono rose et rouge."

# game/dialogs.rpy:2551
translate french matsuri_S_c633905a:

    # "She looked like a miko priestess. She was pretty..."
    "On aurait dit une prêtresse miko. Elle était mignonne..."

# game/dialogs.rpy:2553
translate french matsuri_S_678a9d48:

    # hero "Wow... Sakura-chan... You look superb!"
    hero "Waouh... Sakura-chan... Tu es superbe !"

# game/dialogs.rpy:2554
translate french matsuri_S_4eeac269:

    # s "%(stringhero)s-kun! The yukata suits you very nicely!"
    s "%(stringhero)s-kun ! Ce yukata te va vraiment bien !"

# game/dialogs.rpy:2556
translate french matsuri_S_02c86c6c:

    # "She comes closer and whispers in my ear with a grin."
    "Elle s'approcha et me chuchota à l'oreille."

# game/dialogs.rpy:2557
translate french matsuri_S_9e55e999:

    # s "I wonder what's hidden inside..."
    s "Je me demande ce qu'il y a dessous..."

# game/dialogs.rpy:2558
translate french matsuri_S_e44df551:

    # hero "Hehe maybe you'll see later..."
    hero "Héhé, tu le veras peut-être plus tard..."

# game/dialogs.rpy:2560
translate french matsuri_S_96913011:

    # "I saw a woman come out of the house. That must be Sakura's mother."
    "Je vis une femme arriver au pas de la porte. Sûrement la mère de Sakura."

# game/dialogs.rpy:2561
translate french matsuri_S_af9ec1db:

    # "She looks incredibly young for a mother of an 18-year-old. She looks like she's in her early 30s at least!"
    "Elle était incroyablement jeune pour une mère d'une fille de 18 ans. Elle avait l'air d'être au milieu de sa trentaine !"

# game/dialogs.rpy:2565
translate french matsuri_S_6d0cbd15:

    # smom "Oh, you must be %(stringhero)s-san, I presume!"
    smom "Oh, tu es %(stringhero)s-san, je présume !"

# game/dialogs.rpy:2566
translate french matsuri_S_fde87b49:

    # smom "My daughter never stops talking about you!"
    smom "Ma fille n'arrête pas de me parler de toi !"

# game/dialogs.rpy:2568
translate french matsuri_S_273b854b:

    # "Sakura looked embarrassed and blushed."
    "Sakura, gênée, se mit à rougir."

# game/dialogs.rpy:2569
translate french matsuri_S_2bf9e723:

    # s "Mom!!!"
    s "Maman !!!"

# game/dialogs.rpy:2570
translate french matsuri_S_b8980021:

    # hero "From the yukata you're wearing, I assume that you'll come to the matsuri too, ma'am?"
    hero "A votre yukata, j'imagine que vous allez aussi au festival, m'dame ?"

# game/dialogs.rpy:2572
translate french matsuri_S_ed2ca74b:

    # smom "Yes. Once my husband is ready, we'll go as well."
    smom "Oui. Dès que mon mari sera prêt."

# game/dialogs.rpy:2573
translate french matsuri_S_5fc7645d:

    # s "We'll be going on ahead, mom. See you later!"
    s "On y va en premier, maman. A plus tard !"

# game/dialogs.rpy:2574
translate french matsuri_S_0c6a0f45:

    # smom "Have fun, you two!"
    smom "Amusez-vous bien tous les deux !"

# game/dialogs.rpy:2578
translate french matsuri_S_be3185f9:

    # "We were about to leave but then, Sakura's mother called me."
    "On allait partir lorsque la mère de Sakura m'appela."

# game/dialogs.rpy:2580
translate french matsuri_S_6c045540:

    # "She spoke quietly."
    "Elle me parla à mi-voix."

# game/dialogs.rpy:2581
translate french matsuri_S_8c173d23:

    # smom "I know that you and my daughter are going out."
    smom "Je sais que toi et ma fille vous sortez ensemble."

# game/dialogs.rpy:2582
translate french matsuri_S_c0582308:

    # "Yikes!"
    "Argh !!"

# game/dialogs.rpy:2583
translate french matsuri_S_a5ebdd09:

    # smom "Sakura told me how much you love each other."
    smom "Sakura m'a dit combien vous vous aimez l'un l'autre."

# game/dialogs.rpy:2584
translate french matsuri_S_043f4b2a:

    # smom "I'm so happy that my daughter found someone so kind and so understanding."
    smom "Je suis heureuse de voir que ma fille a trouvé quelqu'un de si doux et compréhensif avec elle."

# game/dialogs.rpy:2585
translate french matsuri_S_47abc600:

    # hero "I-it's fine, ma'am... I...{p}I do love Sakura-chan. More than anything... No matter how she was born."
    hero "C-c'est normal, m'dame... Je...{p}J'aime Sakura-chan. Plus que tout... Peu importe comment elle est née."

# game/dialogs.rpy:2586
translate french matsuri_S_8a914f1f:

    # smom "That's sweet."
    smom "C'est bon de l'entendre."

# game/dialogs.rpy:2587
translate french matsuri_S_9981c331:

    # smom "Can I count on you to make her the happiest woman she's always wished to be?"
    smom "Je peux compter sur toi pour faire d'elle la femme heureuse qu'elle a toujours voulu être ?"

# game/dialogs.rpy:2588
translate french matsuri_S_707d7f10:

    # hero "I promise I will, ma'am!"
    hero "Je vous le promets, m'dame !"

# game/dialogs.rpy:2590
translate french matsuri_S_c944f29d:

    # "She smiled and I went back to Sakura. We waved goodbye to Sakura's mom and made our way to the festival."
    "Elle me fit un sourire alors que je rejoignis Sakura. Puis nous partîmes au festival en saluant la mère de Sakura."

# game/dialogs.rpy:2591
translate french matsuri_S_463b86c5:

    # "Loud noises and music were becoming audible as we got closer to the festival."
    "On entendait déjà au loin du bruit et de la musique."

# game/dialogs.rpy:2596
translate french matsuri_S_9ab29675:

    # s "What were you talking about with mom?"
    s "De quoi vous parliez, toi et maman ?"

# game/dialogs.rpy:2597
translate french matsuri_S_b96431ab:

    # hero "Umm... Nothing much..."
    hero "Oh... Rien de spécial..."

# game/dialogs.rpy:2598
translate french matsuri_S_3edbe663:

    # hero "By the way, your mom looks incredibly young."
    hero "Ta mère a l'air incroyablement jeune, au fait !"

# game/dialogs.rpy:2599
translate french matsuri_S_258d2b74:

    # s "Yeah, she gave birth to me when she was only 16 years old."
    s "Oui. Elle m'a donné naissance à 16 ans."

# game/dialogs.rpy:2600
translate french matsuri_S_bac1bc61:

    # s "My father was her Japanese teacher."
    s "Mon père était son professeur de japonais."

# game/dialogs.rpy:2601
translate french matsuri_S_d3f7f082:

    # hero "Haha, sounds like a manga cliché!"
    hero "Haha, ça fait un peu cliché de manga !"

# game/dialogs.rpy:2602
translate french matsuri_S_8dbb02af:

    # s "Teehee! Yes it is!"
    s "Hihi ! Oui c'est vrai !"

# game/dialogs.rpy:2604
translate french matsuri_S_f72540d4:

    # "She smiled for a while, but then it grew thin."
    "Elle sourit un moment, puis son sourire disparût doucement."

# game/dialogs.rpy:2605
translate french matsuri_S_e1554201:

    # s "..."
    s "..."

# game/dialogs.rpy:2606
translate french matsuri_S_c41191e9:

    # s "I don't like my father very much..."
    s "Je n'aime pas trop mon père, en fait..."

# game/dialogs.rpy:2607
translate french matsuri_S_b2052a3c:

    # hero "Yeah, I remember why, I think. Because he doesn't understand you, right?"
    hero "Je comprends bien ça. Parce qu'il ne te comprend pas, c'est ça ?"

# game/dialogs.rpy:2608
translate french matsuri_S_db0a0f02:

    # s "Sometimes he scares me..."
    s "Parfois il me fait très peur..."

# game/dialogs.rpy:2609
translate french matsuri_S_9465293c:

    # s "Recently he..."
    s "Récemment, il..."

# game/dialogs.rpy:2610
translate french matsuri_S_557b9c7c:

    # s ". . ."
    s ". . ."

# game/dialogs.rpy:2611
translate french matsuri_S_2639bac0:

    # s "He said he would never accept me as his daughter..."
    s "Il a dit qu'il ne m'accepterait jamais comme sa fille..."

# game/dialogs.rpy:2612
translate french matsuri_S_337c15c1:

    # "I didn't know what to say."
    "Je ne savais pas quoi dire."

# game/dialogs.rpy:2613
translate french matsuri_S_e2c7c8ae:

    # "Her father seems to be confused with the case of his child."
    "Son père a vraiment l'air confus par le cas de son enfant."

# game/dialogs.rpy:2614
translate french matsuri_S_0c7d97ed:

    # s "In fact, he even admitted that he has always preferred having a real boy,"
    s "En fait, il a même avoué qu'il avait toujours préféré avoir un vrai garçon."

# game/dialogs.rpy:2615
translate french matsuri_S_9f13d1a6:

    # s "But I'm a boy with everything girly. And it makes him angry..."
    s "Mais je ne suis que né garçon. Je suis une fille pour le reste. Et ça le rend nerveux..."

# game/dialogs.rpy:2616
translate french matsuri_S_66341891:

    # "I nodded again."
    "Je hochai."

# game/dialogs.rpy:2617
translate french matsuri_S_cd99f638:

    # hero "And how does your mom feel, about everything?"
    hero "Et que pense ta mère de tout ça ?"

# game/dialogs.rpy:2619
translate french matsuri_S_257a9f41:

    # s "She preferred a girl at the time, but she doesn't care at all, now."
    s "Elle préférait avoir une fille quand je n'étais pas encore née. Mais aujourd'hui, elle s'en moque."

# game/dialogs.rpy:2620
translate french matsuri_S_53c7acc4:

    # s "No matter if I'm a boy or a girl, I'm her child first of all..."
    s "Peu importe que je sois une fille ou un garçon, je suis avant tout son enfant..."

# game/dialogs.rpy:2621
translate french matsuri_S_08457630:

    # s "She understands me and she takes me for who I am."
    s "Elle me comprend et me prend pour ce que je suis."

# game/dialogs.rpy:2622
translate french matsuri_S_05bae488:

    # "I guessed that easily after the chat I had with her earlier."
    "Je l'avais facilement deviné après ma discussion avec elle."

# game/dialogs.rpy:2623
translate french matsuri_S_636c5687:

    # hero "That's a good thing."
    hero "C'est une bonne chose."

# game/dialogs.rpy:2624
translate french matsuri_S_9600023e:

    # hero "If only your dad saw things like your mom does..."
    hero "Si seulement ton père pouvait voir les choses comme ta mère..."

# game/dialogs.rpy:2625
translate french matsuri_S_88285ade:

    # s "Right."
    s "Oui..."

# game/dialogs.rpy:2626
translate french matsuri_S_c43195f9:

    # hero "You can count on your mother if something bad happens..."
    hero "Au moins, tu peux compter sur ta mère si jamais ça tourne mal..."

# game/dialogs.rpy:2627
translate french matsuri_S_36e0944d:

    # hero "And you know..."
    hero "Et tu sais..."

# game/dialogs.rpy:2628
translate french matsuri_S_5303d699:

    # hero "You can count on Rika-chan, Nanami-chan, and me if you need."
    hero "Tu peux aussi compter sur moi, Rika-chan et Nanami-chan."

# game/dialogs.rpy:2630
translate french matsuri_S_1b982e5f:

    # "Sakura smiled and held my arm tightly."
    "Sakura sourit et me serra tendrement le bras."

# game/dialogs.rpy:2631
translate french matsuri_S_2775e546:

    # s "I know, %(stringhero)s-kun..."
    s "Je sais, %(stringhero)s-kun..."

# game/dialogs.rpy:2636
translate french matsuri_S_8fa16578:

    # "We arrived at the festival and were joined by Rika-chan."
    "On arriva au festival et nous fûmes rejoints par Rika."

# game/dialogs.rpy:2637
translate french matsuri_S_42b57c94:

    # "She was wearing a very pretty purple yukata that showed her shoulders outside."
    "Elle portait un joli yukata violet qui laissait ses épaules à l'air libre."

# game/dialogs.rpy:2639
translate french matsuri_S_856a9a31:

    # r "Hey hey hey, you lovers!!!"
    r "Héhéhé, les amoureux !"

# game/dialogs.rpy:2641
translate french matsuri_S_aa24547d:

    # s "Rika-chan!"
    s "Rika-chan !"

# game/dialogs.rpy:2642
translate french matsuri_S_70def7d1:

    # hero "Rika-chan! Your yukata is great!!"
    hero "Rika-chan ! Ton yukata est magnifique !"

# game/dialogs.rpy:2643
translate french matsuri_S_1c3fbf24:

    # r "Pfft, I know what you really mean, you pervert! {image=heart.png}{p}How are you guys?"
    r "Je sais ce que tu veux dire, pervers !{image=heart.png}{p}Comment ça va, vous deux ?"

# game/dialogs.rpy:2645
translate french matsuri_S_b140b475:

    # r "My, my! Looks like our city rat has himself a nice yukata!"
    r "Eh bien, on dirait que le citadin a un bien chouette yukata !"

# game/dialogs.rpy:2646
translate french matsuri_S_8051a326:

    # hero "Thanks, Rika-chan- {p}Hey wait! I'm no city rat!!!"
    hero "Merci, Rika-chan...{p}Hé, je suis pas un citadin !!!"

# game/dialogs.rpy:2648
translate french matsuri_S_9cb75162:

    # hero "By the way, where is Nanami-chan?"
    hero "Où est Nanami-chan, au fait ?"

# game/dialogs.rpy:2649
translate french matsuri_S_6683fd53:

    # r "Knowing her, she probably went off to playing the shooting games."
    r "Elle est probablement aux stands de tir."

# game/dialogs.rpy:2650
translate french matsuri_S_0fef1f71:

    # hero "Shooting games?"
    hero "Stands de tir ?"

# game/dialogs.rpy:2651
translate french matsuri_S_8644460f:

    # hero "Let's go join her and play too!"
    hero "On n'a qu'à la rejoindre et y jouer aussi !"

# game/dialogs.rpy:2653
translate french matsuri_S_5a861c69:

    # s "Oh I would love to!!!"
    s "Oh j'aimerais beaucoup !!"

# game/dialogs.rpy:2654
translate french matsuri_S_e0fe552c:

    # r "Beware, %(stringhero)s, Sakura is the best shooter in the whole village!"
    r "Fais gaffe, %(stringhero)s, Sakura est la meilleure tireuse du village !"

# game/dialogs.rpy:2655
translate french matsuri_S_64c71cc7:

    # hero "We'll see about that!"
    hero "On va voir si elle peut m'avoir !"

# game/dialogs.rpy:2656
translate french matsuri_S_acc4c1f3:

    # "So we walk to a shooting stand..."
    "So we walk to a shooting stand..."

# game/dialogs.rpy:2660
translate french matsuri_S_1978a1c0:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"

# game/dialogs.rpy:2661
translate french matsuri_S_449ff6ae:

    # "Sakura made three perfect shots in a row on a big plushie of {i}ByeBye-Neko{/i} which fell on its back."
    "Sakura fit trois tirs parfaits et une peluche de {i}ByeBye-Neko{/i} tomba en arrière."

# game/dialogs.rpy:2662
translate french matsuri_S_5f5c92ba:

    # s "Yay! I love plushies!!!"
    s "Yay ! J'adore les peluches !!"

# game/dialogs.rpy:2663
translate french matsuri_S_d15d8e67:

    # n "You're great at this, Sakura-nee!"
    n "Tu es vraiment douée, Sakura-nee !"

# game/dialogs.rpy:2664
translate french matsuri_S_db10b573:

    # n "It doesn't feel like it does in the arcade."
    n "C'est sûr, c'est pas pareil que dans les jeux !"

# game/dialogs.rpy:2665
translate french matsuri_S_b799ab6b:

    # "I smiled."
    "Je souris."

# game/dialogs.rpy:2666
translate french matsuri_S_dd8906ec:

    # "I knew what to do..."
    "Je savais quoi faire..."

# game/dialogs.rpy:2667
translate french matsuri_S_0b28ef8e:

    # "I had to get another plushie for Sakura-chan!"
    "Il me fallait une autre peluche pour Sakura !"

# game/dialogs.rpy:2668
translate french matsuri_S_0d1e3f41:

    # hero "My turn to try!"
    hero "A mon tour !"

# game/dialogs.rpy:2669
translate french matsuri_S_997b966e:

    # "I gave ¥100 to the guy at the stand, took the gun and targeted a cute crocodile plushie that seemed easy to get."
    "Je donnai 100 ¥ au gars du stand, pris le fusil à bouchon et commençai à viser une petite peluche de crocodile qui semblait facile à avoir."

# game/dialogs.rpy:2670
translate french matsuri_S_1978a1c0_1:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"

# game/dialogs.rpy:2671
translate french matsuri_S_c8f69f16:

    # "Missed!"
    "Manqué !"

# game/dialogs.rpy:2672
translate french matsuri_S_924fcb79:

    # hero "Hold on, I'll try again!"
    hero "Je réessaie !"

# game/dialogs.rpy:2673
translate french matsuri_S_2ffa84ff:

    # "¥100 more on the table."
    "100 ¥ de plus sur la table."

# game/dialogs.rpy:2674
translate french matsuri_S_1978a1c0_2:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"

# game/dialogs.rpy:2675
translate french matsuri_S_df4219e2:

    # "Darn!!! The plushie moved a bit but it wasn't enough for it to fall!"
    "Mince ! La peluche a bougé, mais pas assez pour tomber !"

# game/dialogs.rpy:2676
translate french matsuri_S_6cc32b6b:

    # "¥100 more!"
    "Encore 100 ¥ !"

# game/dialogs.rpy:2677
translate french matsuri_S_1978a1c0_3:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"

# game/dialogs.rpy:2678
translate french matsuri_S_bea4f24c:

    # "I got it!!! I got the plushie!!!"
    "Je l'ai !! J'ai la peluche !!"

# game/dialogs.rpy:2679
translate french matsuri_S_67dae2f0:

    # "I was about to give it to Sakura, but I remembered Rika and Nanami."
    "J'allais la donner à Sakura et je me suis alors rapelé de Rika et Nanami."

# game/dialogs.rpy:2680
translate french matsuri_S_ad68e441:

    # "Maybe I should get one for each of them too..."
    "Je devrais peut-être leur en prendre une aussi..."

# game/dialogs.rpy:2681
translate french matsuri_S_52d0a5e1:

    # "As I was about to pay ¥100 more to the guy, Rika shoved herself through."
    "Mais quand j'allais aligner 100 ¥ de plus, Rika arriva."

# game/dialogs.rpy:2682
translate french matsuri_S_602dd045:

    # r "Hold it, city rat!!! It's my turn to play!!!"
    r "Pousse-toi, le citadin, c'est à mon tour !!"

# game/dialogs.rpy:2683
translate french matsuri_S_8834e601:

    # r "I'll get the same plushie with only three shots, you'll see!!!"
    r "Je vais avoir la même peluche en juste trois tirs, tu vas voir !!"

# game/dialogs.rpy:2684
translate french matsuri_S_2a606826:

    # "While Rika and Nanami were busy at the stand, I gave the plushie to Sakura."
    "Alors que Rika et Nanami étaient occupées au stand, je donnai la peluche à Sakura."

# game/dialogs.rpy:2685
translate french matsuri_S_2c879bcb:

    # s "For me?"
    s "Pour moi ?"

# game/dialogs.rpy:2686
translate french matsuri_S_c824434e:

    # s "Oh, it's so cute!!!"
    s "Oh c'est trop chou !!"

# game/dialogs.rpy:2687
translate french matsuri_S_20fdfbfd:

    # s "Thank you, %(stringhero)s-kun!!"
    s "Merci, %(stringhero)s-kun !!"

# game/dialogs.rpy:2688
translate french matsuri_S_f2aa2a51:

    # "She kissed me wildly in front of everybody, in a way that a man is more used to doing than a girl."
    "Elle m'embrassa sauvagement devant tout le monde, d'une manière qu'un garçon serait plus amené à faire qu'une fille."

# game/dialogs.rpy:2689
translate french matsuri_S_fcea0f28:

    # "I felt embarrassed and shy, but happy..."
    "C'était un peu gênant mais j'étais content..."

# game/dialogs.rpy:2690
translate french matsuri_S_15bd7f0a:

    # r "Ah!!! Missed again, darn!"
    r "Ahhh !! Encore raté, bon sang !!"

# game/dialogs.rpy:2691
translate french matsuri_S_b3487258:

    # "Apparently, Rika-chan isn't very good at shooting games..."
    "Apparemment, Rika n'est pas doué au tir..."

# game/dialogs.rpy:2692
translate french matsuri_S_4ca1680a:

    # "But she finally got a plushie too, after a while..."
    "Mais elle a eu finalement sa peluche après un moment..."

# game/dialogs.rpy:2693
translate french matsuri_S_016f8a4d:

    # "I think it's because the guy was seduced by Rika-chan's sexy yukata..."
    "Je crois que le vendeur a été séduit par le yukata sexy de Rika..."

# game/dialogs.rpy:2694
translate french matsuri_S_20e94235:

    # "Gah, seriously..."
    "Arf, sérieusement..."

# game/dialogs.rpy:2695
translate french matsuri_S_015dea9a:

    # hero "How about we get some food now? I feel hungry."
    hero "Si on allait manger, maintenant ? J'ai faim. Pas vous ?"

# game/dialogs.rpy:2696
translate french matsuri_S_80443115:

    # r "Excellent idea!!! Me too!!!"
    r "Bonne idée ! Moi aussi !!"

# game/dialogs.rpy:2697
translate french matsuri_S_3e4fce92:

    # s "Me too, a little."
    s "Moi aussi, un peu."

# game/dialogs.rpy:2698
translate french matsuri_S_278f0f18:

    # r "Let's go! I'll show you a good stand where they make excellent takoyaki!"
    r "Allons-y ! Je connais un excellent stand de takoyakis !"

# game/dialogs.rpy:2703
translate french takoyakis_b486162f:

    # "Our meal was just excellent, as Rika-chan promised!"
    "Notre repas fut vraiment bon, comme Rika le disait !"

# game/dialogs.rpy:2704
translate french takoyakis_b36de308:

    # hero "I'm full!"
    hero "Je suis plein !"

# game/dialogs.rpy:2705
translate french takoyakis_c5430508:

    # r "Told ya it was good! This is the best place to find them!"
    r "Je vous l'avais bien dit ! C'est le meilleur stand pour ceux-là !"

# game/dialogs.rpy:2706
translate french takoyakis_f4e941cb:

    # r "I want more! Hey, more of these please!!!"
    r "S'il vous plait ! J'en voudrais encore !!"

# game/dialogs.rpy:2707
translate french takoyakis_b42e6c2a:

    # n "Can't... eat... more..."
    n "Peux.. Plus... Bouffer..."

# game/dialogs.rpy:2708
translate french takoyakis_032debaa:

    # hero "Rika-chan, how can you eat so much of this stuff??? Do you have a black hole instead of a stomach or what?"
    hero "Rika-chan, comment tu fais pour en manger autant ?? T'as un trou noir dans l'estomac ou quoi ?"

# game/dialogs.rpy:2709
translate french takoyakis_dc489790:

    # s "Actually, Rika-chan can eat a mountain of food without adding on any pounds."
    s "En fait, Rika-chan a le don de pouvoir manger une montagne de choses sans jamais grossir."

# game/dialogs.rpy:2710
translate french takoyakis_11fca699:

    # hero "Really? She's lucky."
    hero "Vraiment ? Elle a de la chance."

# game/dialogs.rpy:2711
translate french takoyakis_70921009:

    # hero "I have to control my own weight to make sure I don't end up too fat..."
    hero "Il faut que je fasse attention de ne pas trop grossir..."

# game/dialogs.rpy:2712
translate french takoyakis_e6e2b7e9:

    # s "Teehee! Same for me!"
    s "Hihi ! Pareil pour moi !"

# game/dialogs.rpy:2717
translate french takoyakis_b3e735ce:

    # "We finished the festival by watching the fireworks..."
    "Le festival se termina par les feux d'artifices..."

# game/dialogs.rpy:2718
translate french takoyakis_d988d3e5:

    # "The show was fabulous."
    "Le spectacle était merveilleux."

# game/dialogs.rpy:2719
translate french takoyakis_7e1eaf4f:

    # "At a moment, Sakura and I kissed discreetly in the dark under the fireworks..."
    "A un moment, Sakura et moi nous sommes embrassés dans le noir, sous les feux d'artifices..."

# game/dialogs.rpy:2722
translate french takoyakis_f06e1942:

    # "Sakura-chan and I were on the way back home."
    "Sakura et moi étions en route pour nos maisons."

# game/dialogs.rpy:2723
translate french takoyakis_5cc5084b:

    # "I'm completely overwhelmed of happiness because of the festival and that night with Sakura-chan."
    "Je me sentais en fête et plein de bonheur à cause du festival et cette nuit avec Sakura."

# game/dialogs.rpy:2724
translate french takoyakis_6cf004ac:

    # "Without a second thought, I tried to act out a cliché from movies I've seen before."
    "C'est pourquoi, sans crainte de rien, je lui ai sorti une réplique que j'avais entendu dans un film."

# game/dialogs.rpy:2725
translate french takoyakis_60443369:

    # hero "Want to come back to my place for a drink?"
    hero "Ça te dirait, un dernier verre chez moi ?"

# game/dialogs.rpy:2726
translate french takoyakis_52a57200:

    # s "Huh...?"
    s "Hmm...?"

# game/dialogs.rpy:2727
translate french takoyakis_676d485e:

    # "I suddenly noticed what I just said. I started to blush and spoke incoherently."
    "Je réalisai soudainement ce que je venais de dire. J'ai commencé à bégayer en rougissant."

# game/dialogs.rpy:2728
translate french takoyakis_8d758b57:

    # hero "I-I-I-I mean if... If you... Well I mean y-y--{nw}"
    hero "J-j-j-je veux dire, si... Enfin si... J'veux dire si tu veux..{nw}"

# game/dialogs.rpy:2729
translate french takoyakis_e043d770:

    # s "Okay."
    s "Oui."

# game/dialogs.rpy:2730
translate french takoyakis_b16c33f4:

    # "I was surprised."
    "J'étais surpris."

# game/dialogs.rpy:2732
translate french takoyakis_dc6dab16:

    # "Sakura watched me with a tinge of shyness."
    "Sakura me regardait avec timidité."

# game/dialogs.rpy:2733
translate french takoyakis_74a6bdef:

    # "She looked like a cute begging kitten."
    "Elle ressemblait à un chaton qui quémandait."

# game/dialogs.rpy:2734
translate french takoyakis_da34f8b5:

    # s "Y-Yes, take me to your home...%(stringhero)s-kun..."
    s "O-oui, emmène-moi chez toi...%(stringhero)s-kun..."

# game/dialogs.rpy:2736
translate french takoyakis_ee16eceb:

    # "My heart skipped a beat."
    "Mon cœur s'est mis à battre fort."

# game/dialogs.rpy:2737
translate french takoyakis_ceb3d2b6:

    # "My own girlfriend... In a kimono...coming to my bedroom...for the night..."
    "Ma propre copine...dans un kimono...venant dans ma chambre...pour y passer la nuit..."

# game/dialogs.rpy:2738
translate french takoyakis_ddafe283:

    # "I'm pretty sure that we wouldn't just sleep that night..."
    "Je suis sûr qu'on ne va pas que y dormir, cette nuit..."

# game/dialogs.rpy:2742
translate french takoyakis_f412f669:

    # "Apparently my parents weren't back from the festival yet."
    "Apparemment, mes parents n'étaient pas encore rentrés."

# game/dialogs.rpy:2745
translate french takoyakis_4bb8cb02:

    # "We entered my house silently and went upstairs and into my bedroom."
    "J'entrai silencieusement chez moi, suivi de Sakura, jusque dans ma chambre."

# game/dialogs.rpy:2747
translate french takoyakis_86309b2e:

    # "When I turned on the lights, Sakura opened her eyes widely."
    "En allumant la lumière, Sakura ouvrit de grands yeux."

# game/dialogs.rpy:2749
translate french takoyakis_abbe7785:

    # s "Whoaaa!!!"
    s "Whoaaa !!!"

# game/dialogs.rpy:2750
translate french takoyakis_215547d3:

    # s "It's incredible!!! All of these posters!"
    s "C'est incroyable, tous ces posters !"

# game/dialogs.rpy:2751
translate french takoyakis_6430e2fa:

    # s "Oh! It's {i}High School Samurai{/i}!"
    s "Oh ! Le {i}High School Samurai{/i} !"

# game/dialogs.rpy:2752
translate french takoyakis_52931ee2:

    # hero "Haha, yeah. I took the habit of collecting posters..."
    hero "Héhé, ouais. J'avais l'habitude de les collectionner..."

# game/dialogs.rpy:2753
translate french takoyakis_f00c4d4e:

    # s "You bought them at Tokyo?"
    s "Tu les as eu à Tokyo ?"

# game/dialogs.rpy:2754
translate french takoyakis_0551d793:

    # hero "Yes. I was living near Gaymerz, the biggest manga shop of Tokyo."
    hero "Oui. Je vivais pas loin du Gaymers, le plus gros magasin de manga de Tokyo."

# game/dialogs.rpy:2756
translate french takoyakis_b4f4239b:

    # s "Oh, I've heard of it!"
    s "Oh, j'en ai entendu parler !"

# game/dialogs.rpy:2757
translate french takoyakis_4e9a74e5:

    # s "I hope I can visit it someday."
    s "J'espère pouvoir un jour le visiter !"

# game/dialogs.rpy:2758
translate french takoyakis_05c9701b:

    # "As she was speaking, I hugged her back lovingly."
    "Alors qu'elle parlait, je serrai son dos contre moi tendrement."

# game/dialogs.rpy:2763
translate french takoyakis_ef409314:

    # "We made sweet love to each other."
    "On a dormi rapidement...après avoir fait l'amour tendrement..."

# game/dialogs.rpy:2764
translate french takoyakis_c6b9d465:

    # "We were so tired after this evening...We dozed off..."
    "On était si fatigués après une telle soirée... On s'est endormis juste après..."

# game/dialogs.rpy:2767
translate french takoyakis_0cee0472:

    # "I woke up slowly."
    "Je me réveillai lentement."

# game/dialogs.rpy:2768
translate french takoyakis_b2766fe2:

    # "Looking at the alarm clock, it's kinda late."
    "Je vis sur le réveil qu'il était tard dans la matinée."

# game/dialogs.rpy:2769
translate french takoyakis_f87aab37:

    # "I felt something warm against me..."
    "Je sentis quelque chose de chaud contre moi..."

# game/dialogs.rpy:2770
translate french takoyakis_58e3a51c:

    # "It was Sakura."
    "C'était Sakura."

# game/dialogs.rpy:2771
translate french takoyakis_ec9d25fd:

    # "She was sleeping naked in my arms..."
    "Elle dormait nue dans mes bras..."

# game/dialogs.rpy:2772
translate french takoyakis_93a9f4ba:

    # "I smile and watched her sleeping..."
    "Je souris en la regardant dans son sommeil..."

# game/dialogs.rpy:2773
translate french takoyakis_4774de34:

    # "Even after the obvious evidence I've seen, I still can't believe that she's actually a boy..."
    "Même après les preuves évidentes que j'avais vues, je n'arrivais toujours pas à croire qu'elle est un garçon..."

# game/dialogs.rpy:2774
translate french takoyakis_c2358eed:

    # "She looks like a girl... She speaks with a voice of a girl... She smells like a girl... She has soft skin like a girl..."
    "Elle ressemble tellement à une fille... Elle parle avec la voix d'une fille... Elle sent comme une fille... Elle a une peau douce comme une fille..."

# game/dialogs.rpy:2775
translate french takoyakis_6f1e912a:

    # "I felt my heart beating sweetly as her warm sweet little hand was on my chest..."
    "Je sentis mon cœur battre en sentant sa petite main sur mon torse..."

# game/dialogs.rpy:2776
translate french takoyakis_eda104c0:

    # "She finally opened her eyes. As she saw me, she smiled and cuddled me tighter with a sweet sigh... and went back to sleep..."
    "Elle finit par ouvrir les yeux. En me voyant, elle fit un sourire et me serra contre elle avec un doux soupir... Puis se rendormit..."

# game/dialogs.rpy:2777
translate french takoyakis_0deabf9f:

    # "I sighed of happiness..."
    "Je soupirai de bonheur..."

# game/dialogs.rpy:2778
translate french takoyakis_a20cefa7:

    # "..."
    "..."

# game/dialogs.rpy:2779
translate french takoyakis_8b8ceb72:

    # "............"
    "............"

# game/dialogs.rpy:2780
translate french takoyakis_9ed134ce:

    # "Oh..."
    "Oh..."

# game/dialogs.rpy:2781
translate french takoyakis_c1399882:

    # "Oh no!!!{p}My parents!!!"
    "Oh non !!{p}Mes parents !!"

# game/dialogs.rpy:2782
translate french takoyakis_fb40c1b5:

    # "They're surely back from the festival!!!"
    "Ils sont sûrement rentrés du festival depuis le temps !!"

# game/dialogs.rpy:2783
translate french takoyakis_3b788596:

    # "What are they gonna say if they see me with a girl in my bed???"
    "Qu'est-ce qu'ils vont dire s'ils me voient dans ma chambre avec une fille nue dans mon lit ?!!"

# game/dialogs.rpy:2784
translate french takoyakis_2cffeee3:

    # "What should I do? Dammit, dammit, dammit!!!"
    "Que faire, bon sang ?!!"

# game/dialogs.rpy:2785
translate french takoyakis_c1fa5fd9:

    # hero "Huh... Sakura-chan?..."
    hero "Euh... Sakura-chan ?..."

# game/dialogs.rpy:2786
translate french takoyakis_dfd2530f:

    # s "{size=-5}%(stringhero)s-kun... I love you...{/size}"
    s "{size=-5}Mmmmm... Je t'aime, mon %(stringhero)s-kun...{/size}"

# game/dialogs.rpy:2787
translate french takoyakis_817848c9:

    # "She fell asleep again!?"
    "Mince, elle s'est rendormie !"

# game/dialogs.rpy:2788
translate french takoyakis_0df5e11e:

    # "I stopped panicking to listen for a moment. I was waiting for an indication that would tell me if my parents were back."
    "J'ai attendu un moment, guettant le moindre bruit annonciateur de la présence de mes parents."

# game/dialogs.rpy:2789
translate french takoyakis_9850f53c:

    # "Nothing."
    "Rien."

# game/dialogs.rpy:2790
translate french takoyakis_d047fac0:

    # "I slowly moved out from my bed, letting Sakura sleep and looked outside the window."
    "Je me levai doucement, laissant Sakura dans les bras de Morphée un peu plus, et regarda par la fenêtre."

# game/dialogs.rpy:2791
translate french takoyakis_f6b451ec:

    # "My parent's car wasn't there... Maybe I can do something at last..."
    "La voiture de mes parents n'était pas là... Peut-être que je peux tenter quelque chose..."

# game/dialogs.rpy:2792
translate french takoyakis_4d679905:

    # s "%(stringhero)s-kun?...."
    s "%(stringhero)s-kun ?...."

# game/dialogs.rpy:2793
translate french takoyakis_08748d66:

    # "Sakura just woke up and sat on the bed."
    "Sakura s'était réveillée et s'était assise sur le lit."

# game/dialogs.rpy:2794
translate french takoyakis_f25fd7bb:

    # hero "Sakura-chan..."
    hero "Sakura-chan..."

# game/dialogs.rpy:2795
translate french takoyakis_ddcb931f:

    # hero "My parents aren't here yet. You should go home now before they come back."
    hero "Mes parents ne sont pas là. Mais tu devrais rentrer avant qu'ils ne reviennent."

# game/dialogs.rpy:2796
translate french takoyakis_7295fac4:

    # hero "My parents are kinda strict when it comes to relationships."
    hero "Mes parents sont plutôt stricts quand il s'agit de relations amoureuses !"

# game/dialogs.rpy:2797
translate french takoyakis_b3eeb634:

    # "Sakura nodded. She looked all sleepy."
    "Sakura acquiesça. Elle avait l'air toute endormie encore."

# game/dialogs.rpy:2798
translate french takoyakis_aee46875:

    # "That was cute. I smiled."
    "C'était adorable. Je souris."

# game/dialogs.rpy:2799
translate french takoyakis_8891ed58:

    # hero "Aww. Had too much fun last night?"
    hero "Aww, on s'est trop amusés, cette nuit ?"

# game/dialogs.rpy:2800
translate french takoyakis_e15b24d1:

    # "Sakura giggled. She stood up and took me in her arms."
    "Sakura rit. Elle se leva et me prit dans ses bras."

# game/dialogs.rpy:2801
translate french takoyakis_67bc7996:

    # "Her warm naked body was against mine."
    "Son corps nu encore chaud contre le mien."

# game/dialogs.rpy:2802
translate french takoyakis_387596f6:

    # "I kissed her again."
    "Je l'embrassai une nouvelle fois."

# game/dialogs.rpy:2804
translate french takoyakis_7dbc9795:

    # "After some time, I helped her put on her kimono and took her to her house..."
    "Après un moment, je l'aidai à mettre son kimono et la raccompagna chez elle..."

# game/dialogs.rpy:2805
translate french takoyakis_5ffe88e0:

    # "We promised to have another date very soon..."
    "On s'est promis de se faire un nouveau rencart bientôt..."

# game/dialogs.rpy:2806
translate french takoyakis_17899d6b:

    # "I went back home and turned on my computer."
    "Je revins chez moi et alluma mon ordinateur."

# game/dialogs.rpy:2810
translate french takoyakis_2b150c3e:

    # write "Dear sister,"
    write "Chère grande sœur."

# game/dialogs.rpy:2812
translate french takoyakis_a50c844b:

    # write "How are things? It's been awhile since I wrote."
    write "Comment vas-tu ? Cela fait un bail que je ne t'avais pas écrit."

# game/dialogs.rpy:2814
translate french takoyakis_54b9d70d:

    # write "You'll never guess what happened!"
    write "Tu ne devineras jamais ce qui m'est arrivé !"

# game/dialogs.rpy:2816
translate french takoyakis_67d2ac31:

    # write "I'm going out with someone!"
    write "Je sors avec quelqu'un !!"

# game/dialogs.rpy:2818
translate french takoyakis_df940a1c:

    # write "She's a wonderful person... A unique person...{w} It's kinda hard to tell you more about this...{w}You have to promise that you'll never tell our parents!"
    write "C'est une personne merveilleuse...vraiment unique... {w}C'est un peu compliqué à expliquer...{w}Il faut que tu me promettes que tu ne diras rien à papa et maman avant."

# game/dialogs.rpy:2820
translate french takoyakis_bc7a4cdb:

    # write "At least, I'm happy... More happy than I have ever been... It's as if I just got the best Christmas present ever."
    write "Ce qui compte c'est que je suis heureux... Heureux comme jamais... C'est comme Noël avant l'heure !"

# game/dialogs.rpy:2821
translate french takoyakis_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:2823
translate french takoyakis_bf522c92:

    # write "I'll send you a photo of us. I don't know how much longer our story will last, but I'm sure I'll never forget it for the rest of my life. What I'm living is really unique!"
    write "Je t'enverrai une photo de nous bientôt. Je ne sais pas combien de temps notre histoire va durer, mais je suis sûr que je ne l'oublierai jamais ! Ce que je vis est vraiment extraordinaire !"

# game/dialogs.rpy:2825
translate french takoyakis_f52f892c:

    # write "I hope everything is okay for you too and that you are as happy as I am..."
    write "J'espère que tout va bien pour toi et que tu es aussi heureuse que moi."

# game/dialogs.rpy:2827
translate french takoyakis_f60fe613:

    # write "Your dear brother{p}%(stringhero)s"
    write "Ton frère{p}%(stringhero)s"

# game/dialogs.rpy:2829
translate french takoyakis_4387dacf:

    # write "PS: {w}The local summer festival was great! I hope you had a wonderful event like this in Tokyo."
    write "PS : {w}Le festival d'été local était génial ! J'espère qu'il y a eu des fêtes comme ça pas loin de chez toi aussi."

# game/dialogs.rpy:2830
translate french takoyakis_76b2fe88_1:

    # nvl clear
    nvl clear

# game/dialogs.rpy:2852
translate french matsuri_N_46751f32:

    # "I came to the festival with Sakura."
    "J'arrivai au festival avec Sakura."

# game/dialogs.rpy:2853
translate french matsuri_N_17894765:

    # "She was wearing a cute yukata. It fit her well."
    "Elle portait un yukata vraiment très mignon. Ça lui allait bien."

# game/dialogs.rpy:2855
translate french matsuri_N_ec440c0a:

    # "I was wearing the brand new yukata that she gave me."
    "Je portais le yukata qu'elle m'avait donné."

# game/dialogs.rpy:2857
translate french matsuri_N_201b46ce:

    # "I was wearing the yukata I recently bought... One which was awfully expensive..."
    "Je portais le yukata que j'avais acheté avec mes économies... Qui ont flambé avec, par dessus le marché..."

# game/dialogs.rpy:2858
translate french matsuri_N_38154d05:

    # "We quickly found Nanami and Rika despite the big crowd."
    "On a rapidement trouvé Nanami et Rika malgré la foule."

# game/dialogs.rpy:2863
translate french matsuri_N_866e240e:

    # "Rika was wearing a very pretty purple yukata that showed her shoulders outside."
    "Rika portait un joli yukata violet qui laissait ses épaules à l'air libre."

# game/dialogs.rpy:2864
translate french matsuri_N_e57fd5f6:

    # "Nanami was wearing a more simple green yukata but it was incredibly cute on her."
    "Nanami portait un yukata vert plus simple mais épiquement mimi sur elle."

# game/dialogs.rpy:2865
translate french matsuri_N_c1358676:

    # r "So, what we do first? I can't wait!"
    r "On fait quoi pour commencer ? J'ai hâte !"

# game/dialogs.rpy:2867
translate french matsuri_N_e60b3319:

    # n "I want to go to the shooting stands!"
    n "Je veux aller aux stands de tir !"

# game/dialogs.rpy:2868
translate french matsuri_N_a6a52bc4:

    # s "That sounds nice!"
    s "Ohh, bonne idée !"

# game/dialogs.rpy:2869
translate french matsuri_N_e8f6b7d0:

    # hero "I'd like that too!"
    hero "Ça me va aussi !"

# game/dialogs.rpy:2870
translate french matsuri_N_f8974ce9:

    # r "Let's go!"
    r "Allons-y !"

# game/dialogs.rpy:2872
translate french matsuri_N_228ae33a:

    # "We arrived just in time. The stand was pretty empty."
    "On arriva au stand au bon moment. Il y avait peu de monde."

# game/dialogs.rpy:2874
translate french matsuri_N_4cdd6550:

    # n "Hey, %(stringhero)s-nii... I challenge you to make a plushie fall before I do!"
    n "Hé, %(stringhero)s-nii... Je te défie de faire tomber une peluche avant moi !"

# game/dialogs.rpy:2875
translate french matsuri_N_2635f363:

    # hero "I thought you weren't good at shooting games."
    hero "Je croyais que t'étais pas douée au tir."

# game/dialogs.rpy:2876
translate french matsuri_N_88c489c7:

    # n "I trained hard! You'll see!"
    n "Je me suis entraînée ! Tu vas voir !"

# game/dialogs.rpy:2881
translate french matsuri_N_cd54b02a:

    # "We both payed ¥100 and started to frantically shoot at the targets with our toy rifles."
    "On paya 100 ¥ et on commença à frénétiquement tirer sur les cibles avec nos fusils à bouchon."

# game/dialogs.rpy:2882
translate french matsuri_N_1978a1c0:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"

# game/dialogs.rpy:2883
translate french matsuri_N_d4bb63e2:

    # "We both missed... We tried again."
    "On manqua tous les deux... On réessaya."

# game/dialogs.rpy:2884
translate french matsuri_N_1978a1c0_1:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"

# game/dialogs.rpy:2885
translate french matsuri_N_df8da603:

    # "We hit the same plush but it barely moved."
    "On toucha la même peluche mais elle bougea à peine."

# game/dialogs.rpy:2886
translate french matsuri_N_8d01865c:

    # n "Eeeeh!"
    n "Eeeeh !"

# game/dialogs.rpy:2887
translate french matsuri_N_1feb4bc0:

    # hero "No fair! It was a critical hit!"
    hero "Pas juste ! C'était un coup critique !"

# game/dialogs.rpy:2888
translate french matsuri_N_cf97790a:

    # n "I'm not done yet!"
    n "J'en ai pas encore fini !"

# game/dialogs.rpy:2889
translate french matsuri_N_3093850e:

    # "We tried again once more..."
    "On réessaya une nouvelle fois..."

# game/dialogs.rpy:2890
translate french matsuri_N_1978a1c0_2:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"

# game/dialogs.rpy:2891
translate french matsuri_N_ffac77fc:

    # "I got it! I got the plush!"
    "Je l'ai ! J'ai eu la peluche !!"

# game/dialogs.rpy:2895
translate french matsuri_N_2530a4ee:

    # hero "Haha I won!"
    hero "Haha ! J'ai gagné !!"

# game/dialogs.rpy:2896
translate french matsuri_N_638da9e6:

    # "It was a cute plush of crocodile."
    "C'était une petite peluche de crocodile."

# game/dialogs.rpy:2897
translate french matsuri_N_b9dca595:

    # "Nanami started to pout."
    "Nanami commença à bouder."

# game/dialogs.rpy:2898
translate french matsuri_N_0d531ead:

    # n "Eeeeh that's unfair, you got it because I hit it a couple times already!"
    n "Ehhh c'est pas juste, tu l'as eu parce que je l'ai touchée plus tôt !"

# game/dialogs.rpy:2899
translate french matsuri_N_1bdd906d:

    # n "I wanted that plush!"
    n "Je voulais cette peluche !..."

# game/dialogs.rpy:2900
translate french matsuri_N_adab4cae:

    # "I laughed and rubbed her hair."
    "Je ris en lui caressant la tête."

# game/dialogs.rpy:2901
translate french matsuri_N_e0e8e5a5:

    # hero "Don't be silly. {p}I got it for you."
    hero "Idiote. C'est pour toi que je l'ai prise.{p}Tiens."

# game/dialogs.rpy:2902
translate french matsuri_N_c55aa101:

    # "I gave Nanami the plushie."
    "Je donnai la peluche à Nanami."

# game/dialogs.rpy:2903
translate french matsuri_N_3eb6b6ea:

    # "She looked at me with a cute expression, then she gave me a big hug."
    "Elle me regarda avec une expression mignonne, puis elle me serra contre elle."

# game/dialogs.rpy:2905
translate french matsuri_N_5693f2ef:

    # n "Yaaay! Thank you, %(stringhero)s-nii!"
    n "Yaay ! Merci, %(stringhero)s-nii !"

# game/dialogs.rpy:2906
translate french matsuri_N_162f5687:

    # r "Hey, get a room, you two!"
    r "Hé, trouvez-vous une chambre, vous deux !"

# game/dialogs.rpy:2907
translate french matsuri_N_9a893c8c:

    # "We all laughed."
    "On a tous ri."

# game/dialogs.rpy:2912
translate french matsuri_N_9ced4dba:

    # hero "I'm hungry... How about we get some food now?"
    hero "Si on mangeait quelque chose, maintenant ? J'ai faim."

# game/dialogs.rpy:2917
translate french matsuri_N_80443115:

    # r "Excellent idea!!! Me too!!!"
    r "Bonne idée ! Moi aussi !"

# game/dialogs.rpy:2918
translate french matsuri_N_01dbb1d1:

    # s "I'm a little hungry too."
    s "Moi aussi, un peu."

# game/dialogs.rpy:2919
translate french matsuri_N_5e163699:

    # r "Let's go! I'll show you an amazing stand where they make the best takoyaki!"
    r "Allons-y ! Je connais un excellent stand de takoyakis !"

# game/dialogs.rpy:2926
translate french matsuri_N2_b3e735ce:

    # "We finished the festival by watching the fireworks..."
    "On finit le festival en regardant les feux d'artifices..."

# game/dialogs.rpy:2927
translate french matsuri_N2_d988d3e5:

    # "The show was fabulous."
    "Le spectacle était magnifique."

# game/dialogs.rpy:2930
translate french matsuri_N2_576baed2:

    # "As we we walked around some more, Nanami took my hand and guided me into the forest behind the temple..."
    "A un moment, Nanami me prit la main et me guida dans la forêt derrière le temple..."

# game/dialogs.rpy:2931
translate french matsuri_N2_c50c979d:

    # "We made discreet, passionate love behind the bushes..."
    "On y a fait l'amour, discrètement et passionnément, cachés derrière les buissons..."

# game/dialogs.rpy:2932
translate french matsuri_N2_633fea5f:

    # "Now that's what I call a night!"
    "Eh bien... C'est ce que j'appelle une sacré nuit !"

# game/dialogs.rpy:2935
translate french matsuri_N2_2ab4bd94:

    # "After escorting Sakura to her house, I went back to my own."
    "Après avoir escorté Sakura chez elle, je rentrai chez moi."

# game/dialogs.rpy:2937
translate french matsuri_N2_6f974e08:

    # "My parents weren't back yet so I just goofed around and read some manga.{p} I also decided to write to my sister."
    "Mes parents n'étaient pas encore rentrés alors j'ai geeké un peu.{p}Et j'ai écrit à ma sœur..."

# game/dialogs.rpy:2940
translate french matsuri_N2_2b150c3e:

    # write "Dear sister,"
    write "Chere grande sœur."

# game/dialogs.rpy:2942
translate french matsuri_N2_92248cbb:

    # write "How are things?"
    write "Comment vas-tu ?"

# game/dialogs.rpy:2944
translate french matsuri_N2_bb83fcbc:

    # write "You'll never guess what happened to me actually!"
    write "Tu ne devineras jamais ce qui m'est arrivé !"

# game/dialogs.rpy:2946
translate french matsuri_N2_4967d059:

    # write "I'm going out with a girl!"
    write "Je sors avec une fille !"

# game/dialogs.rpy:2948
translate french matsuri_N2_534e4421:

    # write "Her name is Nanami. She's a really cute and energetic girl from school.{w} We play a lot of video games together. We're really into each other!"
    write "Elle s'appelle Nanami. C'est une fille de l'école très mignonne et énergique. {w}On arrête pas de se défier aux jeux vidéo, mais on s'aime vraiment beaucoup... "

# game/dialogs.rpy:2950
translate french matsuri_N2_4f13aea8:

    # write "I'm happy... Really happy... I can't wait to introduce her to you, someday! {w}I'll send you a photo of us soon!"
    write "Je suis vraiment heureux !... J'ai hâte de te la présenter un jour !{w} Je t'enverrai une photo de nous deux, bientôt ! Elle est vraiment jolie."

# game/dialogs.rpy:2951
translate french matsuri_N2_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:2953
translate french matsuri_N2_ab4193c3:

    # write "I hope everything going well for you! I can't wait to see you again!"
    write "J'espère que tout va bien chez toi et que tu es aussi heureuse que moi..."

# game/dialogs.rpy:2955
translate french matsuri_N2_f60fe613:

    # write "Your dear brother{p}%(stringhero)s"
    write "Ton frère{p}%(stringhero)s"

# game/dialogs.rpy:2957
translate french matsuri_N2_91609d26:

    # write "PS:{w} The local summer festival was great! I hope you had a wonderful event like this in Tokyo."
    write "PS:{w} Le festival d'été local était génial ! J'espère que tu as eu une fête dans ce genre là où tu es, à Tokyo."

# game/dialogs.rpy:2959
translate french matsuri_N2_deb10d39:

    # "I heard my cellphone ring."
    "Soudainement, mon portable sonna."

# game/dialogs.rpy:2964
translate french matsuri_N2_dbd7e6ae:

    # hero "Hello?"
    hero "Allô ?"

# game/dialogs.rpy:2965
translate french matsuri_N2_77c066ad:

    # n "{i}It's me, dude!{/i}"
    n "{i}C'est moi, mec !{/i}"

# game/dialogs.rpy:2966
translate french matsuri_N2_f295726b:

    # "I chuckled as I recognized Nanami's voice."
    "Je ris en reconnaissant la voix de Nanami."

# game/dialogs.rpy:2967
translate french matsuri_N2_404c955e:

    # hero "We're going out and you still call me dude?"
    hero "On sort ensemble depuis une semaine et tu continues à m'appeler mec ?"

# game/dialogs.rpy:2968
translate french matsuri_N2_284b7d55:

    # n "{i}Well, you're a guy after all, right?{/i}"
    n "{i}Ben t'es bien un mec, non ?{/i}"

# game/dialogs.rpy:2969
translate french matsuri_N2_770ce25a:

    # n "{i}I've seen you from enough close to see it with my own eyes!{/i} {image=heart.png}"
    n "{i}Je t'ai vu d'assez près pour m'en rendre compte ! {image=heart.png}{/i}"

# game/dialogs.rpy:2970
translate french matsuri_N2_7139659c:

    # "I blushed, remembering what we did earlier in the forest."
    "Je rougis en repensant à notre nuit dans les fourrés."

# game/dialogs.rpy:2971
translate french matsuri_N2_e86fa184:

    # hero "Hehe... That was good, yeah..."
    hero "Héhé... C'était vraiment bien, oui..."

# game/dialogs.rpy:2972
translate french matsuri_N2_7bfb8ae4:

    # n "{i}Teehee! To be honest... the more we do it, the more I want to do it again!{/i} {image=heart.png}"
    n "{i}Hihi ! Pour être honnête...plus on le fait, plus j'ai envie de le refaire !{/i} {image=heart.png}"

# game/dialogs.rpy:2973
translate french matsuri_N2_69a9deb4:

    # n "{i}Speaking which, are you busy tomorrow?{/i}"
    n "{i}Justement, tu fais quoi demain ?{/i}"

# game/dialogs.rpy:2974
translate french matsuri_N2_2224fcb7:

    # hero "Except coming to see you? No, nothing at all."
    hero "A part passer du temps avec toi demain ? Rien du tout."

# game/dialogs.rpy:2975
translate french matsuri_N2_8da73ec6:

    # "I laughed and heard Nanami laughing on the other end as well."
    "Je souris en entendant le rire de Nanami."

# game/dialogs.rpy:2976
translate french matsuri_N2_26411a89:

    # n "{i}Alright, %(stringhero)s-nii. {w}I'll wait for you at el-{nw}{/i}"
    n "{i}D'accord, %(stringhero)s-nii. {w}On se retrouve à onz--{nw}{/i}"

# game/dialogs.rpy:2977
translate french matsuri_N2_587273a6:

    # n "{i}.........{/i}"
    n "{i}.........{/i}"

# game/dialogs.rpy:2978
translate french matsuri_N2_fd4b149d:

    # hero "Hello? Nanami-chan?"
    hero "Allô ? Nanami-chan ?"

# game/dialogs.rpy:2979
translate french matsuri_N2_97743246:

    # n "{i}{size=-12}It's my boyfriend.....{p}It's none of your business!!......{/size}{/i}"
    n "{i}{size=-12}C'est mon petit copain.....{p}C'est pas tes affaires !!......{/size}{/i}"

# game/dialogs.rpy:2980
translate french matsuri_N2_5027b83e:

    # n "{i}{size=-12}Big brother!!!......{/size}{/i}"
    n "{i}{size=-12}Grand frère !!!......{/size}{/i}"

# game/dialogs.rpy:2981
translate french matsuri_N2_2f756018:

    # n "{i}I'll call you back.{/i}"
    n "{i}Je te rappelle.{/i}"

# game/dialogs.rpy:2983
translate french matsuri_N2_6c3f05d2:

    # "*{i}click{/i}*"
    "*{i}click{/i}*"

# game/dialogs.rpy:2984
translate french matsuri_N2_2e0a800f:

    # "What was that about...? I shrugged and went back to my computer."
    "Je haussai les épaules et revint sur l'ordinateur."

# game/dialogs.rpy:2985
translate french matsuri_N2_73d69e81:

    # "I have a bad feeling about this... {p}But, she said she'll call back."
    "J'avais un mauvais pressentiment... {p}Mais bon, elle a dit qu'elle rappellerait..."

# game/dialogs.rpy:2988
translate french matsuri_N2_1569a42e:

    # "Some time passed as I goofed around. All of a sudden, I heard the doorbell."
    "Après une demi-heure, la porte sonna."

# game/dialogs.rpy:2990
translate french matsuri_N2_8ef37003:

    # "Suddenly, I got goosebumps. A sense of dread came over me. It pushed me to run downstairs and to open the door as fast I could."
    "Soudainement, mon mauvais pressentiment se fit horriblement clair et opressant. Cela me fit courir au rez-de-chaussée pour ouvrir le plus vite possible."

# game/dialogs.rpy:2992
translate french matsuri_N2_ee440385:

    # "Nanami was at the door. She was in tears."
    "Nanami était à la porte, en larmes."

# game/dialogs.rpy:2993
translate french matsuri_N2_e3c36773:

    # "When I opened up, she ran in the living room."
    "Quand j'ai ouvert, elle a couru dans le salon."

# game/dialogs.rpy:2994
translate french matsuri_N2_fe828537:

    # hero "Nanami-chan?? {p}Are you okay? What's going on?!"
    hero "Nanami-chan ?? {p}Tout va bien ? Qu'est-ce qui se passe ?!"

# game/dialogs.rpy:2995
translate french matsuri_N2_0b51bf1c:

    # n "He's coming! He's scaring me, %(stringhero)s-nii!"
    n "Il arrive ! Il me fait peur, %(stringhero)s-nii !"

# game/dialogs.rpy:2996
translate french matsuri_N2_2952dd4d:

    # hero "Who? Who's coming?"
    hero "Qui ? Qui arrive ?"

# game/dialogs.rpy:2997
translate french matsuri_N2_817e602f:

    # "I got my answer right after I asked."
    "J'eus ma réponse immédiatement."

# game/dialogs.rpy:2998
translate french matsuri_N2_d7a874f3:

    # "A guy entered the house with a demented look on his face."
    "Un mec entra dans la maison avec une expression de folie meurtrière sur le visage."

# game/dialogs.rpy:2999
translate french matsuri_N2_5b3076dd:

    # "I recognize him. He was on Nanami's picture frame in her bedroom."
    "Je le reconnus instantanément. Il était sur la photo encadrée dans la chambre de Nanami."

# game/dialogs.rpy:3000
translate french matsuri_N2_203812e1:

    # hero "Are you Toshio-san?"
    hero "Vous êtes Toshio-san ?"

# game/dialogs.rpy:3001
translate french matsuri_N2_23531e72:

    # "He ignored my question and barged in straight toward Nanami."
    "Il ignora ma question et marchait vers Nanami."

# game/dialogs.rpy:3003
translate french matsuri_N2_f15432c1:

    # "I instinctively interposed myself between them."
    "Je me suis instinctivement interposé entre lui et elle."

# game/dialogs.rpy:3004
translate french matsuri_N2_5d32f865:

    # hero "Whoa! Wait wait wait... What are you going to do to her?!"
    hero "Attendez, là... Vous allez lui faire quoi ?!"

# game/dialogs.rpy:3005
translate french matsuri_N2_91b34485:

    # toshio "Nanami! Come back home right now!"
    toshio "Nanami ! Rentre tout de suite à la maison !"

# game/dialogs.rpy:3006
translate french matsuri_N2_54fe86a6:

    # n "No! You're scaring me, big brother!"
    n "Non ! Tu me fais peur, grand frère !"

# game/dialogs.rpy:3007
translate french matsuri_N2_9e884cfa:

    # "Then Toshio outrageously pointed at me."
    "Puis Toshio pointa dans ma direction."

# game/dialogs.rpy:3008
translate french matsuri_N2_b495b159:

    # toshio "And you...{p}How dare you soil my sister!"
    toshio "Et toi... {p}Comment as-tu osé souiller ma sœur ?!"

# game/dialogs.rpy:3009
translate french matsuri_N2_4d869fcc:

    # "What?!{p}Soil her?! He's crazy!"
    "Quoi ?!!{p}La souiller ?!! Il est complètement cinglé !!"

# game/dialogs.rpy:3010
translate french matsuri_N2_47b87910:

    # hero "Hey man, calm down!"
    hero "Hé mec, calmez-vous !"

# game/dialogs.rpy:3011
translate french matsuri_N2_5f2f5e83:

    # hero "Calm down and listen, please!"
    hero "S'il vous plaît, écoutez-moi !"

# game/dialogs.rpy:3012
translate french matsuri_N2_c025cc4c:

    # "Toshio ignored my words completely and approached me menacingly."
    "Toshio n'écouta pas et s'approcha de moi d'un air menaçant."

# game/dialogs.rpy:3013
translate french matsuri_N2_d17a127d:

    # "Oh shit, he's going to kill me!"
    "Oh non, il va me tuer !"

# game/dialogs.rpy:3014
translate french matsuri_N2_8d99340f:

    # "In pure panic, I spoke what came to mind."
    "Par pure panique, je sortis tout ce qui me passait par la tête."

# game/dialogs.rpy:3015
translate french matsuri_N2_d2e3bc0f:

    # hero "Just calm down! I didn't soil her! I-"
    hero "S'il vous plaît, calmez-vous ! Je ne l'ai pas souillée !!"

# game/dialogs.rpy:3017
translate french matsuri_N2_499d2845:

    # hero "{size=+15}I love her!!{/size}"
    hero "{size=+15}Je l'aime !!{/size}"

# game/dialogs.rpy:3019
translate french matsuri_N2_e3f58031:

    # "There was an awkward silence in the room."
    "Le silence envahit la pièce."

# game/dialogs.rpy:3020
translate french matsuri_N2_0ca571d7:

    # "Toshio had stopped walking... He looked more upset than ever."
    "Toshio s'était arrêté... Il avait l'air encore plus en colère que jamais."

# game/dialogs.rpy:3021
translate french matsuri_N2_cef855da:

    # "Strangely, saying that out loud gave me more courage and strength."
    "Etrangement, avoir confessé ce que j'ai toujours voulu dire à Nanami me redonna force et courage."

# game/dialogs.rpy:3022
translate french matsuri_N2_e6bb1d42:

    # hero "I love her... I love her with all my heart."
    hero "Je l'aime... Je l'aime de toute mon âme."

# game/dialogs.rpy:3023
translate french matsuri_N2_d225817d:

    # hero "I'm in love with her!... All my intentions towards her are from the bottom of my heart!"
    hero "Je suis amoureux d'elle !... Toutes mes intentions sont pures envers elle..."

# game/dialogs.rpy:3024
translate french matsuri_N2_f4b05b05:

    # toshio "How... How dare you..."
    toshio "Co... Comment oses-tu..."

# game/dialogs.rpy:3028
translate french matsuri_N2_bc61413a:

    # "Nanami came between us, protecting me this time. She was holding me tight."
    "Soudainement, Nanami se leva et se mit entre moi et Toshio, me protégeant cette fois. Elle me serra contre elle."

# game/dialogs.rpy:3029
translate french matsuri_N2_ff4ee66e:

    # "Toshio stopped again."
    "Toshio s'arrêta à nouveau."

# game/dialogs.rpy:3030
translate french matsuri_N2_95c7f55c:

    # toshio "Nanami, get the fuck off him!!!"
    toshio "Nanami, écartte-toi de là !"

# game/dialogs.rpy:3031
translate french matsuri_N2_5050139a:

    # n "No! Stop it, Big brother!!"
    n "Non ! Arrête, grand frère !!"

# game/dialogs.rpy:3032
translate french matsuri_N2_746a8fc4:

    # n "If you want to hit him, then hit me first!"
    n "Si tu veux le tabasser, il faudra me passer dessus d'abord !"

# game/dialogs.rpy:3033
translate french matsuri_N2_1053aa7b:

    # n "I love him too, big brother!!{p}{size=+15}I love him!!!{/size}"
    n "Je l'aime aussi, grand frère !!{p}{size=+15}Je l'aime !!!{/size}"

# game/dialogs.rpy:3034
translate french matsuri_N2_bb6eef02:

    # n "He did nothing wrong! I'm the only culprit!"
    n "Il n'a rien fait de mal du tout ! Je suis la seule coupable !"

# game/dialogs.rpy:3035
translate french matsuri_N2_7a8a2dfd:

    # "Toshio stood motionless."
    "Toshio ne bougeait pas."

# game/dialogs.rpy:3036
translate french matsuri_N2_9b1a0a97:

    # "Nanami's confession filled me with joy... But I was too scared to savor it fully."
    "En moi, je sentis du bonheur par la confession de Nanami... Mais la situation m'éffrayait trop pour y penser."

# game/dialogs.rpy:3037
translate french matsuri_N2_a79664fb:

    # "Tears were coming from Nanami's eyes. I knew she meant it. She was just as terrified."
    "Au vu des larmes de Nanami, je sentais que c'était aussi dur pour elle d'y penser tout autant."

# game/dialogs.rpy:3038
translate french matsuri_N2_38b0386e:

    # toshio "Nanami..."
    toshio "Nanami..."

# game/dialogs.rpy:3039
translate french matsuri_N2_e4239ff7:

    # toshio "You can't...{p}You can't love him. You shouldn't."
    toshio "Tu... ne peux pas...{p}Tu ne dois pas l'aimer."

# game/dialogs.rpy:3041
translate french matsuri_N2_89e8a925:

    # toshio "If you love him, someday he'll go away, just like our parents."
    toshio "Si tu l'aimes, un jour il partira, comme nos parents."

# game/dialogs.rpy:3042
translate french matsuri_N2_626d5481:

    # toshio "We shouldn't love anyone again. We only have each other!"
    toshio "On ne doit plus jamais aimer. Il faut qu'on reste soudés."

# game/dialogs.rpy:3043
translate french matsuri_N2_710fb61d:

    # toshio "In this world, we can only count on ourselves, Nanami."
    toshio "Dans ce monde on ne peut compter que sur nous deux, Nanami."

# game/dialogs.rpy:3044
translate french matsuri_N2_c60cf6df:

    # n "No...{p}No you're wrong, big brother..."
    n "Non...{p}Non, tu as tort, grand frère..."

# game/dialogs.rpy:3045
translate french matsuri_N2_6112b197:

    # n "When we lost our parents, I was stuck in the same mindset."
    n "Je pensais comme toi, avant, quand on a perdu nos parents."

# game/dialogs.rpy:3046
translate french matsuri_N2_3646ab26:

    # n "I thought we shouldn't deeply love anyone anymore."
    n "Je pensais qu'on ne devait plus aimer personne pour ne plus souffrir."

# game/dialogs.rpy:3047
translate french matsuri_N2_7531d9d0:

    # n "And as time passed, I thought I was going more and more crazy because of loneliness."
    n "Puis le temps a passé. Je croyais que je deviendrais folle à cause de la solitude."

# game/dialogs.rpy:3048
translate french matsuri_N2_5366f0e4:

    # n "Until I joined Rika-nee's manga club."
    n "Jusqu'à ce que je rejoigne le club manga de Rika-nee."

# game/dialogs.rpy:3049
translate french matsuri_N2_2759bd16:

    # n "Now I have two wonderful friends..."
    n "J'y ai eu deux merveilleuses amies..."

# game/dialogs.rpy:3050
translate french matsuri_N2_d47cc30c:

    # n "And %(stringhero)s definitely changed my life..."
    n "Et %(stringhero)s a définitivement changé ma vie quand il est arrivé..."

# game/dialogs.rpy:3051
translate french matsuri_N2_17bb8570:

    # n "I've never been so happy in my life, thanks to them..."
    n "Je n'ai jamais été aussi heureuse de ma vie grâce à eux..."

# game/dialogs.rpy:3052
translate french matsuri_N2_1a096aae:

    # n "They all made me realize that we shouldn't turn our back to love."
    n "Ils m'ont fait comprendre qu'on ne doit pas tourner le dos à l'amour."

# game/dialogs.rpy:3053
translate french matsuri_N2_9afef4f0:

    # n "The only way to heal our hearts is to embrace love and friendship, big brother... Not being stuck in loneliness!"
    n "Le seul moyen de soigner nos cœurs c'est d'aimer et d'avoir des amis, grand frère... Pas rester dans la solitude !"

# game/dialogs.rpy:3054
translate french matsuri_N2_ff64941d:

    # n "If... If you kill %(stringhero)s, I... I will... I..."
    n "Si... Si tu tues %(stringhero)s, je... Je..."

# game/dialogs.rpy:3055
translate french matsuri_N2_7f7dd2d3:

    # "Nanami broke into tears again... She probably couldn't bear to live without the manga club."
    "Nanami éclata en sanglots dans mes bras en imaginant ce que sa vie deviendrait sans nous."

# game/dialogs.rpy:3057
translate french matsuri_N2_7953243c:

    # "Toshio was still motionless. But his face was overflowing with tears now as he realized what he's done."
    "Toshio ne bougeait pas. Son visage ruisselait de larmes alors qu'il sortait enfin de sa folie."

# game/dialogs.rpy:3058
translate french matsuri_N2_0b7e8bd9:

    # "He fell on his knees, staring down at the floor... Guilt seemed to overpower him."
    "Il tomba à genoux en regardant le sol..."

# game/dialogs.rpy:3059
translate french matsuri_N2_5e48472d:

    # toshio "You..."
    toshio "Tu..."

# game/dialogs.rpy:3060
translate french matsuri_N2_b9326ce8:

    # toshio "You're right, Nanami..."
    toshio "Tu as raison, Nanami."

# game/dialogs.rpy:3061
translate french matsuri_N2_19220ebd:

    # toshio "Oh dear, what have I done..."
    toshio "Mon Dieu, qu'est-ce que j'ai fait..."

# game/dialogs.rpy:3062
translate french matsuri_N2_1e2845d6:

    # toshio "I... I thought you... I don't know..."
    toshio "Je... Je croyais que tu... Je sais pas..."

# game/dialogs.rpy:3063
translate french matsuri_N2_1e3aa01c:

    # toshio "I thought having a boyfriend would make you forget our parents..."
    toshio "Je pensais qu'avoir un petit ami t'avait fait oublier nos parents..."

# game/dialogs.rpy:3064
translate french matsuri_N2_ad121cc5:

    # n "How could I forget them?... I love them more than anything...!"
    n "Comment pourrais-je les oublier ?... Je les aime plus que tout !..."

# game/dialogs.rpy:3065
translate french matsuri_N2_678f6c05:

    # n "And I'm sure that they would be happy to see me with %(stringhero)s and with the others if they were still here..."
    n "Et je suis sûre qu'ils auraient été heureux de me voir avec %(stringhero)s et les autres s'ils avaient été là..."

# game/dialogs.rpy:3066
translate french matsuri_N2_d387126d:

    # toshio "You're right..."
    toshio "Tu as raison..."

# game/dialogs.rpy:3067
translate french matsuri_N2_c1e47d68:

    # toshio "I'm an awful brother. A terrible brother..."
    toshio "Je suis un horrible frère..."

# game/dialogs.rpy:3069
translate french matsuri_N2_721624ab:

    # "Nanami left my arms to hold her brother."
    "Nanami sortit doucement de mes bras pour prendre son frère dans les siens."

# game/dialogs.rpy:3070
translate french matsuri_N2_8e3dd312:

    # n "No, you are not, big brother."
    n "Non, c'est faux, grand frère."

# game/dialogs.rpy:3071
translate french matsuri_N2_98ee3ce3:

    # n "You're just still grieving over what happened to us..."
    n "Tu culpabilises trop, c'est tout..."

# game/dialogs.rpy:3072
translate french matsuri_N2_e056bbe7:

    # n "Since they died, I've never seen you interact with others as much. You were always thinking of working to help us survive."
    n "Depuis qu'ils sont partis, je ne t'ai jamais vu avec des amis ou avec une petite amie. Tu ne penses qu'à bosser pour survivre."

# game/dialogs.rpy:3073
translate french matsuri_N2_2f0b2303:

    # n "That's not healthy... {p}Loneliness isn't healthy..."
    n "Ce n'est pas bon pour la santé... {p}La solitude n'est pas bonne pour la santé..."

# game/dialogs.rpy:3074
translate french matsuri_N2_0a9e2c99:

    # "I watched the scene without a word."
    "Je regardai la scène sans un mot."

# game/dialogs.rpy:3077
translate french matsuri_N2_f5d91d1c:

    # "When Toshio felt better, Nanami released her embrace."
    "Quand Toshio se sentit mieux, Nanami relâcha son étreinte."

# game/dialogs.rpy:3078
translate french matsuri_N2_2ef38f10:

    # hero "You both need some fresh water. I'll get you some."
    hero "Il vous faut de l'eau fraîche. Je vais aller en chercher."

# game/dialogs.rpy:3079
translate french matsuri_N2_01068bf5:

    # toshio "S-sure... Thank you..."
    toshio "O-oui... Merci..."

# game/dialogs.rpy:3080
translate french matsuri_N2_56a7dd0c:

    # toshio "I'm... I'm sorry for..."
    toshio "Je... Désolé, pour..."

# game/dialogs.rpy:3081
translate french matsuri_N2_f3ee0ddb:

    # hero "It's okay..."
    hero "C'est rien..."

# game/dialogs.rpy:3082
translate french matsuri_N2_a44ad6f3:

    # hero "I don't have a little sister. But I might be a bit worried if I knew she was seeing some random dude I didn't know about."
    hero "Je n'ai pas de petite sœur, mais j'imagine que je n'aurais pas été jouasse non plus de la voir avec un mec que je connais pas..."

# game/dialogs.rpy:3083
translate french matsuri_N2_1e293463:

    # n "Hey, I'm the one saying you're a dude, not you!"
    n "Hé, c'est moi qui t'appelle mec, pas toi !"

# game/dialogs.rpy:3084
translate french matsuri_N2_c02bf505:

    # "I giggled softly."
    "Je ris doucement."

# game/dialogs.rpy:3085
translate french matsuri_N2_e1ca6ffd:

    # "I went to the kitchen and came back with three glasses of fresh water."
    "Je partis à la cuisine et revint avec trois verres d'eau glacée. On avait tous besoin de se remettre de nos émotions."

# game/dialogs.rpy:3086
translate french matsuri_N2_9d66422e:

    # hero "Toshio-san, I promise you that I want Nanami-chan to be happy and that I'll do my best to make her happy."
    hero "Toshio-san, je vous promets que je ne veux que le bonheur de Nanami. Et je ferai tout pour ça."

# game/dialogs.rpy:3087
translate french matsuri_N2_b183c263:

    # toshio "T-thank you..."
    toshio "M-merci..."
    toshio "Tu peux me tutoyer, au fait. On est pas si éloignés en âge."
    "Il me fit un sourire que je lui rendis."

# game/dialogs.rpy:3088
translate french matsuri_N2_407f6320:

    # "Nanami smiled at me..."
    "Nanami sourit également à cet échange..."

# game/dialogs.rpy:3090
translate french matsuri_N2_5b99a7c8:

    # "I escorted both Nanami and Toshio back to their house."
    "Je les escortai chez eux."

# game/dialogs.rpy:3091
translate french matsuri_N2_750a3cb5:

    # "I handshaked Toshio and we smiled to each other."
    "Je serrai la main de Toshio et nous sourîmes l'un l'autre."

# game/dialogs.rpy:3092
translate french matsuri_N2_471cd43e:

    # toshio "You better not make her sad, %(stringhero)s-san!"
    toshio "T'as pas intérêt à la rendre triste, %(stringhero)s-san !"

# game/dialogs.rpy:3093
translate french matsuri_N2_3fd749c7:

    # hero "I won't. I promise."
    hero "Ça n'arrivera pas. Je te le promets."

# game/dialogs.rpy:3094
translate french matsuri_N2_611bd54f:

    # "He entered the house."
    "Il rentra chez lui."

# game/dialogs.rpy:3095
translate french matsuri_N2_0f0ea53d:

    # hero "Nanami, wait..."
    hero "Nanami, attends..."

# game/dialogs.rpy:3096
translate french matsuri_N2_943066fb:

    # n "{size=-5}I'm coming, big brother, hold on.{/size}"
    n "{size=-5}Une seconde, grand frère, j'arrive.{/size}"

# game/dialogs.rpy:3098
translate french matsuri_N2_b0f85b49:

    # n "Yes, %(stringhero)s-nii?"
    n "Oui, %(stringhero)s-nii ?"

# game/dialogs.rpy:3099
translate french matsuri_N2_d6c71472:

    # hero "What I said in that moment... {w}It wasn't very enjoyable... {p}So I want to try again, here, right now."
    hero "Dire ça au moment où on l'a fait...{w} On en a pas bien profité...{p}Alors je voudrais réessayer là, maintenant."

# game/dialogs.rpy:3100
translate french matsuri_N2_ce0929b4:

    # "I held her small hands."
    "Je tins ses petites mains."

# game/dialogs.rpy:3101
translate french matsuri_N2_6baf7cff:

    # hero "I love you, Nanami-chan."
    hero "Je t'aime, Nanami-chan."

# game/dialogs.rpy:3103
translate french matsuri_N2_f18aa4de:

    # "Nanami smiled and lovingly kissed my lips."
    "Nanami sourit et embrassa tendrement mes lèvres."

# game/dialogs.rpy:3104
translate french matsuri_N2_30fea132:

    # n "I love you too, %(stringhero)s-nii."
    n "Moi aussi je t'aime, %(stringhero)s-nii."

# game/dialogs.rpy:3106
translate french matsuri_N2_6f7d9b3e:

    # "Then she went into her house."
    "Puis elle rentra chez elle."

# game/dialogs.rpy:3107
translate french matsuri_N2_75c7dfac:

    # "I sighed from relief... What a night..."
    "Je soupirai d'aise... Quelle nuit de fou..."

# game/dialogs.rpy:3108
translate french matsuri_N2_abd512ab:

    # "My emotions were put to the test tonight."
    "Mes émotions ont été mises à rude épreuve, ce soir."

# game/dialogs.rpy:3109
translate french matsuri_N2_a7393e9c:

    # "But I get the strange feeling that I'm not done yet..."
    "Mais je sentais que je n'en avais pas encore fini avec..."

# game/dialogs.rpy:3110
translate french matsuri_N2_230f323d:

    # "That sense of dread was still in the back of my mind. Something else is about to come soon..."
    "Et que quelque chose d'autre allait me tomber dessus bientôt..."

# game/dialogs.rpy:3130
translate french matsuri_R_040d4c37:

    # "When I arrived at the festival, I was joined by Rika-chan."
    "En arrivant au festival, je fus rejoint par Rika."

# game/dialogs.rpy:3131
translate french matsuri_R_42b57c94:

    # "She was wearing a very pretty purple yukata that showed her shoulders outside."
    "Elle portait un joli kimono violet qui laissait paraître ses épaules."

# game/dialogs.rpy:3132
translate french matsuri_R_a636fbfa:

    # "She was insanely sexy!"
    "Elle était épiquement sexy."

# game/dialogs.rpy:3134
translate french matsuri_R_6db6001a:

    # hero "Ah, Rika-chan! That yukata looks great on you!"
    hero "Ah, Rika-chan ! Tu es très sexy ce soir !"

# game/dialogs.rpy:3135
translate french matsuri_R_4ec8e433:

    # r "Hmph! Don't stare too much, pervert!"
    r "Hmpf ! Pervers !"

# game/dialogs.rpy:3136
translate french matsuri_R_8686ea2b:

    # hero "Where are the others?"
    hero "Où sont les autres ?"

# game/dialogs.rpy:3138
translate french matsuri_R_e7b4be7c:

    # r "Well... Sakura couldn't come."
    r "Eh bien... Sakura n'a pas pu venir."

# game/dialogs.rpy:3139
translate french matsuri_R_1457c98c:

    # r "She felt sick so she stayed home."
    r "Elle ne se sentait pas bien alors elle est restée chez elle."

# game/dialogs.rpy:3140
translate french matsuri_R_f0ca7698:

    # r "And Nanami wasn't around either. Festivals aren't really her thing. Too much of a crowd."
    r "Quant à Nanami, le...festival n'a jamais été son truc. Trop de gens."

# game/dialogs.rpy:3141
translate french matsuri_R_4299e4c0:

    # hero "Aw damn, that really sucks... Guess it's just us two..."
    hero "Oh... C'est vraiment dommage..."

# game/dialogs.rpy:3142
translate french matsuri_R_42954815:

    # hero "Do you think we should pay them a visit after the festival?"
    hero "Tu crois qu'on devrait leur rendre visite après la fête ?"

# game/dialogs.rpy:3143
translate french matsuri_R_ae397942:

    # r "Hmm..."
    r "Hmm..."

# game/dialogs.rpy:3145
translate french matsuri_R_67d6aa19:

    # r "That's a good idea."
    r "Bonne idée."

# game/dialogs.rpy:3146
translate french matsuri_R_4a7cd128:

    # r "So, shall we grab a souvenir for them at the festival?"
    r "On leur prendra un petit quelque chose !"

# game/dialogs.rpy:3147
translate french matsuri_R_2bd890ea:

    # "I nodded in agreement."
    "J'acquesçai."

# game/dialogs.rpy:3148
translate french matsuri_R_068f330c:

    # "We started to walk around the festival."
    "Nous commençâmes notre tour du festival."

# game/dialogs.rpy:3152
translate french matsuri_R_19be20ef:

    # "We went to almost every stall, from the fishing game stands to every single food stand."
    "On a presque tout fait, des stands de pêche aux stands de bouffe..."

# game/dialogs.rpy:3153
translate french matsuri_R_ff5174ca:

    # "Rika got pretty pissed off that I was better than her at some of the games."
    "Rika était toujours fâchée que je sois meilleure qu'elle à certains jeux."

# game/dialogs.rpy:3154
translate french matsuri_R_8a3d2cd4:

    # "When she eventually gave up, we continued to roam around. Finally, an outburst."
    "Ça commençait à être ennuyeux, et finalement, alors qu'on parcourait encore les stands, elle éclata."

# game/dialogs.rpy:3156
translate french matsuri_R_8693e2bc:

    # r "Ugh, you know, you really don't know how to be a gentleman!"
    r "T'es vraiment pas un gentleman, toi !"

# game/dialogs.rpy:3157
translate french matsuri_R_6b9f9eed:

    # r "Didn't your mom ever teach you to respect women, city rat!?"
    r "Ta mère ne t'a jamais dit comment se comporter avec les filles, le citadin ?!"

# game/dialogs.rpy:3158
translate french matsuri_R_d78833cc:

    # "What the heck is she talking about?! I got irritated with her."
    "De quoi elle parlait ?! Cela me fit péter un plomb aussi."

# game/dialogs.rpy:3159
translate french matsuri_R_23fae77a:

    # hero "Shut up! If you don't want to be with me then get lost. Nobody asked you to come!"
    hero "Hé, la ferme ! Si tu voulais pas de ce rencart, personne ne t'a dit de venir !"

# game/dialogs.rpy:3160
translate french matsuri_R_848651e9:

    # r "I can leave anytime! I just decided to stay because I felt bad that Sakura-chan and Nanami-chan ditched a loser like you!"
    r "Je pouvais partir quand je voulais ! Je suis restée juste par solidarité après que Sakura-chan et Nanami-chan aient laché un looseur comme toi !"

# game/dialogs.rpy:3161
translate french matsuri_R_c4e42251:

    # r "You're the worst kind of guy! You're not even my type!"
    r "T'es même pas mon genre de mec de toutes façons ! T'es le pire genre de mec possible !!"

# game/dialogs.rpy:3162
translate french matsuri_R_1527ed79:

    # hero "Fine! Whatever! You're not my type of woman either!!!"
    hero "Parfait ! T'es pas mon genre de femme non plus !"

# game/dialogs.rpy:3163
translate french matsuri_R_08fc819f:

    # r "Fine!!!"
    r "Très Bien !!"

# game/dialogs.rpy:3164
translate french matsuri_R_bb288886:

    # hero "Fine!!!"
    hero "Parfait !!"

# game/dialogs.rpy:3165
translate french matsuri_R_1a15f35b:

    # "We turned back against each other, sulking..."
    "On se tourna le dos en ruminant..."

# game/dialogs.rpy:3166
translate french matsuri_R_a20cefa7:

    # "..."
    "..."

# game/dialogs.rpy:3167
translate french matsuri_R_b9e5c171:

    # "And after a few seconds..."
    "Et après quelques secondes..."

# game/dialogs.rpy:3168
translate french matsuri_R_ed6f129a:

    # "Something came over me. I don't know why...but..."
    "Je ne sais pas pourquoi, ça m'est venu d'un coup..."

# game/dialogs.rpy:3171
translate french matsuri_R_d9b9652e:

    # "We both turned around to face each other and we kissed wildly!"
    "On s'est retournés et on s'est embrassés sauvagement !"

# game/dialogs.rpy:3172
translate french matsuri_R_0af6321e:

    # "It was a passionate, torrid kiss in front of a whole crowd of strangers!"
    "Un baiser torride devant tout le monde, comme dans les films !"

# game/dialogs.rpy:3173
translate french matsuri_R_5fbe52ef:

    # "When we stopped, she was looking at me with her pretty bi-colored eyes. She was blushing but she still looked pretty pissed off."
    "Quand on s'arrêta, elle me regarda avec ses beaux yeux bicolores. Son expression était toujours énervée mais elle rougissait."

# game/dialogs.rpy:3174
translate french matsuri_R_76e3f3b2:

    # hero "Ri... Rika-chan... I..."
    hero "R... Rika-chan...Je..."

# game/dialogs.rpy:3175
translate french matsuri_R_cd9391ee:

    # r "Shut up! Just shut up and kiss me again!"
    r "Chut ! Tais-toi et embrasse-moi !"

# game/dialogs.rpy:3183
translate french matsuri_R_83960144:

    # "And we kissed again."
    "Et on s'embrassa à nouveau."

# game/dialogs.rpy:3184
translate french matsuri_R_0d89a6a1:

    # "...Rika took held my hand and guided me into the forest behind the temple."
    "...Quand on s'arrêta, elle me prit la main et me tira vers la forêt derrière le temple."

# game/dialogs.rpy:3188
translate french matsuri_R_02658004:

    # "Hidden by the bushes... we made intense love there."
    "On a fait intensément l'amour dans cette forêt, derrière les fourrés..."

# game/dialogs.rpy:3189
translate french matsuri_R_862f50c1:

    # "It was crazy..."
    "C'était dingue..."

# game/dialogs.rpy:3190
translate french matsuri_R_91cf2fbf:

    # "I didn't think Rika was into me at all."
    "J'étais persuadé qu'elle ne m'aimait pas."

# game/dialogs.rpy:3191
translate french matsuri_R_f1d355d1:

    # "And as strange as it looks, I thought I didn't like her at all either."
    "Et aussi étrange que ça paraîsse, je croyais que, moi non plus, je ne l'aimais pas comme ça."

# game/dialogs.rpy:3192
translate french matsuri_R_9540939d:

    # "Tsundere love is a really strange thing..."
    "L'amour tsundere est vraiment une chose étrange..."

# game/dialogs.rpy:3193
translate french matsuri_R_9d8d28e3:

    # "I never thought that Rika, of all the people in the world, would be my first..."
    "Je n'aurais jamais deviné que Rika, parmi tous les gens de la terre, serait celle qui me donnerait ma première fois..."

# game/dialogs.rpy:3194
translate french matsuri_R_0256c5ee:

    # "...Actually, even though it was both our first time, she was pretty good at it..."
    "...Et en fait, bien que ce soit la première fois pour elle aussi, elle était vraiment douée."

# game/dialogs.rpy:3198
translate french matsuri_R_bff9fa0e:

    # "We were planning to go back to our respective homes."
    "On était sur le point de quitter le festival."

# game/dialogs.rpy:3200
translate french matsuri_R_975037cd:

    # r "Hey, I almost forgot!"
    r "J'allais oublier !"

# game/dialogs.rpy:3201
translate french matsuri_R_09ea128c:

    # r "We should get a present and visit Sakura-chan and Nanami-chan."
    r "On devrait trouver un petit quelque chose et aller voir Sakura et Nanami !"

# game/dialogs.rpy:3202
translate french matsuri_R_23aa15f6:

    # hero "Oh yeah, I almost forgot! Let's go!"
    hero "Ah oui c'est vrai ! Allons-y !"

# game/dialogs.rpy:3203
translate french matsuri_R_1f36ff66:

    # hero "I wonder how she'll react when she finds out about us!"
    hero "Je me demande ce qu'elles vont dire quand elles sauront pour nous deux !"

# game/dialogs.rpy:3205
translate french matsuri_R_c90d887e:

    # r "Haha, I bet she'll just think we're pulling a prank on her or-"
    r "Héhé, moi aussi. Elles vont sûrement croire à une bla-"

# game/dialogs.rpy:3209
translate french matsuri_R_22d23a85:

    # hero "Huh? My cellphone? Who could that be?"
    hero "Mon téléphone ? Qui ça peut être ?"

# game/dialogs.rpy:3213
translate french matsuri_R_e91d81d4:

    # hero "Hello? Who is it?"
    hero "Qui est-ce ?"

# game/dialogs.rpy:3214
translate french matsuri_R_0c279eeb:

    # s "{i}It's me, Sakura. How are you?{/i}"
    s "{i}C'est moi, Sakura. Tu vas bien ?{/i}"

# game/dialogs.rpy:3215
translate french matsuri_R_baf56ae6:

    # hero "Sakura-chan! Are you okay? I heard you were sick."
    hero "Sakura-chan ! Tu vas bien ? Il paraît que tu n'allais pas bien."

# game/dialogs.rpy:3216
translate french matsuri_R_16bb0b38:

    # s "{i}I'm feeling better. Thank you, don't worry.{/i}"
    s "{i}Ça va mieux, là. Merci, ne t'en fais pas.{/i}"

# game/dialogs.rpy:3217
translate french matsuri_R_cafbc553:

    # s "{i}I'm actually at Nana-chan's house. We were playing some video games together.{/i}"
    s "{i}Je suis même allé chez Nanami pour lui tenir compagnie. On joue à la console.{/i}"

# game/dialogs.rpy:3218
translate french matsuri_R_e322e7d4:

    # hero "Oh alright! I'm glad you feel better!"
    hero "D'accord. Content de voir que ça va mieux !"

# game/dialogs.rpy:3219
translate french matsuri_R_311bdbc8:

    # hero "Well hey, we were about to come visit you. So I guess we'll meet you at Nanami-chan's?"
    hero "On était sur le point de te rendre visite. On te rejoint chez Nanami, alors ?"

# game/dialogs.rpy:3220
translate french matsuri_R_cb76d6a6:

    # s "{i}Sure! We'll wait right here for you!{/i}"
    s "{i}Bien sûr ! On vous attend !{/i}"

# game/dialogs.rpy:3222
translate french matsuri_R_1713379a:

    # r "Sakura's feeling better?"
    r "Sakura va mieux ?"

# game/dialogs.rpy:3223
translate french matsuri_R_ea44e64c:

    # hero "Yeah. She's feeling better and she's at Nanami-chan's house."
    hero "Oui. Elle va mieux et elle est chez Nanami."

# game/dialogs.rpy:3224
translate french matsuri_R_17259851:

    # r "Let's go visit them then!"
    r "Allons-y alors !"

# game/dialogs.rpy:3229
translate french matsuri_R_2f5a201e:

    # "Nanami's room was a bit of a mess."
    "La chambre de Nanami était un vrai bazar."

# game/dialogs.rpy:3230
translate french matsuri_R_53129eea:

    # "Her computer setup took up most of the space, besides her bed."
    "A part le lit, la plus grande partie de la chambre était son ordinateur. Un truc dernier cri."

# game/dialogs.rpy:3231
translate french matsuri_R_992fbe8e:

    # "There was also an old TV with several gaming consoles plugged in it."
    "Il y avait aussi une vieille télé avec des consoles branchées dessus."

# game/dialogs.rpy:3232
translate french matsuri_R_3b146827:

    # "It was cramped but, it seemed homely for Nanami."
    "C'était un foutoir impressionnant. Mais Nanami s'y sentait à l'aise."

# game/dialogs.rpy:3233
translate french matsuri_R_61c51bff:

    # "I told the both of them about Rika and I. Sakura couldn't believe it when she heard the news!"
    "Sakura n'arrivait pas à y croire quand on lui annonça la nouvelle !"

# game/dialogs.rpy:3234
translate french matsuri_R_c281b0de:

    # "Nanami was in disbelief and was looking at us with a funny face."
    "Nanami n'y croyait pas plus et nous regardait avec une drôle de tête."

# game/dialogs.rpy:3238
translate french matsuri_R_586a170e:

    # s "That's incredible!"
    s "C'est incroyable !"

# game/dialogs.rpy:3239
translate french matsuri_R_72ae68c4:

    # n "Seriously, guys!!"
    n "Sérieux, les gars !!"

# game/dialogs.rpy:3240
translate french matsuri_R_5bfe62d0:

    # s "I'm so happy for both of you!"
    s "Je suis si heureuse pour vous deux !"

# game/dialogs.rpy:3241
translate french matsuri_R_391891ff:

    # s "I'd never guess you two would end up together! Teehee!"
    s "Je n'aurais jamais cru que vous finiriez ensemble ! Hihi !"

# game/dialogs.rpy:3242
translate french matsuri_R_04b5e9a4:

    # hero "Haha, I have to admit, me neither!"
    hero "Héhé, c'est vrai, moi non plus !"

# game/dialogs.rpy:3243
translate french matsuri_R_2a5f51c2:

    # r "Hmph, it's true. I didn't think I'd fall for him either...He's still an idiot though..."
    r "Hmph, C'est clair, moi non plus je n'y aurais pas crû...  Mais il reste un idiot !..."

# game/dialogs.rpy:3244
translate french matsuri_R_73e2cbfb:

    # hero "Whatever, you're the one whose blushing..."
    hero "Hé oh ! Qui c'est qui rougissait tout à l'heure ?!"

# game/dialogs.rpy:3245
translate french matsuri_R_34e95488:

    # "Sakura giggled as she watched us bicker as usual."
    "Sakura rit en nous regardant nous châmailler comme à notre habitude."

# game/dialogs.rpy:3246
translate french matsuri_R_885f1f06:

    # "Looks like our new relationship didn't change anything about the way we communicate."
    "On dirait que notre nouvelle relation ne changeait pas certaines habitudes."

# game/dialogs.rpy:3247
translate french matsuri_R_7a965734:

    # "But that's okay with me..."
    "C'est pas grave, je m'y suis habitué..."

# game/dialogs.rpy:3254
translate french matsuri_R_0cee0472:

    # "I woke up slowly."
    "Je me révéillai lentement."

# game/dialogs.rpy:3255
translate french matsuri_R_b2766fe2:

    # "Looking at the alarm clock, it's kinda late."
    "D'après le réveil, il était tard dans la matinée."

# game/dialogs.rpy:3256
translate french matsuri_R_f87aab37:

    # "I felt something warm against me..."
    "Je sentis quelque chose de chaud contre moi..."

# game/dialogs.rpy:3257
translate french matsuri_R_2d8ea27f:

    # "It was Rika."
    "C'était Rika."

# game/dialogs.rpy:3258
translate french matsuri_R_8d3f93d7:

    # "She was fast asleep, naked in my arms."
    "Elle dormait nue dans mes bras..."

# game/dialogs.rpy:3259
translate french matsuri_R_b91ebafe:

    # "I let out a smile as I held her tight."
    "Je la regardai dormir avec un sourire..."

# game/dialogs.rpy:3260
translate french matsuri_R_880d8986:

    # "After visiting Sakura and Nanami, I offered her \"to have a drink.\""
    "Après notre visite chez Nanami, j'ai proposé à Rika de \"prendre un dernier verre\" chez moi et elle a accepté."

# game/dialogs.rpy:3261
translate french matsuri_R_87283b9b:

    # "She responded in an incredibly seductive way, \"It's not that I want to sleep at your place...I'm just thirsty...\""
    "En disant, je cite : \"C'est pas que j'ai envie de dormir chez toi ; j'ai juste soif !\"."

# game/dialogs.rpy:3262
translate french matsuri_R_d65a0334:

    # "We made intense and passionate love twice during the night."
    "Puis on a fait l'amour passionnément durant la nuit. Deux fois."

# game/dialogs.rpy:3263
translate french matsuri_R_3e893f32:

    # "It's funny how much of a tsundere cliché she can be sometimes. But I like it a lot."
    "C'est drôle comment elle peut être un cliché de tsundere. Mais j'aime bien ça."

# game/dialogs.rpy:3264
translate french matsuri_R_0726d127:

    # "I pet Rika's hair tenderly. Her hair was longer than I thought with her pigtails removed."
    "Je caressai tendrement la chevelure de Rika. Ses cheveux paraissaient bien plus longs sans ses éternelles couettes..."

# game/dialogs.rpy:3265
translate french matsuri_R_c6b79cfc:

    # "She finally opened her eyes. As she saw me, she smiled and sat on the bed..."
    "Elle finit par ouvrir les yeux. En me voyant, elle sourit et s'assit sur le lit..."

# game/dialogs.rpy:3266
translate french matsuri_R_044a4f8a:

    # "She was still half asleep when she spoke."
    "Sa voix était un peu endormie."

# game/dialogs.rpy:3267
translate french matsuri_R_ddaea844:

    # r "Hmmmm... Did you have a good nap, city rat?"
    r "Hmmm... T'as bien dormi, le citadin ?"

# game/dialogs.rpy:3268
translate french matsuri_R_ffcb2dd8:

    # hero "Geez, even if we're in a relationship, even with you naked in my own bed, you'll still keep calling me that for the rest of your life huh?"
    hero "Mince, même si on est un couple d'amoureux accompli, même en étant nue dans mon propre lit, tu vas continuer à m'appeler comme ça ?"

# game/dialogs.rpy:3269
translate french matsuri_R_51e9effd:

    # r "Mmmm stop complaining... You should be thankful to have a girl like me as your lover..."
    r "Arrête de grogner... Tu devrais remercier les dieux de te permettre d'avoir une petite amie comme moi..."

# game/dialogs.rpy:3270
translate french matsuri_R_13bdddc3:

    # "I gave a small laugh and kissed her. She began to embrace me..."
    "Je ris doucement et embrassa ses lèvres. Elle répondit à mon baiser en m'enlaçant..."

# game/dialogs.rpy:3271
translate french matsuri_R_a20cefa7_1:

    # "..."
    "..."

# game/dialogs.rpy:3272
translate french matsuri_R_b072efdd:

    # "I suddenly realized something:{p}My parents!!!"
    "Soudainement, une pensée me traversa l'esprit :{p}Mes parents !!!"

# game/dialogs.rpy:3273
translate french matsuri_R_361d12e9:

    # "Oh shit, they're surely back from the festival!!!"
    "Ils sont sûrement revenus du festival !!"

# game/dialogs.rpy:3274
translate french matsuri_R_37e91653:

    # "They'll kill me if they see that I brought a girl home, let alone in my bed!!!"
    "Qu'est-ce qu'ils vont dire s'ils me trouvent avec une fille nue dans mon lit ?!!"

# game/dialogs.rpy:3275
translate french matsuri_R_ead2c864:

    # "What should I do? Dammit, dammit!!!"
    "Que vais-je faire, bon sang !!"

# game/dialogs.rpy:3276
translate french matsuri_R_a7bc91fd:

    # "I listened for a moment, waiting for an indication that would tell me if my parents were here."
    "J'attendis un moment, guettant le moindre bruit annonciateur de la présence de mes parents."

# game/dialogs.rpy:3277
translate french matsuri_R_06130763:

    # "Nothing..."
    "Rien."

# game/dialogs.rpy:3278
translate french matsuri_R_bb28b60c:

    # "Rika looked a bit confused...that or she was still half asleep."
    "Rika looked a bit confused...that or she was still half asleep."

# game/dialogs.rpy:3279
translate french matsuri_R_c4b1d086:

    # hero "Rika-chan..."
    hero "Rika-chan..."

# game/dialogs.rpy:3280
translate french matsuri_R_9e07d6af:

    # hero "My parents aren't there. You should go home now before they come back."
    hero "Mes parents ne sont pas là. Tu devrais rentrer chez toi avant qu'ils ne reviennent."

# game/dialogs.rpy:3281
translate french matsuri_R_272a8b83:

    # hero "My parents are kinda strict when it comes to love relationships."
    hero "Mes parents sont plutôt stricts en ce qui concerne les relations amoureuses !"

# game/dialogs.rpy:3282
translate french matsuri_R_7bf71025:

    # "Rika nodded."
    "Rika acquiesça."

# game/dialogs.rpy:3283
translate french matsuri_R_22236077:

    # r "Alright... I understand. My parents would react the same in this kinda situation..."
    r "Ok, je comprends... Mes parents sont comme ça aussi..."

# game/dialogs.rpy:3285
translate french matsuri_R_fb307b0d:

    # "I helped her put on her yukata and took her to her house..."
    "Après un peu de temps, je l'aidai à remettre son yukata et la ramena chez elle..."

# game/dialogs.rpy:3286
translate french matsuri_R_5ffe88e0:

    # "We promised to have another date very soon..."
    "On s'est promis de reprendre un rencart bientôt..."

# game/dialogs.rpy:3287
translate french matsuri_R_17899d6b:

    # "I went back home and turned on my computer."
    "Je rentrai chez moi et alluma mon ordinateur."

# game/dialogs.rpy:3291
translate french matsuri_R_2b150c3e:

    # write "Dear sister,"
    write "Chère grande sœur."

# game/dialogs.rpy:3293
translate french matsuri_R_92248cbb:

    # write "How are things?"
    write "Comment vas-tu ?"

# game/dialogs.rpy:3295
translate french matsuri_R_54b9d70d:

    # write "You'll never guess what happened!"
    write "Tu ne devineras jamais ce qui m'est arrivé !"

# game/dialogs.rpy:3297
translate french matsuri_R_4967d059:

    # write "I'm going out with a girl!"
    write "Je sors avec une fille !"

# game/dialogs.rpy:3299
translate french matsuri_R_c293cd3c:

    # write "Her name is Rika. She's a strange girl. It's a bit hard to understand she's really thinking.{w} She seems to mock me all the time, but we're really into each other."
    write "Elle s'appelle Rika. C'est une fille étrange. C'est dur de comprendre ce qu'elle pense vraiment.{w} On dirait qu'elle se moque de moi tout le temps, mais on s'aime vraiment... "

# game/dialogs.rpy:3301
translate french matsuri_R_1773278b:

    # write "I'm happy... Really happy... I can't wait to introduce her to you someday! {w}I'll send you a photo of us. She's incredibly gorgeous."
    write "Je suis vraiment heureux ! J'ai hâte de te la présenter un jour !{w} Je t'enverrai une photo de nous deux ! Elle est vraiment belle."

# game/dialogs.rpy:3302
translate french matsuri_R_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:3304
translate french matsuri_R_c42ad76e:

    # write "I hope everything is going great for you as well!"
    write "J'espère que tout va bien chez toi et que tu es aussi heureuse que je le suis..."

# game/dialogs.rpy:3306
translate french matsuri_R_f60fe613:

    # write "Your dear brother{p}%(stringhero)s"
    write "Ton frère{p}%(stringhero)s"

# game/dialogs.rpy:3308
translate french matsuri_R_91609d26:

    # write "PS:{w} The local summer festival was great! I hope you had a wonderful event like this in Tokyo."
    write "PS:{w} Le festival d'été local était génial ! J'espère que tu as eu une fête dans ce genre là où tu es, à Tokyo."

# game/dialogs.rpy:3309
translate french matsuri_R_76b2fe88_1:

    # nvl clear
    nvl clear

# game/dialogs.rpy:3330
translate french scene8_66fd8474:

    # centered "{size=+35}FINAL CHAPTER\nI wish I was a real girl{fast}{/size}"
    centered "{size=+35}CHAPITRE FINAL\nJ'aurais tant aimé être une vraie fille{fast}{/size}"

# game/dialogs.rpy:3336
translate french scene8_26da46e6:

    # centered "Three days after the festival..."
    centered "Trois jours après le festival..."

# game/dialogs.rpy:3339
translate french scene8_3193f804:

    # "It was late in the evening. I was in front of my computer, surfing on the web..."
    "Cette nuit, j'étais devant l'ordinateur, comme la plupart des soirs..."

# game/dialogs.rpy:3341
translate french scene8_216308f6:

    # "My cellphone rang."
    "Puis mon téléphone sonna."

# game/dialogs.rpy:3345
translate french scene8_dbd7e6ae:

    # hero "Hello?"
    hero "Oui ?"

# game/dialogs.rpy:3346
translate french scene8_5354350a:

    # r "{i}%(stringhero)s?{/i}"
    r "{i}%(stringhero)s?{/i}"

# game/dialogs.rpy:3348
translate french scene8_30e755be:

    # hero "What's cooking, love?"
    hero "Oui, quoi de neuf, mon ange ?"

# game/dialogs.rpy:3350
translate french scene8_6d9c1d81:

    # hero "Yeah, Rika-chan?"
    hero "Oui, Rika-chan ?"

# game/dialogs.rpy:3351
translate french scene8_237675bd:

    # r "{i}Come to my house right now! It's very important!{/i}"
    r "{i}Amène toi de suite chez moi ! Y a un gros problème !{/i}"

# game/dialogs.rpy:3353
translate french scene8_0bf1cc8f:

    # r "{i}It's about Sakura!{/i}"
    r "{i}C'est à propos de Sakura !{/i}"

# game/dialogs.rpy:3354
translate french scene8_eec796bb:

    # hero "What's going on?"
    hero "Qu'est-ce qui se passe ?"

# game/dialogs.rpy:3355
translate french scene8_87546516:

    # r "{i}Just come!{/i}"
    r "{i}Dépêche-toi !!{/i}"

# game/dialogs.rpy:3357
translate french scene8_495869e3:

    # "*{i}clic{/i}*"
    "*{i}clic{/i}*"

# game/dialogs.rpy:3359
translate french scene8_4f4c3c02:

    # "What could have happened to Sakura? Oh man, I hope she's okay."
    "Bon Dieu, pas Sakura aussi, maintenant !"

# game/dialogs.rpy:3361
translate french scene8_8a4ee6ed:

    # "I switched off my computer, took my bicycle and went to her house, worried to death."
    "J'éteignis l'ordinateur, pris mon vélo et fonça chez Rika, inquiet à mort."

# game/dialogs.rpy:3362
translate french scene8_a60f3c18:

    # "It was late in the night and the little rural streets of the village were a bit scary in the dark."
    "Il était tard le soir et les rues du village étaient un peu éffrayantes dans le noir."

# game/dialogs.rpy:3363
translate french scene8_85e64de3:

    # "Now isn't the time to be scared of the small things."
    "Mais c'était pas le moment d'avoir peur de petits riens."

# game/dialogs.rpy:3365
translate french scene8_b7c54990:

    # "My love needs help!"
    "Mon amour a besoin d'aide !"

# game/dialogs.rpy:3367
translate french scene8_ab18a7cd:

    # "My friend needs help!"
    "Mon amie a besoin d'aide !"

# game/dialogs.rpy:3370
translate french scene8_993a8f06:

    # "*{i}Knock knock{/i}*"
    "*{i}Toc toc{/i}*"

# game/dialogs.rpy:3372
translate french scene8_6d6e7ebc:

    # "The door of the house opened and Rika-chan appeared."
    "La porte s'ouvrit et Rika apparut."

# game/dialogs.rpy:3373
translate french scene8_68bac721:

    # "She looked very worried and upset, as I imagined."
    "Elle avait l'air vraiment inquiète, comme je l'imaginais."

# game/dialogs.rpy:3375
translate french scene8_47dbdd39:

    # r "Ah, there you are. Come i-{nw}"
    r "Ah, te voilà. Entre vi-{nw}"

# game/dialogs.rpy:3377
translate french scene8_27651eaf:

    # "I didn't give her any time to show me where Sakura-chan was."
    "Je ne lui ai même pas laissé le temps de finir sa phrase ou de me dire où se trouvait Sakura."

# game/dialogs.rpy:3378
translate french scene8_0a632bcf:

    # "I instinctively ran into the house and searched for her."
    "J'ai instinctivement foncé dans la maison à sa recherche."

# game/dialogs.rpy:3380
translate french scene8_c676b3c1:

    # "I found her in the living room."
    "Je la trouvai dans le salon."

# game/dialogs.rpy:3382
translate french scene8_37ffbdd8:

    # r "Ah, there you are. Come in, quickly."
    r "Ah, te voila. Entre vite."

# game/dialogs.rpy:3384
translate french scene8_616170d4:

    # "I followed Rika-chan into her house, to the living room."
    "Je suivis Rika chez elle, jusqu'au salon."

# game/dialogs.rpy:3386
translate french scene8_b8ce5033:

    # "Near the kotatsu, there was Sakura sitting on her knees, wearing her pajamas."
    "Près du kotatsu, il y avait Sakura, assise à genoux, dans son pyjama."

# game/dialogs.rpy:3387
translate french scene8_c44364bc:

    # "Her long hair was hiding her face, but she looked shocked."
    "Ses longs cheveux cachaient son visage mais elle avait l'air choquée."

# game/dialogs.rpy:3388
translate french scene8_d47b58c5:

    # "Thank heavens, she's alive."
    "Dieu merci, elle est en vie."

# game/dialogs.rpy:3389
translate french scene8_c89376fe:

    # hero "Sakura-chan! Are you okay?"
    hero "Sakura-chan ! Est-ce que ça va ?"

# game/dialogs.rpy:3390
translate french scene8_1740f374:

    # "She turned her face to me, speechless."
    "Elle se tourna vers moi sans un mot."

# game/dialogs.rpy:3391
translate french scene8_15ef4c6c:

    # "She had a large bruise on her cheek and tears were flowing slowly from her sweet blue eyes."
    "Elle avait un large bleu sur la joue et des larmes ruisselaient de ses grands yeux bleus."

# game/dialogs.rpy:3392
translate french scene8_d0d64bc6:

    # hero "Oh..."
    hero "Oh..."

# game/dialogs.rpy:3393
translate french scene8_74d2762c:

    # hero "Oh no..."
    hero "Oh non..."

# game/dialogs.rpy:3394
translate french scene8_6d9e86b7:

    # hero "Who the heck did this to you?!"
    hero "Qui est l'enfoiré qui t'a fait ça ?!!"

# game/dialogs.rpy:3396
translate french scene8_67ac6839:

    # "Rika arrived at the living room."
    "Rika arriva dans le salon."

# game/dialogs.rpy:3397
translate french scene8_4fc2e4fc:

    # hero "Rika-chan, what happened? Was it the gang from school again?"
    hero "Rika-chan, qu'est-ce qui s'est passé ? C'est encore ces racailles de l'école ?"

# game/dialogs.rpy:3398
translate french scene8_391ed42e:

    # "Rika shook her head."
    "Rika secoua la tête."

# game/dialogs.rpy:3399
translate french scene8_93c707d1:

    # r "Her father did this."
    r "Non. C'est son père."

# game/dialogs.rpy:3400
translate french scene8_03cc985d:

    # hero "What!?{p}Her father!?"
    hero "Quoi ?!{p}Son père ?!"

# game/dialogs.rpy:3401
translate french scene8_7bf71025:

    # "Rika nodded."
    "Rika hocha."

# game/dialogs.rpy:3402
translate french scene8_6da77c2c:

    # r "He was drunk, he hit her and even broke her violin..."
    r "Il était ivre. Il lui en a collé une et lui a même cassé son violon..."

# game/dialogs.rpy:3403
translate french scene8_75cef802:

    # r "Her mother helped her to flee the house. So she came to me. When she told me what happened, I called you."
    r "Sa mère l'a aidée à s'échapper et à venir ici. Quand j'ai su ce qui s'est passé, je t'ai appelé."

# game/dialogs.rpy:3404
translate french scene8_5820756e:

    # r "She needs both of us, more than anything..."
    r "Elle a besoin de nous, plus que jamais..."

# game/dialogs.rpy:3405
translate french scene8_f4f4126a:

    # hero "Does Nanami-chan know?"
    hero "Nanami-chan est au courant ?"

# game/dialogs.rpy:3406
translate french scene8_65b003bb:

    # r "Not yet. I prefer not to worry her."
    r "Pas encore. Je préfère ne pas l'inquiéter."

# game/dialogs.rpy:3407
translate french scene8_8f451a85:

    # r "She cares a lot for Sakura but she's very fragile."
    r "Elle tient vraiment beaucoup à Sakura mais elle est très fragile."

# game/dialogs.rpy:3408
translate french scene8_35bf5a71:

    # "I nodded."
    "J'acquiesçai."

# game/dialogs.rpy:3409
translate french scene8_5bd541e7:

    # "From what I saw, I could see that she really considers Sakura and Rika like her sisters."
    "J'avais appris qu'elle considérait vraiment Rika et Sakura comme ses grandes sœurs."

# game/dialogs.rpy:3410
translate french scene8_a20cefa7:

    # "..."
    "..."

# game/dialogs.rpy:3411
translate french scene8_380894b8:

    # "Anyway, Sakura's father surely did this because he was still pissed that his boy is a girl inside."
    "En tout cas, le père de Sakura a sûrement fait ça parce qu'il est une fois de plus ulcéré de voir son garçon être une fille."

# game/dialogs.rpy:3413
translate french scene8_63e75ac3:

    # "Or maybe he was pissed because Sakura gave away her yukata for men. I've heard that he bought it for her in the hopes of making her a man."
    "Ou bien il a appris que Sakura m'avait donné son yukata pour homme. Elle m'avait dit que son père lui avait acheté il y a un an ou deux dans l'espoir d'en faire un homme."

# game/dialogs.rpy:3414
translate french scene8_0745cc53:

    # "I felt enraged."
    "Je sentis une grande colère."

# game/dialogs.rpy:3415
translate french scene8_1a6c86d8:

    # "But actually, I had no idea of what to do."
    "Mais en fait, je ne savais pas vraiment quoi faire."

# game/dialogs.rpy:3420
translate french scene8_91aef672:

    # "We stayed at Rika-chan's home for the night."
    "On resta chez Rika pour la nuit."

# game/dialogs.rpy:3421
translate french scene8_7267642d:

    # "But I wanted to avenge my friend."
    "Mais je voulais venger mon amie."

# game/dialogs.rpy:3422
translate french scene8_0057c44c:

    # "I had to go there and talk to her father!"
    "Il fallait que j'y aille et que je parle à son père !"

# game/dialogs.rpy:3423
translate french scene8_8e59b631:

    # "While they were both asleep, I stood up and went outside with my bicycle, on my way to Sakura's house."
    "Alors qu'elles dormaient, je me suis levé et j'ai pris mon vélo, en direction de chez Sakura."

# game/dialogs.rpy:3426
translate french scene8_b1145ef5:

    # "In front of the house, there was her father."
    "Devant le jardin, chez elle, il y avait son père."

# game/dialogs.rpy:3427
translate french scene8_8fb982f8:

    # "He was mumbling some gibberish and was holding a bottle of sake."
    "Il avait des paroles incohérentes et il portait une bouteille de saké vide."

# game/dialogs.rpy:3428
translate french scene8_d9426232:

    # "As I could guess, he was still drunk."
    "A ce que je voyais, il était toujours ivre."

# game/dialogs.rpy:3429
translate french scene8_185ef70f:

    # hero "Hey you!"
    hero "Hé, vous !"

# game/dialogs.rpy:3430
translate french scene8_cb6740c6:

    # sdad "Wh--- Who are you?!"
    sdad "T-t'es qui, toi ?!"

# game/dialogs.rpy:3432
translate french scene8_032d6f9c:

    # hero "Name's %(stringhero)s... I'm the boyfriend of your daughter! Her boyfriend!!!"
    hero "Moi c'est %(stringhero)s... Je suis le petit ami de votre fille ! Son petit ami !!!"

# game/dialogs.rpy:3434
translate french scene8_e3dcc113:

    # hero "Name's %(stringhero)s... I'm a friend of your daughter!"
    hero "Moi c'est %(stringhero)s... Je suis un ami de votre fille !"

# game/dialogs.rpy:3435
translate french scene8_18e3dade:

    # sdad "*{i}hiccups{/i}* I don't have any d-d-daughter... I only have a son!..."
    sdad "*{i}hips{/i}* J'ai pas de f-f-f-fille... J'ai qu'un fils !"

# game/dialogs.rpy:3436
translate french scene8_9fd9385d:

    # sdad "And my son is a freaking pervert who dresses like a girl, that's all!!!"
    sdad "Et mon fils n'est qu'un p-p-pervers qui se travestit, c'est tout !!"

# game/dialogs.rpy:3437
translate french scene8_4576c2b6:

    # hero "SHUT UP!!!"
    hero "LA FERME !!!"

# game/dialogs.rpy:3438
translate french scene8_f199e2fa:

    # "I shouted so loud, some dogs in the neighborhood started to bark."
    "Je criai si fort, quelques chiens du voisinage commençaient à aboyer."

# game/dialogs.rpy:3439
translate french scene8_bb4f1d08:

    # hero "Whether he is a girl or a boy,... he's your child!! Your own flesh and blood!!!"
    hero "Qu'il soit une fille ou un garçon... C'est votre enfant !! Le fruit de vos entrailles !!"

# game/dialogs.rpy:3440
translate french scene8_90a161cb:

    # hero "You must accept him as he is!"
    hero "Vous devez l'accepter tel qu'il est !!"

# game/dialogs.rpy:3441
translate french scene8_042b0d72:

    # hero "Sakura accepts her father like he is, so why don't you do the same!!?"
    hero "Sakura accepte son père tel qu'il est, alors pourquoi ne faites vous pas pareil ?!!"

# game/dialogs.rpy:3442
translate french scene8_16e9224f:

    # sdad "*{i}hiccups{/i}*"
    sdad "*{i}hips{/i}*"

# game/dialogs.rpy:3443
translate french scene8_a67faa19:

    # sdad "I d-d-d-on't have any lessons to take from a kid!"
    sdad "J'ai p-p-p-p-pas de leçons à recevoir d'un gamin !!"

# game/dialogs.rpy:3444
translate french scene8_5f87fa70:

    # "He started to shout too. Some lights turned on in the neighborhood, awakened by the argument."
    "Il commença aussi à crier. Quelques lumières du voisinage s'allumèrent, ce dernier réveillé par la dispute."

# game/dialogs.rpy:3445
translate french scene8_4d26840c:

    # "Sakura's mother came out from the house and stood by the porch. She had some bruises on her body and her eyes were red... she must have been crying a lot."
    "Je vis la mère de Sakura debout devant le porche. Elle avait quelques bleus et semblait avoir beaucoup pleuré."

# game/dialogs.rpy:3446
translate french scene8_3a555d15:

    # "She seemed scared and was watching the scene quietly."
    "Elle semblait éffrayée et observait la scène sans un mot."

# game/dialogs.rpy:3447
translate french scene8_380ead38:

    # "Looks like Sakura wasn't the only one who got hurt..."
    "Sakura n'a pas été la seule à souffrir autant, on dirait..."

# game/dialogs.rpy:3448
translate french scene8_4e5e9fd5:

    # "Her father broke his empty bottle, held it like a knife and grinned."
    "Le père brisa sa bouteille de saké et la tint comme un poignard avec un sourire malsain."

# game/dialogs.rpy:3449
translate french scene8_23076b47:

    # sdad "Now get out or I'm gonna stab you... you gay brat!"
    sdad "Maintenant dégage ou j'te tue... espèce de tantouse !"

# game/dialogs.rpy:3450
translate french scene8_415034f0:

    # "Shit, is this guy for real?!"
    "Hé, là il me fait flipper, ce mec !!"

# game/dialogs.rpy:3451
translate french scene8_5ac5be9a:

    # "He began to charge at me brandishing the broken glass. I was paralyzed with fear."
    "Il fonça vers moi avec une grande rapidité. Je fus paralysé par la peur."

# game/dialogs.rpy:3452
translate french scene8_22ee59f4:

    # sdad "{size=+10}Go to hell!!{/size}"
    sdad "{size=+10}Va en enfer !!{/size}"

# game/dialogs.rpy:3453
translate french scene8_f5b011b4:

    # "Just as he closed in, someone jumped in front of me."
    "Au moment où il était près de moi, je vis une ombre passer devant moi."

# game/dialogs.rpy:3456
translate french scene8_1d392ab1:

    # "It was a deafening sound.{p}I heard a painful moan.{p}It resonated in my head even after it was done.."
    "Un bruit sourd.{p}Un gémissement de douleur.{p}Un cri un peu plus loin qui résonna dans ma tête."

# game/dialogs.rpy:3457
translate french scene8_6ead108f:

    # "Time stopped. I just remember being pushed to the floor."
    "Le temps s'arrêta. Je me souvins d'une poussée qui me fit tomber à terre."

# game/dialogs.rpy:3460
translate french scene8_1bd19034:

    # "When I came back to my senses, I saw Sakura's father flinched back, shaken and shocked. "
    "Quand je repris conscience, je vis le père reculer, choqué et effrayé."

# game/dialogs.rpy:3461
translate french scene8_770343cb:

    # "The broken bottle he was holding fell on the ground and shattered."
    "La bouteille cassée qu'il tenait tomba au sol avec un bruit de verre brisé."

# game/dialogs.rpy:3462
translate french scene8_2cf1e3ba:

    # "The pieces were stained with blood."
    "Les éclats de verre étaient teintés de sang."

# game/dialogs.rpy:3464
translate french scene8_43934e89:

    # "On top of me, there was the body of Sakura."
    "Sur moi, il y avait Sakura."

# game/dialogs.rpy:3465
translate french scene8_9e470804:

    # "Her stomach was bleeding profusely."
    "Son ventre saignait de plus en plus."

# game/dialogs.rpy:3466
translate french scene8_fc381d0a:

    # hero "Sakura!!!"
    hero "Sakura !!!"

# game/dialogs.rpy:3467
translate french scene8_c04fe468:

    # hero "{size=+15}Sakura!!! Hang on!!! Sakura!!!{/size}"
    hero "{size=+15}Sakura !!! Tiens bon !!! Sakura !!!{/size}"

# game/dialogs.rpy:3468
translate french scene8_281954cf:

    # "I started to feel tears in my eyes."
    "Je sentis des larmes me monter aux yeux à une vitesse folle."

# game/dialogs.rpy:3469
translate french scene8_b46a96ab:

    # "She saved me by sacrificing herself."
    "Elle s'est sacrifiée pour me sauver."

# game/dialogs.rpy:3470
translate french scene8_2e1ee92a:

    # hero "{size=+15}Please, don't die, Sakura!!!{/size}"
    hero "{size=+15}Je t'interdis de mourir, Sakura !!!{/size}"

# game/dialogs.rpy:3471
translate french scene8_6f1c6352:

    # "Sakura weakly turned her head to me."
    "Sakura tourna faiblement sa tête vers moi."

# game/dialogs.rpy:3473
translate french scene8_22286f8f:

    # "I caressed her cheek tenderly, removing a tear, and kissing her lips softly, my own tears preventing me to see how beautiful she looked, even in that state."
    "Je caressai sa joue tendrement, enlevant une larme, puis embrassant tendrement ses lèvres, mes propres larmes m'empêchant de voir sa beauté, même dans cet état."

# game/dialogs.rpy:3474
translate french scene8_1204b721:

    # s "..........{p}......%(stringhero)s....kun....."
    s "..........{p}......%(stringhero)s....kun....."

# game/dialogs.rpy:3476
translate french scene8_001b31dc:

    # s "I'm... so happy to... be... your girlfriend......"
    s "Je suis...si heureuse...d'être...ta petite amie..."

# game/dialogs.rpy:3477
translate french scene8_d6ab4c63:

    # s "I... love you... so much,...%(stringhero)s....kun...."
    s "Je t'aime...tellement,...%(stringhero)s....kun...."

# game/dialogs.rpy:3478
translate french scene8_a2f900d8:

    # hero "And I love you, Sakura, more than anything!"
    hero "Moi aussi je t'aime, Sakura. Plus que tout au monde !"

# game/dialogs.rpy:3479
translate french scene8_04745b3b:

    # hero "That's why you must stay alive!! Stay with me!!"
    hero "C'est pour ça que tu ne dois pas mourir !! Reste avec moi !!"

# game/dialogs.rpy:3480
translate french scene8_e8a331c8:

    # s "I..."
    s "Je..."

# game/dialogs.rpy:3482
translate french scene8_379966c6:

    # s "T... Too bad it...{w}didn't worked...between us..."
    s "Dommage que....{w}ça n'a pas marché...entre nous..."

# game/dialogs.rpy:3483
translate french scene8_362ff53c:

    # s "But that's okay...{p}You... and the others...{w} took care of me so nicely..."
    s "Mais ce n'est...pas grave...{p}Toi...et les autres...{w}vous prenez si bien soin de moi..."

# game/dialogs.rpy:3484
translate french scene8_0868d776:

    # s "I'm...so happy to... be... {w}your friend......"
    s "Je suis heureuse...d'être...{w}ton amie..."

# game/dialogs.rpy:3485
translate french scene8_f3331226:

    # s "T... Thank...... you..."
    s "M...Merci..."

# game/dialogs.rpy:3487
translate french scene8_a58e1fda:

    # s "I regret I... didn't had the time... to tell Nanami-chan... the truth..."
    s "Je...regrette de...n'avoir pas pu dire.... la vérité à Nana-chan..."

# game/dialogs.rpy:3488
translate french scene8_f8ac1185:

    # s "You know..."
    s "Tu sais..."

# game/dialogs.rpy:3490
translate french scene8_cb894c8f:

    # s "I wish......{p}I was...{p}a real girl..."
    s "J'aurais tant... {p}aimé être... {p}une vraie fille..."

# game/dialogs.rpy:3492
translate french scene8_ccf33f5d:

    # "She smiled at me, then she fainted in my arms."
    "Elle me sourit et perdit connaîssance dans mes bras."

# game/dialogs.rpy:3496
translate french scene8_8e8bdb99:

    # hero "Sakura!!!!!!{p}{size=+15}Sakura!!! Answer me!!!{/size}"
    hero "Sakura !!!!!!{p}{size=+15}Sakura !!! Réponds-moi !!!{/size}"

# game/dialogs.rpy:3497
translate french scene8_43e0fc44:

    # hero "{size=+20}SAKURAAAAAAAAAAAAAAA!!!!!!{/size}"
    hero "{size=+20}SAKURAAAAAAAAAAAAAAA !!!!!!{/size}"

# game/dialogs.rpy:3501
translate french scene8_7a909493:

    # "The police and the ambulance arrived shortly after, with Rika-chan."
    "La police et l'ambulance ne tardèrent pas à arriver, ainsi que Rika."

# game/dialogs.rpy:3502
translate french scene8_dd0ebba0:

    # "There was still a pulse inside Sakura's body, but she was badly hurt."
    "Il y avait un pouls dans le corps de Sakura, mais la blessure était profonde."

# game/dialogs.rpy:3503
translate french scene8_913d018c:

    # "The police arrested her father for attempted homicide and domestic violence."
    "La police arrêta son père pour homicide et violence domestique."

# game/dialogs.rpy:3504
translate french scene8_53b8960a:

    # "Sakura's mom went with her in the ambulance."
    "La mère de Sakura resta avec elle dans l'ambulance."

# game/dialogs.rpy:3506
translate french scene8_8f2b17c9:

    # "I stayed there with Rika-chan for a moment... when everyone was gone."
    "Je me retrouvai seul avec Rika, un moment...lorsque tout le monde fut parti."

# game/dialogs.rpy:3508
translate french scene8_e09c696e:

    # "I cried in Rika-chan's arms, uncontrollably."
    "Je pleurai à chaudes larmes dans les bras de Rika."

# game/dialogs.rpy:3509
translate french scene8_c44c2f72:

    # hero "Sakura... No, damnit, not Sakura..."
    hero "Sakura... Par tous les dieux, pitié, pas Sakura..."

# game/dialogs.rpy:3510
translate french scene8_68b58967:

    # "Rika was sobbing as well."
    "Rika pleurait aussi, silencieusement."

# game/dialogs.rpy:3511
translate french scene8_c6f7844b:

    # "When we calmed down a little, she spoke."
    "Quand on s'arrêta, elle parla."

# game/dialogs.rpy:3512
translate french scene8_d30a40a6:

    # r "You know... {p}I don't know how to say it..."
    r "Tu sais...{p}Je ne savais pas comment te le dire..."

# game/dialogs.rpy:3513
translate french scene8_906477c5:

    # r "But I think it's the right time to tell you..."
    r "Mais je pense que c'est le bon moment..."

# game/dialogs.rpy:3514
translate french scene8_ea880f58:

    # r "Before you came to our club... I was in love with Sakura..."
    r "Avant que tu ne viennes au club... j'étais amoureuse de Sakura..."

# game/dialogs.rpy:3515
translate french scene8_6c7d6f4e:

    # hero "You were? With Sakura-chan?"
    hero "Vraiment ? De Sakura-chan ?"

# game/dialogs.rpy:3516
translate french scene8_73d36031:

    # r "Yes..."
    r "Oui..."

# game/dialogs.rpy:3517
translate french scene8_1a4eb0ff:

    # r "I was attracted by this boy who is so girly..."
    r "J'étais attirée par ce garçon qui est si féminin..."

# game/dialogs.rpy:3518
translate french scene8_e94b0b53:

    # r "After that, you came,... {size=-15}and it changed...{/size}"
    r "Et puis tu es arrivé,... {size=-15}et tout a changé...{/size}"

# game/dialogs.rpy:3519
translate french scene8_402f030d:

    # r "By the way, you know why I consider Sakura as my best friend, now..."
    r "Enfin, tu comprends pourquoi je considère Sakura comme ma meilleure amie, maintenant..."

# game/dialogs.rpy:3520
translate french scene8_e668a3b6:

    # hero "I see..."
    hero "Oui, je vois..."

# game/dialogs.rpy:3521
translate french scene8_7030c013:

    # r "But don't worry... I was shocked when I heard about you two, but..."
    r "Mais ne t'en fais pas... J'ai été choquée quand j'ai appris pour votre couple, mais..."

# game/dialogs.rpy:3522
translate french scene8_09ad0db3:

    # r "I was genuinely happy for the both of you."
    r "J'étais... Et je suis réellement heureuse pour vous deux."

# game/dialogs.rpy:3523
translate french scene8_39b70c91:

    # r "I never seen Sakura so happy before. You changed her life so much..."
    r "Je n'avais jamais vu Sakura rayonner autant de bonheur. Tu as tellement changé sa vie..."

# game/dialogs.rpy:3524
translate french scene8_c0eddcd4:

    # r "It's why I'm not mad at you... Sakura's happiness is all that matters..."
    r "C'est pour ça que je ne t'en veux pas... Le bonheur de Sakura est tout ce qui importe..."

# game/dialogs.rpy:3525
translate french scene8_190df487:

    # r "And now..."
    r "Et maintenant..."

# game/dialogs.rpy:3526
translate french scene8_9a33713f:

    # r "I just hope...{w}That she will be alright!"
    r "J'espère juste...{w}qu'elle va s'en sortir !"

# game/dialogs.rpy:3527
translate french scene8_0bd0684e:

    # "She started to weep too. I had never seen her like that before."
    "Elle recommença à pleurer, plus fortement. Je ne l'avais jamais vue comme ça."

# game/dialogs.rpy:3528
translate french scene8_0fb4909e:

    # "I held her in my arms gently."
    "Je la pris dans mes bras."

# game/dialogs.rpy:3532
translate french scene8_f4f6d6da:

    # r "Are you okay, hun?"
    r "Est-ce que ça va, chéri ?"

# game/dialogs.rpy:3534
translate french scene8_422d11ba:

    # r "Are you okay?"
    r "Est-ce que ça va ?"

# game/dialogs.rpy:3535
translate french scene8_e8331494:

    # hero "I'm fine... Horribly anxious but fine..."
    hero "Je vais bien... Juste horriblement inquiet..."

# game/dialogs.rpy:3536
translate french scene8_f8b52a34:

    # "Rika stayed silent for a moment. But finally, she cried in my arms."
    "Rika resta silencieuse un moment, puis finalement elle pleura dans mes bras."

# game/dialogs.rpy:3537
translate french scene8_a4d9e75c:

    # r "I'm so scared... I don't want to loose Sakura...!"
    r "J'ai tellement peur... Je ne veux pas perdre Sakura !..."

# game/dialogs.rpy:3539
translate french scene8_103c3486:

    # hero "I know, babe... Me neither..."
    hero "Je sais mon ange... Moi non plus..."

# game/dialogs.rpy:3541
translate french scene8_bfb9febc:

    # hero "I know... Me neither..."
    hero "Je sais... Moi non plus..."

# game/dialogs.rpy:3542
translate french scene8_5546a02e:

    # "She cried for a long time in my arms."
    "Elle pleura un long moment dans mes bras."

# game/dialogs.rpy:3543
translate french scene8_46acf1af:

    # "When she stopped, she spoke."
    "Quand elle s'arrêta, elle parla."

# game/dialogs.rpy:3544
translate french scene8_72e004e1:

    # r "You know... She told me that..."
    r "Tu sais... Elle m'a dit que..."

# game/dialogs.rpy:3545
translate french scene8_72694f8f:

    # r "Sakura told me that she loved you..."
    r "Sakura m'a dit qu'elle était amoureuse de toi..."

# game/dialogs.rpy:3546
translate french scene8_1ae45319:

    # hero "Ah?"
    hero "Ah ?"

# game/dialogs.rpy:3547
translate french scene8_6ea028e4:

    # "I had guessed it since the car incident, some days ago."
    "Je l'avais deviné, depuis ce jour avec la voiture, il y a quelques jours."

# game/dialogs.rpy:3548
translate french scene8_4e396aba:

    # "I had the feeling that she was in love with me, sort-of..."
    "J'avais le sentiment qu'elle en pinçait pour moi..."

# game/dialogs.rpy:3549
translate french scene8_77b8faa3:

    # r "She knew that it was an impossible love since you're straight...{p}But she had always loved you with all her heart..."
    r "Elle savait que c'était un amour impossible puisque tu es hétéro...{p}Mais elle t'a toujours aimé de tout son cœur..."

# game/dialogs.rpy:3551
translate french scene8_5f050a71:

    # r "And to be honest... It's thanks to her that we are going out together now."
    r "Et pour tout te dire... C'est grâce à elle si on est ensemble toi et moi, aujourd'hui."

# game/dialogs.rpy:3552
translate french scene8_9fcae4dd:

    # hero "What?"
    hero "Quoi ?"

# game/dialogs.rpy:3553
translate french scene8_601c81c7:

    # hero "But...{p}How? {w}Wh-{nw}"
    hero "Mais...{p}Comment ?{w} Pourq--{nw}"

# game/dialogs.rpy:3554
translate french scene8_b37d1c72:

    # r "She was the one who organized everything in our first date, at the festival, with the help of Nanami."
    r "C'est elle qui a organisé ce rendez-vous au festival, avec l'aide de Nanami."

# game/dialogs.rpy:3555
translate french scene8_4a5bf62a:

    # r "It didn't happen as she expected it, but the result was the same, in the end."
    r "Ça ne s'est pas passé comme prévu, mais le résultat a été le même."

# game/dialogs.rpy:3556
translate french scene8_1900c1b3:

    # hero "That's why they weren't there at the festival?"
    hero "Elle n'était pas malade alors ? C'est pour ça qu'elle n'était pas là au festival ?"

# game/dialogs.rpy:3557
translate french scene8_bec3b4af:

    # r "Yes."
    r "Oui."

# game/dialogs.rpy:3558
translate french scene8_6220eee7:

    # r "I wasn't very okay about the idea first, but she knew that..."
    r "Je n'étais pas très d'accord avec cette idée, au début, mais... Elle savait que..."

# game/dialogs.rpy:3559
translate french scene8_a0391de0:

    # "She blushed."
    "Elle rougit."

# game/dialogs.rpy:3560
translate french scene8_4232035f:

    # r "That I had... feelings for you too..."
    r "Que j'avais... des sentiments pour toi, moi aussi..."

# game/dialogs.rpy:3561
translate french scene8_65088067:

    # "I smiled. Tears started to come again from my eyes."
    "Je souris. Des larmes commençaient à me venir."

# game/dialogs.rpy:3562
translate french scene8_33789624:

    # hero "She wanted us to be all together."
    hero "Elle a voulu qu'on soit ensemble."

# game/dialogs.rpy:3563
translate french scene8_b16b0eb8:

    # hero "That she will be in our relationship like a best unique friend... Like a sister to each of us..."
    hero "Qu'elle soit parmi nous comme une sorte de meilleure amie spéciale... Comme une sœur pour nous tous..."

# game/dialogs.rpy:3564
translate french scene8_e66f523f:

    # hero "The sister who is ready to abandon everything...just for the happiness of her best friends..."
    hero "La sœur prête à tout abandonner...juste pour le bonheur de ses meilleurs amis..."

# game/dialogs.rpy:3565
translate french scene8_f1b0dae1:

    # "She was in love with me... Being lovers was impossible but her friendship and love for me never stopped..."
    "Elle m'aimait... C'était un amour impossible mais son amitié n'a pas baissé d'un poil..."

# game/dialogs.rpy:3566
translate french scene8_cfdac421:

    # "And she even went to create happiness between me and Rika just because she loved us..."
    "Elle a même voulu créer du bonheur entre Rika et moi juste parce qu'elle nous aimait..."

# game/dialogs.rpy:3567
translate french scene8_0bd26214:

    # "It was my turn to cry, thinking about that adorable angel Sakura."
    "C'était mon tour de pleurer, en pensant à cet ange tombé du ciel qu'était Sakura."

# game/dialogs.rpy:3569
translate french scene8_f1b0dae1_1:

    # "She was in love with me... Being lovers was impossible but her friendship and love for me never stopped..."
    "Elle m'aimait... C'était un amour impossible mais son amitié n'a pas baissé d'un poil..."

# game/dialogs.rpy:3570
translate french scene8_86a7a846:

    # "It was my turn to cry, thinking about Sakura's kind heart."
    "C'était mon tour de pleurer, en pensant au cœur pur de Sakura."

# game/dialogs.rpy:3571
translate french scene8_01146bd8:

    # "At the same time, I felt like a jerk..."
    "En même temps, je me sentis comme un imbécile..."

# game/dialogs.rpy:3572
translate french scene8_23b3af63:

    # "Rika-chan held me tight in her arms, rubbing my hair."
    "Rika me prit dans ses bras en me caressant les cheveux."

# game/dialogs.rpy:3573
translate french scene8_f0b3fd2d:

    # "I was confused. It's not something she would do normally..."
    "J'étais confus. Je n'aurais jamais cru qu'elle fasse ça un jour..."

# game/dialogs.rpy:3574
translate french scene8_2e4a7bba:

    # "But I felt better in her arms..."
    "Mais je me sentis mieux dans ses bras..."

# game/dialogs.rpy:3575
translate french scene8_d1802fa3:

    # "The night was silent..."
    "La nuit était silencieuse..."

# game/dialogs.rpy:3582
translate french scene9_e9b7540d:

    # "The next day at school was gloomy for me, Nanami and Rika."
    "Le jour d'école suivant était déprimant."

# game/dialogs.rpy:3584
translate french scene9_43a31284:

    # "At the club, we were just sitting... doing nothing, saying nothing."
    "Au club, on était juste assis... On ne disait rien, ne faisait rien."

# game/dialogs.rpy:3585
translate french scene9_d9d4a59c:

    # "The only noise we could hear was Nanami's crying."
    "Le seul bruit audible dans la salle était les pleurs de Nanami."

# game/dialogs.rpy:3586
translate french scene9_de531cea:

    # "She couldn't stop crying when she heard the news..."
    "Elle ne pouvait plus s'arrêter quand elle avait appris la nouvelle..."

# game/dialogs.rpy:3587
translate french scene9_748065c0:

    # "The club was silent without Sakura..."
    "Le club était silencieux sans Sakura."

# game/dialogs.rpy:3589
translate french scene9_be3ea4ed:

    # "Finally I spoke as I stood up, smashing my fist on the table."
    "Finalement je me levai, abattant mon poing sur la table."

# game/dialogs.rpy:3591
translate french scene9_7a7f745d:

    # hero "I can't stay here like this! I have to the hospital and get news about Sakura!"
    hero "Je ne peux plus rester sans rien faire ! Je dois aller à l'hôpital voir Sakura !"

# game/dialogs.rpy:3592
translate french scene9_cf4c0510:

    # "Rika-chan smiled, determined, and stood up as well."
    "Rika sourit, déterminée, et se leva à son tour."

# game/dialogs.rpy:3593
translate french scene9_00b28479:

    # r "You're right, kid! Let's go there right now!"
    r "T'as raison, gamin ! Allons-y tout de suite !"

# game/dialogs.rpy:3594
translate french scene9_611208b8:

    # r "Nobody said that the manga club would leave one of its members alone!"
    r "Personne ne dira que le club manga a laissé tomber un de ses membres !"

# game/dialogs.rpy:3595
translate french scene9_3966d3ed:

    # "I smiled. I at last found the Rika-chan I knew."
    "Je souris. Je retrouvai la Rika que je connais."

# game/dialogs.rpy:3597
translate french scene9_551012d0:

    # "Finally, Rika-chan stood up, determined."
    "Finalement, Rika se leva, déterminée."

# game/dialogs.rpy:3599
translate french scene9_3cf11638:

    # r "I can't stay here like this! I must go to the hospital and get news about Sakura-chan!"
    r "Je ne peux plus rester sans rien faire ! Je dois aller à l'hôpital voir Sakura !"

# game/dialogs.rpy:3600
translate french scene9_567a77f3:

    # "I nodded and stood up as well."
    "Je hochai, me levant à mon tour."

# game/dialogs.rpy:3601
translate french scene9_00c27679:

    # hero "You're right! Let's go right now!"
    hero "Tu as raison. Allons-y maintenant !"

# game/dialogs.rpy:3602
translate french scene9_2fe99060:

    # "Nanami didn't say a word, but she stopped crying, dried her tears and followed us with a cute determined face."
    "Nanami ne dit rien. Mais elle sécha ses larmes, se leva et nous suivit avec un visage déterminé."

# game/dialogs.rpy:3604
translate french scene9_243a70b8:

    # hero "So, Doctor, how is Sakura?"
    hero "Docteur, comment se porte Sakura ?"

# game/dialogs.rpy:3606
translate french scene9_9eefb10b:

    # "Doctor" "Oh, don't worry, kids. Your friend is fine, now!"
    "Docteur" "Ne vous inquiétez pas. Votre ami va mieux !"

# game/dialogs.rpy:3607
translate french scene9_063c6c0a:

    # "Doctor" "We managed to save him right on time, thanks to his mother's call!"
    "Docteur" "Nous avons réussi à le sauver juste à temps grâce à l'appel de sa mère !"

# game/dialogs.rpy:3608
translate french scene9_24b1777f:

    # "We couldn't believe it! We heaved a sigh of relief."
    "On n'en revenait pas. On eut un soupir de soulagement."

# game/dialogs.rpy:3609
translate french scene9_f6dd7e91:

    # "Doctor" "He should be ready in a few days."
    "Docteur" "Il devrait être rétabli d'ici quelques jours."

# game/dialogs.rpy:3610
translate french scene9_e7741f1a:

    # "Doctor" "He can have visitors, if you want to see him."
    "Docteur" "Il peut avoir des visiteurs, si vous souhaitez le voir."

# game/dialogs.rpy:3611
translate french scene9_61585f71:

    # "We nodded and followed the doctor to Sakura's hospital room."
    "On acquiesça et on suivit le docteur vers la chambre d'hôpital de Sakura."

# game/dialogs.rpy:3613
translate french scene9_81c4b68b:

    # "Her face shone as she saw us."
    "Son visage s'éclaira en nous voyant."

# game/dialogs.rpy:3614
translate french scene9_9b4114a6:

    # s "%(stringhero)s-kun!! Rika-chan!! Nana-chan!!"
    s "%(stringhero)s-kun !! Rika-chan !! Nana-chan !!"

# game/dialogs.rpy:3615
translate french scene9_417ded14:

    # n "Sakura-nee!!!"
    n "Sakura-nee !!!"

# game/dialogs.rpy:3616
translate french scene9_c0cdba56:

    # "Nanami jumped on the bed to hug Sakura."
    "Nanami bondit sur le lit pour serrer Sakura contre elle."

# game/dialogs.rpy:3617
translate french scene9_5fcae85e:

    # "She flinched a little bit by the pain but hugged Nanami back, rubbing her head like child."
    "Sakura eut un petit rictus de douleur mais elle câlina Nanami en lui caressant les cheveux."

# game/dialogs.rpy:3618
translate french scene9_06d60896:

    # s "Easy, little one... I need time for the wound to heal."
    s "Doucement, ma petite... Je ne suis pas encore tout à fait guérie."

# game/dialogs.rpy:3619
translate french scene9_52c6a554:

    # n "I'm not little!"
    n "Je suis pas petite !!"

# game/dialogs.rpy:3620
translate french scene9_00bc78d2:

    # "She said that while going back to crying. But of joy this time."
    "Elle dit cela en sentant les pleurs revenir. Mais de joie, cette fois-ci."

# game/dialogs.rpy:3621
translate french scene9_f46ea2ef:

    # hero "Oh my gosh... Sakura-chan...{p}I'm so happy you survived!"
    hero "Bon Dieu... Sakura-chan... {p}Je suis si heureux que tu aies survécu !"

# game/dialogs.rpy:3622
translate french scene9_b9968b56:

    # hero "I was so scared that you...!"
    hero "J'avais tellement peur que...!"

# game/dialogs.rpy:3623
translate french scene9_1508b046:

    # "She seemed to be really okay, as if nothing ever happened."
    "Elle avait l'air d'aller vraiment mieux, comme si rien ne s'était passé."

# game/dialogs.rpy:3625
translate french scene9_b8aeb0be:

    # s "I will not leave this planet where I can have the best boyfriend ever!"
    s "Pas question de quitter la planète où se trouve le meilleur petit ami du monde !"

# game/dialogs.rpy:3626
translate french scene9_0d3e5a44:

    # s "As well as the best friends!"
    s "Ainsi que les meilleurs amis !"

# game/dialogs.rpy:3628
translate french scene9_0ac19270:

    # s "I will not leave this planet where I can have my best friends!"
    s "Pas question de quitter la planète où se trouve les meilleurs amis du monde !"

# game/dialogs.rpy:3629
translate french scene9_fadc1602:

    # r "We all love you, Sakura! {image=heart.png}"
    r "On t'aime tous, Sakura ! {image=heart.png}"

# game/dialogs.rpy:3630
translate french scene9_ab6bc56f:

    # "Sakura smiled to us."
    "Sakura nous sourit."

# game/dialogs.rpy:3631
translate french scene9_c7659595:

    # s "I love you all too... So much..."
    s "Moi aussi, je vous aime tous... Je vous aime tellement..."

# game/dialogs.rpy:3633
translate french scene9_dad43788:

    # hero "You know, Sakura-chan..."
    hero "Tu sais, Sakura-chan..."

# game/dialogs.rpy:3634
translate french scene9_1acb2356:

    # hero "I feel I was a terrible friend to you..."
    hero "J'ai l'impression... Que j'ai été un horrible ami pour toi..."

# game/dialogs.rpy:3635
translate french scene9_8d593ce9:

    # hero "Please forgive me..."
    hero "Je te demande pardon..."

# game/dialogs.rpy:3636
translate french scene9_79003247:

    # "Sakura put a finger on my lips."
    "Sakura posa un doigt sur mes lèvres."

# game/dialogs.rpy:3637
translate french scene9_69c6fa27:

    # s "Hush! Stop talking nonsense."
    s "Chut ! Arrête de dire des bêtises."

# game/dialogs.rpy:3638
translate french scene9_e9594450:

    # s "You faced my father to defend me and you were almost ready to die for that!"
    s "Tu as affronté mon père pour me défendre, allant jusqu'au péril de ta vie !"

# game/dialogs.rpy:3639
translate french scene9_7b24500a:

    # s "How can you possibly think you're an awful friend?"
    s "Comment peux-tu dire que tu es un ami horrible ?"

# game/dialogs.rpy:3640
translate french scene9_0cde7c21:

    # s "You're my best friend!"
    s "Tu es mon meilleur ami !"

# game/dialogs.rpy:3641
translate french scene9_993b25b5:

    # "Tears rolled down my eyes."
    "Des larmes vinrent à mes yeux. Je souris."

# game/dialogs.rpy:3642
translate french scene9_1cce67e1:

    # hero "You're an angel, Sakura-chan."
    hero "Tu es un ange, Sakura-chan."

# game/dialogs.rpy:3644
translate french scene9_57b35236:

    # r "Did your mom visit you?"
    r "Ta mère est venue te rendre visite ?"

# game/dialogs.rpy:3645
translate french scene9_85c4e509:

    # s "Yes she did."
    s "Oui."

# game/dialogs.rpy:3646
translate french scene9_a3b8f1b9:

    # s "She's a bit sad that father was taken to jail,... {w}But I think she can't love him anymore after what he did."
    s "Elle est un peu triste que père soit en prison,...{w} Mais je pense qu'elle ne peut plus l'aimer après ce qu'il a fait."

# game/dialogs.rpy:3647
translate french scene9_f1990ad2:

    # s "I don't know what to think either. {w}It's a relief that he won't bother me and mom anymore...{w}but he's still my father..."
    s "Je ne sais pas quoi penser non plus. {w}C'est un soulagement de le voir parti...{w}mais c'est toujours mon père..."

# game/dialogs.rpy:3648
translate french scene9_05ffe314:

    # hero "Don't worry, Sakura-chan. We'll all be here for you!"
    hero "Ne t'en fais pas, Sakura-chan... On sera là pour t'aider."

# game/dialogs.rpy:3649
translate french scene9_0c126df5:

    # hero "I don't know how yet, but we'll find out!"
    hero "Je ne sais pas encore comment, mais on trouvera !"

# game/dialogs.rpy:3650
translate french scene9_363e4438:

    # r "Yes, we will!"
    r "Oui !"

# game/dialogs.rpy:3651
translate french scene9_6a9ed22c:

    # n "Yeah!"
    n "Ouais !"

# game/dialogs.rpy:3652
translate french scene9_eecde369:

    # "A tear of joy rolled down Sakura's cheek as she smiled."
    "Une larme de joie roula sur la joue de Sakura alors qu'elle sourit."

# game/dialogs.rpy:3653
translate french scene9_cb78f15e:

    # s "I know you all do."
    s "Je le sais bien."

# game/dialogs.rpy:3654
translate french scene9_e926c122:

    # s "Thank you..."
    s "Merci à tous..."

# game/dialogs.rpy:3655
translate french scene9_81a1d957:

    # "Since Rika-chan brought some manga with us, we did our club activities with Sakura in her room."
    "Vu que Rika avait pris quelques mangas, on a fait les activités du club dans la chambre d'hôpital avec Sakura."

# game/dialogs.rpy:3656
translate french scene9_87845d82:

    # "That was fun."
    "C'était marrant."

# game/dialogs.rpy:3660
translate french scene9_b859f96a:

    # "I was so happy..."
    "J'étais si heureux..."

# game/dialogs.rpy:3661
translate french scene9_4055f551:

    # "I think my luck helped me again..."
    "Je crois que la chance m'a encore aidé..."

# game/dialogs.rpy:3662
translate french scene9_9095e36e:

    # "But this time, it helped Sakura too."
    "Et cette fois, elle a aussi aidé Sakura."

# game/dialogs.rpy:3665
translate french scene9_8dbb56a9:

    # n "Why the doctor said \"he\" to Sakura?"
    n "Pourquoi le docteur a dit \"lui\" pour Sakura ?"

# game/dialogs.rpy:3666
translate french scene9_ad0764fc:

    # "Uh-oh..."
    "Aïe..."

# game/dialogs.rpy:3667
translate french scene9_ff11d5e2:

    # "Well, better late than never."
    "Bon, mieux vaut tard que jamais."

# game/dialogs.rpy:3668
translate french scene9_984c292d:

    # "Since there probably wouldn't be any other better moment for that, We told Nanami about Sakura's secret."
    "Vu qu'il n'y aurait probablement pas d'autre meilleur moment, nous avons tous dit à Nanami le secret de Sakura."

# game/dialogs.rpy:3669
translate french scene9_fcc6280e:

    # "She took it well and understood. Her feelings for Sakura didn't changed at all. Just as I expected of her."
    "Elle l'a bien pris et elle a compris. Ses sentiments pour Sakura n'ont pas changé. Je n'en attendais pas moins."

# game/dialogs.rpy:3670
translate french scene9_a20cefa7:

    # "..."
    "..."

# game/dialogs.rpy:3671
translate french scene9_19a38d50:

    # "Though, for a whole week, she randomly said \"Seriously, guys!!\" multiple times!"
    "Elle nous a quand même crié \"Sérieux, les gars !!\" plusieurs fois pendant quelques jours !"

# game/dialogs.rpy:3672
translate french scene9_7c3cef17:

    # "I think she was a bit upset that we kept the secret to us until now."
    "Je crois qu'elle était un peu vexée qu'on ait gardé le secret tout ce temps."

# game/dialogs.rpy:3673
translate french scene9_0bfbc1f4:

    # "We promised it was the last time we kept a secret from Nanami."
    "On s'est promis de ne plus jamais cacher quoi que ce soit à Nanami."

# game/dialogs.rpy:3677
translate french end_d9a3a0bf:

    # "Summer vacation came."
    "Les vacances d'été arrivèrent."

# game/dialogs.rpy:3678
translate french end_d7bb0ba9:

    # "To all of us, school was over until next September."
    "Pour nous tous, l'école était finie jusqu'à septembre."

# game/dialogs.rpy:3683
translate french end_d95c02cb:

    # "Love can be really strange..."
    "L'amour peut être un sentiment vraiment imprévisible..."

# game/dialogs.rpy:3684
translate french end_c409fabd:

    # "You think you know what kind of person you'll end up with but..."
    "Tu penses savoir quel genre de personne tu aimeras, mais..."

# game/dialogs.rpy:3685
translate french end_5eba593d:

    # "Love at first sight can hit anybody..."
    "Le coup de foudre peut arriver n'importe où..."

# game/dialogs.rpy:3686
translate french end_617e0a41:

    # "I was pretty sure I would only love real girls..."
    "J'étais pratiquement sûr que je n'aimerais que les filles..."

# game/dialogs.rpy:3687
translate french end_e3e4774b:

    # "And my first girlfriend is actually a very girlish boy!"
    "Mais ma petite amie est en fait née garçon !"

# game/dialogs.rpy:3688
translate french end_6292a02f:

    # "Hehe, who could have predicted this?{p}Not me, for sure..."
    "Héhé, qui l'aurait prédit ?{p}Pas moi, c'est certain..."

# game/dialogs.rpy:3689
translate french end_e4e1f13a:

    # "I don't know how much time we will stay together, Sakura and I."
    "Je ne sais pas combien de temps notre histoire durera."

# game/dialogs.rpy:3690
translate french end_32aa38be:

    # "Months? Years? Maybe for the rest of our lives...?"
    "Des mois ? Des années ? Le reste de nos vies ?..."

# game/dialogs.rpy:3691
translate french end_478778fd:

    # "I'm not sure of what the future holds for us..."
    "Je ne sais pas ce que le futur nous réserve..."

# game/dialogs.rpy:3692
translate french end_d076867a:

    # "But if my story with Sakura must end someday,{w} I'll never forget the wonderful times I passed with her."
    "Mais si mon histoire doit se finir un jour avec Sakura,{w} je n'oublierai jamais les merveilleux moments passés avec elle."

# game/dialogs.rpy:3693
translate french end_7dbb760e:

    # "Or him..."
    "Ou lui..."

# game/dialogs.rpy:3694
translate french end_54a608c6:

    # "Who cares..."
    "On s'en fout..."

# game/dialogs.rpy:3695
translate french end_85655125:

    # s "Honey! What are you doing? Come see the view!"
    s "Chéri ! Viens voir la vue, c'est magnifique !"

# game/dialogs.rpy:3696
translate french end_7c43af43:

    # hero "I'm coming, sweetie!"
    hero "J'arrive !!"

# game/dialogs.rpy:3703
translate french end_59a65d95:

    # "We're waiting at our plane at the airport."
    "On attendait notre avion à l'aéroport."

# game/dialogs.rpy:3704
translate french end_ae3dd256:

    # "Nanami and I decided to take some vacations at Nanami's homeland, Okinawa."
    "Nanami et moi avions décidé de passer quelques vacances dans la ville de naissance de Nanami, à Okinawa."

# game/dialogs.rpy:3705
translate french end_d12ce3e4:

    # "My head was full of ondo and traditional music, like at the festival."
    "Ma tête était pleine de ondos et de musiques traditionelles, comme au festival."

# game/dialogs.rpy:3706
translate french end_4452fe89:

    # "Toshio escorted us to the airport.{p}Rika and Sakura were here too."
    "Toshio nous avait conduits à l'aéroport. {p}Rika et Sakura étaient là aussi."

# game/dialogs.rpy:3707
translate french end_8b58bec9:

    # "It's been a while that Rika and Sakura started to going out together, now."
    "Ça fait un moment que Rika et Sakura sortent ensemble, maintenant."

# game/dialogs.rpy:3708
translate french end_e30bea45:

    # "They look like a cute couple of girls."
    "Elles étaient un mignon petit couple."

# game/dialogs.rpy:3709
translate french end_ca017532:

    # toshio "Tell our grand-parents that I love them."
    toshio "Dis à nos grands-parents que je les aime."

# game/dialogs.rpy:3710
translate french end_a7442fed:

    # n "I will, don't worry."
    n "Promis, grand frère."

# game/dialogs.rpy:3711
translate french end_d3330439:

    # toshio "%(stringhero)s-san... Take good care of my sister."
    toshio "%(stringhero)s-san... Prends bien soin de ma sœur."

# game/dialogs.rpy:3712
translate french end_a831d3fe:

    # hero "I will."
    hero "Promis."

# game/dialogs.rpy:3713
translate french end_17ebd900:

    # hero "And you, good luck with your new job."
    hero "Et toi, bonne chance avec ton nouveau boulot."

# game/dialogs.rpy:3714
translate french end_5b348dc8:

    # "My parents hired him at the grocery shop."
    "Mes parents avaient embauché Toshio à l'épicerie."

# game/dialogs.rpy:3715
translate french end_16f4a6f4:

    # "That way, he can renew with his past while keeping going straight forward to his new adult life."
    "Comme ça, il peut renouer avec son passé tout en allant de l'avant avec sa nouvelle vie d'adulte."

# game/dialogs.rpy:3716
translate french end_95d64dcd:

    # s "Take a lot of photos, %(stringhero)s-kun! The whole place is beautiful, it deserve photographs!"
    s "Prends plein de photos, %(stringhero)s-kun ! L'endroit est magnifique, ça vaudrait le coup !"

# game/dialogs.rpy:3717
translate french end_8f710e49:

    # r "And bring back some souvenirs!{p}Like, an okinawa manga or something!"
    r "Et ramène nous un souvenir !!{p}Genre, un manga d'Okinawa ou même chose !"

# game/dialogs.rpy:3718
translate french end_9cf1c80f:

    # "I chuckled at Rika's request."
    "Je ris à la requête de Rika."

# game/dialogs.rpy:3719
translate french end_a725dde6:

    # "Then, I heard the call for our plane."
    "Puis j'entendis l'appel pour notre avion."

# game/dialogs.rpy:3720
translate french end_bb0d139e:

    # "Nanami and I went for our departure."
    "Nanami et moi partirent pour le quai de départ."

# game/dialogs.rpy:3721
translate french end_8ea8e17b:

    # "We waved at our friends and they waved back."
    "On salua nos amis de la main et ils répondirent."

# game/dialogs.rpy:3722
translate french end_ff8fb8bb:

    # "One thing is certain,..."
    "Une chose est certaine..."

# game/dialogs.rpy:3723
translate french end_06a038b7:

    # "It's in moments like this that having friends like that is a true luck."
    "C'est dans des moments comme ça qu'avoir de tels amis est une chance."

# game/dialogs.rpy:3724
translate french end_099873e9:

    # "And with a girlfriend like Nanami,...{w}and friends like I have,..."
    "Et avec une petite amie comme Nanami en supplément,..."

# game/dialogs.rpy:3725
translate french end_3f02d717:

    # "I can tell I'm the luckiest dude ever."
    "Je m'estime être le mec le plus chanceux de la terre."

# game/dialogs.rpy:3726
translate french end_bc578e9b:

    # s "Don't forget to come back!"
    s "N'oubliez pas d'écrire !!!"

# game/dialogs.rpy:3727
translate french end_867c4c03:

    # n "We will, Sakura-nee!"
    n "Promis, Sakura-nee !"

# game/dialogs.rpy:3734
translate french end_2d2fc7dd:

    # "I finally asked Rika-chan to marry me."
    "J'ai finalement demandé Rika en mariage."

# game/dialogs.rpy:3735
translate french end_7d486c68:

    # "And I promised Sakura-chan that she will be the godparent of our children."
    "Et j'ai promis à Sakura qu'elle serait la marraine de nos enfants."

# game/dialogs.rpy:3736
translate french end_16d52d33:

    # "I hope Sakura will find love someday. She really deserves it."
    "J'espère que Sakura trouvera un jour quelqu'un pour elle. Elle le mérite vraiment."

# game/dialogs.rpy:3737
translate french end_8052eee9:

    # "I was actually ready for the ceremony."
    "J'étais prêt pour la cérémonie."

# game/dialogs.rpy:3738
translate french end_d8e35e56:

    # "In the village, we usually do traditional weddings, but Rika wanted an occidental wedding."
    "Au village, on faisait des mariages traditionnels, mais Rika voulait un mariage occidental."

# game/dialogs.rpy:3739
translate french end_e5ffcd4a:

    # "I guess it's for the dress..."
    "J'imagine que c'est pour la robe..."

# game/dialogs.rpy:3740
translate french end_c371d9bb:

    # "It's the best cosplay she ever did. Except, this time, it wasn't a cosplay. It was real."
    "C'est le meilleur cosplay qu'elle ait jamais fait. Sauf que cette fois, c'était du réel."

# game/dialogs.rpy:3741
translate french end_ecc6ece6:

    # "Priest" "Dear %(stringhero)s-san, do you take Rika-san to be your wedded wife, to live together in marriage?"
    "Prêtre" "Cher %(stringhero)s-san, voulez-vous prendre pour épouse Rika-san ici présente, de l'aimer et la chérir dans les liens sacrés du mariage ?"

# game/dialogs.rpy:3742
translate french end_02471825:

    # hero "You bet I do!"
    hero "Tu parles que je veux, oui !"

# game/dialogs.rpy:3743
translate french end_34741d03:

    # r "Can't you just say \"I do.\", you city rat?!"
    r "Tu peux pas juste dire \"oui\", espèce de citadin ?!!"

# game/dialogs.rpy:3744
translate french end_96f20b66:

    # "*{i}laughs{/i}*"
    "*{i}rires{/i}*"

# game/dialogs.rpy:3745
translate french end_c2e91305:

    # hero "Hey do you plan to call me city rat even after being married!"
    hero "Hé, tu vas m'appeler citadin même après qu'on soit mariés ?!"

# game/dialogs.rpy:3746
translate french end_5848829c:

    # r "Yes I will!"
    r "Ouais !"

# game/dialogs.rpy:3747
translate french end_96f20b66_1:

    # "*{i}laughs{/i}*"
    "*{i}rires{/i}*"

# game/dialogs.rpy:3748
translate french end_6ddd1312:

    # "Priest" "Hem hem..."
    "Prêtre" "Hum hum..."

# game/dialogs.rpy:3749
translate french end_aea646e4:

    # "Priest" "And you, dear Rika-san,...do you take %(stringhero)s-san to be your wedded husband, to live together in marriage?"
    "Prêtre" "Et vous, chère Rika-san, voulez-vous prendre pour époux %(stringhero)s-san ici présent, de l'aimer et le chérir dans les liens sacrés du mariage ?"

# game/dialogs.rpy:3750
translate french end_742d691c:

    # "Then, Rika made a smile.{p}A smile I never seen on her before."
    "Puis, Rika fit un sourire.{p}Un sourire que je n'avais jamais vu."

# game/dialogs.rpy:3751
translate french end_2665aacc:

    # "Not that naughty smile of when she prepares a joke or anything."
    "Pas ce sourire malsain de quand elle prépare des blagues."

# game/dialogs.rpy:3752
translate french end_dfe9a447:

    # "A shining, bright smile. The cutest smile she ever made."
    "Un sourire lumineux, radieux. Le plus mignon qu'elle ait jamais fait."

# game/dialogs.rpy:3753
translate french end_b8af1a6a:

    # r "... I do!"
    r "... Oui !"

# game/dialogs.rpy:3806
translate french trueend_906ebb63:

    # centered "You finished the game for the first time!\nGo check the Bonus menu to see what you have unlocked!"
    centered "Vous avez fini le jeu pour la première fois !\nAllez voir ce que vous avez débloqué dans les bonus !"

# game/dialogs.rpy:3811
translate french trueend_66eedbda:

    # centered "You finished the game with Sakura's route for the first time!"
    centered "Vous avez fini la route de Sakura pour la première fois !"

# game/dialogs.rpy:3816
translate french trueend_1561cc61:

    # centered "You finished the game with Rika's route for the first time!"
    centered "Vous avez fini la route de Rika pour la première fois !"

# game/dialogs.rpy:3821
translate french trueend_bb63381f:

    # centered "You finished the game with Nanami's route for the first time!"
    centered "Vous avez fini la route de Nanami pour la première fois !"

# game/dialogs.rpy:3838
translate french bonus1_25742137:

    # hero "Hey girls, do you remember the first anime you ever watched?"
    hero "Hé, les filles, vous vous souvenez du premier animé que vous aviez regardé ?"

# game/dialogs.rpy:3840
translate french bonus1_6e60ac8f:

    # s "Hmm... That's a hard one..."
    s "Hmm... Question difficile..."

# game/dialogs.rpy:3841
translate french bonus1_25a3fcbc:

    # r "Hmm, I think my first one was {i}Sanae-san{/i}."
    r "Hmm, je crois que mon premier était {i}Sanae-san{/i}."

# game/dialogs.rpy:3842
translate french bonus1_1ac8f05b:

    # hero "{i}Sanae-san{/i}?{p}That anime that about politics and stuff?"
    hero "{i}Sanae-san{/i} ?{p}Ce vieil animé qui parle de politique ?"

# game/dialogs.rpy:3843
translate french bonus1_1e55e98f:

    # r "Yeah! That one was nice!"
    r "Ouais ! Il était pas mal !"

# game/dialogs.rpy:3844
translate french bonus1_89c89c91:

    # hero "But it was aired when we were around four or five years old! You could understand what they were talking about?"
    hero "Mais il a été diffusé quand on avait genre 5 ou 6 ans ! T'arrivais à comprendre les dialogues ?"

# game/dialogs.rpy:3846
translate french bonus1_b9b8111c:

    # "Rika gave me a disgusted look."
    "Rika me regarda avec une drôle de tête."

# game/dialogs.rpy:3847
translate french bonus1_17789b93:

    # r "Whatever! Well what about you? What was your first anime?!"
    r "Et toi, tu regardais quoi ?"

# game/dialogs.rpy:3848
translate french bonus1_517679f1:

    # hero "{i}Panpanman{/i}. You know, the one with the donut-headed guy..."
    hero "{i}Panpanman{/i}. Tu sais, le truc avec le mec à tête de beignet..."

# game/dialogs.rpy:3849
translate french bonus1_c5d8e4d4:

    # r "And could you understand everything in that show?"
    r "Et tu comprenais les dialogues ?"

# game/dialogs.rpy:3850
translate french bonus1_8b365fc3:

    # "Ah damn, she's got a point there."
    "Mince, elle marque un point, là."

# game/dialogs.rpy:3851
translate french bonus1_85a0d1bb:

    # "{i}Panpanman{/i} wasn't as complicated as {i}Sanae-san{/i}, but it's just as old. I was young so...I just followed along the animation."
    "{i}Panpanman{/i} était loin d'être aussi compliqué que {i}Sanae-san{/i}, mais il était aussi vieux. J'étais si jeune que je ne comprenais que par les images."

# game/dialogs.rpy:3852
translate french bonus1_61a5b6c2:

    # hero "And you, Nanami-san?"
    hero "Et toi, Nanami-san ?"

# game/dialogs.rpy:3856
translate french bonus1_b075ad7f:

    # n "Hmm..."
    n "Hmm..."

# game/dialogs.rpy:3857
translate french bonus1_2d685261:

    # n "I think my first one was {i}Galaxyboy{/i}..."
    n "Je crois que mon premier était {i}Galaxyboy{/i}..."

# game/dialogs.rpy:3858
translate french bonus1_88ba5484:

    # r "The one with that robot kid?"
    r "Celui avec le gamin-robot ?"

# game/dialogs.rpy:3859
translate french bonus1_31293345:

    # n "Yeah! I remember it being really colorful and fun to watch."
    n "Ouais. Je me souviens que c'était plutôt marrant et coloré."

# game/dialogs.rpy:3860
translate french bonus1_cd1e46e5:

    # n "There was a game called {i}Punkman{/i} with a similar looking robot."
    n "Puis plus tard, il y a eu ce jeu, {i}Punkman{/i}, avec un robot dans le même genre..."

# game/dialogs.rpy:3861
translate french bonus1_f81a7f2c:

    # n "Maybe that's why I remember {i}Galaxyboy{/i} a little."
    n "C'est sans doute pour ça que je me rappelle un peu de {i}Galaxyboy{/i}..."

# game/dialogs.rpy:3863
translate french bonus1_e11f5261:

    # n "Although, I don't remember the story at all!"
    n "Parce qu'à part ça, je ne me souviens même pas de l'histoire !"

# game/dialogs.rpy:3865
translate french bonus1_d7bf3979:

    # hero "What about you, Sakura-san?{p}What was your first anime?"
    hero "Et toi, Sakura-san ? Ton premier animé ?"

# game/dialogs.rpy:3867
translate french bonus1_85feadfd:

    # s "Hmm... Well... I think my first one was {i}La fleur de Paris{/i}..."
    s "Hmm... Je pense que mon premier a été {i}La fleur de Paris{/i}..."

# game/dialogs.rpy:3868
translate french bonus1_7076984c:

    # hero "The one based on a book with that girl from France?"
    hero "Celui qui est basé sur un bouquin français ?"

# game/dialogs.rpy:3869
translate french bonus1_e929395f:

    # hero "That one is more recent... It only aired around six years ago."
    hero "Celui-là est plus récent... Il est sorti il y a 6 ans je crois."

# game/dialogs.rpy:3870
translate french bonus1_4343cc28:

    # s "Yeah. My love for anime started a little later. I was 12 when I started watching it."
    s "Oui. J'ai aimé tardivement les animés. J'avais 12 ans quand j'ai commencé à le regarder."

# game/dialogs.rpy:3871
translate french bonus1_5a7baf3a:

    # s "I loved the story. It was about a girl who disguises herself as a man just to get closer to her king."
    s "J'aimais bien l'histoire de cette fille qui se déguise en homme pour être plus proche de son roi."

# game/dialogs.rpy:3872
translate french bonus1_64c82b58:

    # hero "{i}La fleur de Paris{/i} sounds like a shoujo... I think my sister tried to read it once..."
    hero "{i}La fleur de Paris{/i} est un shoujo, de ce que je me rappelle... Je crois que ma sœur l'a lu..."

# game/dialogs.rpy:3873
translate french bonus1_9c8d2196:

    # s "I didn't know you had a sister!"
    s "Je ne savais pas que tu avais une sœur !"

# game/dialogs.rpy:3874
translate french bonus1_376fc77d:

    # hero "Well, yeah."
    hero "Ben, si..."

# game/dialogs.rpy:3875
translate french bonus1_a4020f07:

    # hero "But she's way older than me. She's already married."
    hero "Mais elle est plus vieille que moi. Elle est déjà mariée."

# game/dialogs.rpy:3876
translate french bonus1_5d47aa0b:

    # hero "She's in Tokyo with her husband."
    hero "Elle est restée à Tokyo avec son mari."

# game/dialogs.rpy:3877
translate french bonus1_3e2e6e7f:

    # r "That's sounds nice!"
    r "C'est cool pour elle !"

# game/dialogs.rpy:3879
translate french bonus1_5a889d44:

    # s "Don't you miss having your sister around?"
    s "Ta sœur ne te manque pas un peu ?"

# game/dialogs.rpy:3880
translate french bonus1_38aa09d8:

    # hero "Oh well, she's always been independent. I got used to it after awhile, so it's okay! I still write to her from time to time."
    hero "Non, ça va. Elle a toujours été très indépendante. J'y suis habitué... Puis on peut toujours s'écrire par e-mail."

# game/dialogs.rpy:3881
translate french bonus1_2e2dc283:

    # hero "By the way, do you guys have siblings?"
    hero "Et vous trois, vous avez des frères et sœurs ?"

# game/dialogs.rpy:3882
translate french bonus1_ceca0652:

    # n "I have an older brother."
    n "J'ai un grand frère."

# game/dialogs.rpy:3883
translate french bonus1_1e3491ce:

    # s "I don't."
    s "Pas moi."

# game/dialogs.rpy:3884
translate french bonus1_7c225574:

    # r "Me neither."
    r "Moi non plus."

# game/dialogs.rpy:3885
translate french bonus1_bc19b4bf:

    # hero "Ever wish you had one?"
    hero "Vous en auriez voulu ?"

# game/dialogs.rpy:3886
translate french bonus1_bdb5b07a:

    # r "Nah, I never did. I'm happy with my current life."
    r "Je ne pense pas. Je suis heureuse dans ma vie actuelle."

# game/dialogs.rpy:3887
translate french bonus1_58cc7ba4:

    # s "Well for me... I sometimes dream of having a big brother."
    s "Moi en revanche... J'aurais beaucoup aimé avoir un grand frère..."

# game/dialogs.rpy:3888
translate french bonus1_dc1a8fcd:

    # n "I can understand that! My big brother is awesome!"
    n "Je comprends ça ! Mon grand frère est super !"

# game/dialogs.rpy:3889
translate french bonus1_f18686d4:

    # s "But I'll never have any siblings. My parents don't want to..."
    s "Mais je n'aurai jamais de frères et sœurs, je pense. Mes parents n'en veulent pas..."

# game/dialogs.rpy:3890
translate french bonus1_7568d34b:

    # hero "It's sad...{p}But maybe it's better like this."
    hero "C'est triste...{p}Mais c'est peut-être mieux comme ça."

# game/dialogs.rpy:3891
translate french bonus1_453d2365:

    # s "Why is that?"
    s "Pourquoi ?"

# game/dialogs.rpy:3892
translate french bonus1_d027b066:

    # hero "Usually big brothers are mean to their little sisters in the first years."
    hero "D'habitude les grands frères ne sont pas très gentils avec leurs sœurs pendant l'enfance."

# game/dialogs.rpy:3893
translate french bonus1_3fae45d0:

    # n "Hmm... My big brother was a jerk to me when I was younger... {p}But not anymore..."
    n "Hmm... Mon grand frère m'embêtait parfois quand j'étais petite...{p}Mais plus maintenant..."

# game/dialogs.rpy:3894
translate french bonus1_6d59e0e0:

    # hero "What do you mean?"
    hero "Comment ça ?"

# game/dialogs.rpy:3895
translate french bonus1_e148f620:

    # n "Well... {w}Nevermind... {p}I guess it's normal for siblings after some time..."
    n "Ben...  {w}Non, rien...{p}J'imagine que c'est normal que ça passe après un certain âge..."

# game/dialogs.rpy:3896
translate french bonus1_207b93d2:

    # hero "You must be right...{p}Anyway,..."
    hero "T'as sûrement raison...{p}En tout cas,..."

# game/dialogs.rpy:3897
translate french bonus1_0ee56e3f:

    # hero "What I mean is, at least nobody can harm you, Sakura-san."
    hero "Ce que je veux dire, c'est, au moins personne ne pourra te faire du mal, Sakura-san."

# game/dialogs.rpy:3898
translate french bonus1_557b9c7c:

    # s ". . ."
    s ". . ."

# game/dialogs.rpy:3899
translate french bonus1_b3b9c0e8:

    # s "Hmm..."
    s "Hmm..."

# game/dialogs.rpy:3901
translate french bonus1_743a5cb3:

    # s "Yes, you're probably right, %(stringhero)s-san."
    s "Oui, tu as probablement raison, %(stringhero)s-san."

# game/dialogs.rpy:3902
translate french bonus1_d501d7aa:

    # "That hesitation again...{p}I don't find it very natural..."
    "Cette hésitation...{p}Elle n'avait pas l'air très naturelle..."

# game/dialogs.rpy:3903
translate french bonus1_b9ad9038:

    # "Is she hiding something?..."
    "Est-ce qu'elle cache quelque chose ?..."

# game/dialogs.rpy:3904
translate french bonus1_7d45cddc:

    # "And Nanami's acting weird too. Maybe I brought up a sore subject..."
    "Pourquoi Nanami a fait pareil ?..."

# game/dialogs.rpy:3905
translate french bonus1_8534ca42:

    # ". . ."
    ". . ."

# game/dialogs.rpy:3906
translate french bonus1_a191a0bc:

    # "Nah, I'm just being paranoid..."
    "Non, je me fais des idées..."

# game/dialogs.rpy:3927
translate french bonus2_8d40556d:

    # "I was watching Sakura as she was looking for her yukata."
    "Je regardai Sakura chercher son yukata dans ma chambre."

# game/dialogs.rpy:3928
translate french bonus2_2eb7bc22:

    # "She noticed that I was staring and she smiled and stopped."
    "Elle remarqua mon sourire et elle s'arrêta en souriant aussi."

# game/dialogs.rpy:3929
translate french bonus2_1ab585f3:

    # s "Why the smile, honey?"
    s "Pourquoi ce sourire, mon chéri ?"

# game/dialogs.rpy:3930
translate french bonus2_f27ba686:

    # hero "Well, now that I've seen you nude... I realized something..."
    hero "Maintenant que je te vois entièrement nue,... j'ai réalisé..."

# game/dialogs.rpy:3931
translate french bonus2_28c06110:

    # hero "You look so girly with that hair and thin body..."
    hero "Tu es si féminine avec ces cheveux et ce corps fin..."

# game/dialogs.rpy:3932
translate french bonus2_1740af69:

    # hero "But you have the chest of a thin guy... and a penis too..."
    hero "Mais tu as le torse d'un garçon très fin... et ce truc en bas..."

# game/dialogs.rpy:3933
translate french bonus2_cf68db7f:

    # hero "I have the evidence that you're born as a boy... And I was pretty sure I would only like girls."
    hero "Je vois parfaitement que tu es née garçon... Et j'ai toujours cru que je n'aimerais que les filles."

# game/dialogs.rpy:3934
translate french bonus2_a2c879ff:

    # hero "But in my eyes, you're way prettier than any woman or girl I've ever seen. And I can't stop loving you anyway..."
    hero "Mais à mes yeux, tu es bien plus belle que n'importe quelle fille au monde. Et je ne peux pas m'arrêter de t'aimer..."

# game/dialogs.rpy:3935
translate french bonus2_bc3f3461:

    # "She smiled. Then she sat beside me on the bed."
    "Elle sourit et s'assit près de moi sur le lit."

# game/dialogs.rpy:3936
translate french bonus2_6e930048:

    # s "Do you love me because I look like a girl?"
    s "Tu m'aimes parce que j'ai l'apparence et le comportement d'une fille ?"

# game/dialogs.rpy:3937
translate french bonus2_32aff81a:

    # s "You think it would be different if I was a real boy?"
    s "Tu penses que les choses auraient été différentes si j'avais été un vrai garçon ?"

# game/dialogs.rpy:3938
translate french bonus2_b543397f:

    # hero "Well..."
    hero "Eh bien..."

# game/dialogs.rpy:3939
translate french bonus2_aa6128c8:

    # hero "To be honest, I..."
    hero "En fait je..."

# game/dialogs.rpy:3940
translate french bonus2_2983804d:

    # "I felt a headache."
    "Je sentis un mal de crâne."

# game/dialogs.rpy:3941
translate french bonus2_f7a250f4:

    # hero "Darn, actually, I have no idea. I don't know at all..."
    hero "Mince, en fait je n'en sais rien du tout..."

# game/dialogs.rpy:3942
translate french bonus2_4c90784c:

    # hero "The first thing that attracted me to you was your feminine features and personality...{w} I didn't realize you were male at the time."
    hero "La première chose qui m'a attiré chez toi, c'est ton corps si parfait...{w}puis ta personnalité et ta douceur ont pris le dessus."

# game/dialogs.rpy:3943
translate french bonus2_7a20d415:

    # hero "I fell in love with you at first sight...But surprisingly, your secret didn't stop me from loving you."
    hero "J'ai eu tout de suite le coup de foudre... Et ton secret n'a rien changé dans mon amour pour toi..."

# game/dialogs.rpy:3944
translate french bonus2_7fb40657:

    # hero "I'm wondering about my own sexuality... Does this mean I'm \"biologically gay\" or something?"
    hero "Je me demande qui je suis... Est-ce que je suis.. Je sais pas, \"biologiquement gay\" ou même chose ?"

# game/dialogs.rpy:3945
translate french bonus2_8b8a7483:

    # hero "Maybe it's not gay since you are a girl inside and that I love you..."
    hero "Ou peut-être pas, si tu es une fille en toi et que je t'aime..."

# game/dialogs.rpy:3946
translate french bonus2_7ae5be64:

    # hero "I have so many questions about myself and I don't have any answers..."
    hero "J'ai tellement de questions sur moi-même et je ne trouve aucune réponse..."

# game/dialogs.rpy:3947
translate french bonus2_0a5fc0a9:

    # "Sakura gently leaned her head against mine and looked into my eyes."
    "Sakura prit gentiment ma tête, colla son front au mien pour me regarder dans les yeux."

# game/dialogs.rpy:3948
translate french bonus2_4951c5b2:

    # s "The important thing isn't what your mind wants."
    s "Tout ce qui importe ce n'est pas ce que ton cerveau veut.."

# game/dialogs.rpy:3949
translate french bonus2_91e5ab91:

    # s "But what your heart wants."
    s "Mais ce que ton cœur veut."

# game/dialogs.rpy:3950
translate french bonus2_c269d052:

    # s "You know... sometimes I ask that about myself too."
    s "Tu sais... Je me pose les mêmes questions que toi, parfois."

# game/dialogs.rpy:3951
translate french bonus2_77498e04:

    # s "My father was always telling me that I should like girls. And I actually do, to be honest..."
    s "Mon père me dit que je dois aimer les filles... Et honnêtement, je les aime aussi..."

# game/dialogs.rpy:3952
translate french bonus2_891bc3a6:

    # s "But as I tried to become more comfortable with who I was..."
    s "Mais en essayant de me rapprocher plus de mon moi réel,...."

# game/dialogs.rpy:3953
translate french bonus2_dbb3d044:

    # s "I started to be attracted to boys as well."
    s "Je me suis senti beaucoup plus attiré par les garçons."

# game/dialogs.rpy:3954
translate french bonus2_97ff28cd:

    # s "I was completely undecided during this time. I was feeling the same exact way you are right now. But I finally figured out..."
    s "Durant tout ce temps où j'ai eu ces questions, je n'arrivais plus à dormir... Je crus devenir folle... Mais j'ai finalement compris."

# game/dialogs.rpy:3955
translate french bonus2_f9789510:

    # s "Girls? Boys? It doesn't matter."
    s "Filles ? Garçons ? Peu importe."

# game/dialogs.rpy:3956
translate french bonus2_6041b77d:

    # s "The most important is: {w}Does the person you love, return as much love as you do?"
    s "Le plus important est : {w}est-ce que la personne que tu aimes te rend le même amour ?"

# game/dialogs.rpy:3957
translate french bonus2_7d1800b4:

    # "Looking at her cute blue eyes made my heart beat louder."
    "En regardant les deux grands lacs de ses yeux, mon cœur battit plus fort."

# game/dialogs.rpy:3958
translate french bonus2_49959dea:

    # "Yeah, she's right. Why does gender matter...?"
    "Elle avait juste. Que m'importait tout ça ?"

# game/dialogs.rpy:3959
translate french bonus2_473681a8:

    # hero "Sakura-chan... My love... My heart wants to stay with you..."
    hero "Sakura-chan... Mon amour... Mon cœur veut rester avec toi..."

# game/dialogs.rpy:3960
translate french bonus2_8482b65d:

    # hero "I don't care about about your body and what others may think."
    hero "Comme je le disais l'autre fois, je me fous de ce que ton corps ou les gens disent."

# game/dialogs.rpy:3961
translate french bonus2_e9d4d741:

    # hero "No matter what happens, my feelings for you won't change. You're the one I want to be with."
    hero "Peu importe ce qui m'arrive, je ne changerai pas mes sentiments pour toi. Tu es celle que j'ai choisie."

# game/dialogs.rpy:3962
translate french bonus2_b8d074ce:

    # hero "You're right... What's gender, anyway? It's just a box to check on a paper."
    hero "Tu as raison... Qu'est-ce qu'un genre, au final ? Une case à cocher sur un formulaire..."

# game/dialogs.rpy:3963
translate french bonus2_9df6f08e:

    # s "Exactly... Genders are just a label..."
    s "Exactement... Les genres ne sont qu'une étiquette..."

# game/dialogs.rpy:3964
translate french bonus2_d9b204fb:

    # hero "I love you, Sakura-chan... I love you..."
    hero "Je t'aime, Sakura-chan... Je t'aime..."

# game/dialogs.rpy:3965
translate french bonus2_96516aa7:

    # hero "And my only hope is that you love me too..."
    hero "Mon seul désir est que tu m'aimes aussi..."

# game/dialogs.rpy:3966
translate french bonus2_05a7a895:

    # "She cuddled me tighter."
    "Elle me serra contre elle."

# game/dialogs.rpy:3967
translate french bonus2_66ae317a:

    # s "I do, %(stringhero)s-kun. I love you, more than anything..."
    s "Je t'aime, %(stringhero)s-kun. Je t'aime plus que tout au monde..."

# game/dialogs.rpy:3968
translate french bonus2_25c4ff40:

    # s "I fell in love for you the first time we met, as well."
    s "Je suis tombée amoureuse de toi à l'instant où on s'est rencontrés."

# game/dialogs.rpy:3969
translate french bonus2_a7a9aeaa:

    # s "You are the man of my life."
    s "Tu es l'homme de ma vie."

# game/dialogs.rpy:3970
translate french bonus2_20830efe:

    # hero "And you are the girl of mine, Sakura-chan. Forever."
    hero "Et toi la femme de ma vie, Sakura-chan. Pour toujours."

# game/dialogs.rpy:3971
translate french bonus2_6b6e7e46:

    # hero "And that secret of yours... Actually, it makes you pretty unique..."
    hero "Et ce secret que tu portes... Ça te rend plutôt unique, en fin de compte..."

# game/dialogs.rpy:3972
translate french bonus2_27fcf534:

    # hero "And I feel so proud and happy to have a so unique girlfriend."
    hero "Et je suis fier et heureux d'avoir une petite amie aussi unique."

# game/dialogs.rpy:3973
translate french bonus2_57845a53:

    # "I kissed her tenderly."
    "Je l'embrassai tendrement."

# game/dialogs.rpy:3974
translate french bonus2_3db00aad:

    # "I leaned on her naked body as I carried on."
    "Je m'allongeai sur son corps nu en continuant."

# game/dialogs.rpy:3975
translate french bonus2_0949b3d0:

    # "I stopped a moment to look at her beautiful face."
    "Je m'arrêtai un moment pour regarder son magnifique visage."

# game/dialogs.rpy:3976
translate french bonus2_ad103b2f:

    # "Some tears were falling."
    "Des larmes coulaient."

# game/dialogs.rpy:3977
translate french bonus2_2b9b486c:

    # hero "Why are you crying, hun?"
    hero "Pourquoi tu pleures, mon ange ?"

# game/dialogs.rpy:3978
translate french bonus2_193b5b96:

    # s "I'm just... so happy... %(stringhero)s-kun..."
    s "Je suis juste... si heureuse... %(stringhero)s-kun..."

# game/dialogs.rpy:3979
translate french bonus2_a9227760:

    # s "I'm so happy that you love me, no matter what I am."
    s "Si heureuse que tu m'aimes, peu importe comment je suis."

# game/dialogs.rpy:3980
translate french bonus2_09611ce1:

    # s "I love you so much, %(stringhero)s-kun... So much..."
    s "Je t'aime tellement, %(stringhero)s-kun... Tellement..."

# game/dialogs.rpy:3981
translate french bonus2_c1ae5d6f:

    # "I cuddled her lovingly. Her body was warming mine."
    "Je la câlinai tendrement. Son corps réchauffait le mien."

# game/dialogs.rpy:3982
translate french bonus2_8d1c6006:

    # "Then I suddenly remembered... My parents! They must not find Sakura here and naked!"
    "Puis je me rappelai soudainement... Mes parents ! Ils ne doivent pas trouver Sakura nue ici !"

# game/dialogs.rpy:3983
translate french bonus2_0f61a346:

    # "After a moment, we stood up and I helped Sakura putting her yukata again..."
    "Après un moment, nous nous sommes levés et j'ai aidé Sakura à remettre son yukata."

# game/dialogs.rpy:3987
translate french bonus2_38df4441:

    # "In front of her house, I felt my stomach twisting. I don't want to be without her..."
    "Devant chez elle, je sentis mon estomac se nouer. Je ne voulais pas repartir sans elle."

# game/dialogs.rpy:3988
translate french bonus2_82ba49f2:

    # hero "I don't want to leave you, I want to stay with you!"
    hero "J'ai pas envie de partir, je veux rester avec toi !"

# game/dialogs.rpy:3989
translate french bonus2_1a74f79c:

    # s "Me too... I miss your arms already... {image=heart.png}"
    s "Moi aussi... Tes bras me manquent déjà... {image=heart.png}"

# game/dialogs.rpy:3990
translate french bonus2_4a161b65:

    # s "It's okay, love, we'll meet again tomorrow at school, remember?"
    s "Ça ira, mon amour. On se reverra à l'école."

# game/dialogs.rpy:3991
translate french bonus2_2b99de4b:

    # s "It will be the last week until the summer vacation."
    s "Ce sera notre dernière semaine d'école avant les grandes vacances."

# game/dialogs.rpy:3992
translate french bonus2_f405c4f8:

    # s "And I will be free during this period."
    s "Je serai libre pendant tout ce temps !"

# game/dialogs.rpy:3993
translate french bonus2_c5fd93a1:

    # hero "So will I..."
    hero "Moi aussi..."

# game/dialogs.rpy:3994
translate french bonus2_4ff85ef0:

    # hero "And you'll be sure that I'll see you everyday during the vacations."
    hero "Et tu seras sûre que je viendrai te voir tous les jours."

# game/dialogs.rpy:3995
translate french bonus2_903c8c1d:

    # "She smiled and pecks my cheek."
    "Elle m'embrassa la joue."

# game/dialogs.rpy:3996
translate french bonus2_f3fb3329:

    # s "I can't wait for it, %(stringhero)s-honey-bun! {image=heart.png}"
    s "J'ai tellement hâte mon petit sucre ! {image=heart.png}"

# game/dialogs.rpy:4015
translate french bonus3_5f502601:

    # write "{b}Headline news : A father tried to murder his daughter.{/b}"
    write "{b}A la une : Un père tente d'assassiner sa fille.{/b}"

# game/dialogs.rpy:4016
translate french bonus3_eae14b39:

    # write "Last night, in the little village of N. in the Osaka prefecture at 1am, a drunken father, 44 years old, attempted to murder the young %(stringhero)s, 19 years old.{w} The father's daughter protected him and got stabbed in the abdomen."
    write "La nuit dernière, dans le village de N. dans la préfecture d'Osaka, vers 1h du matin ; un père ivre, 44 ans, a tenté d'assassiner le jeune %(stringhero)s, 19 ans.{w} La fille de ce père a tenté de le protéger et a été blessée à l'abdomen."

# game/dialogs.rpy:4017
translate french bonus3_1fc20115:

    # write "The father was arrested and the girl, 18 years old, has been taken to the hospital.{w} The reasons of the fight are unknown, but the Police suspect it was due to the effects of the alcohol and the offender's criminal record."
    write "Le père a été arrêté et sa fille, 18 ans, a été emmenée d'urgence à l'hôpital. {w} Les raisons de l'altercation sont inconnues, mais la police suspecte les effets de l'alcool et les antécédents judiciaires de l'agresseur."

# game/dialogs.rpy:4018
translate french bonus3_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:4053
translate french bonus4_244a1f9c:

    # "It was Sunday. Our first Sunday of summer vacation."
    "C'était dimanche. Notre premier dimanche de vacances."

# game/dialogs.rpy:4054
translate french bonus4_d04b10e5:

    # "We decided to hold a picnic near the rice field."
    "On avait décidé de faire un pique-nique près d'un champ de riz."

# game/dialogs.rpy:4055
translate french bonus4_41f4816b:

    # "I remembered it was here where Sakura revealed her secret."
    "Je souris en reconnaissant l'endroit où Sakura m'avait révélé son grand secret."

# game/dialogs.rpy:4057
translate french bonus4_c69f2e23:

    # "It's also here that we kissed for the first time."
    "C'était ici aussi qu'on a eu notre premier baiser."

# game/dialogs.rpy:4058
translate french bonus4_edd1ee56:

    # "Rika organized a little game for us and we started playing after lunch."
    "Rika avait organisé un petit jeu et on commença à jouer après le repas."

# game/dialogs.rpy:4063
translate french bonus4_c4dbd1b7:

    # r "Okay, this is a game based on manga, anime and videogames."
    r "Ok, c'est un jeu basé sur les mangas, la japanimation et les jeux vidéo."

# game/dialogs.rpy:4064
translate french bonus4_e067d3ab:

    # r "It's a simple quiz where the losers will have to throw dice and the punishment is decided by the who rolls the highest."
    r "C'est un simple question-réponse où les deux perdants doivent lancer un dé, et le gage est décidé par celui qui a le score le plus élevé."

# game/dialogs.rpy:4065
translate french bonus4_3d239b9b:

    # hero "Uhhh... What kind of punishment...?"
    hero "Mmm... Quels genres de gages ?"

# game/dialogs.rpy:4067
translate french bonus4_5b7ca773:

    # r "Ohohoho. Scared to lose, city rat?"
    r "Ohohoho. Pourquoi cette question ? T'as peur de perdre, le citadin ?"

# game/dialogs.rpy:4069
translate french bonus4_53fc8dd1:

    # hero "I'm not a city rat! I'm your boyfriend!{p}Bring on the questions, I'm not afraid!"
    hero "Je suis pas un citadin ! Je suis ton mec !{p}Aboule les questions, je suis prêt !!"

# game/dialogs.rpy:4071
translate french bonus4_785461af:

    # hero "I'm not a city rat! Bring on the questions, I'm not afraid!"
    hero "Je suis pas un citadin ! Aboule les questions, je suis prêt !!"

# game/dialogs.rpy:4072
translate french bonus4_0a90e627:

    # s "Please be gentle with %(stringhero)s-kun, it's his first time."
    s "Va doucement avec %(stringhero)s-kun, c'est sa première fois."

# game/dialogs.rpy:4073
translate french bonus4_5e33fd5b:

    # r "Don't worry, Sakura-chan... I'll be just fine..."
    r "T'inquiète, Sakura. J'irai doucement..."

# game/dialogs.rpy:4074
translate french bonus4_57287ff9:

    # "I don't like that smirk on Rika's face..."
    "Je n'ai pas aimé le sourire de Rika..."

# game/dialogs.rpy:4075
translate french bonus4_1f4b7948:

    # "Nanami noticed the smirk as well."
    "Nanami avait le même sourire."

# game/dialogs.rpy:4077
translate french bonus4_bb701bb3:

    # n "Ooooh, you will not like this, %(stringhero)s-nii!"
    n "Ohhhh, tu vas pas aimer ça, %(stringhero)s-nii !"

# game/dialogs.rpy:4079
translate french bonus4_03d1d4c1:

    # n "Ooooh, you will not like this, %(stringhero)s-senpai!"
    n "Ohhhh, tu vas pas aimer ça, %(stringhero)s-senpai !"

# game/dialogs.rpy:4080
translate french bonus4_ae65064e:

    # "Rika took some cards in her bag."
    "Rika prit quelques cartes dans son sac."

# game/dialogs.rpy:4083
translate french bonus4_0158abfa:

    # r "Okay guys, Here's the first question..."
    r "Ok, voici la première question..."

# game/dialogs.rpy:4084
translate french bonus4_c9e531e1:

    # r "\"What is the name of the tsundere girl in the game {i}Real Love '95{/i}?\""
    r "\"Quel est le nom de la tsundere dans le jeu {i}Real Love '95{/i} ?\""

# game/dialogs.rpy:4085
translate french bonus4_9f44cf6d:

    # "Ouch, this first question is hard..."
    "Ouille, pas facile, celle-là... Heureusement, je me rappelle un peu du jeu."

# game/dialogs.rpy:4086
translate french bonus4_db066cbc:

    # "Knowing Rika, I'm surprised about this question, since {i}Real Love '95{/i} is actually an eroge for PC."
    "Connaissant Rika, je suis surpris de la question, vu que {i}Real Love '95{/i} est un eroge pour PC."

# game/dialogs.rpy:4087
translate french bonus4_0e0a402b:

    # "And I remember playing it a few months ago, when I still was in Tokyo."
    "Je me souviens même y avoir joué il y a quelques mois, à Tokyo."

# game/dialogs.rpy:4088
translate french bonus4_d39d805f:

    # "So... The tsundere girl... Hmmmm...{p}Yes! I remember!"
    "Alors, la tsundere...Hmmm....{p}Oui, ça me revient !"

# game/dialogs.rpy:4089
translate french bonus4_e4df0c97:

    # hero "It was Mayumi!"
    hero "C'était Mayumi !"

# game/dialogs.rpy:4092
translate french bonus4_0a0fb39e:

    # r "Good answer, honey!"
    r "Bonne réponse, chéri !"

# game/dialogs.rpy:4094
translate french bonus4_74267ea4:

    # r "Good answer, city rat!"
    r "Bonne réponse, le citadin !"

# game/dialogs.rpy:4095
translate french bonus4_eab1e51f:

    # r "Sakura, Nanami, roll the dice!"
    r "Sakura, Nanami, roulez le dé !"

# game/dialogs.rpy:4096
translate french bonus4_b1e1b336:

    # "Sakura got a 2.{w} Nanami got a 3."
    "Sakura eut un 2. {w} Nanami eut un 3."

# game/dialogs.rpy:4097
translate french bonus4_50bfd4d4:

    # r "Sakura-chan, you lost this round, so Nanami-chan must choose a punishment for you!"
    r "Sakura-chan, t'as perdu le round. Nanami-chan doit te choisir un gage !"

# game/dialogs.rpy:4099
translate french bonus4_86b16bed:

    # "Sakura started to feel shy."
    "Sakura se sentit intimidée."

# game/dialogs.rpy:4100
translate french bonus4_4a9a8b0c:

    # "Nanami made a funny grin."
    "Nanami fit un sourire marrant."

# game/dialogs.rpy:4102
translate french bonus4_724f715d:

    # n "Sakura-nee, you must call us by a cute anime-style name for the rest of the day!"
    n "Sakura-nee, tu dois nous appeler par des noms mignons d'animé pour le reste de la journée !"

# game/dialogs.rpy:4103
translate french bonus4_a42514e8:

    # n "I want to be called \"Senpai\",... Senpai! *{i}giggles{/i}*"
    n "Je veux que tu m'appelles \"Senpai\",... Senpai! *{i}rires{/i}*"

# game/dialogs.rpy:4104
translate french bonus4_997b66a7:

    # r "I want to be called \"Big sister\"!"
    r "Et moi \"Grande sœur\" !"

# game/dialogs.rpy:4106
translate french bonus4_f47c567a:

    # s "Okay!"
    s "Ok !"

# game/dialogs.rpy:4107
translate french bonus4_4b1e12da:

    # s "And you, %(stringhero)s-kun? What should I call you?" nointeract
    s "Et toi, %(stringhero)s-kun? Comment dois-je t'appeler ?" nointeract

# game/dialogs.rpy:4115
translate french bonus4_aab8ef6a:

    # hero "Sakura-chan...{w}I want you to call me \"[hnick!t]\"!"
    hero "Sakura-chan...{w}Je veux que tu m'appelles \"[hnick!t]\" !"

# game/dialogs.rpy:4117
translate french bonus4_7d57ea39:

    # s "E... Eeeeeeeh!!!"
    s "E... Eeeeeeeh !!!"

# game/dialogs.rpy:4118
translate french bonus4_5541ddc2:

    # "She became embarrassed, hiding her little mouth behind her fist... So cute..."
    "Elle se mit à rougir, cachant sa bouche derrière ses petits poings... Trop mimi..."

# game/dialogs.rpy:4119
translate french bonus4_cebdf164:

    # "I blushed myself, waiting for her answer, knowing what she was about to say already..."
    "Je rougis moi-même, attendant sa réponse, imaginant déjà comment ça sortirait..."

# game/dialogs.rpy:4121
translate french bonus4_c5c21450:

    # s "A-Alright... I'll d-do it... [hnick!t]..."
    s "D-d'accord... Je vais le faire... [hnick!t]..."

# game/dialogs.rpy:4123
translate french bonus4_70397aff:

    # hero "Gah! The cuteness!"
    hero "Hngg ! Adorable !"

# game/dialogs.rpy:4124
translate french bonus4_b312e964:

    # "If this was an anime, my nose would be bleeding."
    "J'en tomberais à terre en saignant du nez si j'étais dans un animé !"

# game/dialogs.rpy:4126
translate french bonus4_92f46d2c:

    # r "Hey, wake up, city rat! Quit daydreaming and get ready for the next question. It's Sakura's turn to ask the question!"
    r "Hé debout, le citadin ! Question suivante ! Et selon les règles, c'est à Sakura de l'énoncer !"

# game/dialogs.rpy:4127
translate french bonus4_24294182:

    # "Yikes! It means that it will be me against Rika and Nanami! I must not fail!"
    "Oh-oh ! Ça veut dire que je serai face à Nanami et Rika ! Faut pas que je me rate !"

# game/dialogs.rpy:4131
translate french bonus4_731520a2:

    # s "O... Okay... So..."
    s "O...Ok, alors..."

# game/dialogs.rpy:4132
translate french bonus4_1011202f:

    # s "The question is:{p}\"How many episodes are in the anime {i}Dragon Sphere{/i}?\""
    s "La question est :{p}\"Combien d'épisodes a l'animé {i}Dragon Sphere{/i} ?\""

# game/dialogs.rpy:4133
translate french bonus4_37f34882:

    # "I knew this anime well. I was about to answer but Rika was faster."
    "Je connaissais la réponse. J'allais répondre mais Rika fut plus rapide."

# game/dialogs.rpy:4135
translate french bonus4_60b1dd5c:

    # r "26! {w}And 3 OVAs!"
    r "26 ! {w}Et 3 OAVs !"

# game/dialogs.rpy:4137
translate french bonus4_ed2c49db:

    # s "Correct, Big sister!"
    s "Correct, grande sœur !"

# game/dialogs.rpy:4138
translate french bonus4_c9aee644:

    # s "[hnick!t], Nanami-senpai, roll the dice."
    s "[hnick!t], Nanami-senpai, lancez le dé."

# game/dialogs.rpy:4139
translate french bonus4_906b5890:

    # "I got a five! Yes!"
    "J'ai eu un 5 ! Ouais !"

# game/dialogs.rpy:4140
translate french bonus4_1581741a:

    # "Nanami rolls...{w}and got a 6! Oh come on!"
    "Nanami lance...{w}et obtient un 6 ! Bon sang !"

# game/dialogs.rpy:4141
translate french bonus4_db50cb13:

    # "Nanami directed an evil smile at me. Now I'm pretty scared..."
    "Nanami me jeta un sourire mauvais. Là, j'ai peur..."

# game/dialogs.rpy:4142
translate french bonus4_021acfb6:

    # "She took something in Rika's bag and gave it to me."
    "Elle prit quelque chose dans le sac de Rika et me le donna."

# game/dialogs.rpy:4143
translate french bonus4_d1658509:

    # "Cat-ears!"
    "Un serre-tête avec des oreilles de chat !"

# game/dialogs.rpy:4144
translate french bonus4_3d592873:

    # n "Here, put this on your head and say 'Nyan nyan~'!"
    n "Tiens, mets ça sur ta tête et fais \"Nyan nyan~\" !"

# game/dialogs.rpy:4145
translate french bonus4_e4a0fb20:

    # hero "Heeeeey!{p}I'm pretty sure this was Rika-chan's idea, huh!"
    hero "Eeeeh !!{p}Je suis sûr que c'était une idée de Rika à la base !"

# game/dialogs.rpy:4146
translate french bonus4_eb7fa7f2:

    # r "Who knows?... Get to work, city rat! Or I should say 'City cat'!"
    r "Qui sait ?... Allez, au boulot, le citadin !"

# game/dialogs.rpy:4147
translate french bonus4_8bc9d837:

    # n "Get to work, you slacker! *{i}with a fake strong male voice from some video game{/i}*"
    n "Get to work, you slacker! *{i}en imitant une grosse voix de mec dans un jeu vidéo{/i}*"

# game/dialogs.rpy:4148
translate french bonus4_ff3a6290:

    # "I sighed and I put the ears on my head."
    "Je soupirai et mis les oreilles sur ma tête."

# game/dialogs.rpy:4149
translate french bonus4_cf248989:

    # "We didn't have any mirrors around so I couldn't see how I looked. But I'm sure it was ridiculous."
    "On n'avait pas de miroir sous la main, mais j'imaginais que j'avais l'air ridicule."

# game/dialogs.rpy:4150
translate french bonus4_7ae9ab60:

    # "I put my fists in front of me, kitty-like."
    "Je mis mes poings devant moi comme un chat."

# game/dialogs.rpy:4151
translate french bonus4_ad791bf6:

    # hero "N... Ny... Nyan~ nyan~!"
    hero "N... Ny... Nyan~ nyan~ !"

# game/dialogs.rpy:4157
translate french bonus4_3686e5b0:

    # "Sakura made a big charming smile that instantly made me feel better."
    "Sakura fit un sourire charmant qui me fit instantanément aller mieux."

# game/dialogs.rpy:4159
translate french bonus4_af9b813d:

    # "Sakura made a big charming smile."
    "Sakura fit un sourire adorable."

# game/dialogs.rpy:4160
translate french bonus4_17625773:

    # s "Awwww it's so cuuuuute!!!"
    s "Ohhhh c'est trop mignon !!"

# game/dialogs.rpy:4161
translate french bonus4_6328f30b:

    # "Rika smiled too, but her smile was way more sinister."
    "Rika sourit aussi, mais d'une manière plus sinistre."

# game/dialogs.rpy:4162
translate french bonus4_bda42d96:

    # "Nanami was just laughing out loud. I definitely should look ridiculous."
    "Nanami était pliée de rire. Je devais réellement être ridicule."

# game/dialogs.rpy:4163
translate french bonus4_8d7688c3:

    # r "Hehe, doesn't fit you at all!"
    r "Héhé, ça ne te va tellement pas !"

# game/dialogs.rpy:4164
translate french bonus4_12c6774c:

    # r "Since you lost, it's your turn to ask the question!"
    r "Vu que t'as perdu, c'est à toi de lire la question !"

# game/dialogs.rpy:4169
translate french bonus4_ede3e123:

    # "Rika gave me another card and I read the question."
    "Rika me donna une autre carte et je lus la question."

# game/dialogs.rpy:4170
translate french bonus4_0e18d25a:

    # "I was also thinking about the pledge they could imagine."
    "Je pensais déjà aux gages qu'elles pourraient imaginer."

# game/dialogs.rpy:4171
translate french bonus4_9fe745b2:

    # hero "Okay so...{p}\"What is the name of the seiyuu who plays Haruki in {i}Panty, Bra und Strumpfgürtel{/i}?\""
    hero "Ok alors...{p}\"Comment s'appelle la seiyuu qui double Haruki dans {i}Panty, Bra und Strumpfgürtel{/i} ?\""

# game/dialogs.rpy:4172
translate french bonus4_9c8a3481:

    # "Nanami answered immediately, just before Rika could answer."
    "Nanami répondit immédiatement, juste avant Rika."

# game/dialogs.rpy:4174
translate french bonus4_5cc59595:

    # n "Hidaka Megumi!"
    n "Hidaka Megumi !"

# game/dialogs.rpy:4175
translate french bonus4_a7215825:

    # hero "You got it, Nanami-chan!"
    hero "Dans le mille, Nanami-chan !"

# game/dialogs.rpy:4177
translate french bonus4_fc07c2f9:

    # hero "Rika-chan, Sakura-chan, roll the dice!"
    hero "Rika-chan, Sakura-chan, lancez le dé !"

# game/dialogs.rpy:4178
translate french bonus4_1a6d10bf:

    # "Sakura rolled a 3."
    "Sakura eut un 3."

# game/dialogs.rpy:4180
translate french bonus4_d821c96c:

    # "Rika made a naughty grin and rolled the dice..."
    "Rika fit un sourire mauvais et lança le dé..."

# game/dialogs.rpy:4182
translate french bonus4_619191be:

    # "She pouted when she got a one..."
    "Elle bouda quand elle eut un 1..."

# game/dialogs.rpy:4184
translate french bonus4_667bfdcf:

    # s "Hmmm... Let's see..."
    s "Hmmm... Voyons..."

# game/dialogs.rpy:4187
translate french bonus4_ddb63045:

    # "She thought for a moment but finally made a smile... One worse than Rika-chan's!"
    "Elle réfléchit un moment, puis fit finalement un sourire... Un sourire pire que celui de Rika !"

# game/dialogs.rpy:4188
translate french bonus4_b721b3d2:

    # "It's so unusual to see such a smile on Sakura's face."
    "C'est tellement bizarre de voir ça sur Sakura."

# game/dialogs.rpy:4190
translate french bonus4_226ccb5a:

    # s "Big sister, I want you to kiss [hnick!t] on the mouth, in front of us!"
    s "Grande sœur, Je veux que tu embrasses [hnick!t] sur la bouche devant nous toutes !"

# game/dialogs.rpy:4192
translate french bonus4_288e9d30:

    # s "Big sister, I want you to kiss [hnick!t] on the cheek!"
    s "Grande sœur, Je veux que tu embrasses [hnick!t] sur la joue !"

# game/dialogs.rpy:4194
translate french bonus4_54428ce0:

    # r "Whaaaaaaat???"
    r "Keeeuwaaaaaah ???!!"

# game/dialogs.rpy:4195
translate french bonus4_3efa7eb4:

    # hero "Eeeeeeeeh!!!"
    hero "Eeeeeeeeh !!!"

# game/dialogs.rpy:4198
translate french bonus4_329e5a99:

    # n "Eeeeh but it's my boyfriend!!"
    n "Eeeeh mais c'est mon petit ami !!"

# game/dialogs.rpy:4200
translate french bonus4_7529a0c6:

    # r "In front of you all?!"
    r "Devant tout le monde ?!!"

# game/dialogs.rpy:4202
translate french bonus4_2f9c2ac6:

    # r "I don't want to, Sakura-chan!{p}It's mean!"
    r "Je veux pas, Sakura-chan !{p}C'est pas cool !"

# game/dialogs.rpy:4203
translate french bonus4_d7114010:

    # s "I'm sorry but it's in the rules. You told us earlier, Rika-chan!{p}And it wouldn't be a punishment if it was too easy!"
    s "Désolé mais c'est les règles. C'est toi qui l'a dit, Rika-chan !{p}Et ce serait pas un gage si c'était trop facile !"

# game/dialogs.rpy:4206
translate french bonus4_eb063792:

    # n "I feel like punished too, even if I won..."
    n "J'ai l'impression que ça me punit aussi, même si j'ai gagné !..."

# game/dialogs.rpy:4207
translate french bonus4_a2b2dbfe:

    # "Sakura can be scary sometimes..."
    "Sakura peut vraiment faire peur quand elle veut..."

# game/dialogs.rpy:4209
translate french bonus4_b9f6e14e:

    # r "Hmpf!... {w}A... Alright... I'll do it!"
    r "Hmpf !... {w}D-d'accord... Je vais le faire !"

# game/dialogs.rpy:4210
translate french bonus4_6213e31d:

    # "My eyes wide open, I saw Rika-chan crawling slowly to me with her pissed expression."
    "Mes yeux s'ouvrirent grand en voyant Rika s'approcher de moi avec son expression énervée."

# game/dialogs.rpy:4213
translate french bonus4_7062c20b:

    # "Then we closed our eyes and we kissed, after some hesitation."
    "On ferma les yeux et on s'embrassa après quelques hésitations."

# game/dialogs.rpy:4214
translate french bonus4_23c6d1be:

    # "At the moment our lips touched, I think we both forgot the girls were here because the kiss was long and lovey."
    "Au moment où nos lèvres se sont touchées, je crois qu'on a directement oublié les filles car le baiser fut plus long et doux que nécessaire."

# game/dialogs.rpy:4216
translate french bonus4_6e084a46:

    # "My heart was beating loudly..."
    "Mon cœur battait fort..."

# game/dialogs.rpy:4218
translate french bonus4_0defb206:

    # "Then she closed her eyes and neared her face to my cheek."
    "Puis elle ferma les yeux et approcha son visage de ma joue."

# game/dialogs.rpy:4220
translate french bonus4_f50c2971:

    # "My heart was beating loudly as she draws near..."
    "Mon cœur battait fort alors qu'elle s'approchait..."

# game/dialogs.rpy:4221
translate french bonus4_fcdd662b:

    # ".........{i}*chu~*{/i}......"
    ".........{i}*chu~*{/i}......"

# game/dialogs.rpy:4222
translate french bonus4_e916cd9d:

    # s "Awww, so cute!!!"
    s "Ohhh, trop mignon !!!"

# game/dialogs.rpy:4224
translate french bonus4_4ef9b2c5:

    # n "You better wash your cheek and kiss me at once after this!"
    n "T'as intérêt à te laver la joue et m'embrasser après ça !!"

# game/dialogs.rpy:4226
translate french bonus4_79a8814d:

    # n "Hey, get a room, you two!"
    n "Hé, allez à l'hôtel, vous deux !"

# game/dialogs.rpy:4228
translate french bonus4_d218ebbe:

    # "Then Rika got back to where she was sitting."
    "Puis Rika retourna à sa place."

# game/dialogs.rpy:4230
translate french bonus4_e986db9f:

    # "She was embarrassed but her face was still looking pissed."
    "Elle rougissait mais elle était toujours pas jouasse."

# game/dialogs.rpy:4232
translate french bonus4_98a2e64c:

    # r "T-t-that's private stuff! You're not supposed to watch that!..."
    r "C-c-c'est privé, ce genre de truc ! Vous êtes pas supposés voir ça !..."

# game/dialogs.rpy:4233
translate french bonus4_20c841e6:

    # "I silently agreed, blushing red."
    "J'acquiesçai en silence, rougissant."

# game/dialogs.rpy:4234
translate french bonus4_479b77b6:

    # "But well...as Sakura said, it was a punishment..."
    "Mais comme disait Sakura, c'était un gage..."

# game/dialogs.rpy:4236
translate french bonus4_8dbc83a8:

    # r "D... Don't expect anything, you pervert, you city rat!... It's just a punishment!"
    r "N-n-ne te méprends pas, pervers de citadin !... C'est juste un gage !"

# game/dialogs.rpy:4238
translate french bonus4_e77cd0a8:

    # r "It should be Sakura's job, anyway!"
    r "C'est le boulot de Sakura, ça !"

# game/dialogs.rpy:4240
translate french bonus4_b9726a19:

    # r "It should be Nanami's job, anyway!"
    r "C'est le boulot de Nanami, ça !"

# game/dialogs.rpy:4241
translate french bonus4_929d41bb:

    # n "Right!"
    n "Ouais !"

# game/dialogs.rpy:4242
translate french bonus4_d0abd9fc:

    # hero "Hmph! Fine with me!"
    hero "Hmpf ! Tant mieux !"

# game/dialogs.rpy:4243
translate french bonus4_9b79ecac:

    # "I felt that I'm turning red too..."
    "Je me sentis toujours rougir..."

# game/dialogs.rpy:4245
translate french bonus4_8fc82e11:

    # "Sakura woke me up in my confusion."
    "Sakura me réveilla de ma confusion."

# game/dialogs.rpy:4246
translate french bonus4_ad51f715:

    # s "Hey, I'm your girlfriend, don't you forget!"
    s "Hé, c'est moi ta copine, ne l'oublie pas !"

# game/dialogs.rpy:4247
translate french bonus4_2aa2b9a6:

    # "I smiled and kissed Sakura fondly on the cheek."
    "Je souris et embrassa Sakura sur la joue tendrement."

# game/dialogs.rpy:4248
translate french bonus4_a2e4f8f1:

    # n "Saku and %(stringhero)s, sitting in a tree, K. I. S. S. I. N. G.!..."
    n "Ouh les amoureux-euh !!"

# game/dialogs.rpy:4250
translate french bonus4_4420fe81:

    # "Nanami woke me up in my confusion."
    "Nanami me réveilla de ma confusion."

# game/dialogs.rpy:4251
translate french bonus4_ad51f715_1:

    # s "Hey, I'm your girlfriend, don't you forget!"
    s "Hé, c'est moi ta copine, ne l'oublie pas !"

# game/dialogs.rpy:4252
translate french bonus4_c6260d04:

    # "I smiled and kissed Nanami fondly on the cheek."
    "Je souris et embrassa Nanami sur la joue tendrement."

# game/dialogs.rpy:4253
translate french bonus4_a5c16330:

    # hero "By the way... It's your turn for the next question, Rika-chan..."
    hero "Bon... C'est à toi de poser la question, Rika-chan..."

# game/dialogs.rpy:4259
translate french bonus4_fc3d4898:

    # "The game was long and the punishments was fun... Sometimes evil but fun..."
    "Le jeu était long et les gages étaient marrants... Parfois humiliants mais marrants..."

# game/dialogs.rpy:4260
translate french bonus4_4c6939cf:

    # "When evening came, we started to go back to our homes."
    "Quand le soir vint, on rentra chez nous."

# game/dialogs.rpy:4266
translate french bonus4_f83000c7:

    # "I was escorting Sakura-chan to her house as usual and we were chatting about the picnic."
    "J'escortai Sakura chez elle comme d'habitude et on parlait du pique-nique."

# game/dialogs.rpy:4267
translate french bonus4_ca69579e:

    # s "That was fun... [hnick!t]!"
    s "C'était fun...[hnick!t] !"

# game/dialogs.rpy:4268
translate french bonus4_313e8d85:

    # hero "You know, you don't need to keep calling me [hnick!t], now. The girls aren't with us anymore."
    hero "Tu sais, tu n'as plus besoin de m'appeler \"[hnick!t]\", maintenant. Les filles ne sont plus là."

# game/dialogs.rpy:4269
translate french bonus4_c63b8929:

    # s "It's okay, I like taking the punishment seriously...{p}Unless you don't like me calling you that?"
    s "C'est pas grave. J'aime bien prendre les gages au sérieux...{p}A moins que tu n'aimes pas que je t'appelle comme ça ?"

# game/dialogs.rpy:4270
translate french bonus4_e5dc6483:

    # hero "No, it's okay, I enjoy it!"
    hero "Non, ça va... J'aime bien !"

# game/dialogs.rpy:4273
translate french bonus4_90714309:

    # hero "Even if it might sound weird between lovers..."
    hero "Même si ça sonne bizarre entre amoureux..."

# game/dialogs.rpy:4274
translate french bonus4_b9b5a607:

    # "We smiled together."
    "On sourit ensemble."

# game/dialogs.rpy:4277
translate french bonus4_756dbf3c:

    # s "[hnick!t], do you...{p}Do you think I would look pretty in a maid outfit?"
    s "Dis, [hnick!t],...{p}Tu crois que je serais jolie en soubrette ?"

# game/dialogs.rpy:4279
translate french bonus4_6dc8daef:

    # "I was close to nose bleeding as I was imagining Sakura in a maid outfit."
    "Je manquai de saigner du nez en ayant dans ma tête la vision de Sakura déguisée en maid."

# game/dialogs.rpy:4280
translate french bonus4_6049d7bb:

    # hero "I... I'm pretty sure of it, Sakura-chan!"
    hero "Je... J'en suis sûr, Sakura-chan !"

# game/dialogs.rpy:4281
translate french bonus4_e234cd17:

    # hero "In fact... No matter what you'll wear, you always will be cute to me."
    hero "En fait,... Quoique tu portes, tu seras toujours mignonne pour moi."

# game/dialogs.rpy:4282
translate french bonus4_138db92c:

    # "We blushed together."
    "On rougit ensemble."

# game/dialogs.rpy:4284
translate french bonus4_554402fd:

    # hero "Well... I guess you would..."
    hero "Ben... Je pense que oui..."

# game/dialogs.rpy:4285
translate french bonus4_03905b91:

    # hero "You know how cute you look already."
    hero "Tu sais combien tu es mignonne déjà."

# game/dialogs.rpy:4286
translate french bonus4_a0391de0:

    # "She blushed."
    "Elle rougit."

# game/dialogs.rpy:4289
translate french bonus4_4722aed7:

    # s "You know... I like to call you [hnick!t]..."
    s "Tu sais,... J'aime bien t'appeler [hnick!t]..."

# game/dialogs.rpy:4290
translate french bonus4_1ea38385:

    # s "I always wanted to have a big brother but I didn't got this chance."
    s "J'ai toujours voulu avoir un grand frère mais je n'ai pas eu cette chance."

# game/dialogs.rpy:4291
translate french bonus4_dcd60152:

    # s "So, calling you like this... It's almost as if... If I have one..."
    s "T'appeler comme ça... C'est comme si... j'en avais un pour un instant..."

# game/dialogs.rpy:4292
translate french bonus4_acc2309b:

    # "We blushed together, deeply."
    "On rougit profondément."

# game/dialogs.rpy:4294
translate french bonus4_119e7f5e:

    # "Well, I guess it would be nice for her to have a brother. As long as he doesn't turn crazy easily like Nanami's brother..."
    "J'imagine que ça serait bien qu'elle ait un frère. Tant qu'il ne vire pas fou comme c'est arrivé à celui de Nanami."

# game/dialogs.rpy:4295
translate french bonus4_1f716602:

    # hero "Well... If you still want to call me like this all the time... I won't mind at all, Sakura-chan."
    hero "Ben... Si tu veux continuer à m'appeler comme ça,... ça ne me dérange pas, Sakura-chan."

# game/dialogs.rpy:4296
translate french bonus4_a465601d:

    # hero "To be honest, I always wanted to have a little sister."
    hero "Pour tout te dire, j'ai toujours voulu avoir une petite sœur."

# game/dialogs.rpy:4297
translate french bonus4_b5b85398:

    # s "But you have a big sister, don't you?"
    s "Mais tu as une grande sœur, non ?"

# game/dialogs.rpy:4298
translate french bonus4_3f93cb80:

    # hero "Yeah, but it's not the same thing. Plus, she doesn't live with me anymore..."
    hero "Oui mais ce n'est pas pareil. Et puis, elle ne vit plus avec nous..."

# game/dialogs.rpy:4299
translate french bonus4_6fc9312b:

    # "Sakura nodded..."
    "Sakura hocha..."

# game/dialogs.rpy:4303
translate french bonus4_bd6e96bc:

    # s "So, how's your couple with Rika-chan?"
    s "Comment ça se passe avec Rika-chan ?"

# game/dialogs.rpy:4304
translate french bonus4_a9623e40:

    # hero "Pretty good! I think I'm going to marry her!"
    hero "Plutôt bien ! Je crois que je vais la demander en mariage !"

# game/dialogs.rpy:4306
translate french bonus4_dd27c7e7:

    # s "Really?! That's great!!"
    s "Vraiment ? C'est super !!"

# game/dialogs.rpy:4307
translate french bonus4_699e47c9:

    # hero "You think she will say yes?"
    hero "Tu crois qu'elle dira oui ?"

# game/dialogs.rpy:4309
translate french bonus4_df94e128:

    # s "I'm sure she will."
    s "J'en suis sûre."

# game/dialogs.rpy:4310
translate french bonus4_c15523d1:

    # s "You know, she really loves you more than it looks."
    s "Tu sais, elle t'aime beaucoup plus qu'elle ne le montre."

# game/dialogs.rpy:4311
translate french bonus4_f0c8c519:

    # s "It's just her way of being herself."
    s "Elle est comme ça."

# game/dialogs.rpy:4312
translate french bonus4_10b77e4e:

    # hero "Yeah, I know..."
    hero "Oui, je sais..."

# game/dialogs.rpy:4313
translate french bonus4_7f081c6b:

    # "Then I stopped and I asked Sakura."
    "Puis je m'arrêtai et demanda à Sakura."

# game/dialogs.rpy:4314
translate french bonus4_f25fd7bb:

    # hero "Sakura-chan..."
    hero "Sakura-chan..."

# game/dialogs.rpy:4315
translate french bonus4_7347e2f5:

    # hero "I would be so honored that you become our future children's godparent!"
    hero "Ce serait un honneur pour moi si tu acceptais d'être la marraine de nos futurs enfants !"

# game/dialogs.rpy:4316
translate french bonus4_2662060c:

    # hero "Would you like to?"
    hero "Tu veux bien ?"

# game/dialogs.rpy:4318
translate french bonus4_197853e0:

    # "Sakura got surprised and didn't said a word for a while."
    "Sakura resta surprise et ne dit mot pendant un moment."

# game/dialogs.rpy:4319
translate french bonus4_08e5cd3b:

    # "But her face made a shiny smile, finally."
    "Mais son visage s'éclaira, finalement."

# game/dialogs.rpy:4321
translate french bonus4_a89ea5da:

    # s "Oh yes! Yes, I would love to!"
    s "Oh oui ! Oui, je le veux bien !"

# game/dialogs.rpy:4322
translate french bonus4_95d8a37f:

    # hero "Yes! Thank you, Sakura-chan!"
    hero "Chouette ! Merci, Sakura-chan !"

# game/dialogs.rpy:4323
translate french bonus4_189cb5d4:

    # s "I'm so happy you asked me that!"
    s "Je suis si contente que tu me l'aies demandé !"

# game/dialogs.rpy:4324
translate french bonus4_1170dcb4:

    # hero "You really are a close friend, Sakura-chan..."
    hero "Tu es vraiment une amie proche, Sakura-chan..."

# game/dialogs.rpy:4326
translate french bonus4_2fff80d3:

    # hero "I know we can't technically be brothers and sisters... But then I want you to be a family friend!"
    hero "Je sais qu'on ne peut pas être techniquement frères et sœurs... Mais je veux te donner une place dans ma famille !"

# game/dialogs.rpy:4328
translate french bonus4_c58e083a:

    # hero "I would like so much to have you as a family friend!"
    hero "J'aimerais tellement que tu sois une amie de ma famille !"

# game/dialogs.rpy:4329
translate french bonus4_02601609:

    # "Sakura suddenly hugged me."
    "Sakura me câlina soudainement."

# game/dialogs.rpy:4330
translate french bonus4_8b5ff4e0:

    # s "Thank you so much, %(stringhero)s!... I mean, [hnick!t]!"
    s "Merci, merci %(stringhero)s... Enfin, [hnick!t] !"

# game/dialogs.rpy:4331
translate french bonus4_bdc5b511:

    # "I held her a moment with a smile."
    "On resta un moment dans les bras l'un de l'autre en souriant."

# game/dialogs.rpy:4334
translate french bonus4_7b0de0eb:

    # s "So, how's your couple with Nana-chan?"
    s "Alors, comment ça se passe avec Nana-chan ?"

# game/dialogs.rpy:4335
translate french bonus4_22478f8e:

    # hero "It's great! She's an amazing girl."
    hero "C'est super ! C'est une fille géniale."

# game/dialogs.rpy:4336
translate french bonus4_5ba932be:

    # hero "We plan to go to her grand-parents at Okinawa for the vacations, for a couple of weeks."
    hero "On a prévu aller à Okinawa, chez ses grands-parents, pour deux semaines, pendant les vacances."

# game/dialogs.rpy:4337
translate french bonus4_a54e7c2a:

    # s "That's great!"
    s "C'est super !"

# game/dialogs.rpy:4338
translate french bonus4_d33cd195:

    # s "What you will do, there?"
    s "Vous y ferez quoi ?"

# game/dialogs.rpy:4339
translate french bonus4_15d9f409:

    # hero "I don't know yet."
    hero "Je ne sais pas encore."

# game/dialogs.rpy:4340
translate french bonus4_b396e8be:

    # hero "Since it's Nanami-chan's birth land, she probably knows what's the best."
    hero "Vu que c'est là où est né Nanami-chan, elle sait probablement ce qui vaut le coup."

# game/dialogs.rpy:4341
translate french bonus4_fb2732b4:

    # hero "So far, she said there's a lot of festivals at this period of the year."
    hero "Elle dit qu'il y a beaucoup de festivals en cette période de l'année."

# game/dialogs.rpy:4342
translate french bonus4_aff5d3d2:

    # s "You're so lucky!"
    s "T'en as de la chance !"

# game/dialogs.rpy:4343
translate french bonus4_a3265a2a:

    # s "I wish I could go there."
    s "J'aimerais bien aller là-bas un jour."

# game/dialogs.rpy:4344
translate french bonus4_9597c256:

    # "I nodded, undestanding her feeling."
    "Je hochai, comprenant son sentiment."

# game/dialogs.rpy:4345
translate french bonus4_d8972d72:

    # "Then I got an idea."
    "Puis j'eus une idée."

# game/dialogs.rpy:4346
translate french bonus4_3d45bfe0:

    # hero "Hey... Why not trying to organize a trip for all of us when we will come back?"
    hero "Hé... Pourquoi on organiserait pas un voyage pour le club après mon retour ?"

# game/dialogs.rpy:4347
translate french bonus4_7e322d73:

    # hero "Like, I'll go there with Nanami in recognition, then when we will come back, we will know what are the great spots we can go."
    hero "J'irai avec Nanami en reconnaissance, puis à notre retour, on saura à quels coins on pourra aller tous ensemble."

# game/dialogs.rpy:4348
translate french bonus4_6796bcb2:

    # hero "And while that, we'll save money all together and Rika will organize the trip for all of us!"
    hero "Et pendant ce temps, on gardera des sous et Rika organisera le voyage pour nous tous !"

# game/dialogs.rpy:4350
translate french bonus4_0d18a236:

    # s "That sounds so great!"
    s "Ce serait génial !"

# game/dialogs.rpy:4351
translate french bonus4_52addb72:

    # hero "I'll call Rika tomorrow to propose it!"
    hero "J'appellerai Rika demain pour lui proposer !"

# game/dialogs.rpy:4352
translate french bonus4_8cd12022:

    # hero "I'm sure it will be alot of fun!"
    hero "Je suis sûr qu'on s'amusera bien !"

# game/dialogs.rpy:4357
translate french bonus4_e1a7c00a:

    # "We finally reached Sakura's house."
    "On atteignit finalement la maison de Sakura."

# game/dialogs.rpy:4358
translate french bonus4_94a9ed42:

    # s "See you soon and take care... [hnick!t]."
    s "On se revoit bientôt. Prends soin de toi...[hnick!t]."

# game/dialogs.rpy:4360
translate french bonus4_50eaa374:

    # "Sakura does a maid-like reverence with her little summer dress."
    "Sakura fit une petite révérence avec sa robe d'été."

# game/dialogs.rpy:4361
translate french bonus4_0f54fbae:

    # hero "Take care, Sakura-chan!"
    hero "Prends soin de toi, Sakura-chan !"

# game/dialogs.rpy:4363
translate french bonus4_68a39d42:

    # hero "Take care, little sister!"
    hero "Prends soin de toi, petite sœur !"

# game/dialogs.rpy:4365
translate french bonus4_8dcab874:

    # "She giggled and disappeared in her house."
    "Elle rit et disparût chez elle."

# game/dialogs.rpy:4366
translate french bonus4_6b242521:

    # "That was a fine sunday..."
    "C'était un chouette dimanche..."

# game/dialogs.rpy:4367
translate french bonus4_5206326a:

    # "A good start for the vacations..."
    "Un bon début pour les vacances..."

# game/dialogs.rpy:4380
translate french previewmsg_5de028fa:

    # centered "{size=+12}The story starts but the preview ends here...{/size}"
    centered "{size=+12}The story starts but the preview ends here...{/size}"

# game/dialogs.rpy:4381
translate french previewmsg_f6d47b23:

    # centered "{size=+12}Stay tuned and keep the app in your device to get the free complete update of Kare wa Kanojo in early 2018!{/size}"
    centered "{size=+12}Stay tuned and keep the app in your device to get the free complete update of Kare wa Kanojo in early 2018!{/size}"

translate french strings:

    # dialogs.rpy:34
    old "What should I do?"
    new "Que dois-je faire ?"

    # dialogs.rpy:34
    old "Let's play some online game."
    new "Jouer un peu en ligne."

    # dialogs.rpy:34
    old "Let's rewatch some anime VHS."
    new "Regarder une VHS d'animé."

    # dialogs.rpy:34
    old "Let's study a bit."
    new "Réviser un peu."

    # dialogs.rpy:438
    old "What should I say?"
    new "Que devrais-je dire ?"

    # dialogs.rpy:438
    old "The truth: {i}High School Samurai{/i}, an ecchi shonen manga"
    new "La vérité : {i}High School Samurai{/i}, un shonen ecchi"

    # dialogs.rpy:438
    old "Not completely the truth: {i}Rosario Maiden{/i}, a seinen manga about vampire dolls"
    new "Pas tout à fait la vérité  : {i}Rosario Maiden{/i}, un seinen avec des poupées vampires"

    # dialogs.rpy:940
    old "What should I do?..."
    new "Que devrais-je faire ?..."

    # dialogs.rpy:940
    old "Tell the truth to Sakura"
    new "Dire la vérité à Sakura"

    # dialogs.rpy:940
    old "Keep the secret and buy it another day"
    new "Garder le secret et l'acheter un autre jour"

    # dialogs.rpy:1609
    old "Please help me! What should I do??"
    new "Aidez-moi ! Que dois-je faire ??"

    # dialogs.rpy:1609
    old "No!... It's a boy! I can't love him, I'm not gay!"
    new "Non !... C'est un garçon ! Je ne peux pas être amoureux, je ne suis pas gay !"

    # dialogs.rpy:1609
    old "Whatever! Love doesn't have gender!"
    new "Peu importe ! L'amour n'a pas de genre !"

    # dialogs.rpy:4107
    old "Call me \"Master\"!"
    new "Appelle-moi \"Maître\" !"

    # dialogs.rpy:4107
    old "Call me \"Big brother\"!"
    new "Appelle-moi \"Grand frère\" !"
