﻿# TODO: Translation updated at 2017-10-04 12:12

translate french strings:

    # 00action_file.rpy:26
    old "{#weekday}Monday"
    new "{#weekday}Lundi"

    # 00action_file.rpy:26
    old "{#weekday}Tuesday"
    new "{#weekday}Mardi"

    # 00action_file.rpy:26
    old "{#weekday}Wednesday"
    new "{#weekday}Mercredi"

    # 00action_file.rpy:26
    old "{#weekday}Thursday"
    new "{#weekday}Jeudi"

    # 00action_file.rpy:26
    old "{#weekday}Friday"
    new "{#weekday}Vendredi"

    # 00action_file.rpy:26
    old "{#weekday}Saturday"
    new "{#weekday}Samedi"

    # 00action_file.rpy:26
    old "{#weekday}Sunday"
    new "{#weekday}Dimanche"

    # 00action_file.rpy:37
    old "{#weekday_short}Mon"
    new "{#weekday_short}Lun"

    # 00action_file.rpy:37
    old "{#weekday_short}Tue"
    new "{#weekday_short}Mar"

    # 00action_file.rpy:37
    old "{#weekday_short}Wed"
    new "{#weekday_short}Mer"

    # 00action_file.rpy:37
    old "{#weekday_short}Thu"
    new "{#weekday_short}Jeu"

    # 00action_file.rpy:37
    old "{#weekday_short}Fri"
    new "{#weekday_short}Ven"

    # 00action_file.rpy:37
    old "{#weekday_short}Sat"
    new "{#weekday_short}Sam"

    # 00action_file.rpy:37
    old "{#weekday_short}Sun"
    new "{#weekday_short}Dim"

    # 00action_file.rpy:47
    old "{#month}January"
    new "{#month}Janvier"

    # 00action_file.rpy:47
    old "{#month}February"
    new "{#month}Février"

    # 00action_file.rpy:47
    old "{#month}March"
    new "{#month}Mars"

    # 00action_file.rpy:47
    old "{#month}April"
    new "{#month}Avril"

    # 00action_file.rpy:47
    old "{#month}May"
    new "{#month}Mai"

    # 00action_file.rpy:47
    old "{#month}June"
    new "{#month}Juin"

    # 00action_file.rpy:47
    old "{#month}July"
    new "{#month}Juillet"

    # 00action_file.rpy:47
    old "{#month}August"
    new "{#month}Août"

    # 00action_file.rpy:47
    old "{#month}September"
    new "{#month}Septembre"

    # 00action_file.rpy:47
    old "{#month}October"
    new "{#month}Octobre"

    # 00action_file.rpy:47
    old "{#month}November"
    new "{#month}Novembre"

    # 00action_file.rpy:47
    old "{#month}December"
    new "{#month}Décembre"

    # 00action_file.rpy:63
    old "{#month_short}Jan"
    new "{#month_short}Jan"

    # 00action_file.rpy:63
    old "{#month_short}Feb"
    new "{#month_short}Fev"

    # 00action_file.rpy:63
    old "{#month_short}Mar"
    new "{#month_short}Mar"

    # 00action_file.rpy:63
    old "{#month_short}Apr"
    new "{#month_short}Avr"

    # 00action_file.rpy:63
    old "{#month_short}May"
    new "{#month_short}Mai"

    # 00action_file.rpy:63
    old "{#month_short}Jun"
    new "{#month_short}Juin"

    # 00action_file.rpy:63
    old "{#month_short}Jul"
    new "{#month_short}Juil"

    # 00action_file.rpy:63
    old "{#month_short}Aug"
    new "{#month_short}Août"

    # 00action_file.rpy:63
    old "{#month_short}Sep"
    new "{#month_short}Sep"

    # 00action_file.rpy:63
    old "{#month_short}Oct"
    new "{#month_short}Oct"

    # 00action_file.rpy:63
    old "{#month_short}Nov"
    new "{#month_short}Nov"

    # 00action_file.rpy:63
    old "{#month_short}Dec"
    new "{#month_short}Dec"

    # 00action_file.rpy:235
    old "%b %d, %H:%M"
    new "%b %d, %H:%M"

    # 00action_file.rpy:820
    old "Quick save complete."
    new "Savegarde rapide effectuée."

    # 00gui.rpy:234
    old "Are you sure?"
    new "Êtes-vous sûr(e) de votre choix ?"

    # 00gui.rpy:235
    old "Are you sure you want to delete this save?"
    new "Êtes-vous sûr(e) de vouloir supprimer cette sauvegarde ?"

    # 00gui.rpy:236
    old "Are you sure you want to overwrite your save?"
    new "Êtes-vous sûr(e) de vouloir écraser cette sauvegarde ?"

    # 00gui.rpy:237
    old "Loading will lose unsaved progress.\nAre you sure you want to do this?"
    new "Vous perdrez votre progression actuelle.\nÊtes-vous sûr(e) de votre choix ?"

    # 00gui.rpy:238
    old "Are you sure you want to quit?"
    new "Êtes-vous sûr(e) de vouloir quitter ?"

    # 00gui.rpy:239
    old "Are you sure you want to return to the main menu?\nThis will lose unsaved progress."
    new "Êtes-vous sûr(e) de vouloir revenir à l'écran titre ?\nToute progression non sauvegardée sera perdue."

    # 00gui.rpy:240
    old "Are you sure you want to end the replay?"
    new "Êtes-vous sûr(e) de vouloir finir le replay ?"

    # 00gui.rpy:241
    old "Are you sure you want to begin skipping?"
    new "Êtes-vous sûr(e) de vouloir commencer à sauter ?"

    # 00gui.rpy:242
    old "Are you sure you want to skip to the next choice?"
    new "Êtes-vous sûr(e) de vouloir sauter jusqu'au prochain choix ?"

    # 00gui.rpy:243
    old "Are you sure you want to skip unseen dialogue to the next choice?"
    new "Êtes-vous sûr(e) de vouloir sauter les dialogues non vus jusqu'au prochain choix ?"

    # 00keymap.rpy:259
    old "Saved screenshot as %s."
    new "Capture d'écran sauvegardée sous le nom %s."

    # 00library.rpy:142
    old "Self-voicing disabled."
    new "Voix de synthèse désactivée."

    # 00library.rpy:143
    old "Clipboard voicing enabled. "
    new "Voix clipboard activée. "

    # 00library.rpy:144
    old "Self-voicing enabled. "
    new "Voix de synthèse activée. "

    # 00library.rpy:179
    old "Skip Mode"
    new "Mode saut"

    # 00library.rpy:262
    old "This program contains free software under a number of licenses, including the MIT License and GNU Lesser General Public License. A complete list of software, including links to full source code, can be found {a=https://www.renpy.org/l/license}here{/a}."
    new "Ce programme contient des logiciels libres sous plusieurs licences, y compris la Licence MIT et la GNU Lesser General Public License. Une liste complète de logiciels, y compris des liens vers un code source complet, peuvent être trouvés {a=https://www.renpy.org/l/license}ici{/a}. (Anglais)"

    # 00preferences.rpy:429
    old "Clipboard voicing enabled. Press 'shift+C' to disable."
    new "Voix Clipboard activée. Appuyez sur 'Maj+C' pour la désactiver."

    # 00preferences.rpy:431
    old "Self-voicing would say \"[renpy.display.tts.last]\". Press 'alt+shift+V' to disable."
    new "La voix de synthèse va dire \"[renpy.display.tts.last]\". Appuyez sur 'alt+maj+V' pour la désactiver."

    # 00preferences.rpy:433
    old "Self-voicing enabled. Press 'v' to disable."
    new "Voix de synthèse activée. Appuyez sur 'V' pour la désactiver."

    # 00iap.rpy:217
    old "Contacting App Store\nPlease Wait..."
    new "Connexion à l'App Store en cours..."

    # 00updater.rpy:373
    old "The Ren'Py Updater is not supported on mobile devices."
    new "The Ren'Py Updater is not supported on mobile devices."

    # 00updater.rpy:492
    old "An error is being simulated."
    new "An error is being simulated."

    # 00updater.rpy:668
    old "Either this project does not support updating, or the update status file was deleted."
    new "Le projet ne supporte pas les mises à jour ou bien le fichier de statut de mise à jour a été supprimé."

    # 00updater.rpy:682
    old "This account does not have permission to perform an update."
    new "Ce compte utilisateur n'a pas la permission de faire des mises à jour."

    # 00updater.rpy:685
    old "This account does not have permission to write the update log."
    new "Ce compte utilisateur n'a pas la permission d'écrire de log de mise à jour."

    # 00updater.rpy:710
    old "Could not verify update signature."
    new "Impossible de vérifier la signature de mise à jour."

    # 00updater.rpy:981
    old "The update file was not downloaded."
    new "Le fichier de mise à jour n'a pas été téléchargé."

    # 00updater.rpy:999
    old "The update file does not have the correct digest - it may have been corrupted."
    new "Le fichier de mise à jour n'a pas de digest correct - il est peut-être corrompu."

    # 00updater.rpy:1055
    old "While unpacking {}, unknown type {}."
    new "A l'extraction de {}, type {} inconnu."

    # 00updater.rpy:1399
    old "Updater"
    new "Mise à jour"

    # 00updater.rpy:1406
    old "An error has occured:"
    new "Une erreur est survenue :"

    # 00updater.rpy:1408
    old "Checking for updates."
    new "Recherche de mise à jour"

    # 00updater.rpy:1410
    old "This program is up to date."
    new "Le jeu est à présent à jour."

    # 00updater.rpy:1412
    old "[u.version] is available. Do you want to install it?"
    new "La version [u.version] est disponible. L'installer ?"

    # 00updater.rpy:1414
    old "Preparing to download the updates."
    new "Préparation au téléchargement."

    # 00updater.rpy:1416
    old "Downloading the updates."
    new "Téléchargement des mises à jour."

    # 00updater.rpy:1418
    old "Unpacking the updates."
    new "Dépaquetage des mises à jour."

    # 00updater.rpy:1420
    old "Finishing up."
    new "Finalisation."

    # 00updater.rpy:1422
    old "The updates have been installed. The program will restart."
    new "Les mises à jour ont été installées. Le programme va redémarrer."

    # 00updater.rpy:1424
    old "The updates have been installed."
    new "Les mises à jour ont été installées."

    # 00updater.rpy:1426
    old "The updates were cancelled."
    new "Les mises à jour ont été anulées."

    # 00updater.rpy:1441
    old "Proceed"
    new "Continuer"

    # 00updater.rpy:1444
    old "Cancel"
    new "Annuler"

    # 00gallery.rpy:563
    old "Image [index] of [count] locked."
    new "Image [index] sur [count] verrouillée."

    # 00gallery.rpy:583
    old "prev"
    new "Prec"

    # 00gallery.rpy:584
    old "next"
    new "Suiv"

    # 00gallery.rpy:585
    old "slideshow"
    new "Diaporama"

    # 00gallery.rpy:586
    old "return"
    new "Retour"

    # 00gltest.rpy:64
    old "Graphics Acceleration"
    new "Accélération graphique"

    # 00gltest.rpy:70
    old "Automatically Choose"
    new "Choisir automatiquement"

    # 00gltest.rpy:75
    old "Force Angle/DirectX Renderer"
    new "Forcer le rendu Angle/DirectX"

    # 00gltest.rpy:79
    old "Force OpenGL Renderer"
    new "Forcer le rendu OpenGL"

    # 00gltest.rpy:83
    old "Force Software Renderer"
    new "Forcer le rendu logiciel"

    # 00gltest.rpy:93
    old "Enable"
    new "Activer"

    # 00gltest.rpy:109
    old "Changes will take effect the next time this program is run."
    new "Les changements prendront effet après le redémarrage du jeu."

    # 00gltest.rpy:119
    old "Return"
    new "Retour"

    # 00gltest.rpy:141
    old "Performance Warning"
    new "Problèmes de performances"

    # 00gltest.rpy:146
    old "This computer is using software rendering."
    new "Cet ordinateur utilise le rendu logiciel."

    # 00gltest.rpy:148
    old "This computer is not using shaders."
    new "Cet ordinateur n'utilise pas de shaders."

    # 00gltest.rpy:150
    old "This computer is displaying graphics slowly."
    new "Cet ordinateur affichera le jeu lentement."

    # 00gltest.rpy:152
    old "This computer has a problem displaying graphics: [problem]."
    new "Cet ordinateur a des problèmes pour afficher les graphismes : [problem]"

    # 00gltest.rpy:157
    old "Its graphics drivers may be out of date or not operating correctly. This can lead to slow or incorrect graphics display. Updating DirectX could fix this problem."
    new "Les pilotes graphiques ne sont peut-être pas à jour ou ne fonctionnent pas correctement. Ceci peut rendre le jeu plus lent ou buggué. Mettre à jour DirectX pourrait résoudre le problème."

    # 00gltest.rpy:159
    old "Its graphics drivers may be out of date or not operating correctly. This can lead to slow or incorrect graphics display."
    new "Les pilotes graphiques ne sont peut-être pas à jour ou ne fonctionnent pas correctement. Ceci peut rendre le jeu plus lent ou buggué."

    # 00gltest.rpy:164
    old "Update DirectX"
    new "Mettre à jour DirectX"

    # 00gltest.rpy:170
    old "Continue, Show this warning again"
    new "Continuer et réafficher cet avertissement la prochaine fois."

    # 00gltest.rpy:174
    old "Continue, Don't show warning again"
    new "Continuer et ne plus réafficher cet avertissement."

    # 00gltest.rpy:192
    old "Updating DirectX."
    new "Mise à jour de DirectX."

    # 00gltest.rpy:196
    old "DirectX web setup has been started. It may start minimized in the taskbar. Please follow the prompts to install DirectX."
    new "L'installation de DirectX a démarré. Elle est peut-être minimisée dans la barre des tâches. Veuillez suivre les instructions pour installer DirectX."

    # 00gltest.rpy:200
    old "{b}Note:{/b} Microsoft's DirectX web setup program will, by default, install the Bing toolbar. If you do not want this toolbar, uncheck the appropriate box."
    new "{b}Note :{/b} L'installation web de Microsoft DirectX installera la barre d'outils Bing par défaut. Si vous n'en voulez pas, décochez la case correspondante."

    # 00gltest.rpy:204
    old "When setup finishes, please click below to restart this program."
    new "Cliquez ci-dessous lorsque l'installation sera terminée."

    # 00gltest.rpy:206
    old "Restart"
    new "Redémarrer"

    # 00gamepad.rpy:32
    old "Select Gamepad to Calibrate"
    new "Choisir la manette à calibrer"

    # 00gamepad.rpy:35
    old "No Gamepads Available"
    new "Aucune manette détectée"

    # 00gamepad.rpy:54
    old "Calibrating [name] ([i]/[total])"
    new "Calibration de [name] ([i]/[total])"

    # 00gamepad.rpy:58
    old "Press or move the [control!r] [kind]."
    new "Appuyez ou pressez [control!r] [kind]."

    # 00gamepad.rpy:66
    old "Skip (A)"
    new "Ignorer (A)"

    # 00gamepad.rpy:69
    old "Back (B)"
    new "Retour (B)"

    # _errorhandling.rpym:519
    old "Open"
    new "Ouvrir"

    # _errorhandling.rpym:521
    old "Opens the traceback.txt file in a text editor."
    new "Ouvre traceback.txt dans un éditeur de texte."

    # _errorhandling.rpym:523
    old "Copy"
    new "Copier"

    # _errorhandling.rpym:525
    old "Copies the traceback.txt file to the clipboard."
    new "Copie le contenu de traceback.txt dans le presse-papiers."

    # _errorhandling.rpym:543
    old "An exception has occurred."
    new "Une exception est survenue."

    # _errorhandling.rpym:562
    old "Rollback"
    new "Revenir en arrière"

    # _errorhandling.rpym:564
    old "Attempts a roll back to a prior time, allowing you to save or choose a different choice."
    new "Tente de revenir à l'action précédente."

    # _errorhandling.rpym:567
    old "Ignore"
    new "Ignorer"

    # _errorhandling.rpym:569
    old "Ignores the exception, allowing you to continue. This often leads to additional errors."
    new "Ignore l'exception, vous permettant de continuer. Cela conduit en général à d'autres erreurs."

    # _errorhandling.rpym:572
    old "Reload"
    new "Recharger"

    # _errorhandling.rpym:574
    old "Reloads the game from disk, saving and restoring game state if possible."
    new "Recharge le jeu à partir du disque."

    # _errorhandling.rpym:576
    old "Console"
    new "Console"

    # _errorhandling.rpym:578
    old "Opens a console to allow debugging the problem."
    new "Ouvre la console pour débuggage."

    # _errorhandling.rpym:590
    old "Quits the game."
    new "Quitte le jeu."

    # _errorhandling.rpym:614
    old "Parsing the script failed."
    new "Echec de vérification du script."

    # _errorhandling.rpym:640
    old "Opens the errors.txt file in a text editor."
    new "Ouvre errors.txt dans un éditeur de texte."

    # _errorhandling.rpym:644
    old "Copies the errors.txt file to the clipboard."
    new "Copie le contenu de errors.txt dans le presse-papiers."

# TODO: Translation updated at 2017-10-09 22:31

translate french strings:

    # _compat\gamemenu.rpym:198
    old "Empty Slot."
    new "Emplacement vide."

    # _compat\gamemenu.rpym:355
    old "Previous"
    new "Précédent"

    # _compat\gamemenu.rpym:362
    old "Next"
    new "Suivant"

    # _compat\preferences.rpym:428
    old "Joystick Mapping"
    new "Config Joystick"

    # _developer\developer.rpym:38
    old "Developer Menu"
    new "Developer Menu"

    # _developer\developer.rpym:43
    old "Reload Game (Shift+R)"
    new "Reload Game (Shift+R)"

    # _developer\developer.rpym:45
    old "Console (Shift+O)"
    new "Console (Shift+O)"

    # _developer\developer.rpym:47
    old "Variable Viewer"
    new "Variable Viewer"

    # _developer\developer.rpym:49
    old "Theme Test"
    new "Theme Test"

    # _developer\developer.rpym:51
    old "Image Location Picker"
    new "Image Location Picker"

    # _developer\developer.rpym:53
    old "Filename List"
    new "Filename List"

    # _developer\developer.rpym:57
    old "Show Image Load Log"
    new "Show Image Load Log"

    # _developer\developer.rpym:60
    old "Hide Image Load Log"
    new "Hide Image Load Log"

    # _developer\developer.rpym:95
    old "Nothing to inspect."
    new "Nothing to inspect."

    # _developer\developer.rpym:217
    old "Return to the developer menu"
    new "Return to the developer menu"

    # _developer\developer.rpym:377
    old "Rectangle: %r"
    new "Rectangle: %r"

    # _developer\developer.rpym:382
    old "Mouse position: %r"
    new "Mouse position: %r"

    # _developer\developer.rpym:387
    old "Right-click or escape to quit."
    new "Right-click or escape to quit."

    # _developer\developer.rpym:419
    old "Rectangle copied to clipboard."
    new "Rectangle copied to clipboard."

    # _developer\developer.rpym:422
    old "Position copied to clipboard."
    new "Position copied to clipboard."

    # _developer\developer.rpym:531
    old "✔ "
    new "✔ "

    # _developer\developer.rpym:534
    old "✘ "
    new "✘ "

    # _developer\developer.rpym:539
    old "\n{color=#cfc}✔ predicted image (good){/color}\n{color=#fcc}✘ unpredicted image (bad){/color}\n{color=#fff}Drag to move.{/color}"
    new "\n{color=#cfc}✔ predicted image (good){/color}\n{color=#fcc}✘ unpredicted image (bad){/color}\n{color=#fff}Drag to move.{/color}"

    # _developer\inspector.rpym:38
    old "Displayable Inspector"
    new "Displayable Inspector"

    # _developer\inspector.rpym:61
    old "Size"
    new "Size"

    # _developer\inspector.rpym:65
    old "Style"
    new "Style"

    # _developer\inspector.rpym:71
    old "Location"
    new "Location"

    # _developer\inspector.rpym:122
    old "Inspecting Styles of [displayable_name!q]"
    new "Inspecting Styles of [displayable_name!q]"

    # _developer\inspector.rpym:139
    old "displayable:"
    new "displayable:"

    # _developer\inspector.rpym:145
    old "        (no properties affect the displayable)"
    new "        (no properties affect the displayable)"

    # _developer\inspector.rpym:147
    old "        (default properties omitted)"
    new "        (default properties omitted)"

    # _developer\inspector.rpym:185
    old "<repr() failed>"
    new "<repr() failed>"

    # _layout\classic_load_save.rpym:170
    old "a"
    new "a"

    # _layout\classic_load_save.rpym:179
    old "q"
    new "q"
# TODO: Translation updated at 2017-11-14 20:55

translate french strings:

    # 00director.rpy:693
    old "The interactive director is not enabled here."
    new "The interactive director is not enabled here."

    # 00director.rpy:1480
    old "Done"
    new "Done"

    # 00director.rpy:1488
    old "(statement)"
    new "(statement)"

    # 00director.rpy:1489
    old "(tag)"
    new "(tag)"

    # 00director.rpy:1490
    old "(attributes)"
    new "(attributes)"

    # 00director.rpy:1491
    old "(transform)"
    new "(transform)"

    # 00director.rpy:1516
    old "(transition)"
    new "(transition)"

    # 00director.rpy:1528
    old "(channel)"
    new "(channel)"

    # 00director.rpy:1529
    old "(filename)"
    new "(filename)"

    # 00director.rpy:1554
    old "Change"
    new "Change"

    # 00director.rpy:1556
    old "Add"
    new "Add"

    # 00director.rpy:1562
    old "Remove"
    new "Remove"

    # 00director.rpy:1595
    old "Statement:"
    new "Statement:"

    # 00director.rpy:1616
    old "Tag:"
    new "Tag:"

    # 00director.rpy:1632
    old "Attributes:"
    new "Attributes:"

    # 00director.rpy:1650
    old "Transforms:"
    new "Transforms:"

    # 00director.rpy:1669
    old "Behind:"
    new "Behind:"

    # 00director.rpy:1688
    old "Transition:"
    new "Transition:"

    # 00director.rpy:1706
    old "Channel:"
    new "Channel:"

    # 00director.rpy:1724
    old "Audio Filename:"
    new "Audio Filename:"

    # 00keymap.rpy:254
    old "Failed to save screenshot as %s."
    new "Failed to save screenshot as %s."

    # _developer\developer.rpym:43
    old "Interactive Director (D)"
    new "Interactive Director (D)"

    # _developer\developer.rpym:67
    old "Show Texture Size"
    new "Show Texture Size"

    # _developer\developer.rpym:70
    old "Hide Texture size"
    new "Hide Texture size"

    # _developer\developer.rpym:569
    old "{size_mb:,.1f} MB in {count} textures."
    new "{size_mb:,.1f} MB in {count} textures."

    # 00gltest.rpy:89
    old "NPOT"
    new "NPOT"

    # _errorhandling.rpym:585
    old "Ignores the exception, allowing you to continue."
    new "Ignores the exception, allowing you to continue."

# TODO: Translation updated at 2018-03-14 10:01

translate french strings:

    # _developer\developer.rpym:57
    old "Show Image Load Log (F4)"
    new "Show Image Load Log (F4)"

    # _developer\developer.rpym:60
    old "Hide Image Load Log (F4)"
    new "Hide Image Load Log (F4)"

    # _developer\developer.rpym:447
    old "Type to filter: "
    new "Type to filter: "

    # _developer\developer.rpym:572
    old "Textures: [tex_count] ([tex_size_mb:.1f] MB)"
    new "Textures: [tex_count] ([tex_size_mb:.1f] MB)"

    # _developer\developer.rpym:576
    old "Image cache: [cache_pct:.1f]% ([cache_size_mb:.1f] MB)"
    new "Image cache: [cache_pct:.1f]% ([cache_size_mb:.1f] MB)"

    # 00gltest.rpy:70
    old "Renderer"
    new "Renderer"

    # 00gltest.rpy:131
    old "Powersave"
    new "Powersave"

    # 00gltest.rpy:145
    old "Framerate"
    new "Framerate"

    # 00gltest.rpy:149
    old "Screen"
    new "Screen"

    # 00gltest.rpy:153
    old "60"
    new "60"

    # 00gltest.rpy:157
    old "30"
    new "30"

    # 00gltest.rpy:163
    old "Tearing"
    new "Tearing"

# TODO: Translation updated at 2018-04-07 20:30

translate french strings:

    # 00action_file.rpy:344
    old "Save slot %s: [text]"
    new "Sauvegarder emplacement %s: [text]"

    # 00action_file.rpy:417
    old "Load slot %s: [text]"
    new "Charger emplacement %s: [text]"

    # 00action_file.rpy:459
    old "Delete slot [text]"
    new "Supprimer emplacement [text]"

    # 00action_file.rpy:539
    old "File page auto"
    new "Page de sauvegarde auto"

    # 00action_file.rpy:541
    old "File page quick"
    new "PAge de sauvegarde rapide"

    # 00action_file.rpy:543
    old "File page [text]"
    new "Page de fichiers [text]"

    # 00action_file.rpy:733
    old "Next file page."
    new "Page de fichiers suivante."

    # 00action_file.rpy:797
    old "Previous file page."
    new "Page de fichiers précédente."

    # 00action_file.rpy:876
    old "Quick save."
    new "Sauvegarde rapide."

    # 00action_file.rpy:895
    old "Quick load."
    new "Chargement rapide."

    # 00action_other.rpy:344
    old "Language [text]"
    new "Langage [text]"

    # 00library.rpy:150
    old "bar"
    new "barre"

    # 00library.rpy:151
    old "selected"
    new "sélectionné"

    # 00library.rpy:152
    old "viewport"
    new "viewport"

    # 00library.rpy:153
    old "horizontal scroll"
    new "scroll horizontal"

    # 00library.rpy:154
    old "vertical scroll"
    new "scroll vertical"

    # 00library.rpy:155
    old "activate"
    new "activer"

    # 00library.rpy:156
    old "deactivate"
    new "désactiver"

    # 00library.rpy:157
    old "increase"
    new "augmenter"

    # 00library.rpy:158
    old "decrease"
    new "diminuer"

    # 00preferences.rpy:207
    old "display"
    new "affichage"

    # 00preferences.rpy:219
    old "transitions"
    new "transitions"

    # 00preferences.rpy:228
    old "skip transitions"
    new "passer les transitions"

    # 00preferences.rpy:230
    old "video sprites"
    new "video sprites"

    # 00preferences.rpy:239
    old "show empty window"
    new "show empty window"

    # 00preferences.rpy:248
    old "text speed"
    new "vitesse du texte"

    # 00preferences.rpy:256
    old "joystick"
    new "joystick"

    # 00preferences.rpy:256
    old "joystick..."
    new "joystick..."

    # 00preferences.rpy:263
    old "skip"
    new "passer"

    # 00preferences.rpy:266
    old "skip unseen [text]"
    new "passer le [text] non lus"

    # 00preferences.rpy:271
    old "skip unseen text"
    new "passer les textes non lus"

    # 00preferences.rpy:273
    old "begin skipping"
    new "début du passage"

    # 00preferences.rpy:277
    old "after choices"
    new "après les choix"

    # 00preferences.rpy:284
    old "skip after choices"
    new "passer après les choix"

    # 00preferences.rpy:286
    old "auto-forward time"
    new "temps d'avance auto"

    # 00preferences.rpy:300
    old "auto-forward"
    new "avance auto"

    # 00preferences.rpy:310
    old "auto-forward after click"
    new "avance auto après clic"

    # 00preferences.rpy:319
    old "automatic move"
    new "mouvement auto"

    # 00preferences.rpy:328
    old "wait for voice"
    new "attendre la voix"

    # 00preferences.rpy:337
    old "voice sustain"
    new "voice sustain"

    # 00preferences.rpy:346
    old "self voicing"
    new "voix de synthèse"

    # 00preferences.rpy:355
    old "clipboard voicing"
    new "clipboard voicing"

    # 00preferences.rpy:364
    old "debug voicing"
    new "debug voicing"

    # 00preferences.rpy:373
    old "emphasize audio"
    new "emphasize audio"

    # 00preferences.rpy:382
    old "rollback side"
    new "retour en arrière"

    # 00preferences.rpy:392
    old "gl powersave"
    new "gl powersave"

    # 00preferences.rpy:398
    old "gl framerate"
    new "gl framerate"

    # 00preferences.rpy:401
    old "gl tearing"
    new "gl tearing"

    # 00preferences.rpy:413
    old "music volume"
    new "volume de musique"

    # 00preferences.rpy:414
    old "sound volume"
    new "volume de son"

    # 00preferences.rpy:415
    old "voice volume"
    new "volume de voix"

    # 00preferences.rpy:416
    old "mute music"
    new "stopper la musique"

    # 00preferences.rpy:417
    old "mute sound"
    new "stopper les sons"

    # 00preferences.rpy:418
    old "mute voice"
    new "stopper les voix"

    # 00preferences.rpy:419
    old "mute all"
    new "stopper tous les sons"

# TODO: Translation updated at 2019-04-23 09:59

translate french strings:

    # 00accessibility.rpy:76
    old "Font Override"
    new "Font Override"

    # 00accessibility.rpy:80
    old "Default"
    new "Default"

    # 00accessibility.rpy:84
    old "DejaVu Sans"
    new "DejaVu Sans"

    # 00accessibility.rpy:88
    old "Opendyslexic"
    new "Opendyslexic"

    # 00accessibility.rpy:94
    old "Text Size Scaling"
    new "Text Size Scaling"

    # 00accessibility.rpy:100
    old "Reset"
    new "Reset"

    # 00accessibility.rpy:105
    old "Line Spacing Scaling"
    new "Line Spacing Scaling"

    # 00accessibility.rpy:117
    old "Self-Voicing"
    new "Self-Voicing"

    # 00accessibility.rpy:121
    old "Off"
    new "Off"

    # 00accessibility.rpy:125
    old "Text-to-speech"
    new "Text-to-speech"

    # 00accessibility.rpy:129
    old "Clipboard"
    new "Clipboard"

    # 00accessibility.rpy:133
    old "Debug"
    new "Debug"

    # 00director.rpy:1481
    old "⬆"
    new "⬆"

    # 00director.rpy:1487
    old "⬇"
    new "⬇"

    # 00preferences.rpy:430
    old "font transform"
    new "font transform"

    # 00preferences.rpy:433
    old "font size"
    new "font size"

    # 00preferences.rpy:441
    old "font line spacing"
    new "font line spacing"

    # _developer\developer.rpym:63
    old "Image Attributes"
    new "Image Attributes"

    # _developer\developer.rpym:90
    old "[name] [attributes] (hidden)"
    new "[name] [attributes] (hidden)"

    # _developer\developer.rpym:94
    old "[name] [attributes]"
    new "[name] [attributes]"

    # _developer\developer.rpym:154
    old "Hide deleted"
    new "Hide deleted"

    # _developer\developer.rpym:154
    old "Show deleted"
    new "Show deleted"

    # _errorhandling.rpym:542
    old "Copy BBCode"
    new "Copy BBCode"

    # _errorhandling.rpym:544
    old "Copies the traceback.txt file to the clipboard as BBcode for forums like https://lemmasoft.renai.us/."
    new "Copies the traceback.txt file to the clipboard as BBcode for forums like https://lemmasoft.renai.us/."

    # _errorhandling.rpym:546
    old "Copy Markdown"
    new "Copy Markdown"

    # _errorhandling.rpym:548
    old "Copies the traceback.txt file to the clipboard as Markdown for Discord."
    new "Copies the traceback.txt file to the clipboard as Markdown for Discord."

    # _errorhandling.rpym:683
    old "Copies the errors.txt file to the clipboard as BBcode for forums like https://lemmasoft.renai.us/."
    new "Copies the errors.txt file to the clipboard as BBcode for forums like https://lemmasoft.renai.us/."

    # _errorhandling.rpym:687
    old "Copies the errors.txt file to the clipboard as Markdown for Discord."
    new "Copies the errors.txt file to the clipboard as Markdown for Discord."
